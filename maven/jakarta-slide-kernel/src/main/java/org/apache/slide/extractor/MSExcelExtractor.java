/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/extractor/MSExcelExtractor.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.extractor;

/**
 * Author: Ryan Rhodes
 * Date: Jun 26, 2004
 * Time: 1:53:31 AM
 */

import java.io.*;
import java.util.Iterator;

import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFCell;

public class MSExcelExtractor  extends AbstractContentExtractor
{
    public MSExcelExtractor(String uri, String contentType, String namespace) {
      super(uri, contentType, namespace);
    }

    public Reader extract(InputStream content)  throws ExtractorException
    {
        try
        {
            CharArrayWriter writer = new CharArrayWriter();

            POIFSFileSystem fs = new POIFSFileSystem(content);
            HSSFWorkbook workbook = new HSSFWorkbook(fs);

            for (int i = 0; i < workbook.getNumberOfSheets(); i++ )
            {
                HSSFSheet sheet = workbook.getSheetAt(i);

             Iterator rows = sheet.rowIterator();
                while( rows.hasNext() ) {
                    HSSFRow row = (HSSFRow) rows.next();

                    Iterator cells = row.cellIterator();
                    while( cells.hasNext() ) {
                        HSSFCell cell = (HSSFCell) cells.next();
                        switch ( cell.getCellType() ) {
                            case HSSFCell.CELL_TYPE_NUMERIC:
                                String num = Double.toString(cell.getNumericCellValue()).trim();
                                if(num.length() > 0)
                                    writer.write(num + " ");
                                break;
                            case HSSFCell.CELL_TYPE_STRING:
                                String text = cell.getStringCellValue().trim();
                                if(text.length() > 0)
                                    writer.write(text + " ");
                                break;
                        }
                    }
                }
            }

            return new CharArrayReader(writer.toCharArray());
        }
        catch(Exception e )
        {
            throw new ExtractorException(e.getMessage());
        }
    }

    public static void main(String[] args) throws Exception
    {
        FileInputStream in = new FileInputStream(args[0]);

        MSExcelExtractor ex = new MSExcelExtractor(null, null, null);

        Reader reader = ex.extract(in);

        int c = 0;
        do
        {
            c = reader.read();
            System.out.print((char)c);
        }
        while(c != -1);
    }
}