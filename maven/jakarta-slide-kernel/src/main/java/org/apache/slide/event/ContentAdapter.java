/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/event/ContentAdapter.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.event;

/**
 * Content adapter class
 *
 * @version $Revision: 1.2 $
 */
public class ContentAdapter implements ContentListener {
    public void retrieve(ContentEvent event) throws VetoException {
    }

    public void store(ContentEvent event) throws VetoException  {
    }

    public void create(ContentEvent event) throws VetoException  {
    }

    public void remove(ContentEvent event) throws VetoException  {
    }

    public void fork(ContentEvent event) throws VetoException  {
    }

    public void merge(ContentEvent event) throws VetoException  {
    }
}