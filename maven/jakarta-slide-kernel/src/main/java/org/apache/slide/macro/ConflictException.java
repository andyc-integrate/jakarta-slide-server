/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/macro/ConflictException.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.macro;


import java.io.StringWriter;
import java.io.PrintWriter;

import org.apache.slide.common.SlideException;
import org.apache.slide.util.Messages;

/**
 * The copy/move operation is in conflict, e.g. the path of the destination is not created (yet).
 *
 * @version $Revision: 1.2 $
 */
public class ConflictException extends SlideException {


    // ----------------------------------------------------------- Constructors


    /**
     * Constructor.
     *
     * @param objectUri Uri of the forbidden operation
     */
    public ConflictException(String objectUri) {
        this(objectUri, new SlideException("no cause given", false));
    }


    /**
     * Constructor.
     *
     * @param objectUri Uri of the forbidden operation
     * @param t Throwable containing the reason
     */
    public ConflictException(String objectUri, Throwable t) {
        super(Messages.format(ConflictException.class.getName(), objectUri, computeCause(t)), false);
        this.objectUri = objectUri;
        this.nestedException = t;
    }




    // ----------------------------------------------------- Instance Variables


    /**
     * Object uri.
     */
    private String objectUri;


    /* hold the cause exception, if supplied */
    private Throwable nestedException = null;




    // ------------------------------------------------------------- Properties


    /**
     * Object Uri accessor.
     *
     * @return String object uri
     */
    public String getObjectUri() {
        return objectUri;
    }



    /**
     * computeCause.
     *
     * @param e if getMessage is empty the stack trace of e is used
     */
    private static String computeCause(Throwable e) {
        return computeCause(e==null?"":e.getMessage(), e);
    }

    /**
     * computeCause.
     *
     * @param delieveredCause the cause as a string, if null or empty e is used
     * @param e the exception stacktrace is shown, if cause is not supplied
     */
    private static String computeCause(String delieveredCause, Throwable e) {
        String result = delieveredCause;
        if (delieveredCause == null || delieveredCause.equals("")) {
            StringWriter sw = new StringWriter();
            e.printStackTrace( new PrintWriter(sw, true) ); //autoFlush=true
            result = sw.toString();
        }
        return result;
    }


}
