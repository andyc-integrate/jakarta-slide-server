/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/InvalidQueryException.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.search;

/**
 * Generic search exception. Unprocessable entity. The query could not be
 * executed. The text/xml request entity may have been wellformed / valid but
 * may have contained an unsupported or unimplemented query operator.
 *
 * @version $Revision: 1.2 $
 */
public class InvalidQueryException extends BadQueryException {


    // ----------------------------------------------------------- Constructors


    /**
     * Constructor.
     *
     * @param message Exception message
     */
    public InvalidQueryException(String message) {
        super(message);
    }

        /**
     * Constructor.
     *
     * @param message Exception message
     * @param t root cause
     */
    public InvalidQueryException (String message, Throwable t) {
        super (message, t);
    }
}
