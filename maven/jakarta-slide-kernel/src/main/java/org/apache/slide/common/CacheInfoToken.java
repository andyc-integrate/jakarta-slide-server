/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/common/CacheInfoToken.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */ 

package org.apache.slide.common;

/**
 * Caching strategy token class.
 * 
 */
public final class CacheInfoToken {
    
    
    // ------------------------------------------------------------ Constructor
    
    
    /**
     * Constructor.
     * 
     * @param strategy describes the preferred strategy
     */
    public CacheInfoToken(String strategy) {
        this.strategy = strategy;
    }
    
    
    /**
     * Constructor.
     * 
     */
    public CacheInfoToken() {
    }
    
    
    // ----------------------------------------------------- Instance Variables
    
    
    /**
     * Strategy.
     */
    private String strategy;
    
    
    /**
     * Browsing depth. For example, it can be used to represent the depth of 
     * a PROPFIND.
     */
    private int searchDepth;

    
    /**
     * If true, the cache is used during a PROPFIND and then flushed.
     * This enables the use of a cache to speed up PROPFIND searches
     * without the need for tagging URIs clean or dirty.
     */
    private boolean flushOnCommandDone = true;
    
    
    // ------------------------------------------------------------- Properties
    
    
    /**
     * Returns the strategy.
     */
    public String getStrategy() {
         return strategy;
    }
    
    
    /**
     * set the strategy
     */
    public void setStrategy(String strategy) {
        this.strategy = strategy;
    }
    
    /**
     * Returns the searchDepth.
     */
    public int getSearchDepth() {
         return searchDepth;
    }
    
    
    /**
     * set the searchDepth
     */
    public void setSearchDepth(int searchDepth) {
        this.searchDepth = searchDepth;
    }
    
    
    /**
     * Returns the searchDepth.
     */
    public boolean isFlushOnCommandDone() {
         return flushOnCommandDone;
    }
    
    
    /**
     * set the flushOnCommandDone
     */
    public void setFlushOnCommandDone(boolean flushOnCommandDone) {
        this.flushOnCommandDone = flushOnCommandDone;
    }
    
    
}
