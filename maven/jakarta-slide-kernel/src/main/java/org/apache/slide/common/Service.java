/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/common/Service.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.common;

import java.util.Hashtable;

import javax.transaction.xa.XAResource;

import org.apache.slide.authenticate.CredentialsToken;
import org.apache.slide.util.logger.Logger;

/**
 * Slide Service interface.
 *
 * @version $Revision: 1.2 $
 */
public interface Service
    extends XAResource {
    
    
    // -------------------------------------------------------------- Constants
    
    
    // ------------------------------------------------------ Interface Methods

        
        
    /**
     * Set the scope of the store as specified in domain.xml.
     */
    void setScope(Scope scope);
    
    
    
    
    /**
     * Namespace setter.
     */
    void setNamespace(Namespace namespace);
    
    
    /**
     * Initializes the service with a set of parameters. Those could be :
     * <li>User name, login info
     * <li>Host name on which to connect
     * <li>Remote port
     * <li>JDBC driver whoich is to be used :-)
     * <li>Anything else ...
     *
     * @param parameters Hashtable containing the parameters' names and
     * associated values
     * @exception ServiceParameterErrorException Incorrect service parameter
     * @exception ServiceParameterMissingException Service parameter missing
     */
    void setParameters(Hashtable parameters)
        throws ServiceParameterErrorException,
        ServiceParameterMissingException;
    
    
    /**
     * Connects to the underlying data source (if any is needed).
     *
     * @exception ServiceConnectionFailedException Connection failed
     * @param crdtoken the Credentials token containing e.g. the credential
     */
    void connect(CredentialsToken crdtoken)
        throws ServiceConnectionFailedException;
    
    
    
    /**
     * Connects to the underlying data source (if any is needed).
     *
     * @exception ServiceConnectionFailedException Connection failed
     */
    void connect()
        throws ServiceConnectionFailedException;
    
    
    /**
     * Disconnects from the underlying data source.
     *
     * @exception ServiceDisconnectionFailedException Disconnection failed
     */
    void disconnect()
        throws ServiceDisconnectionFailedException;
    
    
    /**
     * Initializes service.
     *
     * @param token Namespace access token, needed if the service needs to
     * access objects or data within the namespace during its initialization
     * @exception ServiceInitializationFailedException May throw an exception
     * if the service has already been initialized before
     */
    void initialize(NamespaceAccessToken token)
        throws ServiceInitializationFailedException;
    
    
    /**
     * Deletes service underlying data source, if possible (and meaningful).
     *
     * @exception ServiceResetFailedException Reset failed
     */
    void reset()
        throws ServiceResetFailedException;
    
    
    /**
     * This function tells whether or not the service is connected.
     *
     * @return boolean true if we are connected
     * @exception ServiceAccessException Service access error
     */
    boolean isConnected()
        throws ServiceAccessException;
    
    
    /**
     * Connects to the service, if we were not previously connected.
     *
     * @param crdtoken the Credentials token containing e.g. the credential
     * @return boolean true if we were not already connected
     * @exception ServiceAccessException Unspecified service access error
     * @exception ServiceConnectionFailedException Connection failed
     */
    boolean connectIfNeeded(CredentialsToken crdtoken)
        throws ServiceConnectionFailedException, ServiceAccessException;
    
    
    /**
     * Connects to the service, if we were not previously connected.
     *
     * @deprecated
     * @return boolean true if we were not already connected
     * @exception ServiceAccessException Unspecified service access error
     * @exception ServiceConnectionFailedException Connection failed
     */
    boolean connectIfNeeded()
        throws ServiceConnectionFailedException, ServiceAccessException;
    
    
    /**
     * Idicates whether or not the objects managed by this service should be
     * cached. Useful if the service is fast enough, and / or if it already
     * includes its own custom caching mechanism, which might be more efficient
     * than the default one provided by the helpers.
     *
     * @return boolean True if results should be cached
     */
    boolean cacheResults();
    
    
    /**
     * Get logger associated with the service.
     *
     * @return The logger if one has been set for the associated namespace,
     * or the Domain logger otherwise
     */
    Logger getLogger();
    
    
}
