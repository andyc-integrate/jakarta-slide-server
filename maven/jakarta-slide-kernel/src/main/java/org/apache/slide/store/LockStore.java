/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/store/LockStore.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */ 

package org.apache.slide.store;

import java.util.Enumeration;

import org.apache.slide.common.Service;
import org.apache.slide.common.ServiceAccessException;
import org.apache.slide.common.Uri;
import org.apache.slide.lock.LockTokenNotFoundException;
import org.apache.slide.lock.NodeLock;

/**
 * Store for Lock objects.
 * 
 * @version $Revision: 1.2 $
 */
public interface LockStore extends Service {
    
    
    // ------------------------------------------------------ Interface Methods
    
    
    /**
     * Create a new lock.
     * 
     * @param lock Lock token
     * @exception ServiceAccessException Service access error
     */
    void putLock(Uri uri, NodeLock lock)
        throws ServiceAccessException;
    
    
    /**
     * Renew a lock.
     * 
     * @param lock Token to renew
     * @exception ServiceAccessException Service access error
     * @exception LockTokenNotFoundException Lock token was not found
     */
    void renewLock(Uri uri, NodeLock lock)
        throws ServiceAccessException, LockTokenNotFoundException;
    
    
    /**
     * Unlock.
     * 
     * @param lock Token to remove
     * @exception ServiceAccessException Service access error
     * @exception LockTokenNotFoundException Lock token was not found
     */
    void removeLock(Uri uri, NodeLock lock)
        throws ServiceAccessException, LockTokenNotFoundException;
    
    
    /**
     * Kill a lock.
     * 
     * @param lock Token to remove
     * @exception ServiceAccessException Service access error
     * @exception LockTokenNotFoundException Lock token was not found
     */
    void killLock(Uri uri, NodeLock lock)
        throws ServiceAccessException, LockTokenNotFoundException;
    
    
    /**
     * Enumerate locks on an object.
     * 
     * @param uri Uri of the subject
     * @return Enumeration List of {@link org.apache.slide.lock.NodeLock locks}
     * which have been put on the subject
     * @exception ServiceAccessException Service access error
     */
    Enumeration enumerateLocks(Uri uri)
        throws ServiceAccessException;
    
    
}
