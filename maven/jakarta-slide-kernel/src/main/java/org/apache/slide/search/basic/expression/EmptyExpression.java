/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/basic/expression/EmptyExpression.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.search.basic.expression;

import org.apache.slide.search.SearchException;
import org.apache.slide.search.basic.BasicResultSetImpl;
import org.apache.slide.search.basic.ComparableResourcesPool;
import org.apache.slide.search.basic.IBasicExpression;
import org.apache.slide.search.basic.IBasicExpressionFactory;
import org.apache.slide.search.basic.IBasicResultSet;

/**
 * Represents an AND expression.
 *
 * @version $Revision: 1.2 $
 */
public class EmptyExpression implements IBasicExpression {
    
    /**
     * The pool of resources to apply the expression to.
     */
    protected ComparableResourcesPool requestedResourcesPool = null;
    
    private IBasicExpressionFactory factory;
    
    /**
     * Creates an empty expression. This occurs when no <where> clause
     * was supplied
     *
     * @param requestedResourcesPool  the pool of resources to apply the expression to.
     */
    public EmptyExpression (ComparableResourcesPool requestedResourcesPool) {
        this.requestedResourcesPool = requestedResourcesPool;
    }
    
    /**
     * Executes the expression.
     *
     * @return   a Set of RequestedResource objects
     *
     * @throws   SearchException
     */
    public IBasicResultSet execute () throws SearchException {
        return new BasicResultSetImpl(requestedResourcesPool.getPool(),
                                      requestedResourcesPool.partialResult());
    }
    
    /**
     * Method setFactory
     *
     * @param    factory             an IBasicExpressionFactory
     *
     */
    public void setFactory (IBasicExpressionFactory factory) {
        this.factory = factory;
    }
    
    /**
     * Method getFactory
     *
     * @return   an IBasicExpressionFactory
     *
     */
    public IBasicExpressionFactory getFactory() {
        return this.factory;
    }
    
    
    
    /**
     * String representation for debugging purposes.
     *
     * @return   this expression as String
     */
    public String toString () {
        return "";
    }
    
}

