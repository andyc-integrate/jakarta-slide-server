/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/SearchLanguage.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.search;

import org.jdom.Element;

/**
 * Base class for a search language.
 *
 * @version $Revision: 1.2 $
 */
public abstract class SearchLanguage {


    // ------------------------------------------------------- Security Methods


    /**
     * Returns this language's name. This is the name of the first
     * element within the searchrequest.
     */
    public abstract String getName();

    /**
     * Returns the grammar URI for this language, this is
     * what the DASL response Header returns.
     *
     * @return   the URI identifying this language
     */
    public abstract String getGrammarUri ();

    /**
     * Generate a query object from a String, set the maximum depth.
     */
    public abstract SearchQuery parseQuery(String queryString, SearchToken token, PropertyProvider propertyProvider)
        throws BadQueryException;


    /**
     * Generate a query object from a JDOM Element.
     *
     * @param    queryElement        JDOM element containing the query
     * @param    token               the  SearchToken
     * @param    propertyProvider    the  PropertyProvider to use (may be
     *                               <code>null</code>).
     *
     * @return   a SearchQuery
     *
     * @throws   BadQueryException
     *
     */
    public abstract SearchQuery parseQuery(Element queryElement, SearchToken token, PropertyProvider propertyProvider)
        throws BadQueryException;

}
