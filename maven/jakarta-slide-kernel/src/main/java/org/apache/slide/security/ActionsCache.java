package org.apache.slide.security;

import java.util.Map;

class ActionsCache {

    Map aggregation; // actionNode -> Set-of-aggregated-nodes (direct aggregates)

    Map aggregationClosure; // actionNode -> Set-of-aggregated-nodes (transitive closure)
    
    boolean hasLoadingFailed;

    ActionsCache() {
        super();
    }

    void invalidate() {
        hasLoadingFailed = false;
        aggregation = null;
        aggregationClosure = null;
    }
    
    void fail() {
        aggregation = null;
        aggregationClosure = null;
        hasLoadingFailed = true;
    }

    boolean hasBeenLoaded() {
        return aggregation != null || hasLoadingFailed;
    }

    public boolean hasLoadingFailed() {
        return hasLoadingFailed;
    }

}
