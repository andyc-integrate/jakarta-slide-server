/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/event/TransactionEvent.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.event;

import java.util.EventListener;
import java.util.EventObject;

/**
 * Transaction event class
 *
 * @version $Revision: 1.2 $
 */
public class TransactionEvent extends EventObject {
    public final static Begin BEGIN = new Begin();
    public final static Rollback ROLLBACK = new Rollback();
    public final static Commit COMMIT = new Commit();
    public final static Commited COMMITED = new Commited();

    public final static String GROUP = "transaction";
    public final static AbstractEventMethod[] methods = new AbstractEventMethod[] { BEGIN, ROLLBACK, COMMIT, COMMITED };

    public TransactionEvent(Object source) {
        super(source);
    }

    public static class Begin extends EventMethod {
        public Begin() {
            super(GROUP, "begin");
        }

        public void fireEvent(EventListener listener, EventObject event) {
            if ( listener instanceof TransactionListener ) ((TransactionListener)listener).begin((TransactionEvent)event);
        }
    }

    public static class Commit extends VetoableEventMethod {
        public Commit() {
            super(GROUP, "commit");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException {
            if ( listener instanceof TransactionListener ) ((TransactionListener)listener).commit((TransactionEvent)event);
        }
    }

    public static class Commited extends EventMethod {
        public Commited() {
            super(GROUP, "commited");
        }

        public void fireEvent(EventListener listener, EventObject event) {
            if ( listener instanceof TransactionListener ) ((TransactionListener)listener).commited((TransactionEvent)event);
        }
    }

    public static class Rollback extends EventMethod {
        public Rollback() {
            super(GROUP, "rollback");
        }

        public void fireEvent(EventListener listener, EventObject event) {
            if ( listener instanceof TransactionListener ) ((TransactionListener)listener).rollback((TransactionEvent)event);
        }
    }
}