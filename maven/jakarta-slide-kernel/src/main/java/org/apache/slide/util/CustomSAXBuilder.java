/*
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;

import  org.jdom.input.SAXBuilder;
import org.xml.sax.XMLReader;

/**
 * CustomSAXBuilder, looks in META-INF/services/org.apache.slide.SAXBuilder
 * allows slide to use a seperate loader to the rest of the system.
 *
 * @version $Revision: 1.4 $ $Date: 2006-03-27 16:33:47 $
 */
public class CustomSAXBuilder {

    private static final String serviceClass = "org.apache.slide.SAXBuilder";

    // --------------------------------------------------------- Public Methods

    /**
     */
    public static SAXBuilder newInstance() {
		String className = null;
		try
		{
			String serviceKey = "/META-INF/services/" + serviceClass;
			InputStream inStream = CustomSAXBuilder.class.getResourceAsStream(serviceKey);
			if (inStream != null)
			{
				BufferedReader in =
				new BufferedReader(new InputStreamReader(inStream));
				className = in.readLine();
				in.close();

				if (className != null && className.trim().length() > 0)
				{
					Class saxBuilder = Class.forName(className);
                    //ips edit
                    Object createdObject = saxBuilder.newInstance();
					if (createdObject instanceof SAXBuilder) {
                        return (SAXBuilder)createdObject;
                    }
                    return new SAXBuilder(className);
				}
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			System.err.println("Error instantiating class " + className +
								" in CustomSAXBuilder. " +
								"The default will be used.");
			e.printStackTrace();
		}

		//use the default if none is set, or loading it goes badly
		return new SAXBuilder();

    }
}
