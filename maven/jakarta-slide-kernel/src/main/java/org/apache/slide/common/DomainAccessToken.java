/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/common/DomainAccessToken.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */ 

package org.apache.slide.common;

import java.io.Reader;
import java.util.Hashtable;

/**
 * Domain accessor token class.
 * 
 * @version $Revision: 1.2 $
 */
public final class DomainAccessToken {
    
    
    // ------------------------------------------------------------ Constructor
    
    
    /**
     * Constructor.
     */
    DomainAccessToken() {
        namespaceTokens = new Hashtable();
    }
    
    
    // ----------------------------------------------------- Instance Variables
    
    
    /**
     * Default namespace associated with this token.
     */
    Hashtable namespaceTokens;
    
    
    /**
     * True if enables creation of namespaces.
     */
    boolean canCreateNamespaces;
    
    
    /**
     * True if enables removal of namespaces.
     */
    boolean canDeleteNamespaces;
    
    
    // --------------------------------------------------------- Public Methods
    
    
    /**
     * Get a namespace.
     * 
     * @param namespaceName Name of the namespace we want to access
     * @return NamespaceToken null if no namespace of this name exists in the 
     * access token
     */
    public NamespaceAccessToken getNamespaceToken(String namespaceName) {
        return (NamespaceAccessToken) namespaceTokens.get(namespaceName);
    }
    
    
    /**
     * Create a new namespace.
     * 
     * @param namespaceName Name of the new namespace
     * @param Reader Reader in which we can read the namespace 
     * initialization parameters
     */
    public void createNewNamespace(String namespaceName, Reader parameters) 
        throws SlideException {
        /*
        if (canCreateNamespaces) {
            Namespace namespace = new Namespace();
            namespace.setName(namespaceName);
            namespace.loadNamespaceParameters(parameters);
            namespaceTokens.put(namespaceName, namespace);
        }
        */
    }
    
    
    // -------------------------------------------------------- Package Methods
    
    
    /**
     * Add a namespace token to the hastable;
     * 
     * @param namespaceToken 
     */
    void addNamespaceToken(NamespaceAccessToken namespaceToken) {
        namespaceTokens.put(namespaceToken.getName(), namespaceToken);
    }
    
}
