/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/content/Content.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */ 

package org.apache.slide.content;

import org.apache.slide.common.ServiceAccessException;
import org.apache.slide.common.SlideToken;
import org.apache.slide.lock.ObjectLockedException;
import org.apache.slide.security.AccessDeniedException;
import org.apache.slide.structure.LinkedObjectNotFoundException;
import org.apache.slide.structure.ObjectNotFoundException;
import org.apache.slide.event.VetoException;

/**
 * Content.
 * 
 * @version $Revision: 1.2 $
 */
public interface Content {
    
    
    // ------------------------------------------------------ Interface Methods
    
    
    /**
     * Retrieve revision descriptors.
     * 
     * @param strUri Uri
     * @return NodeRevisionDescriptors
     */
    NodeRevisionDescriptors retrieve(SlideToken token, String strUri)
        throws ObjectNotFoundException, AccessDeniedException, 
        LinkedObjectNotFoundException, ServiceAccessException, 
        ObjectLockedException, VetoException;
    
    
    /**
     * Retrieve revision descriptor.
     * 
     * @param revisionDescriptors Node revision descriptors
     * @param revisionNumber Node revision number
     */
    NodeRevisionDescriptor retrieve
        (SlideToken token, NodeRevisionDescriptors revisionDescriptors,
         NodeRevisionNumber revisionNumber)
        throws ObjectNotFoundException, AccessDeniedException, 
        LinkedObjectNotFoundException, ServiceAccessException, 
        RevisionDescriptorNotFoundException, ObjectLockedException, VetoException;
    
    
    /**
     * Retrieve revision descriptor from the latest revision 
     * in the main branch.
     * 
     * @param revisionDescriptors Node revision descriptors
     */
    NodeRevisionDescriptor retrieve
        (SlideToken token, NodeRevisionDescriptors revisionDescriptors)
        throws ObjectNotFoundException, AccessDeniedException, 
        LinkedObjectNotFoundException, ServiceAccessException, 
        RevisionDescriptorNotFoundException, ObjectLockedException, VetoException;
    
    
    /**
     * Retrieve revision descriptor from the latest revision
     * of a branch.
     * @param token The token to access slide
     * @param revisionDescriptors Node revision descriptors
     * @param branch The branch, that contains the revision.  
     */
    NodeRevisionDescriptor retrieve
        (SlideToken token, NodeRevisionDescriptors revisionDescriptors,
         String branch)
        throws ObjectNotFoundException, AccessDeniedException, 
        LinkedObjectNotFoundException, ServiceAccessException, 
        RevisionDescriptorNotFoundException, ObjectLockedException,
        BranchNotFoundException, NodeNotVersionedException, VetoException;
    
    
    /**
     * Retrieve revision content.
     * 
     * @param revisionDescriptors Node revision descriptors
     * @param revisionDescriptor Node revision descriptor
     */
    NodeRevisionContent retrieve(SlideToken token, 
                                 NodeRevisionDescriptors revisionDescriptors,
                                 NodeRevisionDescriptor revisionDescriptor)
        throws ObjectNotFoundException, AccessDeniedException, 
        RevisionNotFoundException, LinkedObjectNotFoundException, 
        ServiceAccessException, RevisionContentNotFoundException, 
        ObjectLockedException, VetoException;
    
    
    /**
     * Retrieve revision content.
     * 
     * @param strUri Uri
     * @param revisionDescriptor Node revision descriptor
     */
    NodeRevisionContent retrieve(SlideToken token, String strUri, 
                                 NodeRevisionDescriptor revisionDescriptor)
        throws ObjectNotFoundException, AccessDeniedException, 
        RevisionNotFoundException, LinkedObjectNotFoundException, 
        ServiceAccessException, RevisionContentNotFoundException, 
        ObjectLockedException, VetoException;
    
    
    /**
     * Create new revision descriptors.
     * 
     * @param strUri Uri
     * @param isVersioned true is the resource is versioned
     */
    public void create(SlideToken token, String strUri, 
                       boolean isVersioned)
        throws ObjectNotFoundException, AccessDeniedException, 
        LinkedObjectNotFoundException, ServiceAccessException, 
        ObjectLockedException, VetoException;
    
    
    /**
     * Create new revision in main branch.
     * 
     * @param strUri Uri
     * @param revisionDescriptor New Node revision descriptor
     * @param revisionContent New Node revision content
     */
    void create(SlideToken token, String strUri, 
                NodeRevisionDescriptor revisionDescriptor, 
                NodeRevisionContent revisionContent)
        throws ObjectNotFoundException, AccessDeniedException, 
        RevisionAlreadyExistException, LinkedObjectNotFoundException, 
        ServiceAccessException, ObjectLockedException, VetoException;
    
    
    /**
     * Create new revision based on a previous revision.
     * 
     * @param strUri Uri
     * @param branch Branch in which to create the revision
     * @param newRevisionDescriptor New revision descriptor
     * @param revisionContent Node revision content
     */
    void create(SlideToken token, String strUri, String branch, 
                NodeRevisionDescriptor newRevisionDescriptor, 
                NodeRevisionContent revisionContent)
        throws ObjectNotFoundException, AccessDeniedException, 
        RevisionAlreadyExistException, LinkedObjectNotFoundException, 
        ServiceAccessException, RevisionDescriptorNotFoundException, 
        ObjectLockedException, NodeNotVersionedException, 
        BranchNotFoundException, VetoException;
    
    
    /**
     * Create a branch based on specified revision.
     * 
     * @param strUri Uri
     * @param branchName Name of the new branch
     * @param basedOnRevisionDescriptor Node revision descriptor of 
     *                                  the revision on which the new branch
     *                                  is based on.
     *
     * @return the NodeRevisionNumber of the created revision.
     */
    NodeRevisionNumber fork(SlideToken token, String strUri, String branchName,
              NodeRevisionDescriptor basedOnRevisionDescriptor)
        throws ObjectNotFoundException, AccessDeniedException, 
        LinkedObjectNotFoundException, ServiceAccessException, 
        RevisionDescriptorNotFoundException, ObjectLockedException,
        NodeNotVersionedException, RevisionAlreadyExistException, VetoException;
    
    
    /**
     * Create a branch based on specified revision.
     * 
     * @param strUri Uri
     * @param branchName Name of the new branch
     * @param basedOnRevisionNumber Node revision number of
     *                                  the revision on which the new branch
     *                                  is based on.
     *
     * @return the NodeRevisionNumber of the created revision.
     */
    NodeRevisionNumber fork(SlideToken token, String strUri, String branchName,
              NodeRevisionNumber basedOnRevisionNumber)
        throws ObjectNotFoundException, AccessDeniedException, 
        LinkedObjectNotFoundException, ServiceAccessException, 
        RevisionDescriptorNotFoundException, ObjectLockedException,
        NodeNotVersionedException, RevisionAlreadyExistException, VetoException;
    
    
    /**
     * Merge specified branches into a single branch.
     * 
     * @param strUri Uri
     * @param mainBranch Branch into which the other branch will be merged
     * @param branch Branch to merge into main branch
     * @param newRevisionDescriptor New revision descriptor
     * @param revisionContent Node revision content
     */
    void merge(SlideToken token, String strUri, 
               NodeRevisionDescriptor mainBranch, 
               NodeRevisionDescriptor branch, 
               NodeRevisionDescriptor newRevisionDescriptor, 
               NodeRevisionContent revisionContent)
        throws ObjectNotFoundException, AccessDeniedException, 
        LinkedObjectNotFoundException, ServiceAccessException, 
        RevisionDescriptorNotFoundException, ObjectLockedException,
        NodeNotVersionedException, BranchNotFoundException,
        RevisionAlreadyExistException, VetoException;
    
    
    /**
     * Merge specified branches into a single branch.
     * 
     * @param strUri Uri
     * @param mainBranch Branch into which the other branch will be merged
     * @param branch Branch to merge into main branch
     * @param newRevisionDescriptor New revision descriptor
     * @param revisionContent Node revision content
     */
    void merge(SlideToken token, String strUri, 
               String mainBranch, String branch, 
               NodeRevisionDescriptor newRevisionDescriptor, 
               NodeRevisionContent revisionContent)
        throws ObjectNotFoundException, AccessDeniedException, 
        LinkedObjectNotFoundException, ServiceAccessException, 
        RevisionDescriptorNotFoundException, ObjectLockedException,
        NodeNotVersionedException, BranchNotFoundException,
        RevisionAlreadyExistException, VetoException;
    
    
    /**
     * Update contents of an existing revision.
     * 
     * @param strUri Uri
     * @param revisionDescriptor Revision descriptor
     * @param revisionContent Revision content
     */
    void store(SlideToken token, String strUri, 
               NodeRevisionDescriptor revisionDescriptor,
               NodeRevisionContent revisionContent)
        throws ObjectNotFoundException, AccessDeniedException, 
        LinkedObjectNotFoundException, ServiceAccessException, 
        RevisionDescriptorNotFoundException, ObjectLockedException, 
        RevisionNotFoundException, VetoException;
    
    
    /**
     * Remove all revisions at this Uri.
     * 
     * @param revisionDescriptors Node revision descriptors
     */
    void remove(SlideToken token, 
                NodeRevisionDescriptors revisionDescriptors)
        throws ObjectNotFoundException, AccessDeniedException, 
        LinkedObjectNotFoundException, ServiceAccessException, 
        RevisionDescriptorNotFoundException, ObjectLockedException, VetoException;
    
    
    /**
     * Remove specified revision.
     * 
     * @param strUri Uri
     * @param revisionDescriptor Node revision descriptor
     */
    void remove(SlideToken token, String strUri, 
                NodeRevisionDescriptor revisionDescriptor)
        throws ObjectNotFoundException, AccessDeniedException, 
        LinkedObjectNotFoundException, ServiceAccessException, 
        RevisionDescriptorNotFoundException, ObjectLockedException, VetoException;
    
    
    /**
     * Remove specified revision.
     * 
     * @param strUri Uri
     * @param revisionNumber Revision number
     */
    void remove(SlideToken token, String strUri, 
                NodeRevisionNumber revisionNumber)
        throws ObjectNotFoundException, AccessDeniedException, 
        LinkedObjectNotFoundException, ServiceAccessException, 
        RevisionDescriptorNotFoundException, ObjectLockedException, VetoException;
    
}
