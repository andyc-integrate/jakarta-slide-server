/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/common/AbstractServiceBase.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.common;

import java.util.Hashtable;

import org.apache.slide.authenticate.CredentialsToken;
import org.apache.slide.util.logger.Logger;

/**
 * Slide Service abstract implementation.
 *
 * @version $Revision: 1.2 $
 */
public abstract class AbstractServiceBase implements Service {
    
    
    // -------------------------------------------------------------- Constants
    
    protected String LOG_CHANNEL = this.getClass().getName();
    
    
    // ----------------------------------------------------- Instance Variables
    
    
    /**
     * Namespace.
     */
    protected Namespace namespace;
    
    
    // the scope of this store as specified in domain.xml
    protected Scope scope;
    
    
    // -------------------------------------------------------- Service Methods
    
    
    
    /**
     * Set the scope of the store as specified in domain.xml.
     */
    public void setScope(Scope scope) {
        this.scope = scope;
    }
    
    
    
    /**
     * Namespace setter.
     */
    public void setNamespace(Namespace namespace) {
        this.namespace = namespace;
    }
    
    
    /**
     * Logger accessor.
     */
    public Logger getLogger() {
        Logger logger = null;
        if (namespace != null) {
            logger = this.namespace.getLogger();
        }
        if (logger == null)
            logger = Domain.getLogger();
        return logger;
    }
    
    
    
    
    /**
     * Initializes the service with a set of parameters. Those could be :
     * <li>User name, login info
     * <li>Host name on which to connect
     * <li>Remote port
     * <li>JDBC driver whoich is to be used :-)
     * <li>Anything else ...
     *
     * @param parameters Hashtable containing the parameters' names
     * and associated values
     * @exception ServiceParameterErrorException Incorrect service parameter
     * @exception ServiceParameterMissingException Service parameter missing
     */
    public abstract void setParameters(Hashtable parameters)
        throws ServiceParameterErrorException,
        ServiceParameterMissingException;
    
    
    /**
     * Connects to the underlying data source (if any is needed).
     * Compatibility implementation for the previous store implementations
     *
     * @param crdtoken the slide token containing e.g. the credential
     * @exception ServiceConnectionFailedException Connection failed
     */
    public void connect(CredentialsToken crdtoken) throws ServiceConnectionFailedException {
        connect();
    }
    
    
    
    /**
     * Connects to the underlying data source (if any is needed).
     *
     * @exception ServiceConnectionFailedException Connection failed
     */
    public abstract void connect()
        throws ServiceConnectionFailedException;
    
    
    /**
     * Disconnects from the underlying data source.
     *
     * @exception ServiceDisconnectionFailedException Disconnection failed
     */
    public abstract void disconnect()
        throws ServiceDisconnectionFailedException;
    
    
    /**
     * Initializes service.
     *
     * @param token Namespace access token, needed if the service needs to
     * access objects or data within the namespace during its initialization
     * @exception ServiceInitializationFailedException May throw an exception
     * if the service has already been initialized before
     */
    public void initialize(NamespaceAccessToken token)
        throws ServiceInitializationFailedException {
    }
    
    
    /**
     * Deletes service underlying data source, if possible (and meaningful).
     *
     * @exception ServiceResetFailedException Reset failed
     */
    public abstract void reset()
        throws ServiceResetFailedException;
    
    
    /**
     * This function tells whether or not the service is connected.
     *
     * @return boolean true if we are connected
     * @exception ServiceAccessException Service access error
     */
    public abstract boolean isConnected()
        throws ServiceAccessException;
    
    
    /**
     * Connects to the service, if we were not previously connected.
     *
     * @param token the Credeantials token containing e.g. the credential
     * @return boolean true if we were not already connected
     * @exception ServiceAccessException Unspecified service access error
     * @exception ServiceConnectionFailedException Connection failed
     */
    public boolean connectIfNeeded(CredentialsToken token)
        throws ServiceConnectionFailedException, ServiceAccessException {
        boolean result = true;
        try {
            result = !isConnected();
        } catch (ServiceAccessException e) {
            // Ignore : Will try to reconnect
        }
        if (result) {
            connect(token);
        }
        return result;
    }
    
    
    
    /**
     * Connects to the service, if we were not previously connected.
     *
     * @return boolean true if we were not already connected
     * @exception ServiceAccessException Unspecified service access error
     * @exception ServiceConnectionFailedException Connection failed
     */
    public boolean connectIfNeeded()
        throws ServiceConnectionFailedException, ServiceAccessException {
        boolean result = true;
        try {
            result = !isConnected();
        } catch (ServiceAccessException e) {
            // Ignore : Will try to reconnect
        }
        if (result) {
            connect();
        }
        return result;
    }
    
    
    /**
     * Indicates whether or not the objects managed by this service should be
     * cached. Caching is enabled by default.
     *
     * @return boolean True if results should be cached
     */
    public boolean cacheResults() {
        return true;
    }
    
    
    // ----------------------------------------------------- XAResource Mathods
    
    
}

