/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/common/ObjectValidationFailedException.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */ 

package org.apache.slide.common;

import org.apache.slide.util.Messages;

/**
 * Thrown when an object validation fails.
 * 
 * @version $Revision: 1.2 $
 */
public class ObjectValidationFailedException extends SlideRuntimeException {
    
    
    // ------------------------------------------------------------ Constructor
    
    
    /**
     * Constructor.
     * 
     * @param uri Uri
     * @param cause The reason, the validation failed 
     */
    public ObjectValidationFailedException(String uri, String cause) {
        super(Messages.format(ObjectValidationFailedException.class.getName(),
                              uri, cause));
    }
    
    
    /**
     * Constructor.
     * 
     * @param cause The reason, the validation failed 
     */
    public ObjectValidationFailedException(String cause) {
        super(Messages.format(ObjectValidationFailedException.class.getName(),
                              "", cause));
    }
    
    
}
