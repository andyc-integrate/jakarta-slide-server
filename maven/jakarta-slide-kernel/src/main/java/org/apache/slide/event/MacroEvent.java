/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/event/MacroEvent.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.event;

import org.apache.slide.common.Namespace;
import org.apache.slide.common.SlideToken;

import java.util.EventListener;
import java.util.EventObject;

/**
 * Macro event class
 *
 * @version $Revision: 1.2 $
 */
public class MacroEvent extends EventObject implements RemoteInformation {
    public final static Move MOVE = new Move();
    public final static Copy COPY = new Copy();
    public final static Delete DELETE = new Delete();

    public final static String GROUP = "macro";
    public final static AbstractEventMethod[] methods = new AbstractEventMethod[] { MOVE, COPY, DELETE };

    protected final static String SOURCE_URI_KEY = "source-uri";
    protected final static String TARGET_URI_KEY = "target-uri";
    private String sourceURI, targetURI;
    private SlideToken token;
    private Namespace namespace;

    public MacroEvent(Object source, SlideToken token, Namespace namespace, String tagetURI) {
        this(source, token, namespace, null, tagetURI);
    }

    public MacroEvent(Object source, SlideToken token, Namespace namespace, String sourceURI, String targetURI) {
        super(source);
        this.sourceURI = sourceURI;
        this.targetURI = targetURI;
        this.token = token;
        this.namespace = namespace;
    }

    public SlideToken getToken() {
        return token;
    }

    public Namespace getNamespace() {
        return namespace;
    }

    public String getSourceURI() {
        return sourceURI;
    }

    public String getTargetURI() {
        return targetURI;
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer(256);
        buffer.append(getClass().getName()).append("[source-uri=").append(sourceURI);
        buffer.append(", target-uri=").append(targetURI);
        buffer.append("]");
        return buffer.toString();
    }

    public String[][] getInformation() {
        return new String [][] { { SOURCE_URI_KEY, sourceURI },
                                 { TARGET_URI_KEY, targetURI }};
    }

    public final static class Copy extends VetoableEventMethod {
        public Copy() {
            super(GROUP, "copy");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException {
            if ( listener instanceof MacroListener ) ((MacroListener)listener).copy((MacroEvent)event);
        }
    }

    public final static class Move extends VetoableEventMethod {
        public Move() {
            super(GROUP, "move");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof MacroListener ) ((MacroListener)listener).move((MacroEvent)event);
        }
    }

    public final static class Delete extends VetoableEventMethod {
        public Delete() {
            super(GROUP, "delete");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof MacroListener ) ((MacroListener)listener).delete((MacroEvent)event);
        }
    }
}