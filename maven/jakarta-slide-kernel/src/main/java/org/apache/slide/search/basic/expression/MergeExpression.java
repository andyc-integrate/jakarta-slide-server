/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/basic/expression/MergeExpression.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.search.basic.expression;

import java.util.Collection;
import java.util.Iterator;

import org.apache.slide.search.InvalidQueryException;
import org.apache.slide.search.SearchException;
import org.apache.slide.search.basic.IBasicExpression;
import org.apache.slide.search.basic.IBasicResultSet;
import org.jdom.Element;

/**
 * Abstract base class for merge expressions (AND, OR).
 *
 * @version $Revision: 1.2 $
 */
public abstract class MergeExpression extends GenericBasicExpression {


    /** all nested expressions */
    private Collection expressionsToMerge = null;

    /**
     * Creates a merge expression according to Element e
     *
     * @param e                       jdom element, that describes the expression
     * @param expressionsToMerge      a Collection of IBasicExpressions to merge.
     */
    MergeExpression (Element e, Collection expressionsToMerge) throws InvalidQueryException {
        super (e);
        this.expressionsToMerge = expressionsToMerge;
        if (expressionsToMerge.size() == 0) {
            throw new InvalidQueryException(getMustHaveMergeExpressionsMessage(e.getName()));
        }
    }

    /**
     * Executes the expression.
     *
     * @return   a Set of RequestedResource objects
     *
     * @throws   SearchException
     */
    public IBasicResultSet execute () throws SearchException {

        Iterator iterator = expressionsToMerge.iterator();
        if (iterator.hasNext()) {
            resultSet = ((IBasicExpression)iterator.next()).execute();
        }
        while (iterator.hasNext()) {
            IBasicExpression expression = (IBasicExpression)iterator.next();
            merge(expression.execute());
        }
        return resultSet;
    }


    /**
     * Merges the given <code>set</code> into the result Set of this expression.
     *
     * @param    set  the Set to merge.
     */
    protected abstract void merge (IBasicResultSet set);


    /**
     * String representation for debugging purposes.
     *
     * @return   this expression as String
     */
    protected String toString (String op) {
        StringBuffer sb = new StringBuffer();

        Iterator it = expressionsToMerge.iterator();

        while (it.hasNext()) {
            sb.append (((IBasicExpression) it.next()).toString());
            if (it.hasNext())
                sb.append (" ").append (op).append (" ");
        }
        return sb.toString();
    }

    /**
     * Returns the message of the InvalidQueryException that is thrown by the
     * constructor the <code>expressionsToMerge</code> set is empty.
     *
     * @param      operationName  the name of the operation (e.g. <code>and</code>)
     *
     * @return     the message of the InvalidQueryException.
     */
    private static String getMustHaveMergeExpressionsMessage(String operationName) {
        return "<" + operationName + "> must have at least on nested expression.";
    }
}


