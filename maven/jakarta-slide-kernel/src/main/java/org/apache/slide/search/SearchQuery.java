/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/SearchQuery.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.search;

import org.apache.slide.common.RequestedProperties;
import org.apache.slide.common.ServiceAccessException;
import org.apache.slide.structure.Structure;

/**
 * Search query.
 *
 * @version $Revision: 1.2 $
 */
public abstract class SearchQuery {
    
    protected Structure structureHelper;
	protected SearchToken searchToken;
    
    /**
	 * Method execute
	 *
	 * @return   a SearchQueryResult
	 *
	 * @throws   ServiceAccessException
	 */
    public abstract SearchQueryResult execute ()
		throws ServiceAccessException;
    
    /**
     * Get all properties that shall be returned in the <propstat/> element
     *
     * @return   a RequestedProperties
     *
     */
	public abstract RequestedProperties requestedProperties ();
    
}
