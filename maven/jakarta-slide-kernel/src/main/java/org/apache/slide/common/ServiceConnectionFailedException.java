/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/common/ServiceConnectionFailedException.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */ 

package org.apache.slide.common;

import org.apache.slide.util.Messages;

/**
 * Service connection failed.
 * 
 * @version $Revision: 1.2 $
 */
public class ServiceConnectionFailedException extends SlideException {
    
    
    // ----------------------------------------------------------- Constructors
    
    
    /**
     * Constructor.
     * 
     * @param service Service from which the exception was thrown
     * @param cause Cause of the exception
     */
    public ServiceConnectionFailedException(Service service, String cause) {
        super(Messages.format(ServiceConnectionFailedException.class.getName(), 
                              service, cause));
    }
    
    
    /**
     * Constructor.
     * 
     * @param service Service from which the exception was thrown
     * @param e Exception which is the initial cause of the problem
     */
    public ServiceConnectionFailedException(Service service, Exception e) {
        super(Messages.format(ServiceConnectionFailedException.class.getName(),
                              service, e.getMessage()));
    }
    
}
