/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/util/conf/Configuration.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.util.conf;

import java.util.*;

/**
 * <code>Configuration</code> is a interface encapsulating a configuration node
 * used to retrieve configuration values. This is a "read only" interface
 * preventing applications from modifying their own configurations.
 * <br />
 *
 * The contract surrounding the <code>Configuration</code> is that once
 * it is created, information never changes.  The <code>Configuration</code>
 * is built by the <code>SAXConfigurationBuilder</code> and the
 * <code>ConfigurationImpl</code> helper classes.
 *
 * @version 1.1.0, 22/05/1999.
 */
public interface Configuration {
    
    /**
     * Return the name of the node.
     *
     * @return name of the <code>Configuration</code> node.
     */
    public String getName();
    
    /**
     * Return a new <code>Configuration</code> instance encapsulating the
     * specified child node.
     *
     * @param child The name of the child node.
     * @return Configuration
     * @exception ConfigurationException If no child with that name exists.
     */
    public Configuration getConfiguration(String child)
        throws ConfigurationException;
    
    /**
     * Return an <code>Enumeration</code> of <code>Configuration<code>
     * elements containing all node children with the specified name.
     *
     * @param name The name of the children to get.
     * @return Enumeration.  The <code>Enumeration</code> will be
     *          empty if there are no nodes by the specified name.
     */
    public Enumeration getConfigurations(String name);

    /**
     * Return the value of specified attribute.
     *
     * @param paramName The name of the parameter you ask the value of.
     * @return String value of attribute.
     * @exception ConfigurationException If no attribute with that name exists.
     */
    public String getAttribute(String paramName)
        throws ConfigurationException;
    
    /**
     * Return the <code>int</code> value of the specified attribute contained
     * in this node.
     *
     * @param paramName The name of the parameter you ask the value of.
     * @return int value of attribute
     * @exception ConfigurationException If no parameter with that name exists.
     *                                   or if conversion to <code>int</code>
     *                                   fails.
     */
    public int getAttributeAsInt(String paramName)
        throws ConfigurationException;
    
    /**
     * Returns the value of the attribute specified by its name as a
     * <code>long</code>.
     *
     * @param paramName The name of the parameter you ask the value of.
     * @return long value of attribute
     * @exception ConfigurationException If no parameter with that name exists.
     *                                   or if conversion to <code>long</code>
     *                                   fails.
     */
    public long getAttributeAsLong(String paramName)
        throws ConfigurationException;
    
    /**
     * Return the <code>float</code> value of the specified parameter contained
     * in this node.
     *
     * @param paramName The name of the parameter you ask the value of.
     * @return float value of attribute
     * @exception ConfigurationException If no parameter with that name exists.
     *                                   or if conversion to <code>float</code>
     *                                   fails.
     */
    public float getAttributeAsFloat(String paramName)
        throws ConfigurationException;
    
    /**
     * Return the <code>boolean</code> value of the specified parameter contained
     * in this node.<br>
     *
     * @param paramName The name of the parameter you ask the value of.
     * @return boolean value of attribute
     * @exception ConfigurationException If no parameter with that name exists.
     *                                   or if conversion to 
     *                                   <code>boolean</code> fails.
     */
    public boolean getAttributeAsBoolean(String paramName)
        throws ConfigurationException;
    
    /**
     * Return the <code>String</code> value of the node.
     *
     * @return the value of the node.
     */
    public String getValue();
    
    /**
     * Return the <code>int</code> value of the node.
     *
     * @return the value of the node.
     * @exception ConfigurationException If conversion to <code>int</code>
     *                                   fails
     */
    public int getValueAsInt()
        throws ConfigurationException;
    
    /**
     * Return the <code>float</code> value of the node.
     *
     * @return the value of the node.
     * @exception ConfigurationException If conversion to <code>float</code>
     *                                   fails.
     */
    public float getValueAsFloat()
        throws ConfigurationException;
    
    /**
     * Return the <code>boolean</code> value of the node.
     *
     * @return the value of the node.
     * @exception ConfigurationException If conversion to <code>boolean</code>
     *                                   fails.
     */
    public boolean getValueAsBoolean()
        throws ConfigurationException;
    
    /**
     * Return the <code>long</code> value of the node.<br>
     *
     * @return the value of the node.
     * @exception ConfigurationException If conversion to <code>long</code>
     *                                   fails.
     */
    public long getValueAsLong()
        throws ConfigurationException;
    
    /**
     * Returns the value of the configuration element as a <code>String</code>.
     * If the configuration value is not set, the default value will be
     * used.
     *
     * @param defaultValue The default value desired.
     * @return String value of the <code>Configuration</code>, or default
     *         if none specified.
     */
    public String getValue(String defaultValue);
    
    /**
     * Returns the value of the configuration element as an <code>int</code>.
     * If the configuration value is not set, the default value will be
     * used.
     *
     * @param defaultValue The default value desired.
     * @return int value of the <code>Configuration</code>, or default
     *         if none specified.
     */
    public int getValueAsInt(int defaultValue);
    
    /**
     * Returns the value of the configuration element as a <code>long</code>.
     * If the configuration value is not set, the default value will be
     * used.
     *
     * @param defaultValue The default value desired.
     * @return long value of the <code>Configuration</code>, or default
     *         if none specified.
     */
    public long getValueAsLong(long defaultValue);
    
    /**
     * Returns the value of the configuration element as a <code>float</code>.
     * If the configuration value is not set, the default value will be
     * used.
     *
     * @param defaultValue The default value desired.
     * @return float value of the <code>Configuration</code>, or default
     *         if none specified.
     */
    public float getValueAsFloat(float defaultValue);
    
    /**
     * Returns the value of the configuration element as a <code>boolean</code>.
     * If the configuration value is not set, the default value will be
     * used.
     *
     * @param defaultValue The default value desired.
     * @return boolean value of the <code>Configuration</code>, or default
     *         if none specified.
     */
    public boolean getValueAsBoolean(boolean defaultValue);
    
    /**
     * Returns the value of the attribute specified by its name as a
     * <code>String</code>, or the default value if no attribute by
     * that name exists or is empty.
     *
     * @param name The name of the attribute you ask the value of.
     * @param defaultValue The default value desired.
     * @return String value of attribute. It will return the default
     *         value if the named attribute does not exist, or if
     *         the value is not set.
     */
    public String getAttribute(String name, String defaultValue);
    
    /**
     * Returns the value of the attribute specified by its name as a
     * <code>int</code>, or the default value if no attribute by
     * that name exists or is empty.
     *
     * @param name The name of the attribute you ask the value of.
     * @param defaultValue The default value desired.
     * @return int value of attribute. It will return the default
     *         value if the named attribute does not exist, or if
     *         the value is not set.
     */
    public int getAttributeAsInt(String name, int defaultValue);
    
    /**
     * Returns the value of the attribute specified by its name as a
     * <code>long</code>, or the default value if no attribute by
     * that name exists or is empty.
     *
     * @param name The name of the attribute you ask the value of.
     * @param defaultValue The default value desired.
     * @return long value of attribute. It will return the default
     *         value if the named attribute does not exist, or if
     *         the value is not set.
     */
    public long getAttributeAsLong(String name, long defaultValue);
    
    /**
     * Returns the value of the attribute specified by its name as a
     * <code>float</code>, or the default value if no attribute by
     * that name exists or is empty.
     *
     * @param name The name of the attribute you ask the value of.
     * @param defaultValue The default value desired.
     * @return float value of attribute. It will return the default
     *         value if the named attribute does not exist, or if
     *         the value is not set.
     */
    public float getAttributeAsFloat(String name, float defaultValue);
    
    /**
     * Returns the value of the attribute specified by its name as a
     * <code>boolean</code>, or the default value if no attribute by
     * that name exists or is empty.
     *
     * @param name The name of the attribute you ask the value of.
     * @param defaultValue The default value desired.
     * @return boolean value of attribute. It will return the default
     *         value if the named attribute does not exist, or if
     *         the value is not set.
     */
    public boolean getAttributeAsBoolean(String name, boolean defaultValue);
    
    /**
     * Return a <code>String</code> indicating the position of this
     * configuration element in a source file or URI.
     *
     * @return String if a source file or URI is specified.  Otherwise
     *         it returns <code>null</code>
     */
    public String getLocation();
    
}
