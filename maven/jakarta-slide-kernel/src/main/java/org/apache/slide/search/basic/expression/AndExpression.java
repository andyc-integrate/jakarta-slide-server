/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/basic/expression/AndExpression.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */



package org.apache.slide.search.basic.expression;
import java.util.Collection;

import org.apache.slide.search.InvalidQueryException;
import org.apache.slide.search.basic.IBasicResultSet;
import org.apache.slide.search.basic.Literals;
import org.jdom.Element;

/**
 * Represents an AND expression.
 *
 * @version $Revision: 1.2 $
 */
public class AndExpression extends MergeExpression {


    /**
     * Creates an AND expression according to Element e
     *
     * @param e                       jdom element, that describes the expression
     * @param expressionsToMerge      a Collection of IBasicExpressions to merge.
     */
    public AndExpression (Element e, Collection expressionsToMerge) throws InvalidQueryException {
        super (e, expressionsToMerge);
    }

    protected void merge (IBasicResultSet theOtherResultSet) {
        resultSet.retainAll (theOtherResultSet);
    }


    /**
     * String representation for debugging purposes.
     *
     * @return   this expression as String
     */
    public String toString () {
        return super.toString (Literals.AND);
    }

}

