/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/authenticate/CredentialsToken.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.authenticate;

import java.security.Principal;

/**
 * Credentials token class.
 *
 */
public final class CredentialsToken {
    
    
    // ------------------------------------------------------------ Constructor
    
    
    /**
     * Constructor.
     *
     * @param credentials Credentials stored in this token
     */
    public CredentialsToken(String credentials) {
        this.credentials = credentials;
        this.trusted = false;
    }
    
    
    /**
     * Constructor.
     *
     * @param principal Principal stored in this token
     */
    public CredentialsToken(Principal principal) {
        this.credentials = principal.getName();
        this.trusted = true;
        this.principal = principal;
    }
    
    
    // ----------------------------------------------------- Instance Variables
    
    /**
     *  principal instance.
     */
    private Principal principal;
    
    /**
     * Credentials, or principal.
     */
    private String credentials;
    
    
    /**
     * Trusted credentials ?
     */
    private boolean trusted;
    
    
    // ------------------------------------------------------------- Properties
    
    
    /**
     * Is this credentials token to be trusted ?
     *
     * @return boolean
     */
    public boolean isTrusted() {
        return trusted;
    }
    
    
    /**
     * Returns the private credentials.
     *
     * @return String
     */
    public String getPrivateCredentials() {
        // FIXME ?
        return credentials;
    }
    
    /**
     * Returns the current principal
     *
     * @return java.security.Principal
     */
    public Principal getPrincipal() {
         return principal;
    }
    
    /**
     * Returns the public creddentials.
     *
     * @return String
     */
    public String getPublicCredentials() {
        return credentials;
    }
    
    
}
