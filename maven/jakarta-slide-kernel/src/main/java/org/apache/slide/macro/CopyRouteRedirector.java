/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/macro/CopyRouteRedirector.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.macro;

// import list
import org.apache.slide.common.SlideException;

/**
 * A CopyRouteRedirector may be passed to the Macro helper in order to have more
 * control on the <code>copy</code> operation. A CopyRouteRedirector
 * may either return the given CopyRoute or any (redirected) CopyRoute.
 *
 * @version $Revision: 1.2 $
 *
 **/
public interface CopyRouteRedirector  {
    
    
    /**
     * A CopyRoute defines the source and the destination URI of a
     * <code>copy</code> operation.
     *
     * @version $Revision: 1.2 $
     *
     **/
    public static class CopyRoute {
        
        /**
         * Message of the IllegalArgumentException that is thrown by constructor
         *  if parameter <code>sourceUri</code> is <code>null</code>.
         */
        public static String SOURCE_MUST_NOT_BE_NULL = "Parameter 'sourceUri' must not be null";
        
        /**
         * Message of the IllegalArgumentException that is thrown by constructor
         * if parameter <code>destinationUri</code> is <code>null</code>.
         */
        public static String DESTINATION_MUST_NOT_BE_NULL = "Parameter 'destinationUri' must not be null";
        
        
        /**
         * The source Uri.
         */
        protected String sourceUri = null;
        
        /**
         * The destination Uri.
         */
        protected String destinationUri = null;
        
        /**
         * Creates a CopyRoute from the given URIs.
         *
         * @param      sourceUri       the Uri of the source.
         *                             Must not be <code>null</code>.
         * @param      destinationUri  the Uri of the destination.
         *                             Must not be <code>null</code>.
         *
         * @throws     IllegalArgumentException if one of the parameters is
         *             <code>null</code>.
         */
        public CopyRoute(String sourceUri, String destinationUri) throws IllegalArgumentException {
            
            if (sourceUri == null) {
                throw new IllegalArgumentException(SOURCE_MUST_NOT_BE_NULL);
            }
            if (destinationUri == null) {
                throw new IllegalArgumentException(DESTINATION_MUST_NOT_BE_NULL);
            }
            this.sourceUri = sourceUri;
            this.destinationUri = destinationUri;
        }
        
        /**
         * Returns the source Uri.
         *
         * @return     the source Uri.
         */
        public String getSourceUri() {
            return sourceUri;
        }
        
        /**
         * Returns the destination Uri.
         *
         * @return     the destination Uri.
         */
        public String getDestinationUri() {
            return destinationUri;
        }
        
        /**
         * Returns a String representation of the CopyRoute.
         *
         * @return     a String representation of the CopyRoute.
         */
        public String toString() {
            return "CopyRoute["+getSourceUri()+", "+getDestinationUri()+"]";
        }
        
        /**
         * Returns <code>true</code> if the other Object is a CopyRoute
         * and both routes source and destination URIs are equal.
         *
         * @param      other  the Object to test for equality.
         *
         * @return     <code>true</code> if the other Object is a CopyRoute
         *             and both routes source and destination URIs are equal.
         */
        public boolean equals(Object other) {
            boolean isEqual = false;
            if (other instanceof CopyRoute) {
                isEqual =
                    ((CopyRoute)other).getSourceUri().equals(getSourceUri()) &&
                    ((CopyRoute)other).getDestinationUri().equals(getDestinationUri());
            }
            return isEqual;
        }
        
        /**
         * Returns the hash code of this instance.
         * Due to specification equal objects must have the same hash code.
         *
         * @return     the hash code of this instance.
         */
        public int hashCode() {
            return getSourceUri().hashCode() + 13*getDestinationUri().hashCode();
        }
    }
    
    
    
    /**
     * Returns the (redirected) CopyRoute to use. Must not be <code>null</code>.
     *
     * @param      copyRoute the original CopyRoute.
     *
     * @return     the (redirected) CopyRoute to use.
     *
     * @throws     SlideException  this Exception will be passed to the caller
     *                             of the Macro helper (contained in the
     *                             MacroCopyException).
     */
    public CopyRoute getRedirectedCopyRoute(CopyRoute copyRoute) throws SlideException;
    
}

