/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/event/LockEvent.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.event;

import org.apache.slide.common.SlideToken;
import org.apache.slide.common.Uri;
import org.apache.slide.common.Namespace;

import java.util.EventListener;
import java.util.EventObject;

/**
 * Lock event class
 *
 * @version $Revision: 1.2 $
 */
public class LockEvent extends EventObject {
    public final static Lock LOCK = new Lock();
    public final static Unlock UNLOCK = new Unlock();
    public final static Renew RENEW = new Renew();
    public final static Kill KILL = new Kill();

    public final static String GROUP = "lock";
    public final static AbstractEventMethod[] methods = new AbstractEventMethod[] { LOCK, UNLOCK, RENEW, KILL };

    private Uri objectUri;
    private Namespace namespace;
    private SlideToken token;

    public LockEvent(Object source, SlideToken token, Namespace namespace, Uri objectUri) {
        super(source);
        this.objectUri = objectUri;
        this.token = token;
        this.namespace = namespace;
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer(256);
        buffer.append(getClass().getName()).append("[lock uri=").append(objectUri);
        buffer.append("]");
        return buffer.toString();
    }

    public Namespace getNamespace() {
        return namespace;
    }

    public SlideToken getToken() {
        return token;
    }

    public final static class Lock extends VetoableEventMethod {
        public Lock() {
            super(GROUP, "lock");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException {
            if ( listener instanceof LockListener ) ((LockListener)listener).lock((LockEvent)event);
        }
    }

    public final static class Unlock extends VetoableEventMethod {
        public Unlock() {
            super(GROUP, "unlock");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof LockListener ) ((LockListener)listener).unlock((LockEvent)event);
        }
    }

    public final static class Renew extends VetoableEventMethod {
        public Renew() {
            super(GROUP, "renew");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof LockListener ) ((LockListener)listener).renew((LockEvent)event);
        }
    }

    public final static class Kill extends VetoableEventMethod {
        public Kill() {
            super(GROUP, "kill");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof LockListener ) ((LockListener)listener).kill((LockEvent)event);
        }
    }
}