/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/SearchToken.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.search;

import org.apache.slide.common.SlideToken;
import org.apache.slide.common.Namespace;
import org.apache.slide.content.Content;
import org.apache.slide.structure.Structure;

/**
 * Represents the context of a search requerst.
 *
 * @version $Revision: 1.2 $
 */
public class SearchToken {
	
	private SlideToken slideToken;
	private Content contentHelper;
	private Structure structureHelper;
	
    private SlideUri slideContext;
	
	private int maxDepth = Integer.MAX_VALUE;
	
    private Namespace namespace;
    
	/**
	 * SearchToken factory.
	 *
	 * @param token      the slideToken
	 * @param content    content helper
	 * @param structure  structure helper
	 * @param maxDepth   the maximum search depth as configurede in Domain.xml
	 *
	 * @return   a SearchToken
	 */
	public static SearchToken createSearchToken
		(SlideToken token, Content content,
         Structure structure, int maxDepth,
         String requestUri, Namespace namespace)
	{
        return new SearchToken (token, content, structure, maxDepth, requestUri, namespace);
	}
	
	/**
	 * SearchToken factory for test purpose.
	 *
	 * @param maxDepth   the maximum search depth as configurede in Domain.xml
	 *
	 * @return   a SearchToken
	 *
	 */
    public static SearchToken createSearchTokenForTestOnly (int maxDepth, String requestUri) {
        return new SearchToken (null, null, null, maxDepth, requestUri, null);
	}
	
	
	/**
	 * Constructor, force use of factory method.
	 *
	 * @param token      the slideToken
	 * @param content    content helper
	 * @param structure  structure helper
	 * @param maxDepth   the maximum search depth as configurede in Domain.xml
	 *
	 */
	private SearchToken (SlideToken token, Content content,
                         Structure structure, int maxDepth,
                         String requestUri, Namespace namespace) {
		
		this.slideToken = token;
		this.contentHelper = content;
		this.structureHelper = structure;
		this.maxDepth = maxDepth;
      this.slideContext = SlideUri.createWithRequestUri(
              (String)token.getParameter("slideContextPath"), requestUri);
      this.namespace = namespace;
    }
    
    /**
     * Method getNamespace
     *
     * @return   the namespace
     *
     */
    public Namespace getNamespace () {
        return namespace;
	}
	
	/**
	 * Method getSlideToken
	 *
	 * @return   the slideToken
	 *
	 */
	public SlideToken getSlideToken() {
		return slideToken;
	}
	
	/**
	 * Method getContentHelper
	 *
	 * @return   the contentHelper
	 *
	 */
	public Content getContentHelper() {
		return contentHelper;
	}
	
	/**
	 * Method getStructureHelper
	 *
	 * @return   the structureHelper
	 *
	 */
	public Structure getStructureHelper() {
		return structureHelper;
	}
	
	/**
	 * Method getMaxDepth
	 *
	 * @return   max depth as configured in Domain.xml
	 *
	 */
	public int getMaxDepth() {
		return maxDepth;
	}
    
    /**
     * Method getSlideContext
     *
     * @return   a SlideUri
     *
     */
    public SlideUri getSlideContext () {
        return slideContext;
    }
}

