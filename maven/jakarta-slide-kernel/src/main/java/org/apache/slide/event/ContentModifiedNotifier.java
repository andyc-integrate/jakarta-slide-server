/*
 *  Copyright 1999-2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.event;

import org.apache.slide.common.Domain;
import org.apache.slide.common.Uri;
import org.apache.slide.util.conf.Configurable;
import org.apache.slide.util.conf.Configuration;
import org.apache.slide.util.conf.ConfigurationException;
import org.apache.slide.util.logger.Logger;

/**
 * Fires an UriModifiedEvent whenever an Uri is modified.
 * 
 */
public class ContentModifiedNotifier extends ContentAdapter implements
		Configurable {
	
	protected static final String LOG_CHANNEL = ContentModifiedNotifier.class.getName();
	
	public ContentModifiedNotifier() {
		Domain.log( "Creating ContentModifiedNotifier", LOG_CHANNEL, Logger.DEBUG );
	}

	public void create( ContentEvent event ) {
		notify( event );
	}
	
	public void fork( ContentEvent event ) {
		// TODO: find out what "fork" does and if it needs watching.
		notify( event );
	}
	
	public void merge( ContentEvent event ) {
		// TODO: find out what "merge" does and if it needs watching.
		notify( event );
	}
     
	public void remove( ContentEvent event ) {
		notify( event );
	}
     
	public void retrieve( ContentEvent event ) {
		notify( event );
	}
     
	public void store( ContentEvent event ) {
		notify( event );
	}
	 
	public void configure(Configuration configuration)
			throws ConfigurationException {
		/*
		 * TODO: Configure the notification mechanism (http, jms, javagroups)
		 */
	}
	
	public void notify( ContentEvent event ) {
		/*
		 * TODO: Modify this to actually send the event to the other systems in a cluster.
		 * Maybe this should be subclassed to allow different messaging implementations?
		 */
		Domain.log( "Called ContentModifiedNotifier.notify for " + event.getUri(), LOG_CHANNEL, Logger.DEBUG );
		if ( UriModifiedEvent.URIMODIFIED.isEnabled() ) {
		    EventDispatcher.getInstance().fireEvent(
		    	UriModifiedEvent.URIMODIFIED, new UriModifiedEvent(this, new Uri( event.getNamespace(), event.getUri() )));
		} else {
			Domain.log( "Can't notify, UriModifiedEvent is disabled.", LOG_CHANNEL, Logger.DEBUG );
		}
	}

}
