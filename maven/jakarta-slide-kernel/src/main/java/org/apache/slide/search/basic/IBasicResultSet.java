/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/basic/IBasicResultSet.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.search.basic;

// import list
import java.util.Set;

/**
 * This Set is returned by {@link org.apache.slide.search.basic.IBasicExpression#execute
 * IBasicExpression.execute()}. It extends {@link java.util.Set Set} by method
 * {@link #isPartialResultSet isPartialResultSet()}.
 *
 * @version $Revision: 1.2 $
 *
 **/
public interface IBasicResultSet extends Set {
    
    /**
     * Returns <code>true</code> if the result set is truncated for any reason.
     *
     * @return     <code>true</code> if the result set is truncated for any reason.
     */
    public boolean isPartialResultSet();
}

