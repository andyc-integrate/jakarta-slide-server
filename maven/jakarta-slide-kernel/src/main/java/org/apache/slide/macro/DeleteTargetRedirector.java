/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/macro/DeleteTargetRedirector.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.macro;

// import list
import org.apache.slide.common.SlideException;

/**
 * A DeleteTargetRedirector may be passed to the Macro helper in order to have more
 * control on the <code>delete</code> operation. A DeleteTargetRedirector
 * may either return the given URI or any (redirected) URI.
 *
 * @version $Revision: 1.2 $
 *
 **/
public interface DeleteTargetRedirector  {
    
    /**
     * Returns the (redirected) target Uri to use. Must not be <code>null</code>.
     *
     * @param      targetUri the original target Uri.
     *
     * @return     the (redirected) target Uri to use.
     *
     * @throws     SlideException  this Exception will be passed to the caller
     *                             of the Macro helper (contained in the
     *                             MacroDeleteException).
     */
    public String getRedirectedTargetUri(String targetUri) throws SlideException;

}

