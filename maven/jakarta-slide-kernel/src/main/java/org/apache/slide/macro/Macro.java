/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/macro/Macro.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.macro;

import org.apache.slide.common.SlideToken;

/**
 * Macro helper class.
 *
 * @version $Revision: 1.2 $
 */
public interface Macro {
    
    public static final MacroParameters DEFAULT_PARAMETERS =
        new MacroParameters();
    
    public static final MacroParameters RECURSIVE_OVERWRITE_PARAMETERS =
        new MacroParameters(true, true);
    
    public final static String ALREADY_COPIED = "alreadyCopied";
    public final static String PARENT_BINDINGS = "parentBindings";
    
    
    // ------------------------------------------------------ Interface Methods
    
    
    /**
     * Recursive copy with overwrite macro.
     *
     * @param token Credentials token
     * @param sourceUri Uri of the source
     * @param destinationUri Uri of the destination
     * @exception CopyMacroException Generic Slide exception
     */
    void copy(SlideToken token, String sourceUri,
              String destinationUri)
        throws CopyMacroException, DeleteMacroException;
    
    /**
     * Recursive copy with overwrite macro.
     *
     * @param token Credentials token
     * @param sourceUri Uri of the source
     * @param destinationUri Uri of the destination
     * @param copyRedirector  the CopyRoutRedirector may be used to redirect
     *                        the source and/or destination URI of the
     *                         <code>copy</code> operation.
     *                        (May be <code>null</code>.)
     * @param copyListener    the CopyListener that will be notified
     *                        before and after copying a resource.
     *                        (May be <code>null</code>)
     * @param deleteRedirector  the DeleteTargetRedirector may be used to redirect
     *                          the <code>delete</code> operation to a different
     *                          target. (May be <code>null</code>.)
     * @param deleteListener  the DeleteListener that will be notified
     *                        before and after deleting a destination
     *                        that will be overwritten by the copy.
     *                        (May be <code>null</code>)
     * @exception CopyMacroException Generic Slide exception
     */
    void copy(SlideToken token, String sourceUri,
              String destinationUri, CopyRouteRedirector copyRedirector, CopyListener copyListener,
              DeleteTargetRedirector deleteRedirector, DeleteListener deleteListener)
        throws CopyMacroException, DeleteMacroException;
    
    
    /**
     * Copy macro.
     *
     * @param token Credentials token
     * @param sourceUri Uri of the source
     * @param destinationUri Uri of the destination
     * @param parameters Macro parameters
     * @exception CopyMacroException Generic Slide exception
     */
    void copy(SlideToken token, String sourceUri,
              String destinationUri, MacroParameters parameters)
        throws CopyMacroException, DeleteMacroException;
    
    /**
     * Copy macro.
     *
     * @param token Credentials token
     * @param sourceUri Uri of the source
     * @param destinationUri Uri of the destination
     * @param parameters Macro parameters
     * @param copyRedirector  the CopyRoutRedirector may be used to redirect
     *                        the source and/or destination URI of the
     *                         <code>copy</code> operation.
     *                        (May be <code>null</code>.)
     * @param copyListener    the CopyListener that will be notified
     *                        before and after copying a resource.
     *                        (May be <code>null</code>)
     * @param deleteRedirector  the DeleteTargetRedirector may be used to redirect
     *                          the <code>delete</code> operation to a different
     *                          target. (May be <code>null</code>.)
     * @param deleteListener  the DeleteListener that will be notified
     *                        before and after deleting a destination
     *                        that will be overwritten by the copy.
     *                        (May be <code>null</code>)
     * @exception CopyMacroException Generic Slide exception
     */
    void copy(SlideToken token, String sourceUri,
              String destinationUri, MacroParameters parameters,
              CopyRouteRedirector copyRedirector, CopyListener copyListener,
              DeleteTargetRedirector deleteRedirector, DeleteListener deleteListener)
        throws CopyMacroException, DeleteMacroException;
    
    
    /**
     * Recursive move with overwrite macro.
     *
     * @param token Credentials token
     * @param sourceUri Uri of the source
     * @param destinationUri Uri of the destination
     * @exception CopyMacroException Exception occured during copy
     * @exception DeleteMacroException Exception occured during deletion
     */
    void move(SlideToken token, String sourceUri,
              String destinationUri)
        throws CopyMacroException, DeleteMacroException;
    
    /**
     * Recursive move with overwrite macro.
     *
     * @param token Credentials token
     * @param sourceUri Uri of the source
     * @param destinationUri Uri of the destination
     * @param copyRedirector  the CopyRoutRedirector may be used to redirect
     *                        the source and/or destination URI of the
     *                         <code>copy</code> operation.
     *                        (May be <code>null</code>.)
     * @param copyListener    the CopyListener that will be notified
     *                        before and after copying a resource.
     *                        (May be <code>null</code>)
     * @param deleteRedirector  the DeleteTargetRedirector may be used to redirect
     *                          the <code>delete</code> operation to a different
     *                          target. (May be <code>null</code>.)
     * @param deleteListener  the DeleteListener that will be notified
     *                        before and after deleting a resource.
     *                        (May be <code>null</code>)
     * @exception CopyMacroException Exception occured during copy
     * @exception DeleteMacroException Exception occured during deletion
     */
    void move(SlideToken token, String sourceUri,
              String destinationUri, CopyRouteRedirector copyRedirector, CopyListener copyListener,
              DeleteTargetRedirector deleteRedirector, DeleteListener deleteListener)
        throws CopyMacroException, DeleteMacroException;
    
    
    /**
     * Move macro.
     *
     * @param token Credentials token
     * @param sourceUri Uri of the source
     * @param destinationUri Uri of the destination
     * @param parameters Macro parameters
     * @exception CopyMacroException Exception occured during copy
     * @exception DeleteMacroException Exception occured during deletion
     */
    void move(SlideToken token, String sourceUri,
              String destinationUri, MacroParameters parameters)
        throws CopyMacroException, DeleteMacroException;
    
    /**
     * Move macro.
     *
     * @param token Credentials token
     * @param sourceUri Uri of the source
     * @param destinationUri Uri of the destination
     * @param parameters Macro parameters
     * @param copyRedirector  the CopyRoutRedirector may be used to redirect
     *                        the source and/or destination URI of the
     *                         <code>copy</code> operation.
     *                        (May be <code>null</code>.)
     * @param copyListener    the CopyListener that will be notified
     *                        before and after copying a resource.
     *                        (May be <code>null</code>)
     * @param deleteRedirector  the DeleteTargetRedirector may be used to redirect
     *                          the <code>delete</code> operation to a different
     *                          target. (May be <code>null</code>.)
     * @param deleteListener  the DeleteListener that will be notified
     *                        before and after deleting a resource.
     *                        (May be <code>null</code>)
     * @exception CopyMacroException Exception occured during copy
     * @exception DeleteMacroException Exception occured during deletion
     */
    void move(SlideToken token, String sourceUri,
              String destinationUri, MacroParameters parameters,
              CopyRouteRedirector copyRedirector, CopyListener copyListener,
              DeleteTargetRedirector deleteRedirector, DeleteListener deleteListener)
        throws CopyMacroException, DeleteMacroException;
    
    
    /**
     * Recursive delete.
     *
     * @param token Credentials token
     * @param targetUri Uri of the object to delete
     * @exception DeleteMacroException Generic Slide exception
     */
    void delete(SlideToken token, String targetUri)
        throws DeleteMacroException;
    
    /**
     * Recursive delete.
     *
     * @param token Credentials token
     * @param targetUri Uri of the object to delete
     * @param deleteRedirector  the DeleteTargetRedirector may be used to redirect
     *                          the <code>delete</code> operation to a different
     *                          target. (May be <code>null</code>.)
     * @param deleteListener  the DeleteListener that will be notified
     *                        before and after deleting a resource.
     *                        (May be <code>null</code>)
     * @exception DeleteMacroException Generic Slide exception
     */
    void delete(SlideToken token, String targetUri,
                DeleteTargetRedirector deleteRedirector, DeleteListener deleteListener)
        throws DeleteMacroException;
    
    
    /**
     * Delete macro.
     *
     * @param token Credentials token
     * @param targetUri Uri of the source
     * @param parameters Macro parameters, not used right now,
     * so it can be null
     * @exception DeleteMacroException Generic Slide exception
     */
    void delete(SlideToken token, String targetUri,
                MacroParameters parameters)
        throws DeleteMacroException;
    
    
    /**
     * Delete macro.
     *
     * @param token Credentials token
     * @param targetUri Uri of the source
     * @param parameters Macro parameters, not used right now,
     * so it can be null
     * @param deleteRedirector  the DeleteTargetRedirector may be used to redirect
     *                          the <code>delete</code> operation to a different
     *                          target. (May be <code>null</code>.)
     * @param deleteListener  the DeleteListener that will be notified
     *                        before and after deleting a resource.
     *                        (May be <code>null</code>)
     * @exception DeleteMacroException Generic Slide exception
     */
    void delete(SlideToken token, String targetUri, MacroParameters parameters,
                DeleteTargetRedirector deleteRedirector, DeleteListener deleteListener)
        throws DeleteMacroException;
    
}
