/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/common/UnableToLocateServiceException.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */ 

package org.apache.slide.common;

import org.apache.slide.util.Messages;

/**
 * Unable to locate service.
 * 
 * @version $Revision: 1.2 $
 */
public class UnableToLocateServiceException extends SlideException {
    
    
    // ----------------------------------------------------------- Constructors
    
    
    /**
     * Constructor.
     * 
     * @param uri Uri on which there was a problem locating a valid service
     */
    public UnableToLocateServiceException(Uri uri) {
	super(Messages.format(UnableToLocateServiceException.class.getName(), 
                              uri));
    }
    
}
