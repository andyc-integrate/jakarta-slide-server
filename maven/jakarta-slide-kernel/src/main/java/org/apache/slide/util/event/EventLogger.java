package org.apache.slide.util.event;

import org.apache.slide.event.GlobalListener;
import org.apache.slide.event.VetoException;
import org.apache.slide.event.VetoableEventMethod;
import org.apache.slide.event.EventMethod;
import org.apache.slide.common.Domain;
import org.apache.slide.util.logger.Logger;

import java.util.EventObject;

/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/util/event/EventLogger.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

public class EventLogger implements GlobalListener {
    protected static final String LOG_CHANNEL = EventLogger.class.getName();

    public void vetoableEventFired(VetoableEventMethod method, EventObject event) throws VetoException {
        Domain.log("Recieved vetoable event with name '"+method.getId()+"': "+event, LOG_CHANNEL, Logger.INFO);
    }

    public void eventFired(EventMethod method, EventObject event) {
        Domain.log("Recieved event with name '"+method.getId()+"': "+event, LOG_CHANNEL, Logger.INFO);
    }
}