/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/common/EmbeddedDomain.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.common;

import java.util.Enumeration;
import java.util.Hashtable;

import org.apache.slide.util.conf.Configuration;
import org.apache.slide.util.conf.ConfigurationException;
import org.apache.slide.util.logger.Logger;

/**
 * Alternate domain designed to ease embedding.
 *
 * @version $Revision: 1.2 $
 */
public class EmbeddedDomain {
    
    
    // ------------------------------------------------------------ Constructor
    
    
    /**
     * Default constructor.
     */
    public EmbeddedDomain() {
        
        // Compatibility with the static domain
        if (!Domain.isInitialized())
            Domain.setDomain(this);
        
    }
    
    
    // ----------------------------------------------------- Instance Variables
    
    
    /**
     * Namespaces hashtable.
     */
    private Hashtable namespaces = new Hashtable();
    
    
    /**
     * Default namespace.
     */
    private String defaultNamespace;
    
    
    /**
     * Namespace name.
     */
    private String name = "slide-domain";
    
    
    // --------------------------------------------------------- Public Methods
    
    
    /**
     * Sets the qualified name of the namespace.
     *
     * @param name Name of the namespace
     */
    public void setName(String name) {
        this.name = name;
    }
    
    
    /**
     * Gets the qulified name of the namespace.
     *
     * @return String Namespace name
     */
    public String getName() {
        return name;
    }
    
    
    /**
     * Return the default namespace of this domain.
     *
     * @return the name of the default namespace
     */
    public String getDefaultNamespace() {
        return defaultNamespace;
    }
    
    
    /**
     * Set the default namespace of this domain.
     *
     * @param defaultNamespace New default namespace name
     */
    public void setDefaultNamespace(String defaultNamespace) {
        this.defaultNamespace = defaultNamespace;
    }
    
    
    /**
     * Set the logger to be used by Slide.
     *
     * @param logger Logger the domain will use
     * @deprecated Use the namespace loggers instead
     */
    public void setLogger(Logger logger) {
        Domain.setLogger(logger);
    }
    
    
    /**
     * Get the Domain logger.
     *
     * @return The domain logger
     * @deprecated Use the namespace loggers instead
     */
    public Logger getLogger() {
        return Domain.getLogger();
    }
    
    
    /**
     * Access a Namespace.
     *
     * @param namespaceName Name of the namespace on which access is requested
     * @return NamespaceAccessToken Access token to the namespace
     */
    public NamespaceAccessToken getNamespaceToken(String namespaceName) {
        Namespace namespace = (Namespace) namespaces.get(namespaceName);
        if (namespace == null)
            return null;
        else
            return new NamespaceAccessTokenImpl(namespace);
    }
    
    
    /**
     * Enumerate namespace names.
     */
    public Enumeration enumerateNamespaces() {
        return (namespaces.keys());
    }
    
    
    /**
     * Add a namespace to this domain.
     *
     * @param name Namespace name
     * @param logger Namespace logger
     * @param definition Input stream to the namepace definition
     * @param configuration Input stream to the namespace configuration
     * @param baseData Input stream to the anmespace base data
     */
    public Namespace addNamespace(String name, Logger logger,
                                  Configuration definition,
                                  Configuration configuration,
                                  Configuration baseData) {
        
        // FIXME: Check parameters
        
        Namespace namespace = new Namespace();
        namespace.setName(name);
        namespace.setLogger(logger);
        
        try {
            namespace.loadParameters(configuration);
            namespace.loadDefinition(definition);
            namespace.loadBaseData(baseData);
            namespace.loadConfiguration(configuration);
        } catch (SlideException e) {
            e.printStackTrace();
            return null;
        } catch (ConfigurationException e) {
            e.printStackTrace();
            return null;
        }
        
        namespaces.put(name, namespace);
        
        return namespace;
        
    }
    
    
    /**
     * Clsose a namespace.
     *
     * @param name Name of the namespace
     */
    public Namespace removeNamespace(String name) {
        
        // FIXME: Check parameters
        
        try {
            Namespace namespace = (Namespace) namespaces.get(name);
            namespace.disconnectServices();
        } catch(Exception e) {
        }
        return (Namespace) namespaces.remove(name);
        
    }
    
    
    /**
     * Get a namespace.
     *
     * @param name Name of the namespace
     * @return Namespace
     */
    public Namespace getNamespace(String name) {
        return (Namespace) namespaces.get(name);
    }
    
    /**
     * Set the specified parameters
     *
     * @param    parameters          the parameters
     *
     */
    public void setParameters( Hashtable parameters ) {
        Domain.setParameters( parameters );
    }
    
    /**
     * Start domain (doesn't do anything yet).
     */
    public void start()
        throws Exception {
        
    }
    
    
    /**
     * Stop domain.
     */
    public void stop()
        throws Exception {
        
        Enumeration active = namespaces.elements();
        while (active.hasMoreElements()) {
            ((Namespace) active.nextElement()).disconnectServices();
        }
        
    }
    
    
    // --------------------------------------------------------- Object Methods
    
    
    /**
     * Get a String representation of this domain.
     */
    public String toString() {
        return getName();
    }
    
    
}
