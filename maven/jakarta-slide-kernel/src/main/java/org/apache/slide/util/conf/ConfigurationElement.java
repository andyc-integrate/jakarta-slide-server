/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/util/conf/ConfigurationElement.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.util.conf;

import java.util.*;

/**
 * <code>ConfigurationElement</code> is a class implementing the 
 * <code>Configuration</code> interface using a tree of Elements as
 * configuration container. The <code>Elements</code> tree is generated from the
 * configuration DOM. Each <code>Element</code> is encapsulated by a 
 * <code>ConfigurationElement</code>.
 *
 *
 * @version (CVS $Revision: 1.2 $ $Date: 2006-01-22 22:49:05 $)
 */
public class ConfigurationElement extends AbstractConfiguration {

    private Element content;
    private Vector children;

    public ConfigurationElement(Element content) {
        this.content = content;
        children = new Vector();
        try {
            for (Enumeration e = content.getChildren(); e.hasMoreElements();) {
                children.addElement(new ConfigurationElement((Element) e.nextElement()));
            }
        } catch (ConfigurationException e) {
        }
    }

    public String getName() {
        return content.getName();
    }

    public String getValue() {
        return content.getData();
    }

    public String getAttribute(String name)
    throws ConfigurationException {
        String attribute = content.getAttributeValue(name);
        if (attribute == null) {
            throw new ConfigurationException("No attribute named \"" + name + "\" is " +
                "associated with the configuration element \"" + this.getName() + "\"",
                this);
        }
        return attribute;
    }

    public Configuration getConfiguration(String name)
    throws ConfigurationException {

        int index = name.indexOf('.');
        if (index == -1) {
            for (Enumeration e = children.elements(); e.hasMoreElements();) {
                Configuration c = (Configuration) e.nextElement();
                if (c.getName().equals(name)) {
                    return c;
                }
            }
        } else {
            return getConfiguration(name.substring(0, index)).getConfiguration(name.substring(index + 1));
        }
        throw new ConfigurationException("No Configuration named \"" + name + "\" is " +
            "associated with the configuration element \"" + this.getName() + "\"", this);
    }

    public Enumeration getConfigurations(String name)
    throws ConfigurationException {

        int index = name.indexOf('.');
        if (index == -1) {
            Vector v = new Vector();
            for (Enumeration e = children.elements(); e.hasMoreElements();) {
                Configuration c = (Configuration) e.nextElement();
                if (c.getName().equals(name)) {
                    v.addElement(c);
                }
            }
            return v.elements();
        } else {
            return getConfiguration(name.substring(0, index)).getConfigurations(name.substring(index + 1));
        }
    }
}
