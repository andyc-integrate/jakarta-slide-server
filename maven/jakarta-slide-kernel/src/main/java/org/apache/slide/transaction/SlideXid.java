/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/transaction/SlideXid.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.transaction;

import javax.transaction.xa.Xid;
import java.util.Arrays;

/**
 * Xid implementation.
 *
 * @version $Revision: 1.2 $
 */
public final class SlideXid implements Xid {


    // ------------------------------------------------------------ Constructor


    /**
     * Constructor.
     */
    public SlideXid(byte[] globalTransactionId, int formatId,
                    byte[] branchQualifier) {
        this.branchQualifier = branchQualifier;
        this.formatId = formatId;
        this.globalTransactionId = globalTransactionId;
    }


    // ----------------------------------------------------- Instance Variables


    /**
     * Branch qualifier.
     */
    private byte[] branchQualifier;


    /**
     * Format id.
     */
    private int formatId;


    /**
     * Global transaction id.
     */
    private byte[] globalTransactionId;


    // ------------------------------------------------------------- Properties


    // ------------------------------------------------------------ Xid Methods


    /**
     * Obtain the format identifier part of the XID.
     *
     * @return Format identifier. O means the OSI CCR format.
     */
    public int getFormatId() {
        return formatId;
    }


    /**
     * Obtain the global transaction identifier part of XID as an array of
     * bytes.
     *
     * @return Global transaction identifier.
     */
    public byte[] getGlobalTransactionId() {
        return globalTransactionId;
    }


    /**
     * Obtain the transaction branch identifier part of XID as an array of
     * bytes.
     *
     * @return Global transaction identifier.
     */
    public byte[] getBranchQualifier() {
        return branchQualifier;
    }


    // --------------------------------------------------------- Public Methods


    /**
     * Obtain a String representation of this xid.
     */
    public String toString() {
        String str = new String(getGlobalTransactionId());
        StringBuffer b = new StringBuffer();
        b.append(str);
        return new String(getGlobalTransactionId()) + "-"
            + new String(getBranchQualifier());
    }

    public boolean equals(Object o) {
        if(o == this) {
            return true;
        }
        if(o == null || !(o instanceof SlideXid)) {
            return false;
        }
        SlideXid s = (SlideXid) o;
        if(!Arrays.equals(globalTransactionId, s.globalTransactionId)) {
            return false;
        } else if (!Arrays.equals(branchQualifier, s.branchQualifier)) {
            return false;
        } else if (formatId != s.formatId) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return globalTransactionId.hashCode() + branchQualifier.hashCode() + formatId;
    }


    // -------------------------------------------------------- Package Methods


    /**
     * Create a new branch based on this Xid.
     */
    Xid newBranch(int branchNumber) {
        return new SlideXid(getGlobalTransactionId(), getFormatId(),
                            Integer.toString(branchNumber).getBytes());
    }


}
