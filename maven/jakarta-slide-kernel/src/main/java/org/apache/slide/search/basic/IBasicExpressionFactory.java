/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/basic/IBasicExpressionFactory.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.search.basic;

import java.util.Collection;

import org.apache.slide.search.BadQueryException;
import org.apache.slide.search.PropertyProvider;
import org.jdom.Element;

/**
 * An IBasicExpression represents an executable part of the expression tree.
 * Different stores may have different implementations.
 *
 * @version $Revision: 1.2 $
 */
public interface IBasicExpressionFactory {

    /** parameter name for the expression factory implementation */
    public String BASIC_EXPRESSION_FACTORY_CLASS =
        "basicExpressionFactoryClass";

    /**
     * Initializes the factory. Is called exactly once and before any call
     * to crateExpression ()
     *
     * @param    query               the  IBasicQuery.
     * @param    propertyProvider    the  PropertyProvider to use (may be
     *                               <code>null</code>).
     *
     * @throws   BadQueryException
     */
    void init (IBasicQuery query, PropertyProvider propertyProvider) throws BadQueryException;

    /**
     * Returns the IBasicQuery to use (set in method {@link #init init()}).
     *
     * @return     the IBasicQuery to use.
     */
    public IBasicQuery getQuery();

    /**
     * Returns the PropertyProvider to use (set in method {@link #init init()}).
     *
     * @return     the PropertyProvider to use.
     */
    public PropertyProvider getPropertyProvider();


    /**
     * Creates a MergeExpression for the given element (AND, OR). The given children
     * are the expressions to merge.
     *
     * @param    name                the name of the Element describing the merge expression.
     * @param    namespace           the namespace of the Element describing the merge expression.
     * @param    expressionsToMerge  the expressions to merge.
     *
     * @return   an IBasicExpression
     *
     * @throws   BadQueryException
     */
    IBasicExpression createMergeExpression (String name, String namespace, Collection expressionsToMerge)
        throws BadQueryException;

    /**
     * Creates a (non-merge) expression (compare...) for the given Element.
     *
     * @param    element             an Element describing the expression.
     *
     * @return   an IBasicExpression
     *
     * @throws   BadQueryException
     */
    IBasicExpression createExpression (Element element)
        throws BadQueryException;

}


