/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/store/SequenceStore.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2004 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.store;

import org.apache.slide.common.Service;
import org.apache.slide.common.ServiceAccessException;

/**
 * Store for sequence support. A sequence is an entity that provides unique numbers.
 * A store supports sequences when it implements this interface and the method 
 * {@link isSupported} returns <code>true</code>.  
 *
 * @version $Revision: 1.2 $
 */
public interface SequenceStore extends Service {
    
    /**
     * Checks if this store instance actually supports sequences. It may seem clear 
     * this store supports sequences as it implements this interface, but a request to the
     * underlying persistence store might be needed to dynamically find out.
     * 
     * @return <code>true</code> if the store supports sequences, <code>false</code> otherwise
     */
    public boolean isSequenceSupported();
    
    /**
     * Checks if the sequence already exists.
     * 
     * @param sequenceName the name of the sequence you want to check 
     * @return <code>true</code> if the sequence already exists, <code>false</code> otherwise
     * @throws ServiceAccessException if anything goes wrong while accessing the sequence 
     */
    public boolean sequenceExists(String sequenceName) throws ServiceAccessException;
    
    /**
     * Creates a sequence if it does not already exist.
     * 
     * @param sequenceName the name of the sequence you want to create 
     * @return <code>true</code> if the sequence has been created, <code>false</code> if it already existed
     * @throws ServiceAccessException if anything goes wrong while accessing the sequence 
     */
    public boolean createSequence(String sequenceName) throws ServiceAccessException;
    
    /**
     * Gets the next value of the sequence. Note that the sequence may not deliver consecutive 
     * or continuous values. The only thing that is assured is the value will be unique 
     * in the scope of the sequence, i.e. this method will never return the 
     * same value for the same sequence. A sequence of valid values <em>might</em> be
     * <pre>1,2,3,4,5,...</pre>, but it might just as well be 
     * <pre>10,787875845,1,2,434,...</pre>.
     * However, it may not be 
     * <pre>1,2,1,3,...</pre>.
     * as a sequence must never return the same value twice or more times.
     * 
     * @param sequenceNamethe name of the sequence you want the next value for
     * @return the next value of the sequence
     * @throws ServiceAccessException if anything goes wrong while accessing the sequence 
     */
    public long nextSequenceValue(String sequenceName) throws ServiceAccessException;
}
