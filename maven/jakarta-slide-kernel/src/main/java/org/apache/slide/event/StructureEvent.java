/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/event/StructureEvent.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.event;

import org.apache.slide.common.SlideToken;
import org.apache.slide.common.Namespace;
import org.apache.slide.structure.ObjectNode;

import java.util.EventListener;
import java.util.EventObject;

/**
 * Structure event class
 *
 * @version $Revision: 1.2 $
 */
public class StructureEvent extends EventObject {
    public final static Retrieve RETRIEVE = new Retrieve();
    public final static Store STORE = new Store();
    public final static Create CREATE = new Create();
    public final static Remove REMOVE = new Remove();
    public final static CreateLink CREATE_LINK = new CreateLink();
    public final static AddBinding ADD_BINDING = new AddBinding();
    public final static RemoveBinding REMOVE_BINDING = new RemoveBinding();

    public final static String GROUP = "structure";
    public final static AbstractEventMethod[] methods = new AbstractEventMethod[] { RETRIEVE, STORE, CREATE, REMOVE, CREATE_LINK, ADD_BINDING, REMOVE_BINDING };

    private SlideToken token;
    private ObjectNode objectNode = null;
    private String uri = null;
    private Namespace namespace;

    public StructureEvent(Object source, SlideToken token, Namespace namespace, String uri) {
        super(source);
        this.uri = uri;
        this.token = token;
        this.namespace = namespace;
    }

    public StructureEvent(Object source, SlideToken token, Namespace namespace, ObjectNode objectNode) {
        super(source);
        this.token = token;
        this.objectNode = objectNode;
        this.namespace = namespace;
    }

    public StructureEvent(Object source, SlideToken token, ObjectNode objectNode, String uri) {
        super(source);
        this.token = token;
        this.objectNode = objectNode;
        this.uri = uri;
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer(256);
        buffer.append(getClass().getName()).append("[uri=").append(uri);
        buffer.append(", objectNode=").append(objectNode);
        buffer.append("]");
        return buffer.toString();
    }

    public SlideToken getToken() {
        return token;
    }

    public ObjectNode getObjectNode() {
        return objectNode;
    }

    public String getUri() {
        return uri;
    }
    
    public Namespace getNamespace() {
        return namespace;
    }

    public final static class Create extends VetoableEventMethod {
        public Create() {
            super(GROUP, "create");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof StructureListener ) ((StructureListener)listener).create((StructureEvent)event);
        }
    }

    public final static class CreateLink extends VetoableEventMethod {
        public CreateLink() {
            super(GROUP, "create-link");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof StructureListener ) ((StructureListener)listener).createLink((StructureEvent)event);
        }
    }

    public final static class Remove extends VetoableEventMethod {
        public Remove() {
            super(GROUP, "remove");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof StructureListener ) ((StructureListener)listener).remove((StructureEvent)event);
        }
    }

    public final static class AddBinding extends VetoableEventMethod {
        public AddBinding() {
            super(GROUP, "add-binding");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof StructureListener ) ((StructureListener)listener).addBinding((StructureEvent)event);
        }
    }

    public final static class RemoveBinding extends VetoableEventMethod {
        public RemoveBinding() {
            super(GROUP, "remove-binding");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof StructureListener ) ((StructureListener)listener).removeBinding((StructureEvent)event);
        }
    }

    public final static class Store extends VetoableEventMethod {
        public Store() {
            super(GROUP, "store");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof StructureListener ) ((StructureListener)listener).store((StructureEvent)event);
        }
    }

    public final static class Retrieve extends VetoableEventMethod {
        public Retrieve() {
            super(GROUP, "retrieve");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof StructureListener ) ((StructureListener)listener).retrieve((StructureEvent)event);
        }
    }
}