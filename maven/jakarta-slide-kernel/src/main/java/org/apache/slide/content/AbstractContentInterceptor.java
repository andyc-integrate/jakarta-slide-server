/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/content/AbstractContentInterceptor.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */ 

package org.apache.slide.content;

import java.util.Hashtable;
import org.apache.slide.common.NamespaceAccessToken;
import org.apache.slide.common.SlideToken;
import org.apache.slide.common.ServiceAccessException;
import org.apache.slide.lock.ObjectLockedException;
import org.apache.slide.security.AccessDeniedException;
import org.apache.slide.structure.LinkedObjectNotFoundException;
import org.apache.slide.structure.ObjectNotFoundException;

/**
 * Provides a basic implementation of the <code>ContentInterceptor</code> 
 * interface.
 * 
 * <p>
 *   This implementation does nothing but store the parameters and the
 *   NamespaceAccessToken, and provide empty implementations of the various 
 *   hook methods. You can extend this class instead of implementing the 
 *   <code>ContentInterceptor</code> interface directly, and only override 
 *   the methods required for your specific interceptor to operate.
 * </p>
 * 
 */
public abstract class AbstractContentInterceptor
    implements ContentInterceptor {
    
    
    // ----------------------------------------------------- Instance Variables
    
    
    /**
     * The namespace access token.
     */
    private NamespaceAccessToken nat;
    
    
    /**
     * Hashtable containing the configuration parameters.
     */
    private Hashtable parameters;
    
    
    // ----------------------------------------------------------- Constructors
    
    
    /**
     * Default constructor.
     */
    public AbstractContentInterceptor() {
        
    }
    
    
    // -------------------------------------- ContentInterceptor Implementation
    
    
    /**
     * Does nothing.
     */
    public void preStoreContent
        (SlideToken token, NodeRevisionDescriptors revisionDescriptors,
         NodeRevisionDescriptor revisionDescriptor,
         NodeRevisionContent revisionContent)
        throws AccessDeniedException, ObjectNotFoundException,
               LinkedObjectNotFoundException, ObjectLockedException,
               ServiceAccessException {
        
    }
    
    
    /**
     * Does nothing.
     */
    public void postStoreContent
        (SlideToken token, NodeRevisionDescriptors revisionDescriptors,
         NodeRevisionDescriptor revisionDescriptor,
         NodeRevisionContent revisionContent)
        throws AccessDeniedException, ObjectNotFoundException,
               LinkedObjectNotFoundException, ObjectLockedException,
               ServiceAccessException {
        
    }
    
    
    /**
     * Does nothing.
     */
    public void preRetrieveContent
        (SlideToken token, NodeRevisionDescriptors revisionDescriptors,
         NodeRevisionNumber revisionNumber,
         NodeRevisionDescriptor revisionDescriptor)
        throws AccessDeniedException, ObjectNotFoundException,
               LinkedObjectNotFoundException, ObjectLockedException,
               ServiceAccessException {
        
    }
    
    
    /**
     * Does nothing.
     */
    public void postRetrieveContent
        (SlideToken token, NodeRevisionDescriptors revisionDescriptors,
         NodeRevisionDescriptor revisionDescriptor,
         NodeRevisionContent revisionContent)
        throws AccessDeniedException, ObjectNotFoundException,
               LinkedObjectNotFoundException, ObjectLockedException,
               ServiceAccessException {
        
    }
    
    
    /**
     * Does nothing.
     */
    public void preRemoveContent   
        (SlideToken token, NodeRevisionDescriptors revisionDescriptors,
         NodeRevisionDescriptor revisionDescriptor)
        throws AccessDeniedException, ObjectNotFoundException,
               LinkedObjectNotFoundException, ObjectLockedException,
               ServiceAccessException {
        
    }
    
    
    /**
     * Does nothing.
     */
    public void postRemoveContent   
        (SlideToken token, NodeRevisionDescriptors revisionDescriptors,
         NodeRevisionDescriptor revisionDescriptor)
        throws AccessDeniedException, ObjectNotFoundException,
               LinkedObjectNotFoundException, ObjectLockedException,
               ServiceAccessException {
        
    }
    
    
    /**
     * Implemented to store the namespace access token as instance variable.
     */
    public void setNamespace
        (NamespaceAccessToken nat) {
        
        this.nat = nat;
    }
    
    
    /**
     * Implemented to store the parameter Hashtable as instance variable.
     */
    public void setParameters
        (Hashtable parameters) {
        
        this.parameters = parameters;
    }
    
    
    // ------------------------------------------------------ Protected Methods
    
    
    /**
     * Returns the namespace access token.
     * 
     * @return the <code>NamespaceAccessToken</code> object, or 
     *         <code>null</code> if the interceptor has not been initialized
     */
    protected NamespaceAccessToken getNamespace() {
        
        return nat;
    }
    
    
    /**
     * Returns the value of the specified parameter.
     * 
     * @param name  name of the parameter to be retrieved
     * @return value of the parameter, or <code>null</code> if the parameter 
     *         was not provided
     */
    protected String getParameter
        (String name) {
        
        return (String)parameters.get(name);
    }
    
    
    /**
     * Returns a Hashtable containing the configuration parameters of the 
     * interceptor.
     * 
     * @return configuration parameters of the interceptor, or 
     *         <code>null</code> if the interceptor has not been initialized
     */
    protected Hashtable getParameters() {
        
        return parameters;
    }
    
    
}

