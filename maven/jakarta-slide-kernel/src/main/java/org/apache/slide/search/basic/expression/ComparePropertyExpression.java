/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/basic/expression/ComparePropertyExpression.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.search.basic.expression;

import org.apache.slide.content.NodeProperty.NamespaceCache;
import org.apache.slide.search.InvalidQueryException;
import org.apache.slide.search.basic.ComparableResource;
import org.apache.slide.search.basic.ComparableResourcesPool;
import org.apache.slide.search.basic.Literals;
import org.jdom.Element;

/**
 * Abstract base class for compare expressions (GT, EQ, is-collection ...).
 *
 * @version $Revision: 1.2 $
 */
public abstract class ComparePropertyExpression extends CompareExpression {

    protected ComparedProperty comparedProperty;


    /** the property's value <literal> */
//    protected String literal;
//
//    /** the property name <prop> */
//    protected String property;
//
//    /** the property's namespace */
//    protected String propNamespace;
//
//    /** indicate, if compare should be casesensitive */
//    protected boolean casesensitive = true;
//

    /**
     * Creates a compare expression according to Element e
     *
     * @param e                       jdom element, that describes the expression
     * @param requestedResourcesPool  the pool of resources to apply the expression to.
     * @param expectLiteral           indicates if a &lt;literal&gt; is expected.
     */
    public ComparePropertyExpression (Element e, ComparableResourcesPool requestedResourcesPool, boolean expectLiteral) throws InvalidQueryException {
        this(e, requestedResourcesPool, new ComparedProperty (e, expectLiteral));
    }
    /**
     * Creates a compare expression according to Element e
     *
     * @param e                       jdom element, that describes the expression
     * @param requestedResourcesPool  the pool of resources to apply the expression to.
     * @param    comparedProperty     the property to compare.
     */
    protected ComparePropertyExpression (Element e, ComparableResourcesPool requestedResourcesPool, ComparedProperty comparedProperty) throws InvalidQueryException {
        super (e, requestedResourcesPool);
        this.comparedProperty = comparedProperty;
    }



    /**
     * The concrete CompareExpression must overwrite this.
     *
     * @param    item    one BasicDataItem out of pool
     *
     * @return   a boolean
     *
     */
    protected abstract boolean compare (ComparableResource item);

    /**
     * String representation for debugging purposes.
     *
     * @return   this expression as String
     */
    protected String toString (String op) {
        return "(" +comparedProperty.getProperty() + " "
            + op + " " + comparedProperty.getLiteral() + ")";
    }

    /**
     * extracs the value of <literal> of an expression
     *
     * @param    e                   an Expression
     *
     * @return   the literal as string
     *
     * @throws   InvalidQueryException if no <literal> found in e
     *
     */
    protected String  getLiteral(Element e) throws InvalidQueryException {
        Element lit = e.getChild (Literals.LITERAL, NamespaceCache.DEFAULT_NAMESPACE);
        if (lit == null)
            throw new InvalidQueryException
                ("No literal element supplied");

        return lit.getText ();
    }
}

