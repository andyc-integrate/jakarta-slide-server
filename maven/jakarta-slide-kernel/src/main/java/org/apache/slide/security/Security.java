/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/security/Security.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.security;

import java.util.Enumeration;

import org.apache.slide.common.Namespace;
import org.apache.slide.common.NamespaceConfig;
import org.apache.slide.common.ServiceAccessException;
import org.apache.slide.common.SlideToken;
import org.apache.slide.structure.ActionNode;
import org.apache.slide.structure.ObjectNode;
import org.apache.slide.structure.ObjectNotFoundException;
import org.apache.slide.structure.SubjectNode;
import org.apache.slide.event.VetoException;

/**
 * Security helper.
 *
 * @version $Revision: 1.2 $
 */
public interface Security {
    
    
    // ------------------------------------------------------ Interface Methods
    
    /** initialises the security if it has been loaded via reflection
     *
     * @param namespace Namespace
     * @param namespaceConfig Namespace configuration
     **/
    void init (Namespace namespace, NamespaceConfig namespaceConfig);
    
    /**
     * Enumerates permissions on an object.
     *
     * @param token Credentials token
     * @param object Object on which permission is granted
     * @exception ServiceAccessException DataSource access error
     * @exception ObjectNotFoundException Specified object was not found
     * in the DataSource
     * @exception AccessDeniedException Insufficent credentials
     */
    Enumeration enumeratePermissions(SlideToken token, ObjectNode object)
        throws ServiceAccessException, ObjectNotFoundException,
        AccessDeniedException;
    
    /**
     * Enumerates permissions on an object.
     *
     * @param    token               a  SlideToken
     * @param    object              an ObjectNode
     * @param    includeInherited    if true, includes inherited permissions
     * @return   an Enumeration
     * @throws   ServiceAccessException
     * @throws   ObjectNotFoundException
     * @throws   AccessDeniedException
     */
    Enumeration enumeratePermissions(SlideToken token, ObjectNode object, boolean includeInherited)
        throws ServiceAccessException, ObjectNotFoundException,
        AccessDeniedException;
    
    
    /**
     * Enumerates permissions on an object.
     *
     * @param token Credentials token
     * @param object Object on which permission is granted
     * @exception ServiceAccessException DataSource access error
     * @exception ObjectNotFoundException Specified object was not found
     * in the DataSource
     * @exception AccessDeniedException Insufficent credentials
     */
    Enumeration enumeratePermissions(SlideToken token, String object)
        throws ServiceAccessException, ObjectNotFoundException,
        AccessDeniedException;
    
    /**
     * Enumerates permissions on an object.
     *
     * @param    token               a  SlideToken
     * @param    object              a  String
     * @param    includeInherited    if true, includes inherited permissions
     * @return   an Enumeration
     * @throws   ServiceAccessException
     * @throws   ObjectNotFoundException
     * @throws   AccessDeniedException
     */
    Enumeration enumeratePermissions(SlideToken token, String object, boolean includeInherited)
        throws ServiceAccessException, ObjectNotFoundException,
        AccessDeniedException;
    
    
    /**
     * Set a new set of permissions on an object.
     *
     * @param token Credentials token
     * @param object Object on which permission is granted
     * @exception ServiceAccessException DataSource access error
     * @exception ObjectNotFoundException Specified object was not found
     * in the DataSource
     * @exception AccessDeniedException Insufficent credentials
     */
    void setPermissions(SlideToken token, String object,
                        Enumeration permissions)
        throws ServiceAccessException, ObjectNotFoundException,
        AccessDeniedException;
    
    
    /**
     * Grants a new permission.
     *
     * @param token Credentials token
     * @param object Object on which permission is granted
     * @param subject The actor, which can perform a action
     * @param action Activity which the actor can perform
     * @exception ServiceAccessException DataSource access error
     * @exception ObjectNotFoundException Specified object was not found
     * in the DataSource
     * @exception AccessDeniedException Insufficent credentials
     */
    void grantPermission(SlideToken token, ObjectNode object,
                         SubjectNode subject, ActionNode action)
        throws ServiceAccessException, ObjectNotFoundException,
        AccessDeniedException, VetoException;
    
    
    /**
     * Grants a new permission.
     *
     * @param token Credentials token
     * @param permission New permission
     * @exception ServiceAccessException DataSource access error
     * @exception ObjectNotFoundException Specified object was not found
     * in the DataSource
     * @exception AccessDeniedException Insufficent credentials
     */
    void grantPermission(SlideToken token, NodePermission permission)
        throws ServiceAccessException, ObjectNotFoundException,
        AccessDeniedException, VetoException;
    
    
    /**
     * Grants a new permission.
     *
     * @param token Credentials token
     * @param object Object on which permission is granted
     * @param subject Subject who can perform the action
     * @param action Action which can be performed
     * @param inheritable Create an inheritable permission
     * @exception ServiceAccessException DataSource access error
     * @exception ObjectNotFoundException Specified object was not found
     * in the DataSource
     * @exception AccessDeniedException Insufficent credentials
     */
    void grantPermission(SlideToken token, ObjectNode object,
                         SubjectNode subject, ActionNode action,
                         boolean inheritable)
        throws ServiceAccessException, ObjectNotFoundException,
        AccessDeniedException, VetoException;
    
    
    /**
     * Deny a new permission.
     *
     * @param token Credentials token
     * @param object Object on which permission is granted
     * @param subject Thee actor which can perform the specified action
     * @param action Activity which the actor can can not perform    
     * @exception ServiceAccessException DataSource access error
     * @exception ObjectNotFoundException Specified object was not found
     * in the DataSource
     * @exception AccessDeniedException Insufficent credentials
     */
    void denyPermission(SlideToken token, ObjectNode object,
                        SubjectNode subject, ActionNode action)
        throws ServiceAccessException, ObjectNotFoundException,
        AccessDeniedException, VetoException;
    
    
    /**
     * Deny a new permission.
     *
     * @param token Credentials token
     * @param permission New permission
     * @exception ServiceAccessException DataSource access error
     * @exception ObjectNotFoundException Specified object was not found
     * in the DataSource
     * @exception AccessDeniedException Insufficent credentials
     */
    void denyPermission(SlideToken token, NodePermission permission)
        throws ServiceAccessException, ObjectNotFoundException,
        AccessDeniedException, VetoException;
    
    
    /**
     * Deny a new permission.
     *
     * @param token Credentials token
     * @param object Object on which permission is granted
     * @param subject Subject who can perform the action
     * @param action Action which can be performed
     * @param inheritable Create an inheritable permission
     * @exception ServiceAccessException DataSource access error
     * @exception ObjectNotFoundException Specified object was not found
     * in the DataSource
     * @exception AccessDeniedException Insufficent credentials
     */
    void denyPermission(SlideToken token, ObjectNode object,
                        SubjectNode subject, ActionNode action,
                        boolean inheritable)
        throws ServiceAccessException, ObjectNotFoundException,
        AccessDeniedException, VetoException;
    
    
    /**
     * Revokes a permission.
     *
     * @param token Credentials token
     * @param object Object on which permission is revoked
     * @param subject Subject who can perform the action
     * @param action Action which can be performed
     * @exception ServiceAccessException DataSource access error
     * @exception ObjectNotFoundException Specified object was not found
     * in the DataSource
     * @exception AccessDeniedException Insufficent credentials
     */
    void revokePermission(SlideToken token, ObjectNode object,
                          SubjectNode subject, ActionNode action)
        throws ServiceAccessException, ObjectNotFoundException,
        AccessDeniedException, VetoException;
    
    
    /**
     * Revokes a permission.
     *
     * @param token Credentials token
     * @param permission Permission to be removed
     * @exception ServiceAccessException DataSource access error
     * @exception ObjectNotFoundException Specified object was not found
     * in the DataSource
     * @exception AccessDeniedException Insufficent credentials
     */
    public void revokePermission(SlideToken token, NodePermission permission)
        throws ServiceAccessException, ObjectNotFoundException,
        AccessDeniedException, VetoException;
            
    
     
    
    /**
     * Check if the credentials given grants permission to perform
     * the specified action on the specified subject.
     *
     * @param token Credentials token
     * @param object Object on which the action is performed
     * @param action Action performed
     * @exception ServiceAccessException DataSource access error
     * @exception AccessDeniedException The credentials does not grant
     * the permission to perform the specified action
     */
    void checkCredentials(SlideToken token, ObjectNode object,
                          ActionNode action)
        throws ServiceAccessException, AccessDeniedException;
    
    
    /**
     * Check whether or not an actor can perform the specified activity
     * on a collection.
     *
     * @param object Object on which access is tested
     * @param subject Subject who seeks to perform the action
     * @param action Action which is to be performed
     * @exception ServiceAccessException DataSource access error
     * @exception ObjectNotFoundException Specified object was not found
     * in the DataSource
     * @exception AccessDeniedException Insufficent credentials
     * @deprecated use signature with SlideToken instead
     */
    void checkPermission(ObjectNode object, SubjectNode subject,
                         ActionNode action)
        throws ServiceAccessException, AccessDeniedException,
        ObjectNotFoundException;
    
    /**
     * Check whether or not an actor (principal) can perform the specified activity
     * on the specified resource.
     *
     * @param    token               a  SlideToken
     * @param    object              Object on which access is tested
     * @param    action              Action which is to be performed
     *
     * @throws   ServiceAccessException
     * @throws   AccessDeniedException
     * @throws   ObjectNotFoundException
     */
    void checkPermission(SlideToken token, ObjectNode object, ActionNode action)
        throws ServiceAccessException, AccessDeniedException,
        ObjectNotFoundException;
    
    /**
     * Check whether or not an actor can perform the specified activity
     * on a collection.
     *
     * @param object Object on which access is tested
     * @param subject Subject who seeks to perform the action
     * @param action Action which is to be performed
     * @return true if the action can be performed
     * @exception ServiceAccessException DataSource access error
     * @exception ObjectNotFoundException Specified object was not found
     * in the DataSource
     * @deprecated use signature with SlideToken instead
     */
    boolean hasPermission(ObjectNode object, SubjectNode subject,
                          ActionNode action)
        throws ServiceAccessException, ObjectNotFoundException;
    
    /**
     * Check whether or not an actor (principal) can perform the specified activity
     * on the specified resource.
     *
     * @param    token               a  SlideToken
     * @param    object              Object on which access is tested
     * @param    action              Action which is to be performed
     *
     * @return   true if the action can be performed
     *
     * @throws   ServiceAccessException
     * @throws   ObjectNotFoundException
     */
    boolean hasPermission(SlideToken token, ObjectNode object, ActionNode action)
        throws ServiceAccessException, ObjectNotFoundException;
    
    /**
     * Check whether or not the current user has the specified role.
     *
     * @param token Credentials token
     * @param role Role
     * @exception ServiceAccessException DataSource access error
     * @exception ObjectNotFoundException Specified object was not found
     * in the DataSource
     */
    boolean hasRole(SlideToken token, String role)
        throws ServiceAccessException, ObjectNotFoundException;
    
    
    /**
     * Check whether or not the current user has the specified role.
     *
     * @param object Object node
     * @param role Role
     * @exception ServiceAccessException DataSource access error
     * @exception ObjectNotFoundException Specified object was not found
     * in the DataSource
     */
    boolean hasRole(ObjectNode object, String role)
        throws ServiceAccessException, ObjectNotFoundException;
    
    
    /**
     * Return the list of roles the specified node has.
     *
     * @param object Object node
     */
    Enumeration getRoles(ObjectNode object);
    
    
    /**
     * Return the list of roles the specified token has.
     *
     * @param token Credentials token
     */
    Enumeration getRoles(SlideToken token)
        throws ServiceAccessException, ObjectNotFoundException;
    
    Enumeration getRoles(SlideToken token, SubjectNode subjectNode)
        throws ServiceAccessException, ObjectNotFoundException;
    
    /**
     * Get the principal associated with the credentials token.
     *
     * @param token Creadentials token
     */
    ObjectNode getPrincipal(SlideToken token)
        throws ServiceAccessException, ObjectNotFoundException;
    
    /**
     * Return true, if-and-only-if checkAction matches permAction.
     *
     * @param    slideToken          a  SlideToken
     * @param    checkAction         an ActionNode
     * @param    permAction          an ActionNode
     * @return   a boolean
     * @throws   ServiceAccessException
     */
    boolean matchAction(SlideToken slideToken, ActionNode checkAction, ActionNode permAction) throws ServiceAccessException;
    
    /**
     * Return true, if-and-only-if checkSubject matches permSubject.
     *
     * @param    token               a  SlideToken
     * @param    checkSubject        the "current" principal
     * @param    permSubject         the principal to check against (e.g. user
     *                               or group from NodePermission or NodeLock)
     *
     * @return   a boolean
     *
     * @throws   ServiceAccessException
     *
     */
    boolean matchPrincipal(SlideToken token, SubjectNode checkSubject, SubjectNode permSubject) throws ServiceAccessException;
}
