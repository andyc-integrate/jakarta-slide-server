/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/store/Store.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.store;

import org.apache.slide.common.*;

/**
 * Represents a store which is used by the Slide API to access data.
 *
 * @version $Revision: 1.2 $
 */
public interface Store extends ContentStore, LockStore, NodeStore,
    RevisionDescriptorStore, RevisionDescriptorsStore, SecurityStore, SequenceStore  {
    
    
    // ------------------------------------------------------ Interface Methods
    
    /**
     * Set the scope of the store as specified in domain.xml.
     */
    void setScope(Scope scope);
    
    
    
    /**
     * Set the name of the store as specified in domain.xml.
     */
    void setName(String name);
    
    
    
    /**
     * Return the name of the store as specified in domain.xml.
     */
    String getName();
    
    /**
     * Get parameter value for specified key
     *
     * @param    key                 an Object
     * @return   an Object
     */
    Object getParameter( Object key );
    
    /**
     * Set the node store associated with this store.
     */
    void setNodeStore(NodeStore nodeStore);
    
    
    /**
     * Set the security store associated with this store.
     */
    void setSecurityStore(SecurityStore securityStore);
    
    
    /**
     * Set the lock store associated with this store.
     */
    void setLockStore(LockStore lockStore);
    
    
    /**
     * Set the revision descriptors store associated with this store.
     */
    void setRevisionDescriptorsStore
        (RevisionDescriptorsStore revisionDescriptorsStore);
    
    
    /**
     * Set the revision descriptor store associated with this store.
     */
    void setRevisionDescriptorStore
        (RevisionDescriptorStore revisionDescriptorStore);
    
    
    /**
     * Set the content store associated with this store.
     */
    void setContentStore(ContentStore contentStore);
    
    
    /**
     * Set the descriptorIndex store associated with this store.
     */
    void setPropertiesIndexer(IndexStore contentStore);
    
    
    /**
     * Set the contentIndex store associated with this store.
     */
    void setContentIndexer(IndexStore contentStore);
    
    /**
     * Sets the sequence store associated with this store.
     */
    void setSequenceStore(SequenceStore sequenceStore);
    
    /**
     * Returns true if binding is supported an enabled for this store
     */
    boolean useBinding();

    /**
     * Acquires an exclusive access lock to a resource. This lock is transient, i.e. it will
     * automatically be released when your transaction terminates. 
     * 
     * @param uri the URI of the resource you want to have exclusive access to 
     * @throws ServiceAccessException thrown if anything goes wrong, including the lock can not be acquired 
     */
    void exclusiveTransientLock(String uri) throws ServiceAccessException;
}
