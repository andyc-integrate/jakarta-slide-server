/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/extractor/MSPowerPointExtractor.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.extractor;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.poifs.eventfilesystem.POIFSReaderListener;
import org.apache.poi.poifs.eventfilesystem.POIFSReaderEvent;
import org.apache.poi.poifs.eventfilesystem.POIFSReader;
import org.apache.poi.poifs.filesystem.DocumentInputStream;

import java.io.*;

/**
 * Author: Ryan Rhodes
 * Date: Jun 27, 2004
 * Time: 3:45:39 AM
 */
public class MSPowerPointExtractor extends AbstractContentExtractor implements POIFSReaderListener
{
    private ByteArrayOutputStream writer = new ByteArrayOutputStream();

    public MSPowerPointExtractor(String uri, String contentType, String namespace) {
        super(uri, contentType, namespace);
    }

    public Reader extract(InputStream content)  throws ExtractorException {
        try {
            POIFSReader reader = new POIFSReader();
            reader.registerListener(this);
            reader.read(content);

            return new InputStreamReader(new ByteArrayInputStream(writer.toByteArray()));
        }
        catch(Exception e) {
                throw new ExtractorException(e.getMessage());
        }
    }

    public void processPOIFSReaderEvent(POIFSReaderEvent event)
    {
        try{
            if(!event.getName().equalsIgnoreCase("PowerPoint Document"))
                return;

            DocumentInputStream input = event.getStream();

            byte[] buffer = new byte[input.available()];
            input.read(buffer, 0, input.available());

            for(int i=0; i<buffer.length-20; i++)
            {
                long type = LittleEndian.getUShort(buffer,i+2);
                long size = LittleEndian.getUInt(buffer,i+4);

                if(type==4008)
                {
                    writer.write(buffer, i + 4 + 1, (int) size +3);
                    i = i + 4 + 1 + (int) size - 1;

                }
            }
        }
        catch (Exception e)
        {

        }
    }

    public static void main(String[] args) throws Exception
    {
        FileInputStream in = new FileInputStream(args[0]);

        MSPowerPointExtractor ex = new MSPowerPointExtractor(null, null, null);

        Reader reader = ex.extract(in);

        int c;
        do
        {
            c = reader.read();

            System.out.print((char)c);
        }
        while( c != -1 );
    }
}