/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/util/logger/SimpleLogger.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.util.logger;

import java.util.Date;
import java.util.Locale;
import java.text.SimpleDateFormat;

/**
 * Simple logger implementation.
 *
 */
public class SimpleLogger implements Logger {
    
    
    // ----------------------------------------------------- Instance Variables
    
    
    /**
     * Logging level of the logger.
     */
    protected int loggerLevel = 0;
    
    
    /**
     * Date / Time format.
     */
    protected SimpleDateFormat dateFormat =
        new SimpleDateFormat("dd MMM yyyy HH:mm:ss", Locale.US);
    
    
    /**
     * Text values for logging priorities.
     */
    protected String[] loggingLevels = { "EMERGENCY", "CRITICAL", "ERROR",
                                         "", "WARNING", "", "INFO", "DEBUG" };
    
    
    // ------------------------------------------------------------- Properties
    
    
    /**
     * Logger level setter.
     *
     * @param loggerLevel New logger level
     */
    public void setLoggerLevel(String channel, int loggerLevel) {
        this.loggerLevel = loggerLevel;
    }
    
    
    /**
     * Logger level setter.
     *
     * @param loggerLevel New logger level
     */
    public void setLoggerLevel(int loggerLevel) {
        setLoggerLevel(DEFAULT_CHANNEL, loggerLevel);
    }
    
    
    /**
     * Logger level getter.
     *
     * @return int logger level
     */
    public int getLoggerLevel() {
        return getLoggerLevel(DEFAULT_CHANNEL);
    }
    
    
    
    /**
     * Logger level getter.
     *
     * @return int logger level
     */
    public int getLoggerLevel(String channel) {
        return loggerLevel;
    }
    
    
    /**
     * Date format setter.
     *
     * @param pattern Format pattern
     */
    public void setDateFormat(String pattern) {
        dateFormat = new SimpleDateFormat(pattern);
    }
    
    
    // --------------------------------------------------------- Logger Methods
    
    
    /**
     * Log an object and an associated throwable thru the specified channel and with the specified level.
     *
     * @param data object to log
     * @param throwable throwable to be logged
     * @param channel channel name used for logging
     * @param level level used for logging
     */
    public void log(Object data, Throwable throwable, String channel, int level) {
        if (isEnabled(channel, level)) {
            String levelValue = "";
            if (channel.equals(DEFAULT_CHANNEL))
                channel = "";
            else
                channel = channel + " - ";
            if ((level >= 0) && (level < loggingLevels.length))
                levelValue = loggingLevels[level];
            if (dateFormat == null) {
                System.out.println(System.currentTimeMillis() + " - "
                                   + channel + levelValue + " - "
                                   + data);
            } else {
                System.out.println(dateFormat.format(new Date()) + " - "
                                   + channel + levelValue + " - "
                                   + data);
            }
            throwable.printStackTrace();
        }
    }

    
    /**
     * Log an object thru the specified channel and with the specified level.
     *
     * @param data The object to log.
     * @param channel The channel name used for logging.
     * @param level The level used for logging.
     */
    public void log(Object data, String channel, int level) {
        if (isEnabled(channel, level)) {
            String levelValue = "";
            if (channel.equals(DEFAULT_CHANNEL))
                channel = "";
            else
                channel = channel + " - ";
            if ((level >= 0) && (level < loggingLevels.length))
                levelValue = loggingLevels[level];
            if (dateFormat == null) {
                System.out.println(System.currentTimeMillis() + " - "
                                   + channel + levelValue + " - "
                                   + data);
            } else {
                System.out.println(dateFormat.format(new Date()) + " - "
                                   + channel + levelValue + " - "
                                   + data);
            }
            if (data instanceof Throwable)
                ((Throwable) data).printStackTrace();
        }
    }
    
    
    /**
     * Log an object with the specified level.
     *
     * @param data The object to log.
     * @param level The level used for logging.
     */
    public void log(Object data, int level) {
        this.log(data, DEFAULT_CHANNEL, level);
    }
    
    
    /**
     * Log an object.
     *
     * @param data The object to log.
     */
    public void log(Object data) {
        this.log(data, DEFAULT_CHANNEL, Logger.DEBUG);
    }
    
            
    
        
    
    /**
     * Check if the channel with the specified level is enabled for logging.
     * This implementation ignores the channel specification
     *
     * @param channel The channel specification
     * @param level   The level specification
     */

    public boolean isEnabled(String channel, int level) {
        return getLoggerLevel() >= level;
    }
            
        
    
    /**
     * Check if the default channel with the specified level is enabled for logging.
     *
     * @param level   The level specification
     */

    public boolean isEnabled(int level) {
        return isEnabled(DEFAULT_CHANNEL, level);
    }


}
