/*****************************************************************************
 * Copyright (C) The Apache Software Foundation. All rights reserved.        *
 * ------------------------------------------------------------------------- *
 * This software is published under the terms of the Apache Software License *
 * version 1.1, a copy of which has been included  with this distribution in *
 * the LICENSE file.                                                         *
 *****************************************************************************/
 
package org.apache.slide.util.conf;

import java.util.*;

/**
 */
public class Element {

    private String name;
    private Hashtable attributes;
    private Vector children;
    private String data;
    private String comment;
    private Element parent;
    
    public Element getParent() {
        return parent;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String s) {
        name = new String(s);
    }

    public boolean hasAttributes() {
        return !attributes.isEmpty();
    }

    public Enumeration getAttributeNames() {
        return attributes.keys();
    }

    public String getAttributeValue(String s) {
        return (String) attributes.get(s);
    }

    public void setAttribute(String s, String s1) {
        attributes.put(s, s1);
    }

    public void removeAttribute(String s) {
        attributes.remove(s);
    }

    public void clearAttributes() {
        attributes.clear();
    }

    public Enumeration getChildren() {
        return children.elements();
    }

    public Enumeration getChildren(String s) {
        Vector vector = new Vector();
        for(Enumeration enumeration = getChildren(); enumeration.hasMoreElements();) {
            Element element = (Element) enumeration.nextElement();
            if(element.getName().equals(s))
                vector.addElement(element);
        }
        return vector.elements();
    }

    public Element getChild(String name) {
        Enumeration enumeration = getChildren(name);
        if (!enumeration.hasMoreElements()) {
            return null;
        }
        return (Element) enumeration.nextElement();
    }

    public boolean hasChildren() {
        return !children.isEmpty();
    }

    public int countChildren() {
        return children.size();
    }

    public void addChild(Element element)
    throws NullPointerException {
        if(element == null) {
            throw new NullPointerException("Child cannot be null");
        }
        children.addElement(element);
    }

    public void clearChildren() {
        children.removeAllElements();
    }

    public String getData() {
        return data;
    }

    public void setData(Object s) {
        data = new String(s.toString()).trim();
    }

    public void clearData() {
        data = new String();
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String s) {
        comment = new String(s);
    }

    public void clearComment() {
        comment = new String();
    }

    public Element() {
        this("", null);
    }

    public Element(String s, Element parent) {
        this.parent = parent;
        name = new String(s);
        attributes = new Hashtable();
        children = new Vector();
        data = new String();
        comment = new String();
    }
    
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("Name=" + name);
        buffer.append(" Data=" + data);
        for (Enumeration e = getChildren(); e.hasMoreElements(); ) {
            Element el = (Element) e.nextElement();
            buffer.append("  " + el.toString());
        }
        return buffer.toString();
    }
}
