/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/extractor/PropertyExtractorTrigger.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.extractor;

import java.io.ByteArrayInputStream;
import java.util.Iterator;
import java.util.Map;

import org.apache.slide.common.PropertyName;
import org.apache.slide.content.NodeRevisionContent;
import org.apache.slide.content.NodeRevisionDescriptor;
import org.apache.slide.content.NodeRevisionDescriptors;
import org.apache.slide.event.ContentAdapter;
import org.apache.slide.event.ContentEvent;
import org.apache.slide.event.VetoException;

/**
 * @version $Revision: 1.2 $
 */
public class PropertyExtractorTrigger extends ContentAdapter {
    public void create(ContentEvent event) throws VetoException {
        store(event);
    }

    public void store(ContentEvent event) throws VetoException {
        try {
            String namespaceName = event.getNamespace() == null ? null : event.getNamespace().getName();
            NodeRevisionDescriptors descriptors = event.getRevisionDescriptors();
            NodeRevisionDescriptor descriptor = event.getRevisionDescriptor();
            NodeRevisionContent content = event.getRevisionContent();
            if ( content != null && descriptor != null ) {
                PropertyExtractor[] extractor = ExtractorManager.getInstance().getPropertyExtractors(namespaceName, descriptors, descriptor);
                for ( int i = 0; i < extractor.length; i++ ) {
                    Map extractedProperties = extractor[i].extract(new ByteArrayInputStream(content.getContentBytes()));
                    for ( Iterator j = extractedProperties.entrySet().iterator(); j.hasNext(); ) {
                        Map.Entry entry = (Map.Entry) j.next();
                        final Object key = entry.getKey();
                        if (key instanceof PropertyName) {
                            final String name = ((PropertyName) key).getName();
                            final String namespace = ((PropertyName) key).getNamespace();
                            descriptor.setProperty(name, namespace, entry.getValue());
                        }
                        else {
                            descriptor.setProperty((String) entry.getKey(), entry.getValue());
                        }
                    }
                }
            }
        } catch ( ExtractorException e ) {
            throw new VetoException(e.getMessage());
        }
    }
}