/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/basic/QueryTree.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.search.basic;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.slide.common.Scope;
import org.apache.slide.search.InvalidScopeException;

/**
 * Represents the scopes for all stores, that are within the scope of one query.
 *
 * @version $Revision: 1.2 $
 *
 **/
public class QueryTree {
    
    private TokenizedScope topNode;
    
    private List allQueryTreeNodes = new ArrayList ();
    
    
    //    QueryTree (Enumeration stores, Scope scope)
    //        throws InvalidScopeException
    //    {
    //        this (stores, scope, new Scope [0]);
    //    }
    
    /**
     * Constructs a query tree
     *
     * @param stores  all stores, that are within this namespace
     * @param scope   the scope of this query
     * @param excluded a list of scopes, that shall be excluded from search
     */
    QueryTree (Enumeration stores, Scope scope, Scope [] excluded)
        throws InvalidScopeException
    {
        // System.out.println (scope);
        topNode = new TokenizedScope (scope);
        
        while (stores.hasMoreElements()) {
            Scope configuredStore = (Scope)stores.nextElement();
            TokenizedScope tConfStore = new TokenizedScope (configuredStore);
            if (tConfStore.isChildOf (topNode) && tConfStore.isNotExcluded (excluded) ) {
                allQueryTreeNodes.add (configuredStore);
            }
        }
        allQueryTreeNodes.add (scope);
        
    }
    
    /**
     * Checks, if the indicated scope has children within this QueryTree.
     *
     * @param    scopeToBeChecked    the Scope to be checked
     *
     * @return   a boolean
     *
     */
    public boolean hasChildren (Scope scopeToBeChecked) {
        boolean result = false;
        TokenizedScope tScopeToBeChecked = new TokenizedScope (scopeToBeChecked);
        Iterator it = allQueryTreeNodes.iterator();
        while (it.hasNext()) {
            TokenizedScope ts = new TokenizedScope ((Scope)it.next());
            if (ts.isChildOf (tScopeToBeChecked))
                return true;
        }
        
        return false;
    }
    
    /**
     * calculates the depth of scope within this QueryTree (relative to the
     * topLevel of tree)
     *
     * @param    scope               a  Scope
     *
     * @return   an int
     *
     */
    public int relativeDepth (Scope scope) {
        TokenizedScope tScope = new TokenizedScope (scope);
        boolean contains = allQueryTreeNodes.contains (tScope);
        
        return tScope.depth - topNode.depth;
    }
    
    
    /**
     * Retrieves an iterator of all scopes in this Tree
     *
     * @return   an Iterator of Scope objects
     *
     */
    public Iterator iterator () {
        return allQueryTreeNodes.iterator();
    }
    
    
    /**
     * Helper class to handle the scopes
     *
     * @version $Revision: 1.2 $
     *
     **/
    class TokenizedScope {
        private int depth;
        Scope scope;
        
        TokenizedScope (Scope   scope) {
            this.scope = scope;
            StringTokenizer st = new StringTokenizer (scope.toString(), "/");
            int noOfTokens = st.countTokens();
            depth = noOfTokens;
        }
        
        /**
         * Method equals
         *
         * @param    o                   an Object
         *
         * @return   a boolean
         *
         */
        public boolean equals (Object o) {
            return scope.equals(o);
        }
        
        /**
         * Method toString
         *
         * @return   a String
         *
         */
        public String toString () {
            return scope.toString();
        }
        
        /**
         * Method isChildOf
         *
         * @param    tScope              a  TokenizedScope
         *
         * @return   a boolean
         *
         */
        public boolean isChildOf (TokenizedScope tScope) {
            if (depth > tScope.depth && toString().startsWith(tScope.toString())) {
                return true;
            }
            else {
                return false;
            }
        }
        
        /**
         * Method isNotExcluded
         *
         * @param    excluded            a  Scope[]
         *
         * @return   a boolean
         *
         */
        public boolean isNotExcluded (Scope []excluded) {
            for (int i = 0; i < excluded.length; i++) {
                if (this.toString().startsWith(excluded [i].toString())) {
                    return false;
                }
            }
            return true;
        }
    }
}


