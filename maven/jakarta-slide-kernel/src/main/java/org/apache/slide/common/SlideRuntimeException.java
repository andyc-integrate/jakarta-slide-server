/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/common/SlideRuntimeException.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */ 

package org.apache.slide.common;

/**
 * Runtime Exception supertype for all Slide components.
 * 
 * @version $Revision: 1.2 $
 */
public class SlideRuntimeException extends RuntimeException {
    
    
    // ----------------------------------------------------------- Constructors
    
    
    /**
     * SlideRuntimeException with logging.
     * 
     * @param message Exception message
     */
    public SlideRuntimeException(String message) {
        this(message, true);
    }
    
    
    /**
     * SlideRuntimeException with or without logging.
     * 
     * @param message Exception message
     * @param showTrace Defines whether or not something is logged
     */
    public SlideRuntimeException(String message, boolean showTrace) {
        super(message);
        if (showTrace) {
            Domain.warn(message);
        }
    }
    
}
