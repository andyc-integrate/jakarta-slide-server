/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/store/RevisionDescriptorsStore.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */ 

package org.apache.slide.store;

import org.apache.slide.common.Service;
import org.apache.slide.common.ServiceAccessException;
import org.apache.slide.common.Uri;
import org.apache.slide.content.NodeRevisionDescriptors;
import org.apache.slide.content.RevisionDescriptorNotFoundException;

/**
 * Store for RevisionDescriptors objects.
 * 
 * @version $Revision: 1.2 $
 */
public interface RevisionDescriptorsStore extends Service {
    
    
    // ------------------------------------------------------ Interface Methods
    
    
    /**
     * Retrieve the revisions informations of an object.
     * 
     * @param uri Uri
     * @exception ServiceAccessException Service access error
     * @exception RevisionDescriptorNotFoundException Revision descriptor 
     * was not found
     */
    NodeRevisionDescriptors retrieveRevisionDescriptors(Uri uri)
        throws ServiceAccessException, RevisionDescriptorNotFoundException;
    
    
    /**
     * Create a new revision information object.
     * 
     * @param uri Uri
     * @param revisionDescriptors Node revision descriptors
     * @exception ServiceAccessException Service access error
     */
    void createRevisionDescriptors(Uri uri, 
                                   NodeRevisionDescriptors revisionDescriptors)
        throws ServiceAccessException;
    
    
    /**
     * Update revision information.
     * 
     * @param uri Uri
     * @param revisionDescriptors Node revision descriptors
     * @exception ServiceAccessException Service access error
     * @exception RevisionDescriptorNotFoundException Revision descriptor 
     * was not found
     */
    void storeRevisionDescriptors(Uri uri, 
                                  NodeRevisionDescriptors revisionDescriptors)
        throws ServiceAccessException, RevisionDescriptorNotFoundException;
    
    
    /**
     * Remove revision information.
     * 
     * @param uri Uri
     * @exception ServiceAccessException Service access error
     */
    void removeRevisionDescriptors(Uri uri)
        throws ServiceAccessException;
    
    
}
