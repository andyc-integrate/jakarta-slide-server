/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/common/PropertyName.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.common;

// import list
import org.apache.slide.content.NodeProperty;

/**
 * This class is a container for the name and namespace of a property.
 *
 * @version $Revision: 1.2 $
 *
 **/
public class PropertyName {
    
    /**
     * The name of the Property.
     */
    protected String name = null;
    
    /**
     * The namespace of the Property.
     */
    protected String namespace = null;
    
    
    /**
     * Creates a PropertyName within the {@link NodeProperty#DEFAULT_NAMESPACE
     * default namespace}.
     *
     * @param      name       the name of the Property.
     */
    public PropertyName(String name) {
        this(name, NodeProperty.DEFAULT_NAMESPACE);
    }
    
    /**
     * Creates a PropertyName.
     *
     * @param      name       the name of the Property.
     * @param      namespace  the namespace of the Property.
     */
    public PropertyName(String name, String namespace) {
        this.name = name;
        this.namespace = namespace;
    }
    
    /**
     * Returns the name of the property.
     *
     * @return     the name of the property.
     */
    public String getName() {
        return name;
    }
    
    /**
     * Returns the namespace of the property.
     *
     * @return     the namespace of the property.
     */
    public String getNamespace() {
        return namespace;
    }
    
    /**
     * Returns <code>true</code> if <code>other</code> is a PropertyName
     * and the name and namespace are equal to this intance' name and namespace.
     *
     * @param      other  the Object to test for equality.
     *
     * @return     <code>true</code> if the object is equal to this one.
     */
    public boolean equals(Object other) {
        
        boolean equal = false;
        if (other instanceof PropertyName) {
            PropertyName otherPropertyName = (PropertyName)other;
            if (getName() == null) {
                equal = (otherPropertyName.getName() == null);
            }
            else {
                equal = getName().equals(otherPropertyName.getName());
            }
            if (getNamespace() == null) {
                equal &= (otherPropertyName.getNamespace() == null);
            }
            else {
                equal &= getNamespace().equals(otherPropertyName.getNamespace());
            }
        }
        return equal;
    }
    
    /**
     * Returns the hash code of this instance. Due to definition equal objects
     * must have the same hash code.
     *
     * @return     the hash code of this instance.
     */
    public int hashCode() {
        
        int hash = 0;
        if (getName() != null) {
            hash = getName().hashCode();
        }
        if (getNamespace() != null) {
            hash += 13 * getNamespace().hashCode();
        }
        return hash;
    }
    
    /**
     * Returns a String representation of the PropertyName.
     *
     * @return     a String representation of the PropertyName.
     */
    public String toString() {
        if (getNamespace() == null) {
            return getName();
        }
        else {
            StringBuffer buffer = new StringBuffer(getNamespace());
            buffer.append(":");
            if (getName() != null) {
                buffer.append(getName());
            }
            return buffer.toString();
        }
    }
}

