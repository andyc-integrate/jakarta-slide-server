/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/basic/expression/BasicExpression.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.search.basic.expression;

import java.util.List;
import org.apache.slide.search.InvalidQueryException;
import org.apache.slide.search.basic.BasicExpressionFactory;
import org.apache.slide.search.basic.BasicResultSetImpl;
import org.apache.slide.search.basic.IBasicExpression;
import org.apache.slide.search.basic.IBasicExpressionFactory;
import org.apache.slide.search.basic.IBasicResultSet;
import org.jdom.Attribute;
import org.jdom.Element;

/**
 * A BasicSearchExpression represents exactly one operator (AND, GT, ...).
 *
 * @version $Revision: 1.2 $
 */
public abstract class BasicExpression implements IBasicExpression {

    protected BasicExpressionFactory expressionFactory;

    /** the JDOM element representing this expression. */
    protected Element   expressionElement;

    /** the resultset of this expression, if it has the state resolved */
    protected IBasicResultSet resultSet = new BasicResultSetImpl();

    /** backptr to the factory that created this expression */
    private IBasicExpressionFactory factory;


    /**
     * constructor. Only called by the conrecte expressions
     *
     * @param e                       the jdom element representing this expression.
     */
    protected BasicExpression (Element e) throws InvalidQueryException {
        this.expressionElement = e;
        List attrList = e.getAttributes();
        if (attrList.size() != 0) {
            Attribute attr = (Attribute) attrList.get(0);
            throw new InvalidQueryException (attr.getQualifiedName() +
                                                 " is an unprocessable entity");
        }
    }

    /**
     * writes backptr to the factory
     *
     * @param    factory             an IBasicExpressionFactory
     *
     */
    public void setFactory (IBasicExpressionFactory factory) {
        this.factory = factory;
    }

    /**
     * factory accessor
     *
     * @return   an IBasicExpressionFactory
     *
     */
    public IBasicExpressionFactory getFactory() {
        return this.factory;
    }

}

