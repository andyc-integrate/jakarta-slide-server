/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/common/SlideToken.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.common;

import java.util.Enumeration;
import java.util.List;
import org.apache.slide.authenticate.CredentialsToken;
import org.apache.slide.store.ResourceId;
import org.apache.slide.structure.ActionNode;
import org.apache.slide.structure.ObjectNode;
import org.apache.slide.structure.SubjectNode;

/**
 * The SlideToken interface identifies the current acting principal and its
 * credentials, and maintains information about the state of the user
 * transaction.
 *
 * <p>
 *   An object implementing this interface is required to use any of the
 *   methods provided by the helpers API. The default implementation of this
 *   interface is {@link SlideTokenImpl SlideTokenImpl}. In the context of a
 *   Java servlet, such an object should be instantiated like this:
 *   <pre>
 *   Principal principal = request.getUserPrincipal();
 *   CredentialsToken credentials;
 *   if (principal == null) {
 *       credentials = new CredentialsToken("");
 *   } else {
 *       credentials = new CredentialsToken(principal);
 *   }
 *   SlideToken token = new SlideTokenImpl(credentials);
 *   </pre>
 * </p>
 *
 * @version $Revision: 1.2 $
 */
public interface SlideToken {
    
    
    
    
    // ------------------------------------------------------------- Properties
    
    
    /**
     * Returns the credentials token.
     *
     * @return String
     */
    public CredentialsToken getCredentialsToken();
    
    
    /**
     * Credentials token mutator.
     */
    public void setCredentialsToken(CredentialsToken credentialsToken);
    
    
    /**
     * Returns the CacheInfo token.
     *
     * @return CacheInfoToken
     */
    public CacheInfoToken getCacheInfoToken() ;
    
    
    /**
     * CacheInfo token mutator.
     */
    public void setCacheInfoToken(CacheInfoToken cacheInfoToken) ;
    
    
    /**
     * Use lock tokens in lock resolution ?
     *
     * @return boolean
     */
    public boolean isEnforceLockTokens() ;
    
    
    /**
     * Enforce lock tokens flag mutator.
     *
     * @param enforceLockTokens New flag value
     */
    public void setEnforceLockTokens(boolean enforceLockTokens) ;
    
    
    /**
     * Force store enlistment flag accessor. If true, that will cause Slide to
     * enlist the store in the current transaction for all operations,
     * to be able to prevent dirty reads when doing complex updates.
     *
     * @return boolean
     */
    public boolean isForceStoreEnlistment() ;
    
    /**
     * Force store enlistment flag mutator. If set to true, that will cause
     * Slide to enlist the store in the current transaction for all
     * operations, to be able to prevent dirty reads when doing complex
     * updates. That value should be set to true only in some very specific
     * critical sections of the code, as this would greatly decrease the
     * ability of Slide to handle multiple concurrent requests.
     *
     * @param forceStoreEnlistment New flag value
     */
    public void setForceStoreEnlistment(boolean forceStoreEnlistment) ;
    
    
    /**
     * Add a new lock token to the lock token list.
     *
     * @param lockId Lock token to add
     */
    public void addLockToken(String lockId) ;
    
    
    /**
     * Removes a lock token from the lock token list.
     *
     * @param lockId Lock token to remove
     */
    public void removeLockToken(String lockId) ;
    
    
    /**
     * Clears the lock token list.
     */
    public void clearLockTokens() ;
    
    /**
     * For debugging purposes ONLY
     */
    public List showLockTokens();
    
    /**
     * Checks if the given lock token is present.
     *
     * @param lockToken Lock token to check
     * @return boolean True if the given lock token is present
     */
    public boolean checkLockToken(String lockToken) ;
    
    
    /**
     * Add a new parameter to the parameter list.
     *
     * @param parameterName Parameter to add
     * @param parameterValue Parameter value
     */
    public void addParameter(String parameterName, Object parameterValue) ;
    
    
    /**
     * Removes a parameter from the parameter list.
     *
     * @param parameterName Parameter to remove
     */
    public void removeParameter(String parameterName) ;
    
    
    /**
     * Clears the parameter list.
     */
    public void clearParameters() ;
    
    
    /**
     * Return parameter list.
     */
    public Enumeration getParameterNames() ;
    
    /**
     * Returns a parameter given by name.
     * @return the Object associated with <code>name</code> or 
     * <code>null</code> if no such parameter exists.
     */
    public Object getParameter(String name);
    
    /**
     * allows to cache the result of a permission check
     */
    public void cachePermission(ObjectNode object, ActionNode action, boolean permission);
    
    /**
     * checks if the permission cache contains an entry for the ObjectNode and
     * ActionNode combination.
     *
     * @return true if granted, false if denied, null if nothing in the cache.
     */
    public Boolean checkPermissionCache(ObjectNode object, ActionNode action);
    
    
    /**
     * Force security check. If false, checkCredentials of SecurityImpl will
     * return immediately.
     *
     * @return   a boolean
     */
    public boolean isForceSecurity();
    
    public void setForceSecurity(boolean forceSecurity);
    
    /**
     * allows to cache the result of a lock check
     */
    public void cacheLock(ObjectNode object, ActionNode action, boolean lock);
    
    /**
     * checks if the lock cache contains an entry for the ObjectNode and
     * ActionNode combination.
     *
     * @return true if locked, false otherwise
     */
    public Boolean checkLockCache(ObjectNode object, ActionNode action);
    
    /**
     * Allows to cache the result of a resolve operation
     */
    public void cacheResolve(Uri uri, ResourceId resourceId);
    
    /**
     * Allows to cache the result of a matchPrincipal operation
     */
    public void cacheMatchPrincipal(SubjectNode checkSubject, SubjectNode matchSubject, boolean match);
    
    /**
     * Checks if the matchPrincipal cache
     * @return the cached Boolean or null
     */
    public Boolean checkMatchPrincipalCache(SubjectNode checkSubject, SubjectNode matchSubject);
    
    /**
     * Checks if the resolve cache contains an entry for the specified uri.
     * @return the cached resourceId or null
     */
    public ResourceId checkResolveCache(Uri uri);
        
    /**
     * Force lock check. If false, checkLock of LockImpl will
     * return immediately.
     *
     * @return   a boolean
     */
    public boolean isForceLock();
    
    public void setForceLock(boolean forceLock);
    
    /**
     * Checks if this request is part of an externally controlled transaction. 
     */
    public boolean isExternalTransaction();

    /**
     * Sets if this request is part of an externally controlled transaction. 
     */
    public void setExternalTx();
}

