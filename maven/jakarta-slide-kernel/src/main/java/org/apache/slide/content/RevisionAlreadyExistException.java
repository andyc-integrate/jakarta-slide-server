/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/content/RevisionAlreadyExistException.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */ 

package org.apache.slide.content;

import org.apache.slide.util.Messages;

/**
 * Revision already exist.
 * 
 * @version $Revision: 1.2 $
 */
public class RevisionAlreadyExistException extends ContentException {
    
    
    // ----------------------------------------------------------- Constructors
    
    
    /**
     * Constructor.
     * 
     * @param objectUri Uri of the object
     * @param number Revision number 
     */
    public RevisionAlreadyExistException(String objectUri, 
                                         NodeRevisionNumber number) {
        super(Messages.format(RevisionAlreadyExistException.class.getName(), 
                              objectUri, number));
        this.objectUri = objectUri;
        this.number = number;
    }
    
    
    // ----------------------------------------------------- Instance Variables
    
    
    /**
     * Object Uri.
     */
    private String objectUri;
    
    
    /**
     * Revision number.
     */
    private NodeRevisionNumber number;
    
    
    // ------------------------------------------------------------- Properties
    
    
    /**
     * Object Uri accessor.
     * 
     * @return String object Uri
     */
    public String getObjectUri() {
        if (objectUri == null) {
            return new String();
        } else {
            return objectUri;
        }
    }
    
    
    /**
     * Revision number accessor.
     * 
     * @return NodeRevisionNumber
     */
    public NodeRevisionNumber getNumber() {
        return number;
    }
    
}
