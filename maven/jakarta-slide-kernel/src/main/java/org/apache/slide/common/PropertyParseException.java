/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/common/PropertyParseException.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.common;

// import list

/**
 * This exception is thrown if parsing a Property fails for any reason.
 *
 * @version $Revision: 1.2 $
 *
 **/
public class PropertyParseException extends SlideException {
    
    /**
     * The exception that was the cause of this exception
     * (may be <code>null</code>)
     */
    protected Throwable cause = null;
    
    /**
     * Creates a PropertyParseException.
     *
     * @param      message  the message of this exception.
     */
    public PropertyParseException(String message) {
        this(message, null);
    }
    
    /**
     * Creates a PropertyParseException.
     *
     * @param      message  the message of this exception.
     * @param      cause    the exception that was the cause of this exception.
     */
    public PropertyParseException(String message, Throwable cause) {
        super(message);
        this.cause = cause;
    }
    
    /**
     * Returns the exception that was the cause of this exception
     * (may be <code>null</code>).
     *
     * @return     the exception that was the cause of this exception.
     */
    public Throwable getCause() {
        return cause;
    }
    
}

