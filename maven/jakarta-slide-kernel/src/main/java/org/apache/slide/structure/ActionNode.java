/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/structure/ActionNode.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.structure;

import java.util.Vector;
import org.apache.slide.content.NodeProperty.NamespaceCache;
import org.jdom.Namespace;

/**
 * Action node class. The namespace is used to distinguish actions with the
 * same simple name.
 *
 * Although this class has a <code>namespace</code> attribute the equality of
 * two <code>ActionNode</code>s can still be determined by comparing their
 * URIs. Therefore this class does not override <code>equals(Object)</code>
 * from <code>ObjectNode</code>.
 *
 * @version $Revision: 1.2 $
 */
public class ActionNode extends ObjectNode {

    /** generic actions */
    public static final String DEFAULT_URI = "default";
    public static final String ALL_URI = "all";

    public static final ActionNode DEFAULT = new ActionNode(DEFAULT_URI, NamespaceCache.DEFAULT_NAMESPACE);
    public static final ActionNode ALL = new ActionNode(ALL_URI, NamespaceCache.DEFAULT_NAMESPACE);

    /**
     * The namespace of the action.
     */
    private Namespace namespace;

    // ----------------------------------------------------------- Constructors


    /**
     * Constructor.
     */
    public ActionNode() {
        super();
    }

    /**
     * Default constructor.
     */
    public ActionNode(String uri) {
        super(uri);
    }

    /**
     * Default constructor.
     */
    public ActionNode(String uri, Vector children, Vector links) {
        super(uri, children, links);
    }

    public ActionNode(String uuri, Vector bindings, Vector parentBindings, Vector links) {
        super(uuri, bindings, parentBindings, links);
    }

    /**
     * Create an <code>ActionNode</code> with a namespace extracted from the
     * <code>privilege-namespace</code> property.
     *
     * @param uri The Slide-internal URI of the ActionNode.
     * @param namespace The namespace of the action.
     */
    public ActionNode(String uri, Namespace namespace) {
        super(uri);
        this.namespace = namespace;
    }

    /**
     * Create an <code>ActionNode</code> without a namespace. If retrieval of
     * the namespace is attempted on the result an exception will be thrown.
     *
     * This method is not required to return a unique instance each time it is
     * invoked.
     *
     * @param actionUri The URI which uniquely identifies the <code>ActionNode</code>.
     * @return An <code>ActionNode</code> without a namespace.
     */
    public static ActionNode getActionNode(String actionUri) {
        if (ActionNode.ALL_URI.equals(actionUri)) {
            return ActionNode.ALL;
        }
        return new ActionNode(actionUri);
    }

    /**
     * Create an ActionNode with a namespace.
     *
     * This method is not required to return a unique instance each time it is
     * invoked.
     *
     * @param actionUri The URI which uniquely identifies the <code>ActionNode</code>.
     * @param namespace The namespace of the <code>ActionNode</code>.
     * @return An <code>ActionNode</code> with a namespace.
     */
    public static ActionNode getActionNode(String actionUri, Namespace namespace) {
        if (ActionNode.ALL_URI.equals(actionUri) && NamespaceCache.DEFAULT_NAMESPACE.equals(namespace)) {
            return ActionNode.ALL;
        }
        return new ActionNode(actionUri, namespace);
    }

    /**
     * Get the namespace. If the <code>ActionNode</code> was constructed
     * without a namespace, an exception will be thrown.
     *
     * @return The namespace of the <code>ActionNode</code>.
     * @throws IllegalStateException The <code>ActionNode</code> was
     *                               constructed without a namespace.
     */
    public Namespace getNamespace() {
        if (this.namespace == null) {
            throw new IllegalStateException("Namespace retrieved without being specified");
        }
        return this.namespace;
    }
}
