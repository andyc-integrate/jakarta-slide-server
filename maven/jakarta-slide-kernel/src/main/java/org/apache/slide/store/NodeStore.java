/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/store/NodeStore.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */ 

package org.apache.slide.store;

import org.apache.slide.common.Service;
import org.apache.slide.common.ServiceAccessException;
import org.apache.slide.common.Uri;
import org.apache.slide.structure.ObjectAlreadyExistsException;
import org.apache.slide.structure.ObjectNode;
import org.apache.slide.structure.ObjectNotFoundException;

/**
 * Store for Node objects.
 * 
 * @version $Revision: 1.2 $
 */
public interface NodeStore extends Service {
    
    
    // ------------------------------------------------------ Interface Methods
    
    
    /**
     * Retrive an object.
     * 
     * @param uri Uri of the object we want to retrieve
     * @exception ServiceAccessException Error accessing the Data Source
     * @exception ObjectNotFoundException The object to retrieve was not found
     */
    ObjectNode retrieveObject(Uri uri)
        throws ServiceAccessException, ObjectNotFoundException;
    
    
    /**
     * Update an object.
     * 
     * @param object Object to update
     * @exception ServiceAccessException Error accessing the Data Source
     * @exception ObjectNotFoundException The object to update was not found
     */
    void storeObject(Uri uri, ObjectNode object)
        throws ServiceAccessException, ObjectNotFoundException;
    
    
    /**
     * Create a new object.
     * 
     * @param object ObjectNode
     * @param uri Uri of the object we want to create
     * @exception ServiceAccessException Error accessing the Data Source
     * @exception ObjectAlreadyExistsException An object already exists 
     * at this Uri
     */
    void createObject(Uri uri, ObjectNode object)
        throws ServiceAccessException, ObjectAlreadyExistsException;
    
    
    /**
     * Remove an object.
     * 
     * @param object Object to remove
     * @exception ServiceAccessException Error accessing the Data Source
     * @exception ObjectNotFoundException The object to remove was not found
     */
    void removeObject(Uri uri, ObjectNode object)
        throws ServiceAccessException, ObjectNotFoundException;
    
    
}
