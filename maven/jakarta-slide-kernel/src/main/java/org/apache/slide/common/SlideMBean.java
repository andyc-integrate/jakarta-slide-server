/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/common/SlideMBean.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */ 

package org.apache.slide.common;

import org.apache.slide.authenticate.SecurityToken;

/**
 * Slide MBean interface.
 * 
 * @version $Revision: 1.2 $
 */
public interface SlideMBean {
    
    
    // -------------------------------------------------------------- Constants
    
    
    /**
     * Status constants.
     */
    public static final String[] states = 
    {"Stopped", "Stopping", "Starting", "Started"};
    
    
    public static final int STOPPED  = 0;
    public static final int STOPPING = 1;
    public static final int STARTING = 2;
    public static final int STARTED  = 3;
    
    
    /**
     * Component name.
     */
    public static final String NAME = "Slide content management server";
    
    
    /**
     * Object name.
     */
    public static final String OBJECT_NAME = ":service=Slide";
    
    
    // ------------------------------------------------------ Interface Methods
    
    
    /**
     * Retruns the Slide component name.
     */
    public String getName();
    
    
    /**
     * Returns the state of the Slide domain.
     */
    public int getState();
    
    
    /**
     * Returns a String representation of the domain's state.
     */
    public String getStateString();
    
    
    /**
     * Auto initializes domain.
     */
    public void init()
        throws Exception;
    
    
    /**
     * Initializes domain, and specify a configuration file to use.
     */
    public void init(String configFile)
        throws Exception;
    
    
    /**
     * Start the domain.
     */
    public void start()
        throws Exception;
    
    
    /**
     * Close all access tokens to the domain.
     */
    public void stop();
    
    
    /**
     * Destroy domain.
     */
    public void destroy();
    
    
    /**
     * Access a Namespace.
     * 
     * @param token Entity which wants access
     * @param namespaceName Name of the namespace on which access is requested
     * @return NamespaceAccessToken Access token to the namespace
     */
    public NamespaceAccessToken accessNamespace(SecurityToken token, 
                                                String namespaceName);
    
    
    /**
     * Close a namespace.
     * 
     * @param token Namespace access token
     */
    public void closeNamespace(NamespaceAccessToken token);
    
    
    /**
     * Clsose a namespace.
     * 
     * @param token Entity which wants to close the namespace
     * @param namespaceName Name of the namespace
     */
    public void closeNamespace(SecurityToken token, String namespaceName);
    
    
    /**
     * Access a Domain.
     * 
     * @param token Service who wants access
     * @return DomainAccessToken Access token to the domain
     */
    public DomainAccessToken accessDomain(SecurityToken token);
    
    
}
