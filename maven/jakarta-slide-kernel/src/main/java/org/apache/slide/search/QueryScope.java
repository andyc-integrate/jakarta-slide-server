/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/QueryScope.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.search;

import java.util.Set;

/**
 * Holds the scope information supplied with the <FROM> element.
 *
 * @version $Revision: 1.2 $
 */
public interface QueryScope {
	
    public static final int DEPTH_0 = 0;
    public static final int DEPTH_1 = 1;
	public static final int DEPTH_INFINITY = Integer.MAX_VALUE;
    
    /**
     * Method getExcludeSet
     *
     * @return   a  Set
     */
    public Set getExcludeSet();
    
    
    /**
     * Method getIncludeSet
     *
     * @return   a  Set
     */
    public Set getIncludeSet();
    
    
    /**
     * href accessor.
	 *
	 * @return   a String
	 *
	 */
    public String getHref();
    
    /**
	 * depth accessor
	 *
	 * @return   one of QueryScope.DEPTH_INFINITY, QueryScope.DEPTH_0
	 *			 or QueryScope.DEPTH_1
	 *
	 */
    public int getDepth();
	
    /**
     * Returns true if the scope specifies a collection
     *
     * @return   a boolean
     *
     */
    public boolean isCollection ();
    
    /**
     * Method setIsCollection
     *
     * @param    isCollection        a  boolean
     *
     */
    public void setIsCollection (boolean isCollection);
    
    
    /**
     * Returns an Iterator for Scope objects, that shall be excluded from
     * SEARCH. An excludedScope must match the scope of a store as defined
     * in Domain.xml
     *
     * @return   an Iterator of Scope objects
     *
     */
    public Set getExcludedScopes ();

}
