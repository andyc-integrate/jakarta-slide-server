/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/common/XMLMarshaller.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */ 

package org.apache.slide.common;

import java.io.Writer;

/**
 * XMLMarshaller helper class.
 * 
 * @version $Revision: 1.2 $
 */
public final class XMLMarshaller {
    
    
    // --------------------------------------------------------- Public Methods
    
    
    /**
     * Saves Slide Data to XML.
     * 
     * @param writer Writer 
     * @param startNode Start generating XML from this node of the Slide tree
     * @exception SlideException 
     */
    public static void marshal(NamespaceAccessToken accessToken, 
                               SlideToken token, Writer writer, 
                               String startNode)
	throws SlideException {
        /*
	try {
	    // Retrieve the start element
	    ObjectNode object = 
                accessToken.getStructureHelper().retrieve(token, startNode);
	    // Generate an associated Castor object representing the ObjectNode
	    org.apache.slide.common.xml.Objectnode slideObjectDef =
		new org.apache.slide.common.xml.Objectnode();
	    saveObjectNode(accessToken, token, object, slideObjectDef);
	    // Marshal the Castor object to the Writer
	    slideObjectDef.marshal(writer);
	} catch (Exception e) {
	    e.printStackTrace();
	}
	*/
    }
    
    
    // -------------------------------------------------------- Private Methods
    
    
    /**
     * Save a ObjectNode to a Castor ObjectNode.
     * 
     * @param object Slide Object
     * @param objectDef Castor ObjectNode definition
     */
    /*
    private static void saveObjectNode
        (NamespaceAccessToken accessToken, CredentialsToken token, 
         ObjectNode object, org.apache.slide.common.xml.Objectnode objectDef) {
        
        // Set the basic fields of the Castor object
        objectDef.setClassname(object.getClass().getName());
        objectDef.setUri(object.getUri().toString());
        
        // If the object is or derives from the LinkNode class, we must also 
        // save it's additional linkedUri field
        if (object instanceof LinkNode) {
            objectDef.setTarget(((LinkNode) object).getLinkedUri().toString());
	}
        
        try {
            // Save permissions associated with the Slide Object
            Enumeration permissions = accessToken.getSecurityHelper()
                .enumeratePermissions(token, object);
            while (permissions.hasMoreElements()) {
                NodePermission permission = 
                    (NodePermission) permissions.nextElement();
                org.apache.slide.common.xml.Permission permissionDef =
                    new org.apache.slide.common.xml.Permission();
                permissionDef.setSubject
                    (permission.getSubjectUri().toString());
                permissionDef.setAction(permission.getActionUri().toString());
                permissionDef.setInheritance
                    ((new Boolean(permission.isInheritable())).toString());
                objectDef.addPermission(permissionDef);
            }
    */
            // Saving version information
            /*
              SlideProperties properties = accessToken.getStructureHelper()
              .retrieve(token, object);
              org.apache.slide.common.xml.Properties propertiesDef =
              new org.apache.slide.common.xml.Properties();
              
              propertiesDef.setContentlanguage
              (properties.getContentLanguage());
              propertiesDef.setContenttype(properties.getContentType());
              if (properties.getCreationDateAsDate() != null) {
              propertiesDef.setCreationdate
              ((new Long(properties.getCreationDateAsDate()
              .getTime())).toString());
              } else {
              propertiesDef.setCreationdate(properties.getCreationDate());
              }
              //propertiesDef.setLatestrevisionid(properties
              //.getLatestRevisionId().toString());
              propertiesDef.setName(properties.getName());
              
              Enumeration revisionIdsList = accessToken.getContentHelper()
              .enumerate(token, properties);
              while (revisionIdsList.hasMoreElements()) {
              SlideRevisionId revisionId = 
              (SlideRevisionId) revisionIdsList.nextElement();
              SlideRevision revision = accessToken.getContentHelper()
              .retrieve(token, properties, revisionId);
              
              org.apache.slide.common.xml.Revision revisionDef =
              new org.apache.slide.common.xml.Revision();
              
              revisionDef.setAuthor(revision.getAuthor());
              revisionDef.setCreationdate
              ((new Long(revision.getCreationDate().getTime())).toString());
              revisionDef.setId(revision.getId().toString());
              revisionDef.setName(revision.getName());
              propertiesDef.addRevision(revisionDef);
              }
              objectDef.setProperties(propertiesDef);
            */
    /*
      } catch(SlideException e) {
	}
	
	// Now, we parse all the children of this object
	try {
	    Enumeration childObjects = accessToken
                .getStructureHelper().getChildren(token, object);
	    while (childObjects.hasMoreElements()) {
		ObjectNode currentObject = 
                    (ObjectNode) childObjects.nextElement();
		org.apache.slide.common.xml.Objectnode currentObjectDef =
		    new org.apache.slide.common.xml.Objectnode();
		// Recursive call to this function
		saveObjectNode(accessToken, token, currentObject, 
                               currentObjectDef);
		objectDef.addObjectnode(currentObjectDef);
	    }
	} catch(SlideException e) {
	}
	
    }
*/  
}
