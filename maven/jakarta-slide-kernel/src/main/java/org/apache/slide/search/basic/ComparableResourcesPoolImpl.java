/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/basic/ComparableResourcesPoolImpl.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.search.basic;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.slide.common.SlideException;
import org.apache.slide.common.SlideToken;
import org.apache.slide.content.Content;
import org.apache.slide.search.BadQueryException;
import org.apache.slide.search.InvalidScopeException;
import org.apache.slide.search.PropertyProvider;
import org.apache.slide.search.QueryScope;
import org.apache.slide.search.SearchToken;
import org.apache.slide.search.SlideUri;
import org.apache.slide.structure.ObjectNode;
import org.apache.slide.structure.Structure;
import org.apache.slide.structure.StructureException;

/**
 * Represents the pool of all resources out of which the query result is
 * computed.
 *
 * @version $Revision: 1.2 $
 */
public class ComparableResourcesPoolImpl implements ComparableResourcesPool {
    
    /**  */
    private Structure structure;
    private Content contentHelper;
    private SlideToken slideToken;
    private QueryScope scope;
    private SearchToken searchToken;
    
    private int scopeDepth;
    private int maxSlideDepth;
    private boolean partialResult = false;
    private Set pool;
    private SlideUri slideContext;
    
    /**
     * The PropertyProvider to use (may be <code>null</code>).
     */
    protected PropertyProvider propertyProvider = null;
    
    /**
     * Constructs a RequestedResourcesPool
     *
     * @param searchToken       the searchToken
     * @param scope             the scope of the query
     * @param propertyProvider  the  PropertyProvider to use (may be
     *                          <code>null</code>).
     *
     * @throws BadQueryException
     */
    public ComparableResourcesPoolImpl (SearchToken searchToken,
                                       QueryScope scope,
                                       PropertyProvider propertyProvider)
        throws BadQueryException
    {
        this.structure  = searchToken.getStructureHelper();
        this.slideToken = searchToken.getSlideToken();
        this.scope      = scope;
        this.propertyProvider = propertyProvider;
        this.contentHelper = searchToken.getContentHelper();
        this.slideContext = searchToken.getSlideContext();
        this.searchToken = searchToken;
        
        scopeDepth = scope.getDepth ();
        maxSlideDepth = searchToken.getMaxDepth();
        
        createPool ();
    }
    
    /**
     * Method resourceIterator
     *
     * @return   an Iterator
     *
     */
    public Iterator resourceIterator() {
        return pool.iterator();
    }
    
    
    /**
     * Method getPool
     *
     * @return   a Set
     *
     */
    public Set getPool() {
        return pool;
    }
    
    
    /**
     * Method createPool
     *
     * @throws   BadQueryException
     *
     */
    private void createPool () throws BadQueryException {
        pool = new HashSet ();
        
        String resourceUri = searchToken.getSlideContext().getSlidePath (scope.getHref());
        
        // Get the object from Data.
        ObjectNode resource = null;
        
        try {
            resource = structure.retrieve (slideToken, resourceUri);
            parseOneObject (resource, 0);
        }
        catch (StructureException e) {
            throw new InvalidScopeException
                ("scope " + resourceUri + " is invalid");
        }
        catch (Exception e) {
            throw new BadQueryException (e.getMessage());
        }
    }
    
    /**
     * Method parseOneObject. Called recursively, result set truncation is done
     * here.
     *
     * @param    object              an ObjectNode
     * @param    currentDepth        an int
     *
     * @throws   SlideException
     *
     */
    private void parseOneObject (ObjectNode object, int currentDepth)
        throws SlideException
    {
        if (currentDepth > scopeDepth)
            return;
        
        if (currentDepth > maxSlideDepth) {
            partialResult = true;
            return;
        }
        
        Enumeration children = null;
        children = structure.getChildren (slideToken, object);
        
        while (children.hasMoreElements()) {
            ObjectNode cur = (ObjectNode)children.nextElement();
            parseOneObject (cur, currentDepth + 1);
        }
        
        ComparableResource item =
            new ComparableResourceImpl (object, searchToken, scope, propertyProvider);
        
        pool.add (item);
    }
    
    /**
     * Indicates if the server truncated the result set.
     *
     * @return   a boolean
     *
     */
    public boolean partialResult() {
        return partialResult;
    }

    /**
     * Returns the scope of this ResourcePool.
     *
     * @return     the scope of this ResourcePool.
     */
    public QueryScope getScope() {
        return scope;
    }

}
