/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/common/RequestedPropertyImpl.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.common;
//import org.apache.slide.search.basic.*;

import org.apache.slide.content.NodeProperty;

/**
 * Holds one property as part of the SELECT element.
 *
 * @version $Revision: 1.2 $
 */
public class RequestedPropertyImpl implements RequestedProperty {
    
    protected String namespace;
    private String propertyName;
    
	/**
	 * Constructs a RequestedProperty using the default namespace as defined
	 * in NodeProperty
	 *
	 * @param      propertyName the name of the property
	 */
    public RequestedPropertyImpl (String propertyName) {
		this.propertyName = propertyName;
		this.namespace    = NodeProperty.DEFAULT_NAMESPACE;
    }
	
	/**
	 * Constructs a RequestedProperty:
	 *
	 * @param      propertyName the name of the property
	 * @param      namespace    the namespace of the property
	 */
    public RequestedPropertyImpl (String propertyName, String namespace) {
		this.propertyName = propertyName;
		this.namespace    = namespace;
    }
	
    
    /**
	 * Method getPropertyName
	 *
	 * @return   the property's name
	 */
    public String getPropertyName() {
		return propertyName;
    }
    
    
    /**
	 * Method getNamespace
	 *
	 * @return   the property's namespace
	 */
    public String getNamespace() {
		return namespace;
    }

	/**
	 * Method getName
	 *
	 * @return   the name of the property
	 *
	 */
	public String getName() {
		return propertyName;
	}

    /**
	 * checks, if another Object is equal to this RequestedProperty
	 *
	 * @param    o                   an Object
	 *
	 * @return   true if equal
	 */
    public boolean equals (Object o) {
		if (! (o instanceof RequestedProperty))
			return false;
		RequestedProperty other = (RequestedProperty)o;
		if (!namespace.equals (other.getNamespace()))
			return false;
		if (!propertyName.equals (other.getName()))
			return false;
		return true;
    }
    
    /**
	 * debugging purpose
	 *
	 * @return   String representation of this RequestedProperty
	 */
    public String toString () {
		return namespace + propertyName;
    }

    public int hashCode() {
        return toString().hashCode();
    }

}
