/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/basic/BasicQueryScope.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.search.basic;

import java.net.URL;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.slide.content.NodeProperty;
import org.apache.slide.search.BadGatewayException;
import org.apache.slide.search.BadQueryException;
import org.apache.slide.search.QueryScope;
import org.jdom.Element;
import org.jdom.Namespace;


/**
 * Holds the scope information supplied with the <FROM> element.
 *
 * @version $Revision: 1.2 $
 */
public class BasicQueryScope implements QueryScope {

    private static Namespace slideNamespace =
        NodeProperty.NamespaceCache.getNamespace (NodeProperty.SLIDE_NAMESPACE);

    /** String representing the URI */
    private String href;

    /** the depth of this query. */
    private int    depth;

    /** indicates if this scope is a collection */
    private boolean isCollection = true;

    /** holds hrefs, that shall be excluded from search. */
    private Set excludedScopes = new HashSet ();


    private Set includeSet = new HashSet ();
    private Set excludeSet = new HashSet ();

    /**
     * Constructs a scope
     *
     * @param      propertyName the name of the property
     */
//    public BasicQueryScope (String href) {
//        this.href  = href;
//    }

    /**
     * Constructs a scope
     *
     * @param      propertyName the name of the property
     */
    public BasicQueryScope (String href, int depth, Set includeSet, Set excludeSet) {
        this.href  = href;
        this.depth = depth;
        this.includeSet = includeSet;
        this.excludeSet = excludeSet;
    }

    /**
     * Constructs a scope from e FROM element
     *
     * @param      propertyName the name of the property
     */
    public BasicQueryScope (Element fromElement)
        throws BadQueryException
    {
        this.depth = QueryScope.DEPTH_INFINITY;

        Namespace namespace  = fromElement.getNamespace();
        String name = fromElement.getName ();

        if (! (namespace.getURI().equals (NodeProperty.DEFAULT_NAMESPACE)
                   && name.equals (Literals.FROM)))
            throw new BadQueryException ("expected DAV:from");

        Element scope = fromElement.getChild (Literals.SCOPE, namespace);

        if (scope.getChildren (Literals.HREF, namespace).size() != 1) {
            throw new BadQueryException ("exactly one href element must be defined");
        }

        href  = scope.getChildTextTrim (Literals.HREF, namespace);

        try {
            URL url = new URL (href);
            throw new BadGatewayException (href + ": Bad Gateway (no server redirection allowed)");
        }
        // must not be a isValid URL (server redirection not implemented)
        catch (java.net.MalformedURLException e) {}

//        if (!href.endsWith("/"))
//            href += "/";

        Element depth = scope.getChild (Literals.DEPTH, namespace);
        if (depth != null) {
            String d = depth.getTextTrim();
            if (d.equals ("0"))
                this.depth = 0;
            else if (d.equals ("1"))
                this.depth = 1;
        }

        Iterator it = scope.getChildren (Literals.EXCLUDE, slideNamespace).iterator();
        while (it.hasNext()) {
            Element excluded = (Element)it.next();
            String href = excluded.getText();
            excludedScopes.add (href);
        }

        it = scope.getChildren (Literals.INCLUDE_LASTPATHSEGEMENT, namespace).iterator();
        while (it.hasNext()) {
            Element incl = (Element)it.next();
            String pattern = incl.getText();
            includeSet.add (pattern);
        }

        it = scope.getChildren (Literals.EXCLUDE_LASTPATHSEGEMENT, namespace).iterator();
        while (it.hasNext()) {
            Element incl = (Element)it.next();
            String pattern = incl.getText();
            excludeSet.add (pattern);
        }

    }


    /**
     * href accessor
     *
     * @return   a String
     *
     */
    public String getHref() {
        return href;
    }

    /**
     * depth accessor
     *
     * @return   an int
     *
     */
    public int getDepth() {
        return depth;
    }


    /**
     * checks, if another Object is equal to this SelectedProperty
     *
     * @param    o                   an Object
     *
     * @return   true if equal
     */
    public boolean equals (Object o) {

        if (! (o instanceof QueryScope))
            return false;

        QueryScope other = (QueryScope)o;
        if (!href.equals (other.getHref()))
            return false;

        if (! (depth == other.getDepth()))
            return false;

        return true;
    }

    /**
     * Debugging purpose
     *
     * @return   String representation of this SelectedProperty
     */
    public String toString () {
        return href + ", depth = " + depth;
    }

    public void setIsCollection(boolean isCollection) {
        this.isCollection = isCollection;
        if (isCollection == true && !href.endsWith("/"))
            href = href + "/";
    }

    public boolean isCollection() {
        return isCollection;
    }


    /**
     * Method getExcludedScopes
     *
     * @return   a Set
     *
     */
    public Set getExcludedScopes() {
        return excludedScopes;
    }

        /**
     * Method getIncludeSet
     *
     * @return   a  Set
     */
    public Set getIncludeSet() {
        return includeSet;
    }

    /**
     * Method getExcludeSet
     *
     * @return   a  Set
     */
    public Set getExcludeSet() {
        return excludeSet;
    }


}
