/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/PropertyProvider.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.search;

// import list
import org.apache.slide.common.SlideException;

import org.apache.slide.content.NodeProperty;

import java.util.Iterator;

/**
 * If a PropertyProvider is passed to {@link org.apache.slide.search.Search#createSearchQuery
 * Search.createSearchQuery()}, the Search helper must not access any of the properties
 * {@link #isSupportedProperty supported by this PropertyProvider} directly (e.g.
 * by using the Content helper) but instead use {@link #getProperty getProperty()}
 * on this provider to retrieve it.
 *
 * @version $Revision: 1.2 $
 *
 **/
public interface PropertyProvider {
    
    /**
     * Returns <code>true</code> if this PropertyProvider can provide the NodeProperty
     * specified by <code>propertyNamespace</code> and <code>propertyName</code>
     * for the resource with the given <code>resourceUri</code>.
     *
     * @param      resourceUri        the URI of the resource.
     * @param      propertyName       the name of the property.
     * @param      propertyNamespace  the namespace of the property.
     *
     * @return     <code>true</code> if this PropertyProvider can provide the NodeProperty.
     *
     * @throws     SlideException
     */
    public boolean isSupportedProperty(String resourceUri, String propertyName, String propertyNamespace) throws SlideException;


    /**
     * Returns an Iterator of PropertyName of all properties supported by this
     * PropertyProvider.
     *
     * @param      resourceUri        the URI of the resource for which to return
     *                                the supported PropertyNames.
     *
     * @return     an Iterator of PropertyName.
     *
     * @throws     SlideException
     */
    public Iterator getSupportedPropertiesNames(String resourceUri) throws SlideException;
    
    /**
     * If the property specified by <code>propertyNamespace</code> and
     * <code>propertyName</code> is {@link #isSupportedProperty supported
     * by this PropertyProvider}, the NodeProperty of the resource located
     * at the given <code>resourceUri</code> will be returned. Otherwise
     * <code>null</code> is returned.
     *
     * @param      resourceUri        the URI of the resource for which to return
     *                                the NodeProperty.
     * @param      propertyName       the name of the property to return.
     * @param      propertyNamespace  the namespace of the property to return.
     *
     * @return     the requested NodeProperty if it is supported, otherwise
     *             <code>null</code>.
     *
     * @throws     SlideException
     */
    public NodeProperty getProperty(String resourceUri, String propertyName, String propertyNamespace) throws SlideException;
    
    /**
     * Returns an Iterator of all NodeProperties supported by this PropertyProvider.
     *
     * @param      resourceUri        the URI of the resource for which to return
     *                                the supported properties.
     *
     * @return     all NodeProperties supported by this provider.
     *
     * @throws     SlideException
     */
    public Iterator getSupportedProperties(String resourceUri) throws SlideException;
    

}

