/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/basic/IBasicExpressionCompiler.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.search.basic;

// import list
import org.apache.slide.search.BadQueryException;

import org.jdom.Element;

/**
 * Compiles an expression represented by a JDOM Element (-tree) into an
 * IBasicExpression (-tree).
 *
 * @version $Revision: 1.2 $
 *
 **/
public interface IBasicExpressionCompiler {

    /**
     * Compiles an IBasicExpression (-tree) from the given <code>expressionElement</code>.
     *
     * @param      expressionElement  the (root) expression Element to compile
     *                                into an IBasicExpression.
     *
     * @return     the compiled IBasicExpression.
     *
     * @throws     BadQueryException  if compiling the expression failed.
     */
    public IBasicExpression compile(Element expressionElement) throws BadQueryException;

}

