/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/method/EventMethod.java,v 1.2 2006-01-22 22:55:20 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:20 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.method;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.slide.common.NamespaceAccessToken;
import org.apache.slide.event.EventDispatcher;
import org.apache.slide.event.GenericEvent;
import org.apache.slide.event.VetoException;
import org.apache.slide.webdav.WebdavException;
import org.apache.slide.webdav.WebdavServletConfig;
import org.apache.slide.webdav.util.NotificationConstants;
import org.apache.slide.webdav.util.WebdavStatus;
import org.jdom.Element;


/**
 * Unsubscribe Method.
 *
 */
public class EventMethod extends AbstractWebdavMethod implements NotificationConstants {
    protected static final String LOG_CHANNEL = EventMethod.class.getName();

    private List eventsToFire = new ArrayList();
    private List vetoableEventsToFire = new ArrayList();

    /**
     * Constructor.
     *
     * @param token     the token for accessing the namespace
     * @param config    configuration of the WebDAV servlet
     */
    public EventMethod(NamespaceAccessToken token,
                         WebdavServletConfig config) {
        super(token, config);
    }

    protected void parseRequest() throws WebdavException {
        try {
            List events = parseRequestContent(E_FIRE_EVENTS).getChildren();
            for ( Iterator i = events.iterator(); i.hasNext(); ) {
                Element event = (Element)i.next();
                // get information
                List informations = event.getChildren();
                String[][] eventInformation = new String[informations.size()][2];
                int counter = 0;
                for ( Iterator j = informations.iterator(); j.hasNext(); ) {
                    Element information = (Element)j.next();
                    eventInformation[counter][0] = information.getAttributeValue(A_INFORMATION_KEY);
                    eventInformation[counter][1] = information.getText();
                    counter++;
                }
                if ( event.getName().equals(E_EVENT) ) {
                    eventsToFire.add(new GenericEvent(this, eventInformation));
                } else if ( event.getName().equals(E_VETOABLE_EVENT) ) {
                    vetoableEventsToFire.add(new GenericEvent(this, eventInformation));
                }
            }
        } catch (Exception e) {
            int statusCode = getErrorCode( (Exception)e );
            sendError( statusCode, e );
            throw new WebdavException( statusCode );
        }
    }

    protected void executeRequest() throws WebdavException {
        try {
            // FIXME: Generate multi status output to give detailed information about every fired event
            if ( GenericEvent.EVENT_FIRED.isEnabled() ) {
                for ( Iterator i = eventsToFire.iterator(); i.hasNext(); ) {
                    GenericEvent event = (GenericEvent)i.next();
                    EventDispatcher.getInstance().fireEvent(GenericEvent.EVENT_FIRED, event);
                }
            }
            if ( GenericEvent.VETOABLE_EVENT_FIRED.isEnabled() ) {
                for ( Iterator i = vetoableEventsToFire.iterator(); i.hasNext(); ) {
                    GenericEvent event = (GenericEvent)i.next();
                    try {
                        EventDispatcher.getInstance().fireVetoableEvent(GenericEvent.VETOABLE_EVENT_FIRED, event);
                    } catch ( VetoException exception ) {
                        // add to multiresponse
                    }
                }
            }
            resp.setStatus(WebdavStatus.SC_OK);
        } catch (Exception e) {
            int statusCode = getErrorCode( (Exception)e );
            sendError( statusCode, e );
            throw new WebdavException( statusCode );
        }
    }
}