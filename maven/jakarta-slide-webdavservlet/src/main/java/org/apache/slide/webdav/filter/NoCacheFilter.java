/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/filter/NoCacheFilter.java,v 1.2 2006-01-22 22:55:20 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:20 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * A simple filter that ensures the two HTTP cache headers
 * (Pragma and Cache-Control) are set to values that will allow Internet
 * Explorer to display non-inline files (.pdf, .doc, etc) when using an SSL
 * connection. This is to work around a bug in IE versions 5.01 through 6.
 * </p>
 * 
 * <p>
 * This class is designed for use in an intranet environment where the
 * security ramifications are controllable. In a public environment something
 * more advanced should be used. Setting Cache-Control to "private" and sniffing
 * the request to make sure the headers need to be modified might be enough.
 * </p>
 * 
 * @see http://support.microsoft.com/default.aspx?scid=KB;EN-US;q316431&
 *
 */
public class NoCacheFilter implements Filter {

	/**
	 * Does nothing.
	 * @param config is ignored
	 */
	public void init(FilterConfig config) throws ServletException {
		// Do nothing
	}

	/**
	 * Sets the Pragma and Cache-Control HTTP headers then calls the
	 * next Filter in the chain.
	 * @param req the HTTP request
	 * @param resp the HTTP response
	 * @param chain the chain of filter objects handling this request. 
	 */
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest)req;
		HttpServletResponse response = (HttpServletResponse)resp;
		
		/*
		 * Ensure headers are set to values that will allow IE to cache the
		 * document. There could be a browser sniff here and a check for the
		 * type of document being requested.
		 */
        response.setHeader("Pragma", "public");
        response.setHeader("Cache-Control", "public");

        /*
         * This must be called after the headers are set because the response
         * is committed before control returns to this method.
         */
		chain.doFilter(req,resp);
	}

	/**
	 * Does nothing.
	 */
	public void destroy() {
		// Do nothing
	}

}
