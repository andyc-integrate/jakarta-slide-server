/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/method/PutMethod.java,v 1.2 2006-01-22 22:55:20 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:20 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.method;

import java.io.IOException;
import java.util.Date;

import org.apache.slide.common.NamespaceAccessToken;
import org.apache.slide.common.ServiceAccessException;
import org.apache.slide.common.SlideException;
import org.apache.slide.content.NodeProperty;
import org.apache.slide.content.NodeRevisionContent;
import org.apache.slide.content.NodeRevisionDescriptor;
import org.apache.slide.content.NodeRevisionDescriptors;
import org.apache.slide.content.NodeRevisionNumber;
import org.apache.slide.content.RevisionAlreadyExistException;
import org.apache.slide.content.RevisionDescriptorNotFoundException;
import org.apache.slide.event.EventDispatcher;
import org.apache.slide.structure.LinkedObjectNotFoundException;
import org.apache.slide.structure.ObjectAlreadyExistsException;
import org.apache.slide.structure.ObjectNotFoundException;
import org.apache.slide.structure.SubjectNode;
import org.apache.slide.util.Configuration;
import org.apache.slide.webdav.WebdavException;
import org.apache.slide.webdav.WebdavServletConfig;
import org.apache.slide.webdav.event.WebdavEvent;
import org.apache.slide.webdav.util.DeltavConstants;
import org.apache.slide.webdav.util.PreconditionViolationException;
import org.apache.slide.webdav.util.PropertyHelper;
import org.apache.slide.webdav.util.UriHandler;
import org.apache.slide.webdav.util.VersioningHelper;
import org.apache.slide.webdav.util.ViolatedPrecondition;
import org.apache.slide.webdav.util.WebdavStatus;
import org.apache.slide.webdav.util.WebdavUtils;
import org.apache.slide.webdav.util.resourcekind.AbstractResourceKind;
import org.apache.slide.webdav.util.resourcekind.CheckedInVersionControlled;
import org.apache.slide.webdav.util.resourcekind.ResourceKind;

/**
 * PUT method.
 *
 */
public class PutMethod
    extends AbstractWebdavMethod
    implements DeltavConstants, WriteMethod {


    // ----------------------------------------------------- Instance Variables
    
    /**
     * The VersioningHelper used by this instance.
     */
    protected VersioningHelper versioningHelper = null;
    
    /**
     * Resource to be written.
     */
    protected String resourcePath;
    
    // ----------------------------------------------------------- Constructors
    
    
    /**
     * Constructor.
     *
     * @param token     the token for accessing the namespace
     * @param config    configuration of the WebDAV servlet
     */
    public PutMethod(NamespaceAccessToken token, WebdavServletConfig config) {
        super(token, config);
    }

    // ------------------------------------------------------ Protected Methods
    
    
    /**
     * Parse XML request.
     *
     * @exception WebdavException Does not happen
     */
    protected void parseRequest()
        throws WebdavException {
        versioningHelper = VersioningHelper.getVersioningHelper(slideToken,
                                                                token,
                                                                req,
                                                                resp,
                                                                config);
        
        resourcePath = requestUri;
        if (resourcePath == null) {
            resourcePath = "/";
        }
    }
    
    /**
     * Execute request.
     *
     * @exception WebdavException Bad request
     */
    protected void executeRequest()
        throws WebdavException, IOException {
        
        // Prevent dirty reads
        slideToken.setForceStoreEnlistment(true);

        // check destination URI
        UriHandler destUh = UriHandler.getUriHandler(resourcePath);
        
        if (destUh.isRestrictedUri()) {
            boolean sendError = true;
            if( destUh.isWorkingresourceUri() ) {
                // PUT on existing WRs is *not* restricted !!!
                try {
                    content.retrieve(slideToken, resourcePath);
                    sendError = false;
                }
                catch( Exception x ) {};
            }
            if( sendError ) {
                int statusCode = WebdavStatus.SC_FORBIDDEN;
                sendError( statusCode, getClass().getName()+".restrictedDestinationUri", new Object[]{resourcePath} );
                throw new WebdavException( statusCode );
            }
        }
        
        try {
            // fire put event
            if ( WebdavEvent.PUT.isEnabled() ) EventDispatcher.getInstance().fireVetoableEvent(WebdavEvent.PUT, new WebdavEvent(this));

            try {

                boolean isLockedNullResource = false;
                
                NodeRevisionDescriptors revisionDescriptors =
                    content.retrieve(slideToken, resourcePath);
                
                NodeRevisionNumber revisionNumber =
                    revisionDescriptors.getLatestRevision();
                NodeRevisionDescriptor oldRevisionDescriptor = null;
                if (revisionNumber != null) {
                    try {
                        oldRevisionDescriptor = content.retrieve
                            (slideToken, revisionDescriptors);
                    } catch (RevisionDescriptorNotFoundException e) {
                    }
                }
                if (WebdavUtils.isCollection(oldRevisionDescriptor)) {
                    int statusCode = WebdavStatus.SC_METHOD_NOT_ALLOWED;
                    sendError( statusCode, getClass().getName()+".mustNotBeCollection" );
                    throw new WebdavException( statusCode );
                }
                
                NodeRevisionDescriptor revisionDescriptor = null;
                if (oldRevisionDescriptor == null) {
                    revisionDescriptor = new NodeRevisionDescriptor();
                } else {
                    revisionDescriptor = oldRevisionDescriptor;
                    revisionDescriptor.setContentLength(-1);
                }
                
                ResourceInfo resourceInfo =
                    new ResourceInfo(resourcePath, revisionDescriptor);
                
                // Checking If headers
                if (!checkIfHeaders(req, resp, resourceInfo)) {
                    return;
                }
                
                ResourceKind resourceKind = AbstractResourceKind.determineResourceKind(token, resourcePath, revisionDescriptor);
                
                versioningHelper.isWriteLocked(slideToken, revisionDescriptors);
                
                // check preconditions
                ViolatedPrecondition violatedPrecondition = getPreconditionViolation(revisionDescriptors, revisionDescriptor, resourceKind);
                if (violatedPrecondition != null) {
                    throw new PreconditionViolationException(violatedPrecondition, resourcePath);
                }
                
                // Changed for DeltaV --start--
                boolean mustCheckIn = false;
                if( Configuration.useVersionControl() &&
                       (resourceKind instanceof CheckedInVersionControlled) &&
                   versioningHelper.mustCheckoutAutoVersionedVCR(revisionDescriptors, revisionDescriptor) ) {
                    
                    versioningHelper.checkout(revisionDescriptors, revisionDescriptor, false, false, true );
                    mustCheckIn = versioningHelper.mustCheckinAutoVersionedVCR(slideToken, revisionDescriptors, revisionDescriptor);
                }
                // Changed for DeltaV --end--
                
                NodeRevisionContent revisionContent =
                    new NodeRevisionContent();
                //revisionContent.setContent(req.getReader());
                
                revisionContent.setContent(req.getInputStream());
                
                // Get content length
                int contentLength = req.getContentLength();
                if (contentLength == (-1)) {
                	contentLength = revisionContent.getContentBytes().length;
                }
                revisionDescriptor.setContentLength(contentLength);
                
                // Last modification date
                revisionDescriptor.setLastModified(new Date());
                
                // Etag generation
                revisionDescriptor.setETag(PropertyHelper.computeEtag(resourcePath, revisionDescriptor) );
                
                // Get content type (allow content-type to be updated here)
                String contentType = req.getContentType();
                if (contentType == null) {
                    contentType = getConfig().getServletContext()
                        .getMimeType(resourcePath);
                }
                if (contentType == null) {
                    contentType = getConfig().getDefaultMimeType();
                }
                revisionDescriptor.setContentType(contentType);
                
                // Normally assume the 'getcontentlanguage'
                // is set, however, before we clear the
                // 'resourcetype' need to check for the case when a
                // 'lock-null' is created just before the initial PUT. In
                // that case need to add the missing properties.
                if (isLockNull(revisionDescriptor)) {
                    //              if (revisionDescriptor.getResourceType().equals("<lock-null/>")) {
                    isLockedNullResource = true;
                    revisionDescriptor.setContentLanguage("en");
                    
                    // Changed for DeltaV --start--
                    if( Configuration.useVersionControl() ) {
                        // Workspace
                        versioningHelper.setWorkspaceProperty( resourcePath, revisionDescriptor );
                    }
                    // Changed for DeltaV --end--
                }
                
                // Resource type
                revisionDescriptor.setResourceType("");
                
                // Owner
                if ( isLockedNullResource ) {
                    // set the owner when updating a lock-null resource
                    String creator = ((SubjectNode)security.getPrincipal(slideToken)).getPath().lastSegment();
                    revisionDescriptor.setCreationUser(creator);
                    revisionDescriptor.setOwner(creator);
                }
                
                content.store(slideToken, resourcePath, revisionDescriptor,
                              revisionContent);
                
                // Changed for DeltaV --start--
                // check if the resource should be put under version-control
                if ( isLockedNullResource ) {
                    if ( Configuration.useVersionControl() && isAutoVersionControl(resourcePath) && !isExcludedForVersionControl(resourcePath) ) {
                        versioningHelper.versionControl(resourcePath);
                    }
                }
                if( Configuration.useVersionControl() && mustCheckIn) {
                    versioningHelper.checkin(revisionDescriptors, revisionDescriptor, false, false, true ); //forkOk=false, keepCheckedOut=false
                }
                // Changed for DeltaV --end--
                
                
                // ETag header
                resp.setHeader("ETag", revisionDescriptor.getETag() );
                
                resp.setStatus(WebdavStatus.SC_NO_CONTENT);
                
            } catch (LinkedObjectNotFoundException e) {
                int statusCode = getErrorCode( e );
                sendError( statusCode, e );
                throw new WebdavException( statusCode );
            } catch (ObjectNotFoundException e) {
                SubjectNode subject = new SubjectNode();
                // Creating an object
                structure.create(slideToken, subject, resourcePath);
                
                NodeRevisionDescriptor revisionDescriptor =
                    new NodeRevisionDescriptor(req.getContentLength());
                
                ResourceInfo resourceInfo =
                    new ResourceInfo(resourcePath, revisionDescriptor);
                resourceInfo.exists = false;
                
                // Checking If headers
                if (!checkIfHeaders(req, resp, resourceInfo)) {
                    int statusCode = WebdavStatus.SC_PRECONDITION_FAILED;
                    sendError( statusCode, "Check If Header failed" );
                    throw new WebdavException( statusCode );
                }
                
                //NodeProperty property = null;
                
                // Creation date
                
                // Resource type
                revisionDescriptor.setResourceType("");
                
                // Source
                revisionDescriptor.setSource("");
                
                // Get content language
                revisionDescriptor.setContentLanguage("en");
                
                // Get content type
                String contentType = req.getContentType();
                if (contentType == null) {
                    contentType = getConfig().getServletContext()
                        .getMimeType(resourcePath);
                }
                if (contentType == null) {
                    contentType = getConfig().getDefaultMimeType();
                }
                revisionDescriptor.setContentType(contentType);
                
                // Last modification date
                revisionDescriptor.setLastModified(new Date());
                
                // Etag generation
                revisionDescriptor.setETag(PropertyHelper.computeEtag(resourcePath, revisionDescriptor));
                
                // Creation date
                revisionDescriptor.setCreationDate(new Date());
                
                // Owner
                String creator = ((SubjectNode)security.getPrincipal(slideToken)).getPath().lastSegment();
                revisionDescriptor.setCreationUser(creator);
                revisionDescriptor.setOwner(creator);
                
                // Added for DeltaV --start--
                if( Configuration.useVersionControl() ) {
                    // Workspace
                    versioningHelper.setWorkspaceProperty( resourcePath, revisionDescriptor );
                }
                // Added for DeltaV --end--
                
                if (isMsProprietarySupport()) {
                    NodeProperty property = null;
                    // Is hidden
                    property = new NodeProperty("ishidden", "0", "MICROSOFT");
                    revisionDescriptor.setProperty(property);
                    
                    // Is collection
                    property = new NodeProperty("iscollection", "0",
                                                "MICROSOFT");
                    revisionDescriptor.setProperty(property);
                    
                    // Is read only
                    property = new NodeProperty("isreadonly", "0",
                                                "MICROSOFT");
                    revisionDescriptor.setProperty(property);
                    
                    // Last accessed
                    property = new NodeProperty("lastaccessed",
                                                    (new Date()).toString(),
                                                "MICROSOFT");
                    revisionDescriptor.setProperty(property);
                    
                }
                
                // Creating revisionDescriptor associated with the object
                NodeRevisionContent revisionContent =
                    new NodeRevisionContent();
                revisionContent.setContent(req.getInputStream());
                
                // Get content length
                int contentLength = req.getContentLength();
                if (contentLength == -1) {
                    contentLength = revisionContent.getContentBytes().length;
                }
                revisionDescriptor.setContentLength(contentLength);
                
                content.create(slideToken, resourcePath, revisionDescriptor,
                               revisionContent);
                
                // check if the resource should be put under version-control
                if ( Configuration.useVersionControl() && isAutoVersionControl(resourcePath) && !isExcludedForVersionControl(resourcePath) ) {
                    versioningHelper.versionControl(resourcePath);
                }
                
                // ETag header
                resp.setHeader("ETag", revisionDescriptor.getETag() );
                
                resp.setStatus(WebdavStatus.SC_CREATED);
                
            }
        }
        catch (PreconditionViolationException e) {
            sendPreconditionViolation(e);
            throw e;
        }
        catch (SlideException e) {
            int statusCode = getErrorCode( e );
            sendError( statusCode, e );
            throw new WebdavException( statusCode );
        }
        catch (Exception e) {
            int statusCode = getErrorCode( e );
            sendError( statusCode, e );
            throw new WebdavException( statusCode );
        }
    }
    
    /**
     * Checks the (DeltaV) preconditions
     * <ul>
     * <li>&lt;DAV:cannot-modify-version-controlled-content&gt;</li>
     * <li>&lt;DAV:cannot-modify-version&gt;</li>
     * </ul>
     *
     * @param      revisionDescriptors  the NodeRevisionDescriptors of the resource
     *                                  to perform the <code>PUT</code> on.
     * @param      revisionDescriptor  the NodeRevisionDescriptor of the resource
     *                                 to perform the <code>PUT</code> on.
     * @param      resourceKind         the ResourceKind of the resource.
     *
     * @return     the precondition that has been violated (if any).
     */
    private ViolatedPrecondition getPreconditionViolation(NodeRevisionDescriptors revisionDescriptors, NodeRevisionDescriptor revisionDescriptor, ResourceKind resourceKind)
        throws ServiceAccessException {
        
        if( Configuration.useVersionControl() ) {
            
            if (resourceKind instanceof CheckedInVersionControlled) {
                
                // check precondition DAV:cannot-modify-version-controlled-content
                String autoVersion = versioningHelper.getAutoVersionElementName(revisionDescriptor);
                if (autoVersion == null) {
                    autoVersion = "";
                }
                if ( !E_CHECKOUT_CHECKIN.equals(autoVersion) &&
                    !E_CHECKOUT_UNLOCKED_CHECKIN.equals(autoVersion) &&
                    !E_CHECKOUT.equals(autoVersion) &&
                    !E_CHECKOUT_IGNORE_UNLOCK.equals(autoVersion) &&
                    !E_LOCKED_CHECKOUT.equals(autoVersion) ) {
                    return new ViolatedPrecondition(C_CANNOT_MODIFY_VERSION_CONTROLLED_CONTENT,
                                                    WebdavStatus.SC_FORBIDDEN);
                }
                if ( E_LOCKED_CHECKOUT.equals(autoVersion) &&
                        ( !versioningHelper.isWriteLocked(slideToken, revisionDescriptors) ) ) {
                    return new ViolatedPrecondition(C_CANNOT_MODIFY_VERSION_CONTROLLED_CONTENT,
                                                    WebdavStatus.SC_FORBIDDEN);
                }
            }
            
            // check precondition DAV:cannot-modify-version
            UriHandler uriHandler = UriHandler.getUriHandler(resourcePath);
            if (uriHandler.isVersionUri()) {
                return new ViolatedPrecondition(C_CANNOT_MODIFY_VERSION,
                                                WebdavStatus.SC_FORBIDDEN);
            }
        }
        return null;
    }
    
    
    
    
    
    
    /**
     * Get return status based on exception type.
     */
    protected int getErrorCode(SlideException ex) {
        try {
            throw ex;
        } catch (RevisionAlreadyExistException e) {
            return WebdavStatus.SC_CONFLICT;
        } catch (ObjectAlreadyExistsException e) {
            return WebdavStatus.SC_CONFLICT;
        } catch (ObjectNotFoundException e) {
            return WebdavStatus.SC_CONFLICT;
        } catch (LinkedObjectNotFoundException e) {
            return WebdavStatus.SC_NOT_FOUND;
        } catch (SlideException e) {
            return super.getErrorCode(e);
        } catch (Exception e) {
            return super.getErrorCode(e);
        }
    }
}




