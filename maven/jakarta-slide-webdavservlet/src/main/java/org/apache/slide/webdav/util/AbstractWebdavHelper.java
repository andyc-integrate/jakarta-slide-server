/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/util/AbstractWebdavHelper.java,v 1.2 2006-01-22 22:55:20 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:20 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.util;

import org.apache.slide.common.NamespaceAccessToken;
import org.apache.slide.common.SlideToken;
import org.apache.slide.content.NodeProperty.NamespaceCache;
import org.apache.slide.util.CustomSAXBuilder;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;


/**
 * Helper class for handling report resources.
 *
 */

public class AbstractWebdavHelper
implements AclConstants, DeltavConstants, DaslConstants {


    // an XML outputter
    protected static XMLOutputter xmlOut = new XMLOutputter();

    // an XML parser
    protected SAXBuilder xmlBuilder = CustomSAXBuilder.newInstance();

    protected static final Namespace DNSP = NamespaceCache.DEFAULT_NAMESPACE;


    // the Slide token
    protected SlideToken sToken = null;

    // the namespace access token
    protected NamespaceAccessToken nsaToken = null;

    /**
     * Protected contructor
     */
    protected AbstractWebdavHelper( SlideToken sToken, NamespaceAccessToken nsaToken ) {
        this.sToken = sToken;
        this.nsaToken = nsaToken;
    }

}

