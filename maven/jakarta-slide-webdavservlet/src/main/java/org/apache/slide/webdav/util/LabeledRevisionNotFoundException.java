/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/util/LabeledRevisionNotFoundException.java,v 1.2 2006-01-22 22:55:21 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:21 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.webdav.util;

// import list
import org.apache.slide.webdav.WebdavException;

/**
 * This Exception is thrown if a revision with the given label was not found.
 *
 * @version $Revision: 1.2 $
 *
 **/
public class LabeledRevisionNotFoundException extends WebdavException {
    
    /**
     * The label that was searched for.
     */
    protected String label = null;
    
    /**
     * The path of the resource (which is either a VHR or a VCR).
     */
    protected String resourcePath = null;
    
    /**
     * Creates a LabeledRevisionNotFoundException.
     *
     * @param      resourcePath  the path of the resource (which is either a
     *                           VHR or a VCR).
     * @param      label         the label that was searched for.
     */
    public LabeledRevisionNotFoundException(String resourcePath, String label) {
        
        super(WebdavStatus.SC_NOT_FOUND);
        this.resourcePath = resourcePath;
        this.label = label;
    }
    
    /**
     * Returns the path of the resource (which is either a VHR or a VCR).
     *
     * @return     the path of the resource (which is either a VHR or a VCR).
     */
    public String getResourcePath() {
        return resourcePath;
    }
    
    /**
     * Returns the label that was searched for.
     *
     * @return     the label that was searched for.
     */
    public String getLabel() {
        return label;
    }
}

