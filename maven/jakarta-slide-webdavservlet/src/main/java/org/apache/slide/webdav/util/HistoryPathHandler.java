/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/util/HistoryPathHandler.java,v 1.2 2006-01-22 22:55:20 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:20 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.slide.common.Domain;

public class HistoryPathHandler extends UriHandler {
    
    final static String HISTORY_PATH =
        Domain.getParameter( I_HISTORYPATH, I_HISTORYPATH_DEFAULT );
    
    static HistoryPathHandler historyPathHandler = new HistoryPathHandler( HISTORY_PATH );
        
    static boolean parameterized = (HISTORY_PATH.indexOf(I_STORE_PLACE_HOLDER_IN_PATH) >= 0);

    
    /**
     * Factory method.
     */
    public static HistoryPathHandler getHistoryPathHandler() {
        return historyPathHandler;
    }
    
    /**
     * Get a resolved UriHandler for this HistoryPathHandler.
     */
    public static UriHandler getResolvedHistoryPathHandler( String namespaceName, UriHandler uh ) {
        if( parameterized ) {
            return getResolvedHistoryPathHandler( uh.getAssociatedBaseStoreName(namespaceName) );
        }
        else
            return historyPathHandler;
    }
    
    /**
     * Get a resolved UriHandler for this HistoryPathHandler.
     */
    public static UriHandler getResolvedHistoryPathHandler( String storeName ) {
        if( parameterized ) {
        StringBuffer b;
        String rp = historyPathHandler.toString();
        int k = rp.indexOf( I_STORE_PLACE_HOLDER_IN_PATH );
        if( k >= 0 ) {
            b = new StringBuffer( rp );
            while( k >= 0 ) {
                b.replace( k, k + I_STORE_PLACE_HOLDER_IN_PATH.length(), storeName );
                k = b.toString().indexOf(I_STORE_PLACE_HOLDER_IN_PATH);
            }
            rp = b.toString();
        }
        return new UriHandler(rp);
    }
        else
            return historyPathHandler;
    }
    
    /**
     * Factory method.
     */
    public static String getHistoryPath() {
        return HISTORY_PATH;
    }
    
    
    private Set resolvedHistoryPaths = null;
    
    /**
     * Protected constructor
     */
    protected HistoryPathHandler( String uri ) {
        super( uri );
    }
    
    /**
     * Return true if the specified URI is a valid history path URI
     */
    public boolean isHistoryPathUri( UriHandler uh ) {
        if( !parameterized )
            return equals( uh );
        
        if( !Domain.namespacesAreInitialized() )
            return false;
        
        if( resolvedHistoryPaths == null )
            resolve();
        
        return resolvedHistoryPaths.contains( uh );
    }
    
    /**
     * Return the resolved history paths
     */
    public List getResolvedHistoryPaths() {
        return getResolvedHistoryPaths( null );
    }
    
    /**
     * Return the resolved history paths
     */
    public List getResolvedHistoryPaths( String storeName ) {
        List result;
        if( parameterized ) {
            if( storeName != null ) {
                result = new ArrayList();
                result.add( getResolvedHistoryPathHandler(storeName) );
            }
            else {
                resolve();
                result = new ArrayList( resolvedHistoryPaths );
            }
        }
        else {
            result = new ArrayList();
            result.add( HISTORY_PATH );
        }
        return result;
    }
    
    /**
     * Resolve this history path handler
     */
    private void resolve() {
        resolvedHistoryPaths = new HashSet();
        Iterator i = allStoreNames.iterator();
        while( i.hasNext() ) {
            String storeName = (String)i.next();
            UriHandler rpuh = getResolvedHistoryPathHandler( storeName );
            if( allScopes.contains(rpuh) )
                resolvedHistoryPaths.add( rpuh );
        }
    }
}


