/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/util/resourcekind/VersionControlledImpl.java,v 1.2 2006-01-22 22:55:21 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:21 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.util.resourcekind;

import java.util.Set;


public class VersionControlledImpl extends AbstractResourceKind implements VersionControlled {
    
    protected static ResourceKind singleton = null;
    
    /**
     * Factory method.
     */
    static public ResourceKind getInstance() {
        if( singleton == null )
            singleton = new VersionControlledImpl();
        return singleton;
    }
    
    /**
     * Protected constructor
     */
    protected VersionControlledImpl() {
    }

    /**
     * Get the set properties supported by this resource kind.
     * @param filter Q_PROTECTED_ONLY or Q_COMPUTED_ONLY (no filtering if null)
     * @param excludedFeatures array of F_* constants (no filtering if null or empty)
     * @see org.apache.slide.webdav.util.WebdavConstants
     * @see org.apache.slide.webdav.util.DeltavConstants
     * @see org.apache.slide.webdav.util.AclConstants
     * @see org.apache.slide.webdav.util.DaslConstants
     */
    public Set getSupportedLiveProperties( String[] excludedFeatures ) {
        Set s = super.getSupportedLiveProperties( excludedFeatures );
        if( isSupportedFeature(F_VERSION_CONTROL, excludedFeatures) )
            s.add( P_AUTO_VERSION );
        if( isSupportedFeature(F_VERSION_HISTORY, excludedFeatures) )
            s.add( P_VERSION_HISTORY );
        return s;
    }

    /**
     * Get the set methods supported by this resource kind.
     */
    public Set getSupportedMethods() {
        Set s = super.getSupportedMethods();
        if( isSupportedFeature(F_VERSION_CONTROL) )
            s.add( M_VERSION_CONTROL );
        if( isSupportedFeature(F_MERGE) )
            s.add( M_MERGE );
        return s;
    }

    /**
     * Get the set reports supported by this resource kind.
     */
    public Set getSupportedReports() {
        Set s = super.getSupportedReports();
        if( isSupportedFeature(F_VERSION_CONTROL) )
            s.add( R_VERSION_TREE );
        return s;
    }
}

