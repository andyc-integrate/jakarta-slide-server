/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/WebdavMethodFactory.java,v 1.2 2006-01-22 22:49:06 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:06 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav;

import org.apache.slide.webdav.method.DefaultMethodFactory;

/**
 * Factory encapsulating the creation of WebdavMethod implementations.
 *
 */
public abstract class WebdavMethodFactory {
    
    
    // ----------------------------------------------------- Instance Variables
    
    // --------------------------------------------------------- Static Methods
    
    /**
     * Creates a new instance of a WebdavMethodFactory implementation.
     * 
     * @param config    configuration of the WebDAV servlet
     */
    public static WebdavMethodFactory newInstance(WebdavServletConfig config) {
        
        WebdavMethodFactory factory = null;
        String className = config.getMethodFactory();
        if (className != null) {
            try {
                Class factoryClass = Class.forName(className);
                factory = (WebdavMethodFactory)factoryClass.newInstance();
            } catch (Exception e) {
                // TOO BAD, we'll use the default method factory instead
            }
        }
        
        if (factory == null) {
            factory = new DefaultMethodFactory();
        }
        factory.setConfig(config);
        
        return factory;
    }
    
    
    // ------------------------------------------------ Public Abstract Methods
    
    
    /**
     * Creates a WebdavMethod implementation for the given method name.
     * 
     * @param name      WebDAV/HTTP method name ("GET", "PROPFIND", etc.)
     * @return      the corresponding WebdavMethod implementation, or 
     *              <tt>null</tt> if the method is unknown or unsupported
     */
    public abstract WebdavMethod createMethod(String name);
    
    
    // --------------------------------------------- Protected Abstract Methods
    
    
    /**
     * Returns the configuration of the WebDAV servlet.
     * 
     * @return configuration of the WebDAV servlet
     */
    protected abstract void setConfig(WebdavServletConfig config);
    
    
}

