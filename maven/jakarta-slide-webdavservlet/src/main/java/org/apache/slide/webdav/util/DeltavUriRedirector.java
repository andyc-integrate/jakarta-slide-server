/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/util/DeltavUriRedirector.java,v 1.2 2006-01-22 22:55:20 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:20 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.util;

import org.apache.slide.content.NodeRevisionNumber;
import org.apache.slide.util.Configuration;

/**
 * Redirects version URIs to the associated histoy URI.
 *
 */
public class DeltavUriRedirector {
    
    /**
     *
     */
    public static String redirectUri( String uri ) {
        String result = uri;
        
        if( Configuration.useVersionControl() ) {
            UriHandler rUh = UriHandler.getUriHandler( uri );
            if( rUh.isVersionUri() )
                result = rUh.getAssociatedHistoryUri();
        }

        return result;
    }
    
    /**
     *
     */
    public static NodeRevisionNumber redirectLatestRevisionNumber( String uri ) {
        NodeRevisionNumber result = null;

        if( Configuration.useVersionControl() ) {
            UriHandler rUh = UriHandler.getUriHandler( uri );
            if( rUh.isHistoryUri() ) {
                result = NodeRevisionNumber.HIDDEN_0_0;
            }
            else if( rUh.isVersionUri() ) {
                result = new NodeRevisionNumber( rUh.getVersionName() );
            }
        }
        
        return result;
    }
}

