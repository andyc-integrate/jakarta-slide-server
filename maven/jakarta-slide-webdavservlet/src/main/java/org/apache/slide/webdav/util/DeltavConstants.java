/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/util/DeltavConstants.java,v 1.2 2006-01-22 22:55:20 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:20 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * DeltaV constants.
 *
 */
public interface DeltavConstants extends WebdavConstants {

    /** Internal constants */
    String I_NEXT_HISTORY_NAME                                            = "next-history-name"; //property
    String I_INITIAL_HISTORY_NAME                                         = "1";
    String I_NEXT_WORKINGRESOURCE_NAME                                    = "next-workingresource-name"; //property
    String I_INITIAL_WORKINGRESOURCE_NAME                                 = "1";
    String I_CHECKIN_LOCKTOKEN                                            = "checkin-locktoken";
    String I_HISTORYPATH                                                  = "historypath";
    String I_HISTORYPATH_DEFAULT                                          = "/history";
    String I_WORKSPACEPATH                                                = "workspacepath";
    String I_WORKSPACEPATH_DEFAULT                                        = "/workspace";
    String I_WORKINGRESOURCEPATH                                          = "workingresourcepath";
    String I_WORKINGRESOURCEPATH_DEFAULT                                  = "/workingresource";
    String I_AUTO_VERSION                                                 = "auto-version";
    String I_AUTO_VERSION_DEFAULT                                         = "checkout-checkin";
    String I_AUTO_VERSION_CONTROL                                         = "auto-version-control";
    String I_AUTO_VERSION_CONTROL_DEFAULT                                 = "false";
    String I_CHECKOUT_FORK                                                = "checkout-fork";
    String I_CHECKOUT_FORK_DEFAULT                                        = "forbidden";
    String I_CHECKIN_FORK                                                 = "checkin-fork";
    String I_CHECKIN_FORK_DEFAULT                                         = "forbidden";
    String I_STORE_PLACE_HOLDER_IN_PATH                                   = "${store}";
    String I_VERSIONCONTROL_EXCLUDEPATH                                   = "versioncontrol-exclude";
    String I_VERSIONCONTROL_EXCLUDEPATH_DEFAULT                           = "";



    /** Features */
    String F_ACTIVITY                                                     = "activity";
    String F_BASELINE                                                     = "baseline";
    String F_CHECKOUT_IN_PLACE                                            = "checkout-in-place";
    String F_LABEL                                                        = "label";
    String F_MERGE                                                        = "merge";
    String F_UPDATE                                                       = "update";
    String F_VERSION_CONTROL                                              = "version-control";
    String F_VERSION_CONTROLLED_COLLECTION                                = "version-controlled-collection";
    String F_VERSION_HISTORY                                              = "version-history";
    String F_WORKING_RESOURCE                                             = "working-resource";
    String F_WORKSPACE                                                    = "workspace";

    /** Headers */
    String H_LABEL                                                        = "Label";

    /** XML Elements */
    String E_ACTIVITY                                                     = "activity";
    String E_APPLY_TO_VERSION                                             = "apply-to-version";
    String E_ADD                                                          = "add";
    String E_CHECKIN                                                      = "checkin";
    String E_CHECKIN_FORK                                                 = "checkin-fork";
    String E_CHECKIN_RESPONSE                                             = "checkin-response";
    String E_CHECKOUT                                                     = "checkout";
    String E_CHECKOUT_IGNORE_UNLOCK                                       = "checkout-ignore-unlock";
    String E_CHECKOUT_CHECKIN                                             = "checkout-checkin";
    String E_CHECKOUT_FORK                                                = "checkout-fork";
    String E_CHECKOUT_RESPONSE                                            = "checkout-response";
    String E_CHECKOUT_UNLOCKED_CHECKIN                                    = "checkout-unlocked-checkin";
    String E_DISCOURAGED                                                  = "discouraged";
    String E_FORBIDDEN                                                    = "forbidden";
    String E_FORK_OK                                                      = "fork-ok";
    String E_KEEP_CHECKED_OUT                                             = "keep-checked-out";
    String E_LOCKED_CHECKOUT                                              = "locked-checkout";
    String E_LABEL                                                        = "label";
    String E_LABEL_NAME                                                   = "label-name";
    String E_MKWORKSPACE                                                  = "mkworkspace";
    String E_MKWORKSPACE_RESPONSE                                         = "mkworkspace-response";
    String E_NAME                                                         = "name";
    String E_OPTIONS                                                      = "options";
    String E_OPTIONS_RESPONSE                                             = "options-response";
    String E_PROPERTY                                                     = "property";
    String E_SUPPORTED_LIVE_PROPERTY                                      = "supported-live-property";
    String E_SUPPORTED_METHOD                                             = "supported-method";
    String E_SUPPORTED_REPORT                                             = "supported-report";
    String E_UNCHECKOUT                                                   = "uncheckout";
    String E_UNCHECKOUT_RESPONSE                                          = "uncheckout-response";
    String E_UPDATE                                                       = "update";
    String E_VERSION                                                      = "version";
    String E_VERSION_CONTROL                                              = "version-control";
    String E_VERSION_CONTROL_RESPONSE                                     = "version-control-response";
    String E_VERSION_HISTORY                                              = "version-history";
    String E_VERSION_HISTORY_COLLECTION_SET                               = "version-history-collection-set";
    String E_VERSION_HISTORY_SET                                          = "version-history-set";
    String E_WORKSPACE_COLLECTION_SET                                     = "workspace-collection-set";

    /** XML Attributes */
    String A_NAME                                                         = "name";
    String A_NAMESPACE                                                    = "namespace";

    /** Property filters */
    String Q_PROTECTED_ONLY                                               = "protected-only";
    String Q_COMPUTED_ONLY                                                = "computed-only";

    /** Live Properties */
    String P_ACTIVITY_CHECKOUT_SET                                        = "activity-checkout-set";
    String P_ACTIVITY_SET                                                 = "activity-set";
    String P_ACTIVITY_VERSION_SET                                         = "activity-version-set";
    String P_AUTO_MERGE_SET                                               = "auto-merge-set";
    String P_AUTO_UPDATE                                                  = "auto-update";
    String P_AUTO_VERSION                                                 = "auto-version";
    String P_BASELINE_COLLECTION                                          = "baseline-collection";
    String P_BASELINE_CONTROLLED_COLLECTION                               = "baseline-controlled-collection";
    String P_BASELINE_CONTROLLED_COLLECTION_SET                           = "baseline-controlled-collection-set";
    String P_CHECKED_IN                                                   = "checked-in";
    String P_CHECKED_OUT                                                  = "checked-out";
    String P_CHECKIN_FORK                                                 = "checkin-fork";
    String P_CHECKOUT_FORK                                                = "checkout-fork";
    String P_CHECKOUT_SET                                                 = "checkout-set";
    String P_COMMENT                                                      = "comment";
    String P_CREATOR_DISPLAYNAME                                          = "creator-displayname";
    String P_CURRENT_ACTIVITY_SET                                         = "current-activity-set";
    String P_CURRENT_WORKSPACE_SET                                        = "current-workspace-set";
    String P_ECLIPSED_SET                                                 = "eclipsed-set";
    String P_LABEL_NAME_SET                                               = "label-name-set";
    String P_MERGE_SET                                                    = "merge-set";
    String P_PREDECESSOR_SET                                              = "predecessor-set";
    String P_ROOT_VERSION                                                 = "root-version";
    String P_SUBACTIVITY_SET                                              = "subactivity-set";
    String P_SUBBASELINE_SET                                              = "subbaseline-set";
    String P_SUCCESSOR_SET                                                = "successor-set";
    String P_SUPPORTED_LIVE_PROPERTY_SET                                  = "supported-live-property-set";
    String P_SUPPORTED_METHOD_SET                                         = "supported-method-set";
    String P_SUPPORTED_REPORT_SET                                         = "supported-report-set";
    String P_UNRESERVED                                                   = "unreserved";
    String P_VERSION_CONTROLLED_BINDING_SET                               = "version-controlled-binding-set";
    String P_VERSION_CONTROLLED_CONFIGURATION                             = "version-controlled-configuration";
    String P_VERSION_HISTORY                                              = "version-history";
    String P_VERSION_NAME                                                 = "version-name";
    String P_VERSION_SET                                                  = "version-set";
    String P_WORKSPACE                                                    = "workspace";
    String P_WORKSPACE_CHECKOUT_SET                                       = "workspace-checkout-set";

    String[] DELTAV_PROPERTIES = new String[] {
        P_ACTIVITY_CHECKOUT_SET,
            P_ACTIVITY_SET,
            P_ACTIVITY_VERSION_SET,
            P_AUTO_MERGE_SET,
            P_AUTO_UPDATE,
            P_AUTO_VERSION,
            P_BASELINE_COLLECTION,
            P_BASELINE_CONTROLLED_COLLECTION,
            P_BASELINE_CONTROLLED_COLLECTION_SET,
            P_CHECKED_IN,
            P_CHECKED_OUT,
            P_CHECKIN_FORK,
            P_CHECKOUT_FORK,
            P_CHECKOUT_SET,
            P_COMMENT,
            P_CREATOR_DISPLAYNAME,
            P_CURRENT_ACTIVITY_SET,
            P_CURRENT_WORKSPACE_SET,
            P_ECLIPSED_SET,
            P_LABEL_NAME_SET,
            P_MERGE_SET,
            P_PREDECESSOR_SET,
            P_ROOT_VERSION,
            P_SUBACTIVITY_SET,
            P_SUBBASELINE_SET,
            P_SUCCESSOR_SET,
            P_SUPPORTED_LIVE_PROPERTY_SET,
            P_SUPPORTED_METHOD_SET,
            P_SUPPORTED_REPORT_SET,
            P_UNRESERVED,
            P_VERSION_CONTROLLED_BINDING_SET,
            P_VERSION_CONTROLLED_CONFIGURATION,
            P_VERSION_HISTORY,
            P_VERSION_NAME,
            P_VERSION_SET,
        P_WORKSPACE,
        P_WORKSPACE_CHECKOUT_SET
    };

    List DELTAV_PROPERTY_LIST = Collections.unmodifiableList(Arrays.asList(DELTAV_PROPERTIES));

    /** Methods */
    String M_BASELINE_CONTROL                                             = "BASELINE-CONTROL";
    String M_CHECKIN                                                      = "CHECKIN";
    String M_CHECKOUT                                                     = "CHECKOUT";
    String M_LABEL                                                        = "LABEL";
    String M_MERGE                                                        = "MERGE";
    String M_MKACTIVITY                                                   = "MKACTIVITY";
    String M_MKWORKSPACE                                                  = "MKWORKSPACE";
    String M_REPORT                                                       = "REPORT";
    String M_UNCHECKOUT                                                   = "UNCHECKOUT";
    String M_UPDATE                                                       = "UPDATE";
    String M_VERSION_CONTROL                                              = "VERSION-CONTROL";

    /** Reports */
    String R_COMPARE_BASELINE                                             = "compare-baseline";
    String R_EXPAND_PROPERTY                                              = "expand-property";
    String R_LATEST_ACTIVITY_VERSION                                      = "latest-activity-version";
    String R_LOCATE_BY_HISTORY                                            = "locate-by-history";
    String R_MERGE_PREVIEW                                                = "merge-preview";
    String R_VERSION_TREE                                                 = "version-tree";

    /** Pre- and postconditions */
    String C_ACTIVITY_CHECKIN                                             = "activity-checkin";
    String C_ACTIVITY_LOCATION_OK                                         = "activity-location-ok";
    String C_ADD_MUST_BE_NEW_LABEL                                        = "add-must-be-new-label";
    String C_ADD_OR_SET_LABEL                                             = "add-or-set-label";
    String C_ADD_TO_HISTORY                                               = "add-to-history";
    String C_ANCESTOR_VERSION                                             = "ancestor-version";
    String C_APPLY_REQUEST_TO_LABELED_VERSION                             = "apply-request-to-labeled-version";
    String C_ATOMIC_ACTIVITY_CHECKIN                                      = "atomic-activity-checkin";
    String C_AUTO_CHECKIN                                                 = "auto-checkin";
    String C_AUTO_CHECKOUT                                                = "auto-checkout";
    String C_AUTO_CHECKOUT_CHECKIN                                        = "auto-checkout-checkin";
    String C_AUTO_UPDATE                                                  = "auto-update";
    String C_BASELINE_CONTROLLED_MEMBERS_MUST_BE_CHECKED_IN               = "baseline-controlled-members-must-be-checked-in";
    String C_BASELINES_FROM_SAME_HISTORY                                  = "baselines-from-same-history";
    String C_CANCEL_CHECKED_OUT                                           = "cancel-checked-out";
    String C_CANNOT_ADD_TO_EXISTING_HISTORY                               = "cannot-add-to-existing-history";
    String C_CANNOT_COPY_COLLECTION_VERSION                               = "cannot-copy-collection-version";
    String C_CANNOT_COPY_HISTORY                                          = "cannot-copy-history";
    String C_CANNOT_MERGE_CHECKED_OUT_RESOURCE                            = "cannot-merge-checked-out-resource";
    String C_CANNOT_MODIFY_CHECKED_IN_PARENT                              = "cannot-modify-checked-in-parent";
    String C_CANNOT_MODIFY_DESTINATION_CHECKED_IN_PARENT                  = "cannot-modify-destination-checked-in-parent";
    String C_CANNOT_MODIFY_PROTECTED_PROPERTY                             = "cannot-modify-protected-property";
    String C_CANNOT_MODIFY_VERSION                                        = "cannot-modify-version";
    String C_CANNOT_MODIFY_VERSION_CONTROLLED_CONFIGURATION               = "cannot-modify-version-controlled-configuration";
    String C_CANNOT_MODIFY_VERSION_CONTROLLED_CONTENT                     = "cannot-modify-version-controlled-content";
    String C_CANNOT_MODIFY_VERSION_CONTROLLED_PROPERTY                    = "cannot-modify-version-controlled-property";
    String C_CANNOT_RENAME_HISTORY                                        = "cannot-rename-history";
    String C_CANNOT_RENAME_VERSION                                        = "cannot-rename-version";
    String C_CANNOT_RENAME_WORKING_RESOURCE                               = "cannot-rename-working-resource";
    String C_CHECKED_IN                                                   = "checked-in";
    String C_CHECKED_OUT_FOR_MERGE                                        = "checked-out-for-merge";
    String C_CHECKIN_ACTIVITY                                             = "checkin-activity";
    String C_CHECKIN_FORK_DISCOURAGED                                     = "checkin-fork-discouraged";
    String C_CHECKIN_FORK_FORBIDDEN                                       = "checkin-fork-forbidden";
    String C_CHECKOUT_NOT_ALLOWED                                         = "checkout-not-allowed";
    String C_CHECKOUT_OF_CHECKED_OUT_VERSION_IS_DISCOURAGED               = "checkout-of-checked-out-version-is-discouraged";
    String C_CHECKOUT_OF_CHECKED_OUT_VERSION_IS_FORBIDDEN                 = "checkout-of-checked-out-version-is-forbidden";
    String C_CHECKOUT_OF_VERSION_WITH_DESCENDANT_IS_DISCOURAGED           = "checkout-of-version-with-descendant-is-discouraged";
    String C_CHECKOUT_OF_VERSION_WITH_DESCENDANT_IS_FORBIDDEN             = "checkout-of-version-with-descendant-is-forbidden";
    String C_COPY_CREATES_NEW_RESOURCE                                    = "copy-creates-new-resource";
    String C_CREATE_BASELINE_COLLECTION                                   = "create-baseline-collection";
    String C_CREATE_NEW_BASELINE                                          = "create-new-baseline";
    String C_CREATE_VERSION                                               = "create-version";
    String C_CREATE_VERSION_CONTROLLED_CONFIGURATION                      = "create-version-controlled-configuration";
    String C_CREATE_WORKING_RESOURCE                                      = "create-working-resource";
    String C_CREATE_WORKING_RESOURCE_FROM_CHECKED_IN_VERSION              = "create-working-resource-from-checked-in-version";
    String C_DELETE_ACTIVITY_REFERENCE                                    = "delete-activity-reference";
    String C_DELETE_VERSION_REFERENCE                                     = "delete-version-reference";
    String C_DELETE_VERSION_SET                                           = "delete-version-set";
    String C_DELETE_WORKING_RESOURCE                                      = "delete-working-resource";
    String C_DELETE_WORKSPACE_MEMBERS                                     = "delete-workspace-members";
    String C_DEPTH_UPDATE                                                 = "depth-update";
    String C_DESCENDANT_VERSION                                           = "descendant-version";
    String C_INITIALIZE_ACTIVITY                                          = "initialize-activity";
    String C_INITIALIZE_ACTIVITY_SET                                      = "initialize-activity-set";
    String C_INITIALIZE_PREDECESSOR_SET                                   = "initialize-predecessor-set";
    String C_INITIALIZE_UNRESERVED                                        = "initialize-unreserved";
    String C_INITIALIZE_VERSION_CONTENT_AND_PROPERTIES                    = "initialize-version-content-and-properties";
    String C_INITIALIZE_VERSION_CONTROLLED_BINDINGS                       = "initialize-version-controlled-bindings";
    String C_INITIALIZE_VERSION_HISTORY_BINDINGS                          = "initialize-version-history-bindings";
    String C_INITIALIZE_WORKSPACE                                         = "initialize-workspace";
    String C_IS_CHECKED_OUT                                               = "is-checked-out";
    String C_KEEP_CHECKED_OUT                                             = "keep-checked-out";
    String C_LABEL_MUST_EXIST                                             = "label-must-exist";
    String C_LINEAR_ACTIVITY                                              = "linear-activity";
    String C_MERGE_BASELINE                                               = "merge-baseline";
    String C_MERGE_MUST_BE_COMPLETE                                       = "merge-must-be-complete";
    String C_MERGE_SUBBASELINES                                           = "merge-subbaselines";
    String C_MODIFY_CONFIGURATION                                         = "modify-configuration";
    String C_MUST_BE_ACTIVITY                                             = "must-be-activity";
    String C_MUST_BE_BASELINE                                             = "must-be-baseline";
    String C_MUST_BE_CHECKED_IN                                           = "must-be-checked-in";
    String C_MUST_BE_CHECKED_OUT                                          = "must-be-checked-out";
    String C_MUST_BE_CHECKED_IN_VERSION_CONTROLLED_RESOURCE               = "must-be-checked-in-version-controlled-resource"; // NOT in RFC3253
    String C_MUST_BE_CHECKED_OUT_VERSION_CONTROLLED_RESOURCE              = "must-be-checked-out-version-controlled-resource";
    String C_MUST_BE_VERSION                                              = "must-be-version";
    String C_MUST_BE_NEW_LABEL                                            = "must-be-new-label";
    String C_MUST_BE_VERSION_HISTORY                                      = "must-be-version-history";
    String C_MUST_HAVE_NO_VERSION_CONTROLLED_MEMBERS                      = "must-have-no-version-controlled-members";
    String C_MUST_NOT_CHANGE_EXISTING_CHECKED_IN_OUT                      = "must-not-change-existing-checked-in-out";
    String C_MUST_NOT_COPY_VERSIONING_PROPERTY                            = "must-not-copy-versioning-property";
    String C_MUST_NOT_HAVE_LABEL_AND_APPLY_TO_VERSION                     = "must-not-have-label-and-apply-to-version";
    String C_MUST_NOT_UPDATE_BASELINE_COLLECTION                          = "must-not-update-baseline-collection";
    String C_MUST_SELECT_VERSION_IN_HISTORY                               = "must-select-version-in-history";
    String C_NEW_VERSION_CONTROLLED_COLLECTION                            = "new-version-controlled-collection";
    String C_NEW_VERSION_CONTROLLED_RESOURCE                              = "new-version-controlled-resource";
    String C_NEW_VERSION_HISTORY                                          = "new-version-history";
    String C_NO_CHECKED_OUT_BASELINE_CONTROLLED_COLLECTION_MEMBERS        = "no-checked-out-baseline-controlled-collection-members";
    String C_NO_MODIFICATION                                              = "no-modification";
    String C_NO_OVERWRITE_BY_AUTO_UPDATE                                  = "no-overwrite-by-auto-update";
    String C_NO_VERSION_DELETE                                            = "no-version-delete";
    String C_ONE_BASELINE_CONTROLLED_COLLECTION_PER_HISTORY_PER_WORKSPACE = "one-baseline-controlled-collection-per-history-per-workspace";
    String C_ONE_CHECKOUT_PER_ACTIVITY_PER_HISTORY                        = "one-checkout-per-activity-per-history";
    String C_ONE_VERSION_CONTROLLED_RESOURCE_PER_HISTORY_PER_WORKSPACE    = "one-version-controlled-resource-per-history-per-workspace";
    String C_ONE_VERSION_PER_HISTORY_PER_BASELINE                         = "one-version-per-history-per-baseline";
    String C_PRESERVE_VERSIONING_PROPERTIES                               = "preserve-versioning-properties";
    String C_PUT_UNDER_VERSION_CONTROL                                    = "put-under-version-control";
    String C_REFERENCE_VERSION_CONTROLLED_CONFIGURATION                   = "reference-version-controlled-configuration";
    String C_REMOVE_LABEL                                                 = "remove-label";
    String C_REPORT_PROPERTIES                                            = "report-properties";
    String C_RESOURCE_MUST_BE_NULL                                        = "resource-must-be-null";
    String C_RESTORE_CONTENT_AND_DEAD_PROPERTIES                          = "restore-content-and-dead-properties";
    String C_SELECT_EXISTING_BASELINE                                     = "select-existing-baseline";
    String C_SET_BASELINE_CONTROLLED_COLLECTION_MEMBERS                   = "set-baseline-controlled-collection-members";
    String C_SUPPORTED_LIVE_PROPERTY                                      = "supported-live-property";
    String C_SUPPORTED_REPORT                                             = "supported-report";
    String C_UPDATE_ACTIVITY_REFERENCE                                    = "update-activity-reference";
    String C_UPDATE_AUTO_UPDATE                                           = "update-auto-update";
    String C_UPDATE_CHECKED_OUT_REFERENCE                                 = "update-checked-out-reference";
    String C_UPDATE_CONTENT_AND_PROPERTIES                                = "update-content-and-properties";
    String C_UPDATE_MERGE_SET                                             = "update-merge-set";
    String C_UPDATE_PREDECESSOR_SET                                       = "update-predecessor-set";
    String C_UPDATE_SUBBASELINES                                          = "update-subbaselines";
    String C_UPDATE_VERSION_CONTROLLED_COLLECTION_MEMBERS                 = "update-version-controlled-collection-members";
    String C_UPDATE_WORKSPACE_REFERENCE                                   = "update-workspace-reference";
    String C_VERSION_CONTROL_WORKING_COLLECTION_MEMBERS                   = "version-control-working-collection-members";
    String C_VERSION_CONTROLLED_CONFIGURATION_MUST_NOT_EXIST              = "version-controlled-configuration-must-not-exist";
    String C_VERSION_HISTORY_HAS_ROOT                                     = "version-history-has-root";
    String C_VERSION_HISTORY_IS_TREE                                      = "version-history-is-tree";
    String C_WORKSPACE_LOCATION_OK                                        = "workspace-location-ok";
    String C_WORKSPACE_MEMBER_MOVED                                       = "workspace-member-moved";
    String C_WORKSPACE_MOVED                                              = "workspace-moved";
}

