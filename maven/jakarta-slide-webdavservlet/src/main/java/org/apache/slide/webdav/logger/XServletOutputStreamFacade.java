/*
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


package org.apache.slide.webdav.logger;

import java.io.IOException;

import javax.servlet.ServletOutputStream;

import org.apache.slide.common.Domain;

/**
 *
 */
public class XServletOutputStreamFacade extends ServletOutputStream {

    
    public static final String DEFAULT_CHAR_ENCODING = "8859_1";
    public static final byte[] bCRLF = (System.getProperty("line.separator")).getBytes();

    ServletOutputStream prStream;
    //PrintStream prStream;
    XByteBuffer bb;
    
    /** Encoding - first time print() is used.
    IMPORTANT: print() is _bad_, if you want to write Strings and mix
    bytes you'd better use a real writer ( it's faster ).
    But _don't_ use the servlet writer - since you'll not be able to write
    bytes.
    */
    String enc;
    
    protected XServletOutputStreamFacade( ServletOutputStream soStream, String enc) {
        Domain.debug("Create XServletOutputStreamFacade");
//      this.prStream = new PrintStream( soStream, true );
//      if ( DEFAULT_CHAR_ENCODING.equals(enc) ) {
//          this.enc = null;
//      } else {
//          this.enc = enc;
//      }
        this.prStream = soStream;
        bb = new XByteBuffer();
    }

    // -------------------- Write methods --------------------
    
    public void write(int i) throws IOException {
        Domain.debug("XServletOutputStreamFacade:write(int)");
        bb.write(i);
        prStream.write(i);
    }

    public void write(byte[] b) throws IOException {
        Domain.debug("XServletOutputStreamFacade:write(byte[])");
        write(b,0,b.length);
    }
    
    public void write(byte[] b, int off, int len) throws IOException {
        Domain.debug("XServletOutputStreamFacade:write(byte[] b, int off, int len)");
        bb.write( b, off, len );
        prStream.write(b, off, len);
    }

    // -------------------- Servlet Output Stream methods --------------------
    
    /** Alternate implementation for print, using String.getBytes(enc).
    It seems to be a bit faster for small strings, but it's 10..20% slower
    for larger texts ( nor very large - 5..10k )

    That seems to be mostly because of byte b[] - the writer has an
    internal ( and fixed ) buffer.

    Please use getWriter() if you want to send strings.
    */
    public void print(String s) throws IOException {
        Domain.debug("XServletOutputStreamFacade:print(" + s + " )");
        if (s==null) {
            s="null";
        }
        byte b[]=null;
        if( enc==null) {
            b=s.getBytes();
        } else {
            try {
                b=s.getBytes( enc );
            } catch (java.io.UnsupportedEncodingException ex) {
                b=s.getBytes();
            enc=null;
            }
        }
        write( b );
        prStream.print( s );
    }
    
    public void println() throws IOException {
        Domain.debug("XServletOutputStreamFacade:println()");
        write(bCRLF);
        prStream.println();
    }

    public void println(String s) throws IOException {
        Domain.debug("XServletOutputStreamFacade:println(" + s + " )");
        if (s==null) {
            s="null";
        }
        byte b[]=null;
        if( enc==null) {
            b=s.getBytes();
        } else {
            try {
                b=s.getBytes( enc );
            } catch (java.io.UnsupportedEncodingException ex) {
                b=s.getBytes();
            enc=null;
            }
        }
        write(b);
        write(bCRLF);
        prStream.println(s);
    }

    /** Will send the buffer to the client.
     */
    public void flush() throws IOException {
        Domain.debug("XServletOutputStreamFacade:flush()");
        prStream.flush();
    }

    public void close() throws IOException {
        Domain.debug("XServletOutputStreamFacade:close()");
        prStream.close();
    }

    /**
     * Returns the content of the byte buffer as string.
     *
     * @return      content of the byte buffer as string.
     */
    public String getOutputBuffer() throws IOException {
        return bb.getBufferContent();
    }

    
}

