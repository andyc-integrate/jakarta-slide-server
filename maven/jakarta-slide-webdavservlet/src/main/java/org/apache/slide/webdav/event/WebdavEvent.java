/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/event/WebdavEvent.java,v 1.2 2006-01-22 22:55:20 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:20 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.event;

import java.util.EventListener;
import java.util.EventObject;

import org.apache.slide.event.AbstractEventMethod;
import org.apache.slide.event.VetoException;
import org.apache.slide.event.VetoableEventMethod;

/**
 * Webdav event class
 *
 * @version $Revision: 1.2 $
 */
public final class WebdavEvent extends EventObject {
    public static Get GET = new Get();
    public static Put PUT = new Put();
    public static PropFind PROPFIND = new PropFind();
    public static PropPatch PROPPATCH = new PropPatch();
    public static Bind BIND = new Bind();
    public static Rebind REBIND = new Rebind();
    public static Unbind UNBIND = new Unbind();
    public static Mkcol MKCOL = new Mkcol();
    public static Copy COPY = new Copy();
    public static Move MOVE = new Move();
    public static Delete DELETE = new Delete();
    public static Lock LOCK = new Lock();
    public static Unlock UNLOCK = new Unlock();
    public static Acl ACL = new Acl();
    public static Report REPORT = new Report();
    public static Search SEARCH = new Search();
    public static VersionControl VERSION_CONTROL = new VersionControl();
    public static Options OPTIONS = new Options();
    public static Checkin CHECKIN = new Checkin();
    public static Checkout CHECKOUT= new Checkout();
    public static Uncheckout UNCHECKOUT = new Uncheckout();
    public static Update UPDATE = new Update();
    public static Label LABEL = new Label();
    public static Mkworkspace MKWORKSPACE = new Mkworkspace();
    public static Subscribe SUBSCRIBE = new Subscribe();
    public static Unsubscribe UNSUBSCRIBE = new Unsubscribe();
    public static Poll POLL = new Poll();

    public final static String GROUP = "webdav";
    public final static AbstractEventMethod[] methods = new AbstractEventMethod[] { GET, PUT, PROPFIND, PROPPATCH, BIND,
                                                                                    UNBIND, REBIND, MKCOL, COPY, MOVE, DELETE, LOCK, UNLOCK, ACL,
                                                                                    REPORT, SEARCH, VERSION_CONTROL, OPTIONS, CHECKIN, CHECKOUT, UNCHECKOUT,
                                                                                    UPDATE, LABEL, MKWORKSPACE, SUBSCRIBE, UNSUBSCRIBE, POLL };

    public WebdavEvent(Object source) {
        super(source);
    }

    public AbstractEventMethod[] getMethods() {
        return methods;
    }

    public String getGroup() {
        return GROUP;
    }

    public final static class Get extends VetoableEventMethod {
        public Get() {
            super(GROUP, "get");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof WebdavListener ) ((WebdavListener)listener).get((WebdavEvent)event);
        }
    }

    public final static class Put extends VetoableEventMethod {
        public Put() {
            super(GROUP, "put");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof WebdavListener ) ((WebdavListener)listener).put((WebdavEvent)event);
        }
    }

    public final static class PropFind extends VetoableEventMethod {
        public PropFind() {
            super(GROUP, "propfind");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof WebdavListener ) ((WebdavListener)listener).propFind((WebdavEvent)event);
        }
    }

    public final static class PropPatch extends VetoableEventMethod {
        public PropPatch() {
            super(GROUP, "proppatch");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof WebdavListener ) ((WebdavListener)listener).propPatch((WebdavEvent)event);
        }
    }

    public final static class Bind extends VetoableEventMethod {
        public Bind() {
            super(GROUP, "bind");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof WebdavListener ) ((WebdavListener)listener).bind((WebdavEvent)event);
        }
   }

    public final static class Rebind extends VetoableEventMethod {
        public Rebind() {
            super(GROUP, "rebind");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof WebdavListener ) ((WebdavListener)listener).rebind((WebdavEvent)event);
        }
    }

    public final static class Unbind extends VetoableEventMethod {
        public Unbind() {
            super(GROUP, "unbind");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof WebdavListener ) ((WebdavListener)listener).unbind((WebdavEvent)event);
        }
    }

    public final static class Mkcol extends VetoableEventMethod {
        public Mkcol() {
            super(GROUP, "mkcol");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof WebdavListener ) ((WebdavListener)listener).mkcol((WebdavEvent)event);
        }
    }

    public final static class Copy extends VetoableEventMethod {
        public Copy() {
            super(GROUP, "copy");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof WebdavListener ) ((WebdavListener)listener).copy((WebdavEvent)event);
        }
    }

    public final static class Move extends VetoableEventMethod {
        public Move() {
            super(GROUP, "move");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof WebdavListener ) ((WebdavListener)listener).move((WebdavEvent)event);
        }
    }

    public final static class Delete extends VetoableEventMethod {
        public Delete() {
            super(GROUP, "delete");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof WebdavListener ) ((WebdavListener)listener).delete((WebdavEvent)event);
        }
    }

    public final static class Lock extends VetoableEventMethod {
        public Lock() {
            super(GROUP, "lock");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof WebdavListener ) ((WebdavListener)listener).lock((WebdavEvent)event);
        }
    }

    public final static class Unlock extends VetoableEventMethod {
        public Unlock() {
            super(GROUP, "unlock");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof WebdavListener ) ((WebdavListener)listener).unlock((WebdavEvent)event);
        }
    }

    public final static class Acl extends VetoableEventMethod {
        public Acl() {
            super(GROUP, "acl");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof WebdavListener ) ((WebdavListener)listener).acl((WebdavEvent)event);
        }
    }

    public final static class Report extends VetoableEventMethod {
        public Report() {
            super(GROUP, "report");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof WebdavListener ) ((WebdavListener)listener).report((WebdavEvent)event);
        }
    }

    public final static class Search extends VetoableEventMethod {
        public Search() {
            super(GROUP, "search");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof WebdavListener ) ((WebdavListener)listener).search((WebdavEvent)event);
        }
    }

    public final static class VersionControl extends VetoableEventMethod {
        public VersionControl() {
            super(GROUP, "version-control");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof WebdavListener ) ((WebdavListener)listener).versionControl((WebdavEvent)event);
        }
    }

    public final static class Options extends VetoableEventMethod {
        public Options() {
            super(GROUP, "options");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof WebdavListener ) ((WebdavListener)listener).options((WebdavEvent)event);
        }
    }

    public final static class Update extends VetoableEventMethod {
        public Update() {
            super(GROUP, "update");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof WebdavListener ) ((WebdavListener)listener).update((WebdavEvent)event);
        }
    }

    public final static class Checkin extends VetoableEventMethod {
        public Checkin() {
            super(GROUP, "checkin");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof WebdavListener ) ((WebdavListener)listener).checkin((WebdavEvent)event);
        }
    }

    public final static class Checkout extends VetoableEventMethod {
        public Checkout() {
            super(GROUP, "checkout");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof WebdavListener ) ((WebdavListener)listener).checkout((WebdavEvent)event);
        }
    }

    public final static class Uncheckout extends VetoableEventMethod {
        public Uncheckout() {
            super(GROUP, "uncheckout");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof WebdavListener ) ((WebdavListener)listener).uncheckout((WebdavEvent)event);
        }
    }

    public final static class Label extends VetoableEventMethod {
        public Label() {
            super(GROUP, "label");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof WebdavListener ) ((WebdavListener)listener).label((WebdavEvent)event);
        }
    }

    public final static class Mkworkspace extends VetoableEventMethod {
        public Mkworkspace() {
            super(GROUP, "mkworkspace");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof WebdavListener ) ((WebdavListener)listener).label((WebdavEvent)event);
        }
    }

    public final static class Subscribe extends VetoableEventMethod {
        public Subscribe() {
            super(GROUP, "subscribe");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof WebdavListener ) ((WebdavListener)listener).subscribe((WebdavEvent)event);
        }
    }

    public final static class Unsubscribe extends VetoableEventMethod {
        public Unsubscribe() {
            super(GROUP, "unsubscribe");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof WebdavListener ) ((WebdavListener)listener).unsubscribe((WebdavEvent)event);
        }
    }

    public final static class Poll extends VetoableEventMethod {
        public Poll() {
            super(GROUP, "poll");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof WebdavListener ) ((WebdavListener)listener).poll((WebdavEvent)event);
        }
    }
}