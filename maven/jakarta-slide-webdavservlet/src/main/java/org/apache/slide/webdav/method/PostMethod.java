/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/method/PostMethod.java,v 1.2 2006-01-22 22:55:20 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:20 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.method;

import java.io.IOException;

import org.apache.slide.common.NamespaceAccessToken;
import org.apache.slide.webdav.WebdavException;
import org.apache.slide.webdav.WebdavServletConfig;
import org.apache.slide.webdav.util.WebdavStatus;

/**
 * POST method.
 *
 */
public class PostMethod extends PutMethod {
    
    
    // ----------------------------------------------------------- Constructors
    
    
    /**
     * Constructor.
     * 
     * @param token     the token for accessing the namespace
     * @param config    configuration of the WebDAV servlet
     */
    public PostMethod(NamespaceAccessToken token, WebdavServletConfig config) {
        super(token, config);
    }
    
    
    protected void executeRequest()
        throws WebdavException, IOException {
        
        if (!isCollection(requestUri)) {
            super.executeRequest();
        }
        else {
            int statusCode = WebdavStatus.SC_CONFLICT;
            sendError( statusCode, getClass().getName()+".mustNotBeCollection" );
            throw new WebdavException( statusCode );
        }

    }
    
}
