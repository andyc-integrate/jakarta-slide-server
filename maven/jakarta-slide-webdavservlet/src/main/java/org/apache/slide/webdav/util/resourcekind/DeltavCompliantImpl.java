/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/util/resourcekind/DeltavCompliantImpl.java,v 1.2 2006-01-22 22:55:21 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:21 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.util.resourcekind;

import java.util.Set;


public class DeltavCompliantImpl extends AbstractResourceKind implements DeltavCompliant {
    
    protected static ResourceKind singleton = null;
    
    /**
     * Factory method.
     */
    static public ResourceKind getInstance() {
        if( singleton == null )
            singleton = new DeltavCompliantImpl();
        return singleton;
    }
    
    /**
     * Protected constructor
     */
    protected DeltavCompliantImpl() {
    }
    
    /**
     * Get the set properties supported by this resource kind.
     * @param filter Q_PROTECTED_ONLY or Q_COMPUTED_ONLY (no filtering if null)
     * @param excludedFeatures array of F_* constants (no filtering if null or empty)
     * @see org.apache.slide.webdav.util.WebdavConstants
     * @see org.apache.slide.webdav.util.DeltavConstants
     * @see org.apache.slide.webdav.util.AclConstants
     * @see org.apache.slide.webdav.util.DaslConstants
     */
    public Set getSupportedLiveProperties( String[] excludedFeatures ) {
        Set s = super.getSupportedLiveProperties( excludedFeatures );
        s.add( P_CREATIONDATE );
        s.add( P_MODIFICATIONDATE );
        s.add( P_DISPLAYNAME );
        s.add( P_GETCONTENTLANGUAGE );
        s.add( P_GETCONTENTLENGTH );
        s.add( P_GETCONTENTTYPE );
        s.add( P_GETETAG );
        s.add( P_GETLASTMODIFIED );
        s.add( P_RESOURCETYPE );
        s.add( P_SOURCE );
        if( isSupportedFeature(F_LOCKING, excludedFeatures) ) {
            s.add( P_LOCKDISCOVERY );
            s.add( P_SUPPORTEDLOCK );
        }
        if( isSupportedFeature(F_ACCESS_CONTROL, excludedFeatures) ) {
            s.add( P_OWNER );
            s.add( P_CREATIONUSER );
            s.add( P_MODIFICATIONUSER );
            s.add( P_SUPPORTED_PRIVILEGE_SET );
            s.add( P_CURRENT_USER_PRIVILEGE_SET );
            s.add( P_ACL );
            s.add( P_ACL_RESTRICTIONS );
            s.add( P_INHERITED_ACL_SET );
            s.add( P_PRINCIPAL_COLLECTION_SET );
            s.add( P_PRIVILEGE_COLLECTION_SET );
        }
        if (isSupportedFeature(F_BINDING, excludedFeatures)) {
            s.add( P_RESOURCE_ID );
            s.add( P_PARENT_SET );
        }
        if( isSupportedFeature(F_VERSION_CONTROL, excludedFeatures) ) {
            s.add( P_COMMENT );
            s.add( P_CREATOR_DISPLAYNAME );
            s.add( P_SUPPORTED_METHOD_SET );
            s.add( P_SUPPORTED_LIVE_PROPERTY_SET );
            s.add( P_SUPPORTED_REPORT_SET );
        }
        if( isSupportedFeature(F_WORKSPACE, excludedFeatures) )
            s.add( P_WORKSPACE );
        if( isSupportedFeature(F_BASELINE, excludedFeatures) )
            s.add( P_VERSION_CONTROLLED_CONFIGURATION );
        return s;
    }
    
    /**
     * Get the set methods supported by this resource kind.
     */
    public Set getSupportedMethods() {
        Set s = super.getSupportedMethods();
        s.add( M_CONNECT );
        s.add( M_COPY );
        s.add( M_DELETE );
        s.add( M_GET );
        s.add( M_HEAD );
        s.add( M_MOVE );
        s.add( M_OPTIONS );
        s.add( M_POST );
        s.add( M_PROPFIND );
        s.add( M_PROPPATCH );
        s.add( M_PUT );
        s.add( M_TRACE );
        if( isSupportedFeature(F_LOCKING) ) {
            s.add( M_LOCK );
            s.add( M_UNLOCK );
        }
        if( isSupportedFeature(F_ACCESS_CONTROL) )
            s.add( M_ACL );
        if( isSupportedFeature(F_ACCESS_CONTROL) || isSupportedFeature(F_VERSION_CONTROL) )
            s.add( M_REPORT );
        if( isSupportedFeature(F_SEARCHING_AND_LOCATING) ) {
            s.add( M_SEARCH );
        }
        return s;
    }
    
    /**
     * Get the set reports supported by this resource kind.
     */
    public Set getSupportedReports() {
        Set s = super.getSupportedReports();
        if( isSupportedFeature(F_ACCESS_CONTROL) || isSupportedFeature(F_VERSION_CONTROL) )
            s.add( R_EXPAND_PROPERTY );
        return s;
    }
}

