/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/method/report/PrincipalSearchPropertySetReport.java,v 1.2 2006-01-22 22:55:20 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:20 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.method.report;

import java.io.IOException;
import java.util.Iterator;

import org.apache.slide.common.NamespaceAccessToken;
import org.apache.slide.common.NamespaceConfig;
import org.apache.slide.common.RequestedProperty;
import org.apache.slide.common.ServiceAccessException;
import org.apache.slide.common.SlideException;
import org.apache.slide.common.SlideToken;
import org.apache.slide.common.UriPath;
import org.apache.slide.webdav.WebdavServletConfig;
import org.apache.slide.webdav.util.AclConstants;
import org.apache.slide.webdav.util.PreconditionViolationException;
import org.apache.slide.webdav.util.ViolatedPrecondition;
import org.apache.slide.webdav.util.WebdavStatus;
import org.apache.slide.webdav.util.resourcekind.PrincipalImpl;
import org.jdom.Element;


/**
 * DAV:principal-search-property-set report worker.
 *
 */
public class PrincipalSearchPropertySetReport extends AbstractReport implements AclConstants {

    /**
     * Constructor
     *
     * @param    slideToken          a  SlideToken
     * @param    token               a  NamespaceAccessToken
     * @param    config              a  WebdavServletConfig
     * @param    serverUrl           a  String
     * @param    contextPath         a  String
     */
    public PrincipalSearchPropertySetReport(SlideToken slideToken, NamespaceAccessToken token, WebdavServletConfig config, String slideContextPath) {
        super(slideToken, token, config, slideContextPath);
    }

    /**
     * Initialize report worker with specified report element
     *
     * @param    resourcePath        a  String
     * @param    principalSearchPropertySetElm   an Element
     *
     * @throws   PreconditionViolationException
     */
    public void init(String resourcePath, Element principalSearchPropertySetElm) throws PreconditionViolationException {
        if (principalSearchPropertySetElm.getChildren().size() > 0) {
            throw new PreconditionViolationException(
                new ViolatedPrecondition("empty-principal-search-property-set-element",
                                         WebdavStatus.SC_BAD_REQUEST,
                                         "DAV:principal-search-property-set element must be empty"),
                resourcePath
            );
        }
    }

    /**
     * Execute report and add results to given multistatus element
     *
     * @param    resourcePath                       a  String
     * @param    principalSearchPropertySetElm      an Element
     * @param    depth                              an int
     *
     * @throws   SlideException
     * @throws   IOException
     */
    public void execute(String resourcePath, Element principalSearchPropertySetElm, int depth) throws SlideException, IOException {
        Iterator props = retriever.getAllPropertyNames(PrincipalImpl.getInstance())
            .getRequestedProperties();
        while (props.hasNext()) {
            RequestedProperty p = (RequestedProperty)props.next();
            Element principalSearchPropertyElm = new Element(E_PRINCIPAL_SEARCH_PROPERTY, DNSP);
//            Namespace nsp = DNSP.getURI().equals(p.getNamespace())
//                ? DNSP
//                : Namespace.getNamespace(p.getNamespace());
            principalSearchPropertyElm.addContent(new Element(p.getName(), DNSP));
            principalSearchPropertySetElm.addContent(principalSearchPropertyElm);
        }
    }

    /**
     * Method checkPreconditions
     * @param    resourcePath        a  String
     * @param    depth               an int
     * @throws   PreconditionViolationException
     * @throws   ServiceAccessException
     */
    public void checkPreconditions(String resourcePath, int depth) throws PreconditionViolationException, ServiceAccessException {
        if (depth != 0) {
            throw new PreconditionViolationException(
                new ViolatedPrecondition("depth-must-be-zero",
                                         WebdavStatus.SC_BAD_REQUEST,
                                         "This report is only defined for depth=0."),
                resourcePath
            );
        }
        UriPath resourcepath = new UriPath(resourcePath);
        NamespaceConfig namespaceConfig = token.getNamespaceConfig();
        UriPath userspath = namespaceConfig.getUsersPath() != null
            ? new UriPath(namespaceConfig.getUsersPath())
            : null;
        UriPath groupspath = namespaceConfig.getGroupsPath() != null
            ? new UriPath(namespaceConfig.getGroupsPath())
            : null;
        UriPath rolespath = namespaceConfig.getRolesPath() != null
            ? new UriPath(namespaceConfig.getRolesPath())
            : null;
        if (!resourcepath.equals(userspath) &&
            !resourcepath.equals(groupspath) &&
            !resourcepath.equals(rolespath)) {
            throw new PreconditionViolationException(
                new ViolatedPrecondition("valid-request-uri",
                                         WebdavStatus.SC_BAD_REQUEST,
                                         "This report is only defined for one of the collections identified in the value of the DAV:principal-collection-set property."),
                resourcePath
            );
        }
    }
}
