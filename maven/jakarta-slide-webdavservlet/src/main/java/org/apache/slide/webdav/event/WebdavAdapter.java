/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/event/WebdavAdapter.java,v 1.2 2006-01-22 22:55:20 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:20 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.event;

import org.apache.slide.event.VetoException;

/**
 * Webdav adapter class
 *
 * @version $Revision: 1.2 $
 */
public class WebdavAdapter implements WebdavListener {
    public void get(WebdavEvent event) throws VetoException {
    }

    public void put(WebdavEvent event) throws VetoException {
    }

    public void propFind(WebdavEvent event) throws VetoException {
    }

    public void propPatch(WebdavEvent event) throws VetoException {
    }

    public void bind(WebdavEvent event) throws VetoException {
    }

    public void rebind(WebdavEvent event) throws VetoException {
    }

    public void unbind(WebdavEvent event) throws VetoException {
    }

    public void mkcol(WebdavEvent event) throws VetoException {
    }

    public void copy(WebdavEvent event) throws VetoException {
    }

    public void move(WebdavEvent event) throws VetoException {
    }

    public void delete(WebdavEvent event) throws VetoException {
    }

    public void lock(WebdavEvent event) throws VetoException {
    }

    public void unlock(WebdavEvent event) throws VetoException {
    }

    public void acl(WebdavEvent event) throws VetoException {
    }

    public void report(WebdavEvent event) throws VetoException {
    }

    public void search(WebdavEvent event) throws VetoException {
    }

    public void versionControl(WebdavEvent event) throws VetoException {
    }

    public void options(WebdavEvent event) throws VetoException {
    }

    public void update(WebdavEvent event) throws VetoException {
    }

    public void checkin(WebdavEvent event) throws VetoException {
    }

    public void checkout(WebdavEvent event) throws VetoException {
    }

    public void uncheckout(WebdavEvent event) throws VetoException {
    }

    public void label(WebdavEvent event) throws VetoException {
    }

    public void mkworkspace(WebdavEvent event) throws VetoException {
    }

    public void subscribe(WebdavEvent event) throws VetoException {
    }

    public void unsubscribe(WebdavEvent event) throws VetoException {
    }

    public void poll(WebdavEvent event) throws VetoException {
    }
}