/*
 * $Header: /cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus\040Edit/src/webdav/server/org/apache/slide/webdav/util/UriHandler.java,v 1.3 2006/03/15 10:42:31 peter-cvs Exp $
 * $Revision: 1.3 $
 * $Date: 2006/03/15 10:42:31 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.util;

import org.apache.slide.common.*;
import org.apache.slide.content.*;
import org.apache.slide.content.NodeProperty.NamespaceCache;
import org.apache.slide.event.VetoException;
import org.apache.slide.lock.ObjectLockedException;
import org.apache.slide.security.AccessDeniedException;
import org.apache.slide.structure.*;

import java.util.*;


/**
 * Helper class for the handling of URIs
 */
public class UriHandler implements DeltavConstants, AclConstants, DaslConstants {

    /**
     * When using the history-collection-hack setting, these are the possible folder ID's that
     * paths are seperated into.
     */
    protected static final char[] FOLDER_ID =
            {'b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','y','z'};
    /**
     * This is how many branches each node of the tree may contain. It is equal to the number
     * of possible folder ID's.
     */
    protected static final int BRANCH_FACTOR = FOLDER_ID.length;

    protected static final HashMap ID_TO_NUMBER_MAP = new HashMap();
    static {
        for (int i = 0; i < FOLDER_ID.length; i++) {
            ID_TO_NUMBER_MAP.put(new Character(FOLDER_ID[i]), new Integer(i));
        }
    }

    /**
     * Determines whether the hack that restricts the size of collections in
     * the history collection is configured to be used.
     */
    protected static boolean useHistoryCollectionHack = true;



    /**
     * Sets whether the hack that restricts the size of collections in
     * the history collection is configured to be used.
     */
    public static void setGloballyUseHistoryCollectionHack(boolean useHistoryCollectionHack) {
        // XXX it is truely ugly to do things this way, it would be better to have an Uri factory
        // that gets initialized with this value
        UriHandler.useHistoryCollectionHack = useHistoryCollectionHack;
    }

    /**
     * Factory method.
     */
    public static UriHandler getUriHandler( String resourcePath ) {
        return new UriHandler( resourcePath );
        }

    /**
     * Factory method.
     */
    public static UriHandler
        getUriHandler( NodeRevisionDescriptors nrds, NodeRevisionDescriptor nrd ) {

        StringBuffer b = new StringBuffer();
        String uri = nrds.getUri();
        UriHandler uriHandler = UriHandler.getUriHandler( uri );
        if ( ! uriHandler.isHistoryUri() ) {
            // any resource
            b.append( uri );
        }
        else {
            if( (NodeRevisionNumber.HIDDEN_0_0).equals(nrd.getRevisionNumber()) ) {
                // history resource
                b.append( uri );
            }
            else {
                // version resource
                b.append( uri );
                if( !uri.endsWith("/") ) {
                    b.append( "/" );
                }
                b.append( nrd.getRevisionNumber().toString() );
            }
        }
        return new UriHandler( b.toString() );
    }

    public static StringBuffer convertNumberToCharacterPath(long historyNum, boolean forHistory) {
        StringBuffer retVal = new StringBuffer();
        long quotient = historyNum;
        do {
            // get remainder
            // ok to cast as int since BRANCH_FACTOR will be an int, and the result will be less than BRANCH_FACTOR
            int remainder = (int)(quotient % BRANCH_FACTOR);

            // shift quotient
            quotient /= BRANCH_FACTOR;

            retVal.append(FOLDER_ID[remainder]);
            //System.out.println("char: "+FOLDER_ID[remainder] + " , digit:"+remainder);
            if (quotient == 0) {
                // we append a number just so it can't be confused with a folder name
                retVal.append(remainder);
            } else {
                // we're going round the loop again so add a URL directory seperator
                retVal.append('/');
            }
        } while (quotient != 0);
        //System.out.println(retVal);
        return retVal;
    }

    /**
     * Takes a history number and converts it to a path.
     * @param historyNum The history number
     * @return A string buffer containing a path of the form "x/s/z/a1"
     */
    public static StringBuffer convertNumberToCharacterPath(long historyNum) {
        return convertNumberToCharacterPath(historyNum, true);
    }

    public static long convertCharacterPathToNumber(String path) {
        String[] tokens = path.split("/");
        long runningTotal = 0;
        // loop counts down since most significant digit is last in this format.
        for(int i = tokens.length - 1; i >= 0; i--) {
            // the last token in the list is not just a single character since it is the character with its
            // position in FOLDER_ID appended. This makes sure we do not confuse items with folders.
            if(i == tokens.length - 1) {
                if(tokens[i].length() == 0 || tokens[i].length() == 1)
                    throw new IllegalArgumentException("Given path is not in correct format, last token is not " +
                            "of form [char][number]: "+tokens[i]);
                tokens[i] = ""+tokens[i].charAt(0);
            }
            if(tokens[i].length() != 1)
                throw new IllegalArgumentException("Given path is not in correct format, all members "+
                    "except the final one must be single characters: "+tokens[i]);
            int digit = ((Integer)ID_TO_NUMBER_MAP.get(new Character(tokens[i].charAt(0)))).intValue();
            runningTotal += digit*(long)Math.pow(BRANCH_FACTOR,i);
        }
        return runningTotal;
    }

    /**
     * Generates the next available history URI and returns an URI handler for it.
     * @param sToken the Slide token
     * @param nsaToken the namespace access token
     * @param uh the URI handler of the resource being set under version control
     */
    public static UriHandler createNextHistoryUri(SlideToken sToken,
                                                  NamespaceAccessToken nsaToken,
                                                  UriHandler uh)
            throws ObjectNotFoundException,
                   AccessDeniedException,
                   ObjectLockedException,
                   LinkedObjectNotFoundException,
                   ServiceAccessException,
                   RevisionDescriptorNotFoundException,
                   RevisionNotFoundException,
                   VetoException {

        UriHandler result = null;
        String nsName = nsaToken.getName();
        UriHandler hpathHandler =
            HistoryPathHandler.getResolvedHistoryPathHandler( nsName, uh );
        Content content = nsaToken.getContentHelper();
        String hpath = hpathHandler.toString();

        Structure structure = nsaToken.getStructureHelper();
        String uniqueUri = structure.generateUniqueUri(sToken, hpath);

        if (uniqueUri != null) {
            if (UriHandler.useHistoryCollectionHack) {
                // XXX this is a bit silly: first compose it, now analyze it...
                String nextHnStr = uniqueUri.substring(uniqueUri.lastIndexOf('/') + 1);
                // XXX start with (at least) BRANCH_FACTOR to
                // assure no resources are created directly in history folder
                long nextHnLong = Long.parseLong(nextHnStr) + BRANCH_FACTOR;

                // make hpath/a/b/c out of 1234
                String historyPath = convertNumberToCharacterPath(nextHnLong).toString();

                ensurePathExists(hpath, historyPath, structure, sToken, content);

                String fullPath = hpath + "/" + historyPath;

                return new UriHandler(fullPath);

            } else {
                return new UriHandler(uniqueUri);
            }
        } else {

            NodeRevisionDescriptors hpathNrds = content.retrieve(sToken, hpath);
            NodeRevisionDescriptor hpathNrd = content.retrieve(sToken, hpathNrds);
            NodeProperty nextHnProp = hpathNrd.getProperty(I_NEXT_HISTORY_NAME, NamespaceCache.SLIDE_URI);

            if (UriHandler.useHistoryCollectionHack) {

                if (nextHnProp == null || nextHnProp.getValue() == null) {
                    // XXX start with historyCollectionHackChildren to assure no
                    // resources are created directly in history folder
                    nextHnProp = new NodeProperty(I_NEXT_HISTORY_NAME, BRANCH_FACTOR+"", NamespaceCache.SLIDE_URI);
                    nextHnProp.setKind(NodeProperty.Kind.PROTECTED);
                    hpathNrd.setProperty(nextHnProp);
                }

                String nextHnStr = (String) nextHnProp.getValue();
                long nextHnLong = Long.parseLong(nextHnStr);
                String convertedHPath = convertNumberToCharacterPath(nextHnLong).toString();

                ensurePathExists(hpath, convertedHPath, structure, sToken, content);

                // this code isn't doing what it's supposed to be doing. I think we need to mimic the first
                // branch in this code that makes the directories.
//                if (nextHnLong % BRANCH_FACTOR == 0) {
//                    // create parent collection
//                    int lastSlash = convertedHPath.lastIndexOf('/');
//                    String dirPath;
//                    if(lastSlash != -1)
//                        dirPath = hpath + "/" + convertedHPath.substring(0, convertedHPath.lastIndexOf('/')+1);
//                    else
//                        dirPath = hpath + "/";
//                    try {
//                        structure.create(sToken, new SubjectNode(), dirPath);
//                        content.create(sToken, dirPath, new NodeRevisionDescriptor(0), null);
//                    } catch (ObjectAlreadyExistsException oae) {
//                        Domain.warn("Object " + dirPath + " already exists.");
//                    } catch (RevisionAlreadyExistException rae) {
//                        Domain.warn("Revision " + dirPath + " already exists.");
//                    }
//                }
                String completePath = hpath + "/" + convertedHPath;
                result = new UriHandler(completePath);

                nextHnProp = new NodeProperty(I_NEXT_HISTORY_NAME, String.valueOf(nextHnLong + 1),
                        NamespaceCache.SLIDE_URI);
                hpathNrd.setProperty(nextHnProp);

            } else {

                if (nextHnProp == null) {
                    // convert to slide namespace if this property is still
                    // in DAV: namespace
                    nextHnProp = hpathNrd.getProperty(I_NEXT_HISTORY_NAME);
                    if (nextHnProp != null) {
                        hpathNrd.removeProperty(nextHnProp);
                        nextHnProp = new NodeProperty(I_NEXT_HISTORY_NAME, nextHnProp.getValue(),
                                NamespaceCache.SLIDE_URI);
                        nextHnProp.setKind(NodeProperty.Kind.PROTECTED);
                        hpathNrd.setProperty(nextHnProp);
                    }
                }
                if (nextHnProp == null || nextHnProp.getValue() == null) {
                    nextHnProp = new NodeProperty(I_NEXT_HISTORY_NAME, I_INITIAL_HISTORY_NAME, NamespaceCache.SLIDE_URI);
                    nextHnProp.setKind(NodeProperty.Kind.PROTECTED);
                    hpathNrd.setProperty(nextHnProp);
                }
                String nextHnStr = (String) nextHnProp.getValue();
                long nextHnLong = Long.parseLong(nextHnStr);
                boolean nextExists = true;
                while(nextExists) {

                    // Replaced use of IPS "Exists" slide action with native
                    // slide code to avoid dependency issues. This just tries to
                    // retrieve the specified URI until it finds one that it
                    // can't get - i.e. a free space
                    try {
                        String uri = hpath + "/" + nextHnLong;
                        content.retrieve(sToken, uri);
                    }
                    catch (StructureException e) {

                        // This is the "expected" exception for things that don't exist
                        nextExists = false;
                    }
                    catch (AccessDeniedException e) {

                        // This is bad, but not fatal
                        nextExists = false;

                        // Log the issue
                        System.out.println(e.getMessage());
                        e.printStackTrace();
                    }
                    catch (SlideException e) {

                        // This is bad, but not fatal
                        nextExists = false;

                        // Log the issue
                        System.out.println(e.getMessage());
                        e.printStackTrace();
                    }
                    nextHnLong++;
                }
                result = new UriHandler(hpath + "/" + (nextHnLong - 1));
                nextHnProp = new NodeProperty(I_NEXT_HISTORY_NAME, String.valueOf(nextHnLong),
                        NamespaceCache.SLIDE_URI);
                hpathNrd.setProperty(nextHnProp);

            }
            content.store(sToken, hpath, hpathNrd, null); //revisionContent = null
            return result;
        }
    }

    private static void ensurePathExists(String hpath, String subHPath, Structure structure, SlideToken sToken, Content content) throws ServiceAccessException, LinkedObjectNotFoundException, AccessDeniedException, VetoException, ObjectNotFoundException, ObjectLockedException {
        StringBuffer pathBuffer = new StringBuffer(hpath);
        String[] dirs = subHPath.split("/");
        for (int i = 0; i < dirs.length - 1; i++) {
            pathBuffer.append('/');
            pathBuffer.append(dirs[i]);
            // as we have no information how this path may look like we will have to assure the full
            // path is created
            String dir = pathBuffer.toString();
            try {
                structure.retrieve(sToken, dir);
            } catch (ObjectNotFoundException onfe) {
                try {
                    structure.create(sToken, new SubjectNode(), dir);
                    content.create(sToken, dir, new NodeRevisionDescriptor(0), null);
                } catch (ObjectAlreadyExistsException oae) {
                    Domain.warn("Object " + dir + " already exists.");
                } catch (RevisionAlreadyExistException rae) {
                    Domain.warn("Revision " + dir + " already exists.");
                }
            }
        }
    }


    /**
     * Generates the next available workingresource URI and returns an URI handler for it.
     * @param sToken the Slide token
     * @param nsaToken the namespace access token
     * @param uh the URI handler of the version resource being checked-out
     */
    public static UriHandler
    createNextWorkingresourceUri( SlideToken sToken, NamespaceAccessToken nsaToken, UriHandler uh )
    throws ObjectNotFoundException, AccessDeniedException, ObjectLockedException,
    LinkedObjectNotFoundException, ServiceAccessException,
    RevisionDescriptorNotFoundException, RevisionNotFoundException, VetoException {

        UriHandler result = null;
        String nsName = nsaToken.getName();
        UriHandler wrpathHandler =
            WorkingresourcePathHandler.getResolvedWorkingresourcePathHandler( nsName, uh );
        Content content = nsaToken.getContentHelper();
        String wrpath = wrpathHandler.toString();

        Structure structure = nsaToken.getStructureHelper();
        String uniqueUri = structure.generateUniqueUri(sToken, wrpath);

        if (uniqueUri != null) {
            result = new UriHandler(uniqueUri);
        } else {
            NodeRevisionDescriptors wrpathNrds =
                content.retrieve( sToken, wrpath );

            NodeRevisionDescriptor wrpathNrd =
                content.retrieve( sToken, wrpathNrds );

            NodeProperty nextWrnProp = wrpathNrd.getProperty(I_NEXT_WORKINGRESOURCE_NAME,
                                                             NamespaceCache.SLIDE_URI);
            if (nextWrnProp == null) {
                // convert to slide namespace if this property is still
                // in DAV: namespace
                nextWrnProp = wrpathNrd.getProperty( I_NEXT_WORKINGRESOURCE_NAME );
                if (nextWrnProp != null) {
                    wrpathNrd.removeProperty(nextWrnProp);
                    nextWrnProp = new NodeProperty(I_NEXT_WORKINGRESOURCE_NAME,
                                                   nextWrnProp.getValue(),
                                                   NamespaceCache.SLIDE_URI);
                    nextWrnProp.setKind( NodeProperty.Kind.PROTECTED );
                    wrpathNrd.setProperty( nextWrnProp );
                }
            }

            if( nextWrnProp == null || nextWrnProp.getValue() == null ) {
                nextWrnProp =
                    new NodeProperty(I_NEXT_WORKINGRESOURCE_NAME,
                                     I_INITIAL_WORKINGRESOURCE_NAME,
                                     NamespaceCache.SLIDE_URI );
                nextWrnProp.setKind( NodeProperty.Kind.PROTECTED );
                wrpathNrd.setProperty( nextWrnProp );
            }

            String nextWrnStr = (String)nextWrnProp.getValue();
            result = new UriHandler( wrpath+"/"+nextWrnStr );

            long nextWrnLong = Long.parseLong( nextWrnStr );
            nextWrnProp = new NodeProperty(I_NEXT_WORKINGRESOURCE_NAME,
                                           String.valueOf(nextWrnLong + 1),
                                           NamespaceCache.SLIDE_URI );
            wrpathNrd.setProperty( nextWrnProp );

            content.store( sToken, wrpath, wrpathNrd, null ); //revisionContent = null
        }
        return result;
    }

    /**
     * Creates a VR URI for the specified version in the specified history and
     * returns an URI handler for it.
     */
    public static UriHandler
        createVersionUri( UriHandler vhrUri, String version ) {
        return new UriHandler( vhrUri, version );
    }

    /**
     * Registerd stores: namespaceName -> (scope -> store)
     */
    protected static Map registeredStores = new HashMap();
    protected static Set allScopes = new HashSet();
    protected static Set allStoreNames = new HashSet();

    /**
     *  Called when a Namespace registers a Store
     */
    public static void notifyStoreCreated( String namespaceName, String scope, String storeName ) {
        Map storesInNamespace = (Map)registeredStores.get( namespaceName );
        if( storesInNamespace == null ) {
            storesInNamespace = new HashMap();
            registeredStores.put( namespaceName, storesInNamespace );
        }
        UriHandler scopeUh = new UriHandler(scope);
        storesInNamespace.put( scopeUh, storeName );
        allScopes.add( scopeUh );
        allStoreNames.add( storeName );
    }

    /**
     *  Return the best matching scope for the specified URI in the specified namespace
     */
    public static UriHandler bestMatchingScope( String namespaceName, UriHandler uh ) {
        UriHandler result = null;
        Map storesInNamespace = (Map)registeredStores.get( namespaceName );
        Iterator i = storesInNamespace.keySet().iterator();
        int matchSize = 0;
        while( i.hasNext() ) {
            UriHandler scopeUh = (UriHandler)i.next();
            if( scopeUh.isAncestorOf(uh) && scopeUh.length() > matchSize ) {
                matchSize = scopeUh.length();
                result = scopeUh;
            }
        }
        return result;
    }

    private String uri;
    private String[] uriTokens = null;


    /**
     * Protected constructor
     */
    public UriHandler( String uri ) {
        this.uri = uri;

        StringTokenizer t = new StringTokenizer( uri, "/" );
        int n = t.countTokens();
        this.uriTokens = new String[n];
        for( int i = 0; i < n; i++ )
            uriTokens[i] = t.nextToken();
    }

    /**
     * Protected constructor
     */
    protected UriHandler( String[] uriTokens, int number ) {
        String[] t = new String[number];
        for( int i = 0; i < number; i++ )
            t[i] = uriTokens[i];
        this.uriTokens = t;
    }

    /**
     * Protected constructor for VR URIs
     */
    protected UriHandler( UriHandler vhrUri, String versionName ) {
        int n = vhrUri.length();
        this.uriTokens = new String[n + 1];
        for( int i = 0; i < n; i++ )
            uriTokens[i] = vhrUri.getUriTokens()[i];
        uriTokens[n] = versionName;
    }

    /**
     * Get the URI as string
     */
    public String getUri() {
        if( uri == null )
            uri = toString();
            return uri;
    }

    /**
     * Get the name (last token)
     */
    public String getName() {
        if( uriTokens.length == 0 )
            return null;
        return uriTokens[uriTokens.length - 1];
    }

    /**
     * Get the UriHandler for the parent URI, null if this URI is root.
     */
    public UriHandler getParentUriHandler() {
        if( uriTokens.length == 0 )
            return null;
        return new UriHandler( uriTokens, uriTokens.length - 1 );
    }

    /**
     *  Return true, if this handler's tokens are the initial tokens of the specified handler.
     *  Example: /a/b is ancestor of /a/b/x/y/z
     */
    public boolean isAncestorOf( UriHandler uh ) {
        if( uriTokens.length > uh.length() )
                    return false;
        for( int i = 0; i < uriTokens.length; i++ )
            if( !uriTokens[i].equals(uh.getUriTokens()[i]) )
        return false;
        return true;
    }

    /**
     * Get the tokens
     */
    public String[] getUriTokens() {
        return uriTokens;
    }

    /**
     * Return true, if this URI determines the root.
     */
    public boolean isRootUri() {
        return (uriTokens.length == 0);
    }

    /**
     * Return true, if this URI determines the history path.
     */
    public boolean isHistoryPathUri() {
        HistoryPathHandler hpathHandler = HistoryPathHandler.getHistoryPathHandler();
        return hpathHandler.isHistoryPathUri( this );
    }

    /**
     * Return true, if this is a history URI
     */
    public boolean isHistoryUri() {
        if(isVersionUri())
            return false;
        HistoryPathHandler hpathHandler = HistoryPathHandler.getHistoryPathHandler();
        String[] hpathTokens = hpathHandler.getUriTokens();

        if ((hpathTokens.length + 1) == uriTokens.length) {
            if (hpathHandler.isHistoryPathUri(getParentUriHandler())) {
                try {
                    Integer.parseInt(uriTokens[uriTokens.length - 1]);
                    return true;
                } catch (NumberFormatException x) {
                }
            }
        }
        // we have two possible settings for history formats, so check the second one.
        if (/*UriHandler.useHistoryCollectionHack &&*/ hpathHandler.isAncestorOf(this)) {
            if (uriTokens.length < hpathTokens.length + 1) {
                return false;
            }
            String h = uriTokens[uriTokens.length - 1];
            if(h.length() < 2)
                return false;
            char firstChar = h.charAt(0);
            int digit = ((Integer)ID_TO_NUMBER_MAP.get(new Character(firstChar))).intValue();
            int strDigit = Integer.parseInt(h.substring(1));
            return strDigit == digit;
        }
        return false;
    }

    /**
     * Return true, if this is a version URI
     */
    public boolean isVersionUri() {
        HistoryPathHandler hpathHandler = HistoryPathHandler.getHistoryPathHandler();
        String[] hpathTokens = hpathHandler.getUriTokens();
        String[] thisTokens = getUriTokens();
        if(!hpathHandler.isAncestorOf(this))
            return false;
        boolean validNum = true;
        try {
            Double.parseDouble(thisTokens[thisTokens.length - 1]);
        } catch (NumberFormatException nfe) {
            return false;
        }

        // in the old format, there are two levels under the history path for a version URI
        boolean oldVersionFormat = (hpathTokens.length + 2) == uriTokens.length;
        // in the new one, there are AT LEAST two levels.
        boolean newVersionFormat = /*UriHandler.useHistoryCollectionHack &&*/ uriTokens.length >= hpathTokens.length + 2;

        return (oldVersionFormat || newVersionFormat);
    }

    /**
     * Return true, if this URI determines the workspace path.
     */
    public boolean isWorkspacePathUri() {
        WorkspacePathHandler wspathHandler = WorkspacePathHandler.getWorkspacePathHandler();
        return wspathHandler.isWorkspacePathUri( this );
    }

    /**
     * Return true, if this is a valid workspace URI
     */
    public boolean isWorkspaceUri() {
        WorkspacePathHandler wspathHandler = WorkspacePathHandler.getWorkspacePathHandler();
        String[] wspathTokens = wspathHandler.getUriTokens();

        if( (wspathTokens.length + 1) == uriTokens.length ) {
            if( wspathHandler.isWorkspacePathUri(getParentUriHandler()) ) {
                return true;
            }
        }
                    return false;
            }

    /**
     * Return true, if this is an URI of a resource in a workspace
     */
    public boolean isResourceInWorkspaceUri() {
        WorkspacePathHandler wspathHandler = WorkspacePathHandler.getWorkspacePathHandler();
        String[] wspathTokens = wspathHandler.getUriTokens();

        if( (wspathTokens.length + 1) < uriTokens.length ) {
            UriHandler p = new UriHandler( uriTokens, wspathTokens.length + 1 );
            if( p.isWorkspaceUri() ) {
            return true;
        }
        }
        return false;
    }

    /**
     * Return true, if this URI determines the workingresource path.
     */
    public boolean isWorkingresourcePathUri() {
        WorkingresourcePathHandler wrpathHandler = WorkingresourcePathHandler.getWorkingresourcePathHandler();
        return wrpathHandler.isWorkingresourcePathUri( this );
    }

    /**
     * Return true, if this is a valid workingresource URI
     */
    public boolean isWorkingresourceUri() {
        WorkingresourcePathHandler wrpathHandler = WorkingresourcePathHandler.getWorkingresourcePathHandler();
        String[] wrpathTokens = wrpathHandler.getUriTokens();

        if( (wrpathTokens.length + 1) == uriTokens.length ) {
            if( wrpathHandler.isWorkingresourcePathUri(getParentUriHandler()) ) {
                try {
                    Integer.parseInt( uriTokens[uriTokens.length - 1] );
            return true;
        }
                catch( NumberFormatException x ) {};
    }
        }
        return false;    }

    /**
     * Returns the workspace identifier from the uri
     * @pre isWorkspaceUri()
     */
    public String getWorkspaceName() {
        UriHandler wspathHandler = WorkspacePathHandler.getWorkspacePathHandler();
        String[] wspathTokens = wspathHandler.getUriTokens();

        if( !isWorkspaceUri() )
            throw new IllegalStateException(
                "URI "+uri+" is not a workspace URI" );

        return uriTokens[wspathTokens.length];
    }

    /**
     * Returns the history identifier from the uri
     * @pre isHistoryUri() || isVersionUri()
     */
//    public String getHistoryName() {
//        UriHandler hpathHandler = HistoryPathHandler.getHistoryPathHandler();
//        String[] hpathTokens = hpathHandler.getUriTokens();
//
//        if (UriHandler.useHistoryCollectionHack) {
//            if (isHistoryUri()) {
//                return getName();
//            } else if (isVersionUri()) {
//                return uriTokens[uriTokens.length - 2];
//            } else {
//                throw new IllegalStateException("URI " + toString() + " is neither history nor version URI");
//            }
//        } else {
//            if (!isHistoryUri() && !isVersionUri())
//                throw new IllegalStateException("URI " + uri + " is neither history nor version URI");
//
//            return uriTokens[hpathTokens.length];
//        }
//    }

    /**
     * Returns the version name from the uri
     *
     * @pre isVersionUri()
     */
    public String getVersionName() {
        if (!isVersionUri())
            throw new IllegalStateException("URI " + uri + " is not a version URI");
        return getName();
    }

    /**
     * Returns the associated history URI of this version URI
     *
     * @pre isVersionUri()
     */
    public String getAssociatedHistoryUri() {
        if( !isVersionUri() )
            throw new IllegalStateException(
                "URI "+uri+" is not a version URI" );

        StringBuffer b = new StringBuffer();
        for( int i = 0; i < (uriTokens.length - 1); i++ ) {
            b.append( "/" );
            b.append( uriTokens[i] );
        }
        return b.toString();
    }

    /**
     * Returns the associated workspace URI if this URI represents a resource
     * in a workspace; if the resource is a workspace itself, its own URI is returned.
     * @return the associated workspace URI; null, if the resource is neither a workspace itself
     *         nor contained in a workspace
     */
    public String getAssociatedWorkspaceUri() {
        if( !isWorkspaceUri() && !isResourceInWorkspaceUri() )
            return null;

        UriHandler wspathHandler = WorkspacePathHandler.getWorkspacePathHandler();
        String[] wspathTokens = wspathHandler.getUriTokens();
        StringBuffer b = new StringBuffer();
        for( int i = 0; i < (wspathTokens.length + 1); i++ ) {
            b.append( "/" );
            b.append( uriTokens[i] );
        }
        return b.toString();
    }

    /**
     * Returns the associated <b>base</b> store name.
     * Examples (assume historyPath is parameterized: /history/${store}):
     *   uri=/history/mycoll/1       --->  result=mycoll  // history URI
     *   uri=/history/files/1/1.17   --->  result=files   // version URI
     *   uri=/files/foo/bar.xml      --->  result=files   // best matching scope
     * If the URI is e.g. a version URI but historyPath is not parameterized, no associated
     * base store can be determined.
     *
     * @return the associated base store name.
     */
    public String getAssociatedBaseStoreName( String namespaceName ) {
        String result = "";

        if( isHistoryPathUri() || isHistoryUri() || isVersionUri() ) {
            if( HistoryPathHandler.parameterized ) {
                result = extractStoreName( HistoryPathHandler.historyPathHandler );
            }
            else {
                Domain.info( "Cannot determine base store name for URI "+this );
            }
        }
        else if( isWorkspacePathUri() || isWorkspaceUri() || isResourceInWorkspaceUri() ) {
            if( WorkspacePathHandler.parameterized ) {
                result = extractStoreName( WorkspacePathHandler.workspacePathHandler );
            }
            else {
                Domain.info( "Cannot determine base store name for URI "+this );
            }
        }
        else if( isWorkingresourcePathUri() || isWorkingresourceUri() ) {
            if( WorkingresourcePathHandler.parameterized ) {
                result = extractStoreName( WorkingresourcePathHandler.workingresourcePathHandler );
            }
            else {
                Domain.info( "Cannot determine base store name for URI "+this );
            }
        }
        else {
            UriHandler scopeUh = bestMatchingScope( namespaceName, this );
            Map storesInNamespace = (Map)registeredStores.get( namespaceName );
            result = (String)storesInNamespace.get( scopeUh );
        }
        return result;
    }

    /**
     * Extract the store name from a parameterized path.
     */
    private String extractStoreName( UriHandler pUh ) {
        String result = "";
        String[] pTokens = pUh.getUriTokens();
        int ptIndex = -1;
        for( int i = 0; i < pTokens.length; i++ ) {
            if( pTokens[i].indexOf(I_STORE_PLACE_HOLDER_IN_PATH) >= 0 ) {
                ptIndex = i;
                break;
            }
        }
        if( ptIndex >= 0 ) {
            result = extractStoreName( pTokens[ptIndex], uriTokens[ptIndex] );
        }
        return result;
    }

    /**
     * Extract the store name from a parameterized path.
     */
    private String extractStoreName( String prmStr, String srcStr ) {
        String tail = null;
        int start = prmStr.indexOf( I_STORE_PLACE_HOLDER_IN_PATH );
        int endP = start + I_STORE_PLACE_HOLDER_IN_PATH.length();
        int endS = -1;

        if( endP < prmStr.length() ) {
            tail = prmStr.substring( endP );
            endS = srcStr.lastIndexOf( tail );
        }
        else
            endS = srcStr.length();

        return srcStr.substring( start, endS );
    }

    /**
     * Returns <code>true</code> if the usage of this URI
     * is restricted by the server, means the server may refuse to
     * create resource at this URI due to a client request like <code>PUT</code>.
     * (e.g. an URI that determines a resource in a history folder).
     *
     * @return     <code>true</code> if the usage of the URI is restricted.
     */
    public boolean isRestrictedUri() {
        if( isRootUri() )
            return true;
        if( isVersionUri() )
            return true;
        if( isHistoryUri() )
            return true;
        if( isHistoryPathUri() )
            return true;
        if( isWorkspaceUri() )
            return true;
        if( isWorkspacePathUri() )
            return true;
        if( isWorkingresourceUri() )
            return true;
        if( isWorkingresourcePathUri() )
            return true;

        UriHandler p = getParentUriHandler();
        if( p != null ) {
            if( p.isRootUri() )
                return true;
            if( p.isHistoryPathUri() )
                return true;
            if( p.isWorkingresourcePathUri() )
                return true;
        }
        return false;
    }

    /**
     *  Overwrites Object.equals()
     */
    public boolean equals( Object o ) {
        if( !(o instanceof UriHandler) )
            return false;
        String[] oTokens = ((UriHandler)o).getUriTokens();
        if( oTokens.length != uriTokens.length )
            return false;
        for( int i = 0; i < uriTokens.length; i++ )
            if( !oTokens[i].equals(uriTokens[i]) )
                return false;
        return true;
    }

    /**
     *  Overwrites Object.hashCode()
     */
    public int hashCode() {
        return uriTokens.length;
    }

    /**
     *  Get the length of this handler.
     */
    public int length() {
        return uriTokens.length;
    }

    /**
     * Return string representation of the object
     */
    public String toString() {
        StringBuffer b = new StringBuffer();
        if( uriTokens.length == 0 )
            b.append("/");
        else
            for( int i = 0; i < uriTokens.length; i++ )
            b.append("/").append( uriTokens[i] );
        return b.toString();
    }
}

