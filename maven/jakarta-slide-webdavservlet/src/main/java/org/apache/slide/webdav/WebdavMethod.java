/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/WebdavMethod.java,v 1.2 2006-01-22 22:49:06 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:06 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Basic interface for classes that implement processing of a specific WebDAV 
 * method.
 * 
 */
public interface WebdavMethod {
    
    
    // --------------------------------------------------------- Public Methods
    
    
    /**
     * Called by the WebDAV servlet to delegate the actual processing of the 
     * request to the method implementation.
     * 
     * @throws WebdavException   if some error occurred while processing the
     *                           request. HTTP/WebDAV status code will be 
     *                           available through 
     *                           {@link WebdavException#getStatusCode WebdavException.getStatusCode()}
     */
    public void run(HttpServletRequest req, HttpServletResponse resp)
        throws WebdavException;
    
    
}

