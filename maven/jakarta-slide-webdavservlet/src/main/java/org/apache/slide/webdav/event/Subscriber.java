/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/event/Subscriber.java,v 1.3 2006-04-19 15:06:55 peter-cvs Exp $
 * $Revision: 1.3 $
 * $Date: 2006-04-19 15:06:55 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.event;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TimerTask;

import org.apache.slide.event.ResourceEvent;
import org.apache.slide.webdav.method.AbstractWebdavMethod;

/**
 * @version $Revision: 1.3 $
 */
public class Subscriber {
    protected static final String LOG_CHANNEL = Subscriber.class.getName();
    final static String UPDATE = "Update";
    final static String DELETE = "Delete";
    final static String MOVE = "Move";
    final static String NEW_MEMBER = "Update/newmember";
    final static String NEW_MAIL = "pragma/<http://schemas.microsoft.com/exchange/newmail>";
    
    private String callback;
    private String notificationType, uri;
    private int depth, notificationDelay, subscriptionLifetime, id;
    private long subscriptionEnd;
    private List events = new ArrayList();
    private TimerTask lifetime, notify;

    public Subscriber(String uri, String callback, String notificationType, int notificationDelay, int subscriptionLifetime, int depth) {
        this.callback = callback;
        this.notificationType = notificationType;
        this.notificationDelay = notificationDelay;
        this.subscriptionLifetime = subscriptionLifetime;
        this.subscriptionEnd = System.currentTimeMillis() + subscriptionLifetime*1000;
        this.uri = uri;
        this.depth = depth;
    }

    public void addEvent(ResourceEvent event) {
        events.add(event);
    }

    public List getEvents() {
        return events;
    }

    public void clearEvents() {
        events = new ArrayList();
    }

    public TimerTask getLifetime() {
        return lifetime;
    }

    public long getSubscriptionEnd() {
    	return subscriptionEnd;
    }
    
    public void setLifetime(TimerTask lifetime) {
        this.lifetime = lifetime;
    }

    public TimerTask getNotify() {
        return notify;
    }

    public void setNotify(TimerTask notify) {
        this.notify = notify;
    }

	public int getDepth() {
		return depth;
	}

	public String getUri() {
		return uri;
	}
    
	public boolean matches(String type, ResourceEvent event) {
    	// check if event matches notification-type
    	// see http://msdn.microsoft.com/library/default.asp?path=/library/en-us/wss/wss/_webdav_notification_type_header.asp
    	// for details
    	if ( type.equalsIgnoreCase(notificationType) || 
    		( type.equalsIgnoreCase(NEW_MEMBER) && notificationType.equalsIgnoreCase(UPDATE) && depth > 0 ) || 
    		( type.equalsIgnoreCase(DELETE) && notificationType.equalsIgnoreCase(UPDATE) && depth > 0 ) ){
    		String eventUri = event.getUri();
    		if ( eventUri != null && uri != null ) {
    			if ( depth == 0 && eventUri.equals(uri.toString()) ) return true;
    			if ( depth == AbstractWebdavMethod.INFINITY && eventUri.startsWith(uri.toString()) ) return true;
    			if ( eventUri.startsWith(uri.toString() )) {
    				String subpath = eventUri.substring(uri.toString().length());   
    				StringTokenizer tokenizer = new StringTokenizer(subpath, "/");
    				if ( tokenizer.countTokens() <= depth ) return true;
    			}
    		}
    	}
        return false;
    }

    public boolean hasCallback() {
       return this.callback != null && this.callback.length() > 0;
    }
    
    public String getCallback() {
        return callback;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public int getNotificationDelay() {
        return notificationDelay;
    }

    public void setSubscriptionLifetime(int subscriptionLifetime) {
        this.subscriptionLifetime = subscriptionLifetime;
    }

    public int getSubscriptionLifetime() {
        return subscriptionLifetime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}