/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/util/WebdavConstants.java,v 1.2 2006-01-22 22:55:21 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:21 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * WebDAV constants.
 *
 */
public interface WebdavConstants {

    /** URI schemes */
    String S_DAV                  = "DAV:";             // DAV: namespace URI
    String S_LOCK_TOKEN           = "opaquelocktoken:"; // LOCK feature
    
    /** Features */
    String F_WEBDAV               = "1"; // class 1
    String F_LOCKING              = "2"; // class 2
    String F_SLIDE                = "slide"; // what does this feature exactly mean?

    /** Methods */
    String M_CONNECT              = "CONNECT";
    String M_COPY                 = "COPY";
    String M_DELETE               = "DELETE";
    String M_GET                  = "GET";
    String M_HEAD                 = "HEAD";
    String M_LOCK                 = "LOCK";
    String M_MKCOL                = "MKCOL";
    String M_MOVE                 = "MOVE";
    String M_OPTIONS              = "OPTIONS";
    String M_POST                 = "POST";
    String M_PROPFIND             = "PROPFIND";
    String M_PROPPATCH            = "PROPPATCH";
    String M_PUT                  = "PUT";
    String M_TRACE                = "TRACE";
    String M_UNLOCK               = "UNLOCK";

    /** Headers */
    String H_CACHE_CONTROL        = "Cache-Control";
    String H_DAV                  = "DAV";
    String H_DEPTH                = "Depth";
    String H_DESTINATION          = "Destination";
    String H_IF                   = "If";
    String H_LOCK_TOKEN           = "Lock-Token";
    String H_LOCATION             = "Location";
    String H_OVERWRITE            = "Overwrite";
    String H_STATUS_URI           = "Status-URI";
    String H_TIMEOUT              = "Timeout";
    String H_TRANSACTION          = "Transaction";
    String H_CONTENT_TYPE         = "Content-Type";

    /** XML Elements */
    String E_ABORT                = "abort";
    String E_ACTIVELOCK           = "activelock";
    String E_ALLPROP              = "allprop";
    String E_COMMIT               = "commit";
    String E_COLLECTION           = "collection";
    String E_DEPTH                = "depth";
    String E_DST                  = "dst";
    String E_EXCLUSIVE            = "exclusive";
    String E_ERROR                = "error";
    String E_GROUPOPERATION       = "groupoperation";
    String E_HREF                 = "href";
    String E_KEEPALIVE            = "keepalive";
    String E_LINK                 = "link";
    String E_LOCAL                = "local";
    String E_LOCKDISCOVERY        = "lockdiscovery";
    String E_LOCKENTRY            = "lockentry";
    String E_LOCKINFO             = "lockinfo";
    String E_LOCKNULL             = "lock-null";
    String E_LOCKSCOPE            = "lockscope";
    String E_LOCKTOKEN            = "locktoken";
    String E_LOCKTYPE             = "locktype";
    String E_MULTISTATUS          = "multistatus";
    String E_OMIT                 = "omit";
    String E_OWNER                = "owner";
    String E_PROP                 = "prop";
    String E_PROPERTYBEHAVIOR     = "propertybehavior";
    String E_PROPERTYUPDATE       = "propertyupdate";
    String E_PROPFIND             = "propfind";
    String E_PROPNAME             = "propname";
    String E_PROPSTAT             = "propstat";
    String E_REMOVE               = "remove";
    String E_REPORT               = "report";
    String E_RESPONSE             = "response";
    String E_RESPONSEDESCRIPTION  = "responsedescription";
    String E_SET                  = "set";
    String E_SHARED               = "shared";
    String E_SRC                  = "src";
    String E_STATUS               = "status";
    String E_TIMEOUT              = "timeout";
    String E_TRANSACTION          = "transaction";
    String E_TRANSACTIONINFO      = "transactioninfo";
    String E_TRANSACTIONSTATUS    = "transactionstatus";
    String E_WRITE                = "write";

    /** Live Properties */
    String P_CREATIONDATE         = "creationdate";
    String P_MODIFICATIONDATE     = "modificationdate";
    String P_DISPLAYNAME          = "displayname";
    String P_GETCONTENTLANGUAGE   = "getcontentlanguage";
    String P_GETCONTENTLENGTH     = "getcontentlength";
    String P_GETCONTENTTYPE       = "getcontenttype";
    String P_GETETAG              = "getetag";
    String P_GETLASTMODIFIED      = "getlastmodified";
    String P_LOCKDISCOVERY        = "lockdiscovery";
    String P_RESOURCETYPE         = "resourcetype";
    String P_SOURCE               = "source";
    String P_SUPPORTEDLOCK        = "supportedlock";

    String[] WEBDAV_PROPERTIES = new String[] {
        P_CREATIONDATE,
        P_MODIFICATIONDATE,
        P_DISPLAYNAME,
        P_GETCONTENTLANGUAGE,
        P_GETCONTENTLENGTH,
        P_GETCONTENTTYPE,
        P_GETETAG,
        P_GETLASTMODIFIED,
        P_LOCKDISCOVERY,
        P_RESOURCETYPE,
        P_SOURCE,
        P_SUPPORTEDLOCK
    };

    List WEBDAV_PROPERTY_LIST = Collections.unmodifiableList(Arrays.asList(WEBDAV_PROPERTIES));
}

