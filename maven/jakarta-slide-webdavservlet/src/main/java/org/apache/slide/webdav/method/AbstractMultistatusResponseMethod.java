/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/method/AbstractMultistatusResponseMethod.java,v 1.2 2006-01-22 22:55:20 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:20 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.method;

import org.apache.slide.common.NamespaceAccessToken;
import org.apache.slide.common.NestedSlideException;
import org.apache.slide.common.SlideException;
import org.apache.slide.webdav.WebdavException;
import org.apache.slide.webdav.WebdavServletConfig;
import org.apache.slide.webdav.util.WebdavStatus;

/**
 * Abstract class for methods which return a multistatus error report, like
 * MOVE, DELETE, COPY, LOCK.
 *
 */
public abstract class AbstractMultistatusResponseMethod extends AbstractWebdavMethod {


    // ----------------------------------------------------- Instance Variables


    /**
     * Id of the resosurce or collection which is to be copied.
     */
    protected String sourceUri;


    /**
     * Uri of the target.
     */
    protected String destinationUri;


    /**
     * Overwrite.
     */
    protected boolean overwrite;


    // ----------------------------------------------------------- Constructors


    /**
     * Constructor.
     *
     * @param token     the token for accessing the namespace
     * @param config    configuration of the WebDAV servlet
     */
    public AbstractMultistatusResponseMethod(NamespaceAccessToken token,
                                             WebdavServletConfig config) {
        super(token, config);
    }


    // ------------------------------------------------------ Protected Methods


    /**
     * Parse request.
     *
     * @exception WebdavException Does not happen
     */
    protected void parseRequest()
        throws WebdavException {

        sourceUri = requestUri;
        if (sourceUri == null) {
            sourceUri = "/";
        }

        destinationUri = requestHeaders.getDestination();

        if (destinationUri == null) {
            int statusCode = WebdavStatus.SC_BAD_REQUEST;
            sendError( statusCode, getClass().getName()+".missingDestinationHeader" );
            throw new WebdavException( statusCode );
        }

        destinationUri = parseUri(destinationUri);
        overwrite = requestHeaders.getOverwrite(true);
    }

    /**
     * Checks if a 207 should be generated. A 207 is generated when the request URI is a
     * collection and exactely one nested exception is present and the request URI and
     * the nested exception URI are identical.
     *
     * @return boolean return true, if a 207 response code should be generated
     */

    public static boolean generateMultiStatusResponse(boolean isCollection, NestedSlideException causeException, String resourceURI) {
        boolean isMultiStatus = false;
        if (isCollection) {
            if (causeException.getExceptionsCount() > 1) {
                isMultiStatus = true;
            }
            else if (causeException.getExceptionsCount() == 1) {
                SlideException unpackedException = causeException.unpackSingleException();
                isMultiStatus =  ! resourceURI.equals(MethodUtil.getURI(unpackedException));
            }
        }
        return isMultiStatus;
    }
}

