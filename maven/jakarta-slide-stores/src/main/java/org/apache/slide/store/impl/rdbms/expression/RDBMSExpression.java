/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/stores/org/apache/slide/store/impl/rdbms/expression/RDBMSExpression.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.store.impl.rdbms.expression;

import org.apache.slide.search.SearchException;
import org.apache.slide.search.basic.IBasicExpression;
import org.apache.slide.search.basic.IBasicExpressionFactory;
import org.apache.slide.search.basic.IBasicResultSet;

/**
 * Base class for RDBMS expressions.
 */
public abstract class RDBMSExpression implements IBasicExpression {

    protected RDBMSExpressionFactory _factory;
    protected RDBMSQueryContext _context;

    public RDBMSExpression(RDBMSQueryContext context) {
        _context = context;
    }
    
    public IBasicExpressionFactory getFactory() {
        return _factory;
    }

    public void setFactory(IBasicExpressionFactory factory) {
        _factory = (RDBMSExpressionFactory) factory;
    }

    protected abstract IBasicResultSet compile(RDBMSMergeExpression expression) 
    throws SearchException;

}