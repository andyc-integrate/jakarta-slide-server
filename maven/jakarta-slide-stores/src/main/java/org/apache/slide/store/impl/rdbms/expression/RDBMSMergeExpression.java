/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/stores/org/apache/slide/store/impl/rdbms/expression/RDBMSMergeExpression.java,v 1.2 2006-01-22 22:49:06 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:06 $
 *
 * ====================================================================
 *
 * Copyright 1999-2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.store.impl.rdbms.expression;

import java.util.Collection;
import java.util.Iterator;

import org.apache.slide.search.BadQueryException;
import org.apache.slide.search.SearchException;
import org.apache.slide.search.basic.IBasicExpression;
import org.apache.slide.search.basic.IBasicResultSet;

/**
 */
public class RDBMSMergeExpression extends RDBMSExpression {

    private final String _name;
    private final String _namespace;
    private final Collection _rdbmsExpressions;
    private final Collection _otherExpressions;

    public RDBMSMergeExpression(String name, 
                                String namespace, 
                                RDBMSQueryContext context,
                                Collection rdbmsExpressions, 
                                Collection otherExpressions) throws BadQueryException {
        super(context);
        _name = name;
        _namespace = namespace;
        _rdbmsExpressions = rdbmsExpressions;
        _otherExpressions = otherExpressions;
    }    

    public IBasicResultSet execute() throws SearchException {
        return compile(null);
    }
    
    protected IBasicResultSet compile(RDBMSMergeExpression expression) throws SearchException {
        if (_rdbmsExpressions != null && _rdbmsExpressions.size() > 0) {
        	_context.criteria().add(" ( ");
            Iterator iter = _rdbmsExpressions.iterator();
            while (iter.hasNext()) {
                ((RDBMSExpression) iter.next()).compile(this);
                if (iter.hasNext()) {
                    _context.criteria().add(" " + _name + " ");
                }
            }
        	_context.criteria().add(" ) ");
        }
        if (_otherExpressions != null) {
            IBasicExpression merger = _factory.createStandardMergeExpression(
                    _name, _namespace, _otherExpressions);
            return merger.execute();
        }
        return _context.results();
    }
    
    protected String getName() {
        return _name;
    }

    protected Collection getRDBMSExpressions() {
        return _rdbmsExpressions;
    }

}