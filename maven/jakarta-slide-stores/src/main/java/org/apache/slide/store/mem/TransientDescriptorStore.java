/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/stores/org/apache/slide/store/mem/TransientDescriptorStore.java,v 1.2 2006-01-22 22:49:06 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:06 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.store.mem;

import org.apache.slide.common.ServiceAccessException;
import org.apache.slide.common.Uri;
import org.apache.slide.content.NodeRevisionDescriptor;
import org.apache.slide.content.NodeRevisionNumber;
import org.apache.slide.content.RevisionDescriptorNotFoundException;
import org.apache.slide.store.RevisionDescriptorStore;


/**
 */
public class TransientDescriptorStore extends AbstractTransientStore implements
      RevisionDescriptorStore
{
   // Note: we don't clone the descriptors because ExtendedStore does

   public NodeRevisionDescriptor retrieveRevisionDescriptor(Uri uri,
         NodeRevisionNumber revisionNumber) throws ServiceAccessException,
         RevisionDescriptorNotFoundException
   {
      debug("retrieveRevisionDescriptor {0} {1}", uri, revisionNumber);
      NodeRevisionDescriptor descriptor = 
         (NodeRevisionDescriptor)get(new VersionedUriKey(uri, revisionNumber));
      if (descriptor != null) {
         return descriptor;
      } else {
         throw new RevisionDescriptorNotFoundException(uri.toString());
      }
   }

   public void createRevisionDescriptor(Uri uri,
         NodeRevisionDescriptor revisionDescriptor)
         throws ServiceAccessException
   {
      debug("createRevisionDescriptor {0} {1}", uri, revisionDescriptor.getRevisionNumber());
      put(new VersionedUriKey(uri, revisionDescriptor.getRevisionNumber()), 
            revisionDescriptor);
   }

   public void storeRevisionDescriptor(Uri uri,
         NodeRevisionDescriptor revisionDescriptor)
         throws ServiceAccessException, RevisionDescriptorNotFoundException
   {
      debug("createRevisionDescriptor {0} {1}", uri, revisionDescriptor.getRevisionNumber());
      VersionedUriKey key = new VersionedUriKey(uri, 
            revisionDescriptor.getRevisionNumber());
      NodeRevisionDescriptor descriptor = (NodeRevisionDescriptor)get(key);
      if (descriptor != null) {
         put(key, revisionDescriptor);
      } else {
         throw new RevisionDescriptorNotFoundException(uri.toString());
      }
   }

   public void removeRevisionDescriptor(Uri uri,
         NodeRevisionNumber revisionNumber) throws ServiceAccessException
   {
      debug("removeRevisionDescriptor {0} {1}", uri, revisionNumber);
      remove(new VersionedUriKey(uri, revisionNumber));
   }
}
