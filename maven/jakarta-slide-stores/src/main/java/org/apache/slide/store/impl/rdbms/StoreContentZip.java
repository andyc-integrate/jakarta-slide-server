/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/stores/org/apache/slide/store/impl/rdbms/StoreContentZip.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2003 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.store.impl.rdbms;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * Title: StoreContentZip
 *
 * This util class can generally be used to zip/unzip an inputstream
 * Returns the zip/unzip data as both output/input streams. This is used
 * in the J2EEContentStore
 * @version $Revision: 1.2 $
 */

class StoreContentZip {

	protected static final int ZIP_BUFFER = 2048;
	private long contentLength	= 0;
	private long initialContentLength = -1;
	private OutputStream theOS	= null;

	/**
	 * Constructor for StoreContentZip.
	 */
	public StoreContentZip() {
		super();
		contentLength = 0;
	}

 /**
   * This method compress the input stream and returns the outputstream
   * @param InputStream inIPS
   * @exception  IOException,ZipException
   * @return the compressed OutputStream
   */

	public void Zip(InputStream inIPS)
						throws IOException, ZipException{
		int byteCount = 0;
		contentLength = 0;
                initialContentLength = 0;
		byte data[] = new byte[ZIP_BUFFER];
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ZipOutputStream zoutp = new ZipOutputStream(baos);
		zoutp.putNextEntry(new ZipEntry("zippedfile"));
		while((byteCount = inIPS.read(data,0,ZIP_BUFFER)) != -1 ) {
			zoutp.write(data,0,byteCount);
                        initialContentLength += byteCount;
		}
		zoutp.finish();
		zoutp.flush();
		zoutp.close();
		baos.flush();
		baos.close();
		contentLength = (long)baos.size();
		theOS = baos;
	}

 /**
   * This method decompress the input stream and returns the outputstream
   * @param InputStream inIPS
   * @exception  IOException,ZipException
   * @return the decompressed OutputStream
   */
	public void UnZip(InputStream inIPS)
						throws IOException, ZipException{
		int byteCount = 0;
		contentLength = 0;
		byte indata[] = new byte[ZIP_BUFFER];
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ZipInputStream zinp = new ZipInputStream(inIPS);
		while (zinp.getNextEntry() != null) {
			while ((byteCount = zinp.read(indata,0,ZIP_BUFFER)) != -1 ) {
				baos.write(indata,0,byteCount);
			}
		}
		contentLength = (long)baos.size();
		baos.flush();
		baos.close();
		zinp.close();
		theOS = baos;
	}

 /**
   * This method returns the compressed/decompressed stream as InputStream
   * @param void
   * @exception  IOException,ZipException
   * @return the processed InputStream
   */
	public InputStream getInputStream()
						throws IOException, ZipException{
		return new ByteArrayInputStream(
			((ByteArrayOutputStream)theOS).toByteArray());
	}

 /**
   * This method returns the compressed/decompressed stream as O/PStream
   * @param void
   * @exception  IOException,ZipException
   * @return the processed InputStream
   */
	public OutputStream getOutputStream()
						throws IOException, ZipException{
		return theOS;
	}

	/**
	 * Gets the length.
	 * @return return the length of the un/compressed Stream
	 */
	public long getContentLength() {
		return contentLength;
	}

        public long getInitialContentLength() {
		return initialContentLength;
	}
        
}
