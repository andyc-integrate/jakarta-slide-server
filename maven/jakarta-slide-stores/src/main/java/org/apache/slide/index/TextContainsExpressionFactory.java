/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/stores/org/apache/slide/index/TextContainsExpressionFactory.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.index;

import org.apache.slide.search.basic.IBasicExpressionFactory;
import org.apache.slide.search.basic.IBasicQuery;
import org.apache.slide.search.basic.IBasicExpression;
import org.apache.slide.search.PropertyProvider;
import org.apache.slide.search.BadQueryException;
import org.apache.slide.content.NodeProperty;
import org.jdom.Element;

import org.apache.lucene.analysis.Analyzer;

import java.util.Collection;

/**
 * Date: Jun 24, 2004
 * Time: 11:42:35 PM
 */
public class TextContainsExpressionFactory implements IBasicExpressionFactory {


    private IBasicQuery query;
    protected PropertyProvider propertyProvider;

    private String rootPath;
    private Analyzer analyzer;

    /**
     * Constructor
     *
     * @param    rootPath           path to the content files
     *
     */
    public TextContainsExpressionFactory (String rootPath, Analyzer analyzer)
    {
        this.rootPath = rootPath;
        this.analyzer = analyzer;
    }

    /**
     * called for merge expressions (or, and). Not defined here
     *
     * @param    mergeOperator       and, or
     * @param    namespace           the namespace of this expression
     * @param    expressionsToMerge  all expressions, that shall be merged
     *
     * @return   an IBasicExpression
     *
     * @throws   BadQueryException
     *
     */
    public IBasicExpression createMergeExpression (String mergeOperator,
                                                   String namespace,
                                                   Collection expressionsToMerge)
        throws BadQueryException
    {
        return null;
    }

    /**
     * Called by the expression compiler for each leave expression.
     *
     * @param    element             an Element discribing the expression
     *
     * @return   an IBasicExpression
     *
     * @throws   BadQueryException
     *
     */
    public IBasicExpression createExpression (Element element)
        throws BadQueryException
    {
        TextContainsExpression result = null;

        if (element == null)
        {
            throw new BadQueryException ("expected a where criteria");
        }
        else
        {
            String namespace = element.getNamespace().getURI();
            if (namespace.equals (NodeProperty.NamespaceCache.DEFAULT_URI))
                result = createDAVExpression (element);

            // allow store specific extensions
            //  else if (namespace.equals (MyNamespace))
            //      result = createMyExpression (element);
        }
        result.setFactory(this);
        return result;
    }


    /**
     * Called, when the expression is in the default (DAV:) namespace.
     *
     *
     * @param    e                   an Element
     *
     * @return   a BasicExpressionTemplate
     *
     */
    private TextContainsExpression createDAVExpression (Element e)
    {
        String name = e.getName();
        TextContainsExpression result = null;

        if (name.equals ("contains"))
        {
            String searchedText = e.getTextTrim();
            result = new TextContainsExpression (searchedText, rootPath, analyzer);
        }

        return result;
    }

    /**
     * called by BasicExpressionCompiler after construction.
     *
     * @param    query               the associated BasicQuery
     * @param    propertyProvider    the PropertyProvider for this expression.
     *
     * @throws   BadQueryException
     *
     */
    public void init(IBasicQuery query, PropertyProvider propertyProvider)
        throws BadQueryException
    {
        this.query = (IBasicQuery) query;
        this.propertyProvider = propertyProvider;
    }

    /**
     * Method getPropertyProvider
     *
     * @return   the PropertyProvider
     *
     */
    public PropertyProvider getPropertyProvider()
    {
        return propertyProvider;
    }

    /**
     * Method getQuery
     *
     * @return   the IBasicQuery
     *
     */
    public IBasicQuery getQuery()
    {
        return query;
    }
}
