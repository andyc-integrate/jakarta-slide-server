/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/stores/org/apache/slide/index/BasicExpressionTxtContainsSample.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2004 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */



package org.apache.slide.index;

import org.apache.slide.search.basic.*;

import java.util.Collection;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.Hits;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Searcher;
import org.apache.slide.common.SlideException;
import org.apache.slide.search.BadQueryException;
import org.apache.slide.search.RequestedResource;
import org.apache.slide.search.SearchException;
import org.apache.slide.structure.ObjectNode;
import org.apache.slide.structure.SubjectNode;

/**
 * A very basic sample for a store specific Expression. Depending on the
 * complexity of the concrete store specific implementation, iut might be
 * a good idea to have an Expression class for each DAV: expression
 * (SQLEqExpression, SQLOrExpression, ...)
 *
 * @version $Revision: 1.2 $
 */
public class BasicExpressionTxtContainsSample implements IBasicExpression
{
    /** an example for an executable command */
    String searchedText;
    
    String indexPath;
    
    /** backptr to the factory */
    IBasicExpressionFactory factory;
    
    /**
     * constructor for a compare expression like gt, eq, ...
     * For your concrete implementation you are free, which parameters have to
     * be passed, let the factory give you everything you need.
     */
    BasicExpressionTxtContainsSample (String searchedText, String rootPath)
    {
        this.searchedText = searchedText;
        this.indexPath = rootPath;
    }
    
    /**
     * constructor for a merge expression
     */
    BasicExpressionTxtContainsSample (String mergeOperator,
                                      Collection children,
                                      IBasicExpressionFactory factory)
        throws BadQueryException
    {
        //        this.factory = factory;
        //        Iterator it = children.iterator();
        //        BasicExpressionTxtContainsSample firstChild = (BasicExpressionTxtContainsSample)it.next();
        //
        //        if (firstChild == null)
        //            throw new BadQueryException (mergeOperator + " needs at least one nested element");
        //
        //        theExecutableCommand = firstChild.theExecutableCommand;
        //
        //        // create the executable command
        //        while (it.hasNext()) {
        //            BasicExpressionTxtContainsSample exp = (BasicExpressionTxtContainsSample)it.next();
        //            theExecutableCommand += " " + mergeOperator + " " + exp.theExecutableCommand;
        //        }
    }
    
    /**
     * fake executer. The executable command is printed and a fake result is created.
     *
     * @return   an IBasicResultSet
     *
     * @throws   SearchException
     *
     */
    public IBasicResultSet execute() throws SearchException
    {
        IBasicResultSet result = new BasicResultSetImpl (false);
        
        try
        {
            Searcher searcher = new IndexSearcher(indexPath);
            Analyzer analyzer = new StandardAnalyzer();
            
            Query query = QueryParser.parse(searchedText, "contents", analyzer);
            Hits hits = searcher.search (query);
            int noOfHits = hits.length();
            
            for (int i = 0; i < noOfHits; i++)
            {
                Document doc = hits.doc(i);
                String uri = doc.get("documentId");
                System.out.println(uri);
                RequestedResource resource = createResource(uri);
                result.add (resource);
            }
        }
        catch (Exception e)
        {
            throw new SearchException (e);
        }
        
        return  result;
    }
    
    private RequestedResource createResource(String uri) throws SearchException
    {
        ObjectNode node = new SubjectNode(uri); // this will return the root folder
        RequestedResource resource = null;
        IBasicQuery query = factory.getQuery();
        
        try
        {
            resource = new ComparableResourceImpl
                (node, query.getSearchToken(), query.getScope(),
                 factory.getPropertyProvider());
        }
        catch (SlideException e)
        {
            throw new SearchException (e);
        }
        return resource;
    }
    
    public void setFactory (IBasicExpressionFactory factory)
    {
        this.factory = factory;
    }
    
    public IBasicExpressionFactory getFactory()
    {
        return this.factory;
    }
}

