/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/stores/org/apache/slide/store/txfile/XMLResourceDescriptor.java,v 1.2 2006-01-22 22:49:06 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:06 $
 *
 * ====================================================================
 *
 * Copyright 1999-2004 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.store.txfile;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.transaction.file.FileResourceManager;
import org.apache.commons.transaction.file.ResourceManagerException;
import org.apache.commons.transaction.util.LoggerFacade;
import org.apache.slide.common.ServiceAccessException;
import org.apache.slide.common.Uri;
import org.apache.slide.structure.ObjectAlreadyExistsException;
import org.apache.slide.structure.ObjectNotFoundException;
import org.jdom.JDOMException;

/**
 * XML descriptor as a resource in a {@link FileResourceManager}.
 *
 * @see FileResourceManager
 * @see TxXMLFileDescriptorsStore
 */
public class XMLResourceDescriptor extends AbstractXMLResourceDescriptor {

    protected static final String PATH_EXTENSION = ".def.xml";

    protected final FileResourceManager rm;
    protected final TxXMLFileDescriptorsStore store;
    protected final String loadPath;
    protected LoggerFacade logger;

    /**
     * Creates an XML descriptor resource.
     *
     * @param uri uri of the resource
     * @param store store to use for error reporting
     * @param rm resource manager to load / store this descriptor from / to
     * @param txId identifier for the transaction in which the descriptor is to be managed
     * @param characterEncoding charcter enconding used to store this descriptor in XML
     * @throws ServiceAccessException if anything goes wrong at system level
     */
    public XMLResourceDescriptor(
        Uri uri,
        TxXMLFileDescriptorsStore store,
        FileResourceManager rm,
        Object txId,
        String characterEncoding)
        throws ServiceAccessException {

        super(uri, txId, characterEncoding);

        logger = rm.getLogger().createLogger(XMLResourceDescriptor.class.getName());

        this.rm = rm;
        this.store = store;

        this.loadPath = uri.toString() + PATH_EXTENSION;
    }

    /**
     * Stores this descriptor to the resource manager.
     *
     * @throws ServiceAccessException if anything goes wrong at system level
     * @throws ObjectNotFoundException if the descriptor has not been created before
     */
    public void save() throws ServiceAccessException, ObjectNotFoundException {
        if (txId == null) {
            store.throwInternalError("Not inside tx");
        }
        logger.logFine("Tx " + txId + " saves data for " + loadPath);

        OutputStream os = null;
        try {
            os = rm.writeResource(txId, loadPath);
            save(os);
            registeredForSaving = false;
        } catch (ResourceManagerException e) {
            if (e.getStatus() == ResourceManagerException.ERR_NO_SUCH_RESOURCE) {
                throw new ObjectNotFoundException(uri);
            } else {
                store.throwInternalError(e, uri);
            }
        } catch (IOException e) {
            store.throwInternalError(e);
        } finally {
            try {
                if (os != null) {
                    os.close();
                }
            } catch (IOException e) {
            }
        }
    }

    /**
     * Creates this descriptor in the resource manager.
     *
     * @throws ServiceAccessException if anything goes wrong at system level
     * @throws ObjectAlreadyExistsException if the descriptor already exists
     */
    public void create() throws ServiceAccessException, ObjectAlreadyExistsException {
        logger.logFiner("Tx " + txId + " creates " + loadPath);
        if (txId == null) {
            store.throwInternalError("Not inside tx");
        }
        try {
            rm.createResource(txId, loadPath, false);
            init();
        } catch (ResourceManagerException e) {
            if (e.getStatus() == ResourceManagerException.ERR_RESOURCE_EXISTS) {
                throw new ObjectAlreadyExistsException(uri.toString());
            } else {
                store.throwInternalError(e, uri);
            }
        }
    }

    /**
     * Deletes this descriptor from the resource manager.
     *
     * @throws ServiceAccessException if anything goes wrong at system level
     * @throws ObjectNotFoundException if the descriptor does not exist
     */
    public void delete() throws ServiceAccessException, ObjectNotFoundException {
        logger.logFiner("Tx " + txId + " deletes " + loadPath);
        if (txId == null) {
            store.throwInternalError("Not inside tx");
        }
        try {
            rm.deleteResource(txId, loadPath, false);
            object = null;
            registeredForSaving = false; // do not save what is no longer there
        } catch (ResourceManagerException e) {
            if (e.getStatus() == ResourceManagerException.ERR_NO_SUCH_RESOURCE) {
                throw new ObjectNotFoundException(uri.toString());
            } else {
                store.throwInternalError(e, uri);
            }
        }
    }

    /**
     * Loads this descriptor from the resource manager.
     *
     * @throws ServiceAccessException if anything goes wrong at system level
     * @throws ObjectNotFoundException if the descriptor does not exist
     */
    public void load() throws ServiceAccessException, ObjectNotFoundException {
        logger.logFiner("Tx " + txId + " loads data for " + loadPath);

        InputStream is = null;
        try {
            if (txId != null) {
                if (rm.resourceExists(txId, loadPath)) {
                    is = rm.readResource(txId, loadPath);
                    load(is);
                } else {
                    init();
                }
            } else {
                logger.logFinest("Faking read access from outside tx for " + loadPath);
                if (rm.resourceExists(loadPath)) {
                    is = rm.readResource(loadPath);
                    load(is);
                } else {
                    init();
                }
            }
        } catch (JDOMException je) {
            store.throwInternalError(je, uri);
        } catch (IOException ioe) {
            store.throwInternalError(ioe, uri);
        } catch (ResourceManagerException e) {
            if (e.getStatus() == ResourceManagerException.ERR_NO_SUCH_RESOURCE) {
                throw new ObjectNotFoundException(uri);
            } else {
                store.throwInternalError(e, uri);
            }
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
            } catch (IOException e) {
            }
        }
    }

}
