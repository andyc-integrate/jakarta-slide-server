/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/stores/org/apache/slide/store/impl/rdbms/expression/RDBMSNotPropContainsExpression.java,v 1.2 2006-01-22 22:49:06 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:06 $
 *
 * ====================================================================
 *
 * Copyright 1999-2004 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.store.impl.rdbms.expression;

import org.apache.slide.content.NodeProperty.NamespaceCache;
import org.apache.slide.search.basic.Literals;
import org.jdom.Element;

public class RDBMSNotPropContainsExpression extends RDBMSCompareExpression {

    public RDBMSNotPropContainsExpression(Element element, RDBMSQueryContext context) {
        super(element, context);
    }

    protected String compile() {
        Element property = (Element) _element.getChild(Literals.PROP, NamespaceCache.DEFAULT_NAMESPACE).getChildren().get(0);
        Element literal = _element.getChild(Literals.LITERAL, NamespaceCache.DEFAULT_NAMESPACE);
        return "NOT (p" + _tableIndex + ".PROPERTY_NAME = '" + property.getName() + "' AND " +
                "p" + _tableIndex + ".PROPERTY_NAMESPACE = '" + property.getNamespaceURI() + "' AND " +
                "p" + _tableIndex + ".PROPERTY_VALUE LIKE '%" + getPropertyValue(literal) + "%')";
    }

    private String getPropertyValue(Element literal) {
        String value = literal.getTextNormalize();
        StringBuffer sb = new StringBuffer(value.length() + 10);
        for (int i = 0, l = value.length(); i < l; i++) {
            char c = value.charAt(i);
            if (c == '_' || c == '%') {
                sb.append("\\\\");
            }
            sb.append(c);
        }
        return sb.toString();
    }

}
