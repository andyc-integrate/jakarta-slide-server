/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/roles/slideroles/basic/RootRoleImpl.java,v 1.3 2007-07-03 10:52:14 jamie-cvs Exp $
 * $Revision: 1.3 $
 * $Date: 2007-07-03 10:52:14 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */ 

package slideroles.basic;

import java.util.Vector;
import org.apache.slide.structure.SubjectNode;

/**
 * Root role class.
 * 
 * @version $Revision: 1.3 $
 */
public class RootRoleImpl extends SubjectNode
    implements RootRole {
    
    
    // ----------------------------------------------------------- Constructors
    
    
    /**
     * Constructor.
     */
    public RootRoleImpl() {
        super();
    }
    
    
    /**
     * Default constructor.
     */
    public RootRoleImpl(String uri) {
        super(uri);
    }
    
    
    /**
     * Default constructor.
     */
    public RootRoleImpl(String uri, Vector children, Vector links) {
        super(uri, children, links);
    }
    
    public RootRoleImpl(String uuri, Vector bindings, Vector parentset, Vector links) {
        super(uuri, bindings, parentset, links);
    }
    
}
