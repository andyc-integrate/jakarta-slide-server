package org.apache.slide.projector;

import java.util.Locale;

import org.apache.slide.projector.i18n.ErrorMessage;

public class ProcessException extends Exception {
    private ErrorMessage errorMessage;

    public ProcessException(ErrorMessage errorMessage, Throwable throwable) {
        super(errorMessage.getSummary(Locale.getDefault(), throwable.getMessage()), throwable);
        this.errorMessage = errorMessage;
    }

    public ProcessException(ErrorMessage errorMessage) {
        super(errorMessage.getSummary(Locale.getDefault(), "no message available"));
        this.errorMessage = errorMessage;
    }

    public ErrorMessage getErrorMessage() {
        return errorMessage;
    }
}