package org.apache.slide.projector;

import org.apache.slide.projector.i18n.ErrorMessage;

/**
 * @version $Revision: 1.2 $
 */

public class Information {
    public final static int DEBUG = 0;
    public final static int INFO = 1;
    public final static int WARNING = 2;
    public final static int ERROR = 3;

    private int severity, number;
    private ErrorMessage errorMessage;
    private String[] involvedParameters;

    public Information(int severity, ErrorMessage errorMessage, String[] involvedParameters) {
        this.severity = severity;
        this.errorMessage = errorMessage;
        this.involvedParameters = involvedParameters;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public int getSeverity() {
        return severity;
    }

    public ErrorMessage getErrorMessage() {
        return errorMessage;
    }

    public String[] getInvolvedParameters() {
        return involvedParameters;
    }

    public boolean isParameterInvolved(String paramterName) {
        for ( int i = 0; i < involvedParameters.length; i++ ) {
            if ( involvedParameters[i].equals(paramterName) ) return true;
        }
        return false;
    }
}