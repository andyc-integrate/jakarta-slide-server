package org.apache.slide.projector;

import java.util.Map;

import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;

public interface Processor {
    /**
     * Implement this method to provide the logic of the processor
     * 
     * @param parameter a map containing the parameters for this process call.
     *                  This values are typically simple values of type <code>String</code> or
     *                  be resource references
     * @return a array of results
     * @throws Exception 
     */
    public Result process(Map parameter, Context context) throws Exception;

    // The following methods describe the behaviour of the processor
    // The parameters needed
    public ParameterDescriptor[] getParameterDescriptors();

    // The states/results that this renderer generates
    public ResultDescriptor getResultDescriptor();
}