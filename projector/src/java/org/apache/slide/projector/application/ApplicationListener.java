package org.apache.slide.projector.application;

import org.apache.slide.projector.URI;

public interface ApplicationListener {
	public void install(String type, URI application, URI configuration);
	
	public void uninstall(String type, URI application, URI configuration);

	public void update(String type, URI application, URI configuration);
}