package org.apache.slide.projector.application;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.slide.projector.URI;

public final class Application {
	public final static String PROCESSORS = "processors";
	public final static String MESSAGES = "messages";
	public final static String JOBS = "jobs";
	
	private URI uri;
	private String name, displayName, vendor, description;
	private Map content = new HashMap();
	private List dependecies = new ArrayList();
	private String version; 
	
	Application(URI applicationUri) {
		this.uri = applicationUri;
	}
	
	public List getDependencies() {
		return dependecies;
	}
	
	public void addDependency(Dependency dependency) {
		dependecies.add(dependency);
	}
	
	public URI getUri() {
		return uri;
	}
	
	String getDescription() {
		return description;
	}

	void setDescription(String description) {
		this.description = description;
	}

	String getDisplayName() {
		return displayName;
	}

	void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	String getVendor() {
		return vendor;
	}

	void setVendor(String vendor) {
		this.vendor = vendor;
	}

	void addContent(String type, URI contentUri) {
		List contentUris = (List)content.get(type);
		if ( contentUris == null ) {
			contentUris = new ArrayList();
			content.put(type, contentUris);
		}
		contentUris.add(contentUri);
	}

	List getContent(String type) {
		return (List)content.get(type);
	}

	Map getContent() {
		return content;
	}
	
	String getVersion() {
		return version;
	}
	
	void setVersion(String version) {
		this.version = version;
	}
	
	/**
	 * @return Returns the name.
	 */
	String getName() {
		return name;
	}
	
	/**
	 * @param name The name to set.
	 */
	void setName(String name) {
		this.name = name;
	}
}