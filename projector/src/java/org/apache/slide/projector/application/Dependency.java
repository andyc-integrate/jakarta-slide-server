/*
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.projector.application;

/**
 */
public class Dependency {
	String requiredApplication;
	String requiredVersion;
	
	public Dependency(String requiredApplication, String requiredVersion) {
		this.requiredApplication = requiredApplication;
		this.requiredVersion = requiredVersion;
	}

	/**
	 * @return Returns the requiredApplication.
	 */
	public String getRequiredApplication() {
		return requiredApplication;
	}
	/**
	 * @return Returns the requiredVersion.
	 */
	public String getRequiredVersion() {
		return requiredVersion;
	}
}
