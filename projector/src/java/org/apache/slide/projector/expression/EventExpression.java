/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/projector/src/java/org/apache/slide/projector/expression/EventExpression.java,v 1.2 2006-01-22 22:45:14 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:45:14 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.projector.expression;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.slide.projector.Projector;
import org.apache.slide.projector.engine.Job;
import org.apache.slide.projector.engine.Scheduler;
import org.apache.slide.projector.value.URIValue;
import org.apache.webdav.lib.Subscriber;

import de.zeigermann.xml.XMLStringWriter;
import de.zeigermann.xml.XMLWriter;

/**
 * The EventExpression class
 * 
 */
public class EventExpression implements Expression, Subscriber {
	public final static String URI = "uri";
	public final static String DEPTH = "depth";
	
	private String method;
    private Map properties = new HashMap();
    private boolean eval = false;
    private Job job;

    public EventExpression(String method) {
        this.method = method;
    }

    public void addProperty(String key, String value) {
    	properties.put(key, value);
    }
    
    public Expression activate(Job job) {
    	EventExpression activatedExpression = new EventExpression(method);
    	activatedExpression.setJob(job);
    	activatedExpression.properties.putAll(properties);
    	String uri = (String)properties.get(URI);
    	int depth = 0;
    	String depthProperty = (String)properties.get(DEPTH);
    	if ( depthProperty != null ) {
    		depth = Integer.valueOf(depthProperty).intValue(); 
    	}
    	Projector.getRepository().subscribe(method, new URIValue(uri), depth, activatedExpression, Projector.getCredentials());
    	return activatedExpression;
    }

    public void setJob(Job job) {
    	this.job = job;
    }
    
    public boolean evaluate() {
        return eval;
    }

    public void save(XMLStringWriter writer) {
        if ( eval ) {
            new TrueExpression().save(writer);
        } else {
            writer.writeStartTag(XMLWriter.createStartTag("event", "method", method));
            for ( Iterator i = properties.entrySet().iterator(); i.hasNext(); ) {
            	Map.Entry entry = (Map.Entry)i.next();
            	writer.writeEmptyElement(XMLWriter.createEmptyTag("property", new String[] { "key", "value" }, new String[] { (String)entry.getKey(), (String)entry.getValue() }));
            }
            writer.writeEndTag(XMLWriter.createEndTag("event"));
        }
    }

	public void notify(String uri, Map information) {
		eval = true;
    	Projector.getRepository().unsubscribe(new URIValue((String)properties.get(URI)), this, Projector.getCredentials());
		Scheduler.getInstance().notify(job);
    }
}