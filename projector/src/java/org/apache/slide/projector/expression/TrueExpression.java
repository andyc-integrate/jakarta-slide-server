/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/projector/src/java/org/apache/slide/projector/expression/TrueExpression.java,v 1.2 2006-01-22 22:45:14 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:45:14 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.projector.expression;

import org.apache.slide.projector.engine.Job;

import de.zeigermann.xml.XMLStringWriter;
import de.zeigermann.xml.XMLWriter;

/**
 * The TrueExpression class
 * 
 */
public class TrueExpression implements Expression {
    public Expression activate(Job job) {
    	return this;
    }
    
    public boolean evaluate() {
        return true;
    }

    public void save(XMLStringWriter writer) {
        writer.writeEmptyElement(XMLWriter.createEmptyTag("true"));                            
    }
}
