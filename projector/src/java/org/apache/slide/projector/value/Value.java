package org.apache.slide.projector.value;

public interface Value {
    public String getContentType();
}