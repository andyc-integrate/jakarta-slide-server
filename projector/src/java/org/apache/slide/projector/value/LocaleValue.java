package org.apache.slide.projector.value;

import java.util.Locale;

public class LocaleValue implements Value {
    private Locale locale;

    public final static String CONTENT_TYPE = Locale.class.getName();

    public LocaleValue(Locale locale) {
        this.locale = locale;
    }

    public Locale getLocale() {
        return locale;
    }

    public String getContentType() {
        return CONTENT_TYPE;
    }
}