package org.apache.slide.projector.value;

import org.apache.slide.projector.URI;

public class URIValue extends StringValue implements URI {
    public final static String CONTENT_TYPE = "uri";

    public URIValue(String uri) {
        super(uri);
    }

    public boolean startsWith(URI uri) {
        return toString().startsWith(uri.toString());
    }

    public boolean isRelative() {
        String uri = toString();
        return (uri != null && uri.length() > 0 && uri.charAt(0) != '/');
    }

    public boolean equals(Object o) {
        return (this == o || (o != null && o instanceof URIValue && ((URIValue)o).toString().equals(toString())));
    }

    public int hashCode() {
        return toString().hashCode();
    }

    public String getContentType() {
        return CONTENT_TYPE;
    }
}