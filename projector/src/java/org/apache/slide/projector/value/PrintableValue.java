package org.apache.slide.projector.value;

/**
 * @version $Revision: 1.2 $
 */

public interface PrintableValue extends Value {
    public String toString();

    public StringBuffer print(StringBuffer buffer);
}
