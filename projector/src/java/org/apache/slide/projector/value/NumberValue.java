package org.apache.slide.projector.value;


public class NumberValue implements PrintableValue {
    private Number number;

    public final static String CONTENT_TYPE = Number.class.getName();

    public NumberValue(Number number) {
        this.number = number;
    }

    public Number getNumber() {
        return number;
    }

    public String getContentType() {
        return CONTENT_TYPE;
    }

    public StringBuffer print(StringBuffer buffer) {
        return buffer.append(number);
    }
}