package org.apache.slide.projector.value;


public class ArrayValue implements Value {
    private Value []array;

    public final static String CONTENT_TYPE = "application/x-java-array";

    public ArrayValue(Value[] array) {
        this.array = array;
    }

    public Value[] getArray() {
        return array;
    }

    public String getContentType() {
        return CONTENT_TYPE;
    }
}