package org.apache.slide.projector.value;

import java.io.IOException;
import java.io.InputStream;

public class InputStreamValue extends AbstractStreamableValue {
    private boolean isDocument;
    private String contentType, characterEncoding;
    private InputStream stream;
    private int contentLength;

    public InputStreamValue(InputStream stream, String contentType, String characterEncoding, int contentLength, boolean document) {
        isDocument = document;
        this.stream = stream;
        this.contentType = contentType;
        this.characterEncoding = characterEncoding;
        this.contentLength = contentLength;
    }

    public InputStream getInputStream() throws IOException {
        return stream;
    }

    public boolean isDocument() {
        return isDocument;
    }

    public int getContentLength() {
        return contentLength;
    }

    public String getContentType() {
        return contentType;
    }

    public String getCharacterEncoding() {
        return characterEncoding;
    }
}