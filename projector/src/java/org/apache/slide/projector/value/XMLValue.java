package org.apache.slide.projector.value;

import org.jdom.DocType;
import org.jdom.Element;

public interface XMLValue extends Value {
    public final static String CONTENT_TYPE = "text/xml";

    public DocType getDocumentType();

    public Element getRootElement();
}