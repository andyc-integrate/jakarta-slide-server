package org.apache.slide.projector.value;


import java.io.IOException;
import java.io.InputStream;

public interface StreamableValue extends Value {
    public InputStream getInputStream() throws IOException;

    public String getCharacterEncoding();

    int getContentLength();

    public boolean isDocument();
}