package org.apache.slide.projector.value;

import java.util.HashMap;
import java.util.Map;

public class MapValue implements Value {
    private Map map;

    public final static String CONTENT_TYPE = "application/x-java-map";

    public MapValue() {
    	map = new HashMap();
    }
    
    public MapValue(Map map) {
        this.map = map;
    }

    public MapValue(String key, Value value) {
        this();
        addEntry(key, value);
    }

    public void addEntry(String key, Value value) {
    	map.put(key, value);
    }
    
    public Map getMap() {
        return map;
    }

    public String getContentType() {
        return CONTENT_TYPE;
    }

}