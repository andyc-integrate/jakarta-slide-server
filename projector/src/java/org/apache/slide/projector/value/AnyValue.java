package org.apache.slide.projector.value;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.Store;
import org.apache.slide.projector.URI;
import org.apache.slide.projector.engine.ProcessorManager;
import org.apache.slide.projector.processor.process.Process;

public class AnyValue implements Value {
	public String CONTENT_TYPE = "projector/stored-value";
	
	private URI processorUri;
    private String result;
	private int storeId;
    private String key;
    private String domain;
    private Value input;

	/**
	 * @param processor
	 * @param result
	 * @param store
	 * @param key
	 * @param domain
	 */
	public AnyValue(String processor, Value input, String result, int storeId, String key, String domain) {
		if ( processor != null ) {
			this.processorUri = new URIValue(processor);
			this.input = input;
		}
		this.result = result;
		this.storeId = storeId;
		this.key = key;
		this.domain = domain;
	}

	public String getDomain() {
		return domain;
	}

	public Value getInput() {
		return input;
	}

	public String getKey() {
		return key;
	}
	
	public URI getProcessorUri() {
		return processorUri;
	}
	
	public String getResult() {
		return result;
	}
	
	public int getStoreId() {
		return storeId;
	}
	
	public Object load(Context context) throws Exception {
		Object value = null;
		if ( key != null ) {
			if ( storeId == Store.NONE ) {
				storeId = Store.STEP;
			} 
			String evaluatedKey = Process.evaluateKey(key, context);
			Store store = context.getStore(storeId);
			if ( store != null ) { 
				if ( domain != null ) {
					MapValue mapResource = (MapValue)store.get(domain);
					if ( mapResource != null ) {
						value = mapResource.getMap().get(evaluatedKey);
					}
				} else {
					value = store.get(evaluatedKey);
				}
			}
		}
        if ( processorUri != null ) {
        	if ( input != null ) {
        		value = input;
        	}
            value = ProcessorManager.getInstance().process(processorUri, value, result, context);
        }
        return value;
	}

	public String getContentType() {
		return CONTENT_TYPE;
	}

}