package org.apache.slide.projector.value;


public class BooleanValue implements PrintableValue {
    public final static BooleanValue TRUE = new BooleanValue(true);
    public final static BooleanValue FALSE = new BooleanValue(false);

    private boolean booleanValue;

    public final static String CONTENT_TYPE = Boolean.class.getName();

    public BooleanValue(boolean booleanValue) {
        this.booleanValue = booleanValue;
    }

    public boolean booleanValue() {
        return booleanValue;
    }

    public String getContentType() {
        return CONTENT_TYPE;
    }

    public String toString() {
    	return String.valueOf(booleanValue);
    }

    public StringBuffer print(StringBuffer buffer) {
        return buffer.append(booleanValue);
    }
}