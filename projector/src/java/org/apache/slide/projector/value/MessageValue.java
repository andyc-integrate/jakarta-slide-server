package org.apache.slide.projector.value;

import java.util.Locale;

import org.apache.slide.projector.i18n.MessageManager;
import org.apache.slide.projector.i18n.MessageNotFoundException;

public class MessageValue implements Value {
    public final static String CONTENT_TYPE = "application/slide-message";
    public final static String ID = "id";
    public final static String ARGUMENTS = "arguments";

    protected String id;
    protected Value[] arguments;

    public MessageValue(String messageId) {
        this.id = messageId;
        this.arguments = new Value[0];
    }

    public MessageValue(String messageId, Object[] arguments) {
        this.id = messageId;
        this.arguments = new Value[arguments.length];
        for ( int i = 0; i < arguments.length; i++ ) {
        	this.arguments[i] = new ObjectValue(arguments[i]);
        }
    }

    public String getId() {
        return id;
    }

    public Value[] getArguments() {
    	return arguments;
    }
    
    public String getText(String key, Locale locale) throws MessageNotFoundException {
        return MessageManager.getText(id, key, arguments, locale);
    }

    public String getText(String key, String defaultText, Locale locale) {
        return MessageManager.getText(id, key, arguments, locale, defaultText);
    }

    public String getContentType() {
        return CONTENT_TYPE;
    }
}