package org.apache.slide.projector.value;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.jdom.DocType;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

public class ElementValue  extends AbstractStreamableValue implements XMLValue {
    private final static XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());

    private Element element;
    private byte[] bytes = null;

    public ElementValue(Element element) {
        this.element = element;
    }

    public InputStream getInputStream() throws IOException {
        if ( bytes == null ) {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            xmlOutputter.output(element, outputStream);
            bytes = outputStream.toByteArray();
        }
        InputStream inputStream = new ByteArrayInputStream(bytes);
        return inputStream;
    }

    public void enableMultipleStreaming() {}

    public int getContentLength() {
        return 1;
    }

    public boolean isDocument() {
        return false;
    }

    public DocType getDocumentType() {
        return new DocType(element.getName());
    }

    public Element getRootElement() {
        return element;
    }

    public String getContentType() {
        return CONTENT_TYPE;
    }

    public String getCharacterEncoding() {
        return "UTF-8";
    }
}
