package org.apache.slide.projector.value;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

public class DateValue extends ObjectValue {
	public final static String CONTENT_TYPE = "application/projector-date";
    
    public DateValue(Date date) {
        super(date);
    }

    public Date getDate() {
    	return (Date)getObject();
    }
    
    public InputStream getInputStream() throws IOException {
      return new ByteArrayInputStream(((String)getObject()).getBytes("UTF-8"));
    }

    public String toString() {
        return String.valueOf(getObject());
    }

    public String getContentType() {
        return CONTENT_TYPE;
    }
}