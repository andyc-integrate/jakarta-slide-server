package org.apache.slide.projector.value;


public class ObjectValue implements Comparable, Value {
    private Object object;

    public ObjectValue(Object object) {
        this.object = object;
    }

    public Object getObject() {
        return object;
    }

    public String getContentType() {
        return object.getClass().getName();
    }

    public String toString() {
    	return object.toString();
    }
     
	public int compareTo(Object o) {
		Object comparableObject = null;
		if ( o instanceof ObjectValue ) {
			comparableObject = ((ObjectValue)o).getObject();
		}
		if ( comparableObject != null && object.getClass().equals(comparableObject.getClass()) ) {
			return ((Comparable)object).compareTo(comparableObject);
		}
		return 0;
	}
}