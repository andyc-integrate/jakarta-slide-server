package org.apache.slide.projector.value;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.slide.projector.ContentType;

public class StringValue extends ObjectValue implements StreamableValue, PrintableValue {
	public static final StringValue EMPTY = new StringValue("");

	private boolean isDocument;
    private String contentType = "text/plain";

    public StringValue(String content) {
        super(content);
        contentType = ContentType.determineContentType(content);
        isDocument = ContentType.determineIsDocument(content);
    }

    public StringValue(String content, boolean document) {
        super(content);
        isDocument = document;
    }

    public StringValue(String content, String contentType, boolean document) {
        super(content);
        isDocument = document;
        this.contentType = contentType;
    }

    public InputStream getInputStream() throws IOException {
      return new ByteArrayInputStream(((String)getObject()).getBytes("UTF-8"));
    }

    public String toString() {
        return (String)getObject();
    }

    public boolean isDocument() {
        return isDocument;
    }

    public int getContentLength() {
        return toString().length();
    }

    public String getContentType() {
        return contentType;
    }

    public String getCharacterEncoding() {
        return "UTF-8";
    }

    public StringBuffer print(StringBuffer buffer) {
        return buffer.append(getObject());
    }
}