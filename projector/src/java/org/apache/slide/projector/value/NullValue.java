/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/projector/src/java/org/apache/slide/projector/value/NullValue.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.projector.value;

import de.zeigermann.xml.XMLStringWriter;
import de.zeigermann.xml.XMLWriter;

/**
 * The NullResource class
 * 
 */
public class NullValue extends ObjectValue implements PrintableValue {
    public final static String CONTENT_TYPE = "application/unset-resource";
    public final static NullValue NULL = new NullValue();
    
    public NullValue() {
        super(null);
    }

    public String getContentType() {
        return CONTENT_TYPE;
    }

    public StringBuffer print(StringBuffer buffer) {
    	return buffer;
    }

    public void save(XMLStringWriter writer) {
        writer.writeEmptyElement(XMLWriter.createEmptyTag("value"));
    }
}
