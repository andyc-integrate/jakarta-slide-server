package org.apache.slide.projector.value;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.slide.projector.util.StreamHelper;

/**
 * @version $Revision: 1.2 $
 */

public class MultipleStreamableValue extends AbstractStreamableValue {
    private StreamableValue value;
    private byte[] bytes;

    public MultipleStreamableValue(StreamableValue resource) {
        this.value = resource;
    }

    public InputStream getInputStream() throws IOException {
        if ( bytes == null ) {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            StreamHelper.copy(value.getInputStream(), outputStream);
            bytes = outputStream.toByteArray();
        }
        return new ByteArrayInputStream(bytes);
    }

    public int getContentLength() {
        return value.getContentLength();
    }

    public boolean isDocument() {
        return value.isDocument();
    }

    public String getContentType() {
        return value.getContentType();
    }

    public String getCharacterEncoding() {
        return value.getCharacterEncoding();
    }
}
