package org.apache.slide.projector;

import java.io.IOException;

public interface Store {
    public final static int NONE = -1;
    public final static int REQUEST_PARAMETER = 0; // Reqeust parameter (can only be of type String or String[])
    public final static int REQUEST_ATTRIBUTE = 1; // Request attribute
    public final static int REQUEST_HEADER = 2;
    public final static int SESSION = 3;
    public final static int COOKIE = 4;
    public final static int CONTEXT = 5;
    public final static int CACHE = 6;
    public final static int REPOSITORY = 7;
    public final static int INPUT = 8;
    public final static int OUTPUT = 9;
    public final static int TRANSIENT_PROCESS = 10; // Transient information bound to the instance of the current process
    public final static int PERSISTENT_PROCESS = 11; // Persistent information bound to the instance of the current process
    public final static int FORM = 12;
    public final static int STEP = 13;

    public final static String[] stores = { "request-parameter", "request-attribute", "request-header", "session", "cookie", "context", "cache",  "repository", "input", "output", "process", "persistent-process", "form", "step" };

    void put(String key, Object value, long timeout) throws IOException;

    void put(String key, Object value) throws IOException;

    Object get(String key) throws IOException;

    void dispose(String key) throws IOException;
}