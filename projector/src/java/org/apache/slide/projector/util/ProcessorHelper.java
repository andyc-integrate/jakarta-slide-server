package org.apache.slide.projector.util;

import org.apache.slide.projector.Processor;
import org.apache.slide.projector.descriptor.ParameterDescriptor;

/**
 * @version $Revision: 1.2 $
 */

public class ProcessorHelper {
    public static ParameterDescriptor getParameterDescriptor(Processor processor, String parameterName) {
        ParameterDescriptor[] parameterDescriptors = processor.getParameterDescriptors();
        for ( int i = 0; i < parameterDescriptors.length; i++ )  {
            if ( parameterDescriptors[i].getName().equals(parameterName)) {
                return parameterDescriptors[i];
            }
        }
        return null;
    }
}