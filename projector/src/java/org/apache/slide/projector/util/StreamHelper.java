package org.apache.slide.projector.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.slide.projector.value.StreamableValue;

public class StreamHelper {
    public static String streamToString(StreamableValue resource) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        copy(resource.getInputStream(), outputStream);
        return outputStream.toString(resource.getCharacterEncoding());
    }

    public static void copy(InputStream inputStream, OutputStream outputStream) throws IOException {
        byte buffer[] = new byte[1024];
        int len = buffer.length;
        while (true) {
            len = inputStream.read(buffer);
            if (len == -1) break;
            outputStream.write(buffer, 0, len);
        }
        inputStream.close();
        outputStream.close();
    }
}