package org.apache.slide.projector.util;

import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.slide.projector.Store;
import org.apache.slide.projector.value.MapValue;
import org.apache.slide.projector.value.Value;

public class StoreHelper {
    private static Logger logger = Logger.getLogger(StoreHelper.class.getName());

    public static Object get(Store store, String domain, String key) {
    	try {
    		MapValue mapResource = (MapValue)store.get(domain);
    		if ( mapResource != null ) {
    			Map map = mapResource.getMap();
    			return (Value)map.get(key);
    		}
    	} catch ( IOException exception ) {
    		logger.log(Level.SEVERE, "Error while accessing store", exception);
    	}
    	return null;
    }

    public static void put(Store store, String domain, String key, Value value) {
    	try {
    		MapValue mapResource = (MapValue)store.get(domain);
    		if ( mapResource != null ) {
    			Map map = mapResource.getMap();
    			map.put(key, value);
    		} else {
    			mapResource = new MapValue(key, value);
    			store.put(domain, mapResource);
    		}
    	} catch ( IOException exception ) {
    		logger.log(Level.SEVERE, "Error while accessing store", exception);
    	}
    }
    
    public static int getStoreByName(String store) {
        for ( int i = 0; i < Store.stores.length; i++ ) {
            if ( Store.stores[i].equals(store) ) return i;
        }
        return Store.NONE;
    }
}