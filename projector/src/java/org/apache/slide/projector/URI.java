package org.apache.slide.projector;

import org.apache.slide.projector.value.Value;

public interface URI extends Value {
    boolean startsWith(URI uri);

    boolean isRelative();
}