package org.apache.slide.projector;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.Credentials;

public interface Context {
    /**
     *
     *
     * @param storeId Store ID's are defined in Store interface
     * @return The store or null if not available in this context
     */
    Store getStore(int storeId);

    void setProcessId(String processId);
    
    String getProcessId();
    
    void setBookmark(URI processor) throws IOException;
    
    URI getBookmark() throws IOException;
    
    void setProcess(URI process);
    
    URI getProcess();

    void setStep(String step);
    
    String getStep();
    
    void setStepStore(Store store);
    
    void setInputParameters(Map map);
    
    void setCredentials(Credentials credentials);
    
    Credentials getCredentials();

    void addInformation(Information information);

    public List getInformations();
}