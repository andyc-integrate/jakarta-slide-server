package org.apache.slide.projector;

import org.apache.slide.projector.value.StreamableValue;

public interface ConfigurableProcessor extends Processor {
    /**
     * This method is called before the first invokation of the process method
     * occurs.
     *
     * @param config the resource containing the configuration of the processor
     * @throws ConfigurationException is thrown, if the processor
     *                                could not be configured properly by the given resource
     */
    void configure(StreamableValue config) throws ConfigurationException;
}