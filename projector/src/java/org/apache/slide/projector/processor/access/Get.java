package org.apache.slide.projector.processor.access;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.Projector;
import org.apache.slide.projector.URI;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultEntryDescriptor;
import org.apache.slide.projector.descriptor.URIValueDescriptor;
import org.apache.slide.projector.i18n.DefaultMessage;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.processor.SimpleProcessor;
import org.apache.slide.projector.value.URIValue;
import org.apache.slide.projector.value.Value;

public class Get extends SimpleProcessor {
    public Value process(Value input, Context context) throws Exception {
        URI uri = new URIValue(input.toString());
        return Projector.getRepository().getResource(uri, context.getCredentials());
    }

    public ParameterDescriptor getParameterDescriptor() {
        return new ParameterDescriptor(INPUT, new ParameterMessage("simpleProcessor/parameter/input"), new URIValueDescriptor());
    }

    public ResultEntryDescriptor getResultEntryDescriptor() {
        return new ResultEntryDescriptor(OUTPUT, new DefaultMessage("simpleProcessor/result/output"), "*", true);
    }
}