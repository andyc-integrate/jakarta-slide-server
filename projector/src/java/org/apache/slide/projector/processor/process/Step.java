package org.apache.slide.projector.processor.process;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.slide.projector.Store;
import org.apache.slide.projector.URI;
import org.apache.slide.projector.value.URIValue;
import org.jdom.Element;

import de.zeigermann.xml.simpleImporter.ConversionHelpers;

public class Step {
    private static Logger logger = Logger.getLogger(Step.class.getName());

    private URI processorURI;
    private String name;
    private Map parameterConfigurations = new HashMap();
    private Map resultConfigurations = new HashMap();
    private List routingConfigurations = new ArrayList();

    public String getName() {
        return name;
    }

    public URI getProcessorURI() {
        return processorURI;
    }

    public Map getParameterConfigurations() {
        return parameterConfigurations;
    }

    public Map getResultConfigurations() {
        return resultConfigurations;
    }

    public List getRoutingConfigurations() {
        return routingConfigurations;
    }

    public void configure(Element element) {
		name = element.getAttributeValue("id");
        processorURI = new URIValue(element.getAttributeValue("processor"));
        List loadElements = element.getChildren("load");
		for ( Iterator i = loadElements.iterator(); i.hasNext(); ) {
			Element loadElement = (Element)i.next();
            String parameterName = loadElement.getAttributeValue("parameter");
            ParameterConfiguration parameterConfiguration = new ParameterConfiguration(parameterName);
            Iterator childIterator = loadElement.getChildren().iterator();
            if ( childIterator.hasNext() ) {
            	Element valueElement = (Element)childIterator.next();
            	parameterConfiguration.configure(valueElement);
            	parameterConfigurations.put(parameterName, parameterConfiguration);
            }
		}
		List saveElements = element.getChildren("save");
		for ( Iterator i = saveElements.iterator(); i.hasNext(); ) {
			Element saveElement = (Element)i.next();
            String resultName = saveElement.getAttributeValue("result");
            String resultStore = saveElement.getAttributeValue("store");
            String resultDomain = saveElement.getAttributeValue("domain");
            String resultKey = saveElement.getAttributeValue("key");
            String timeoutAsString = saveElement.getAttributeValue("timeout");
            long timeout = -1;
            if ( timeoutAsString != null ) {
            	timeout = Long.valueOf(timeoutAsString).longValue();
            }
            boolean presentableResult = ConversionHelpers.getBoolean(saveElement.getAttributeValue("presentable"), false);
            ResultConfiguration resultConfiguration;
            if (resultStore == null) {
            	resultConfiguration = new ResultConfiguration(resultName, Store.NONE, resultDomain, resultKey, presentableResult, timeout);
            } else {
                resultConfiguration = new ResultConfiguration(resultName, resultStore, resultDomain, resultKey, presentableResult, timeout);
            }
            resultConfigurations.put(resultName, resultConfiguration);
		}
		List routeElements = element.getChildren("route");
		for ( Iterator i = routeElements.iterator(); i.hasNext(); ) {
			Element routeElement = (Element)i.next();
            String state = routeElement.getAttributeValue("state");
            String exception = routeElement.getAttributeValue("exception");
            String step = routeElement.getAttributeValue("step");
            String returnValue = routeElement.getAttributeValue("return");
            if (state != null) {
                RoutingConfiguration routingConfiguration = new RoutingConfiguration(state);
                if (returnValue != null) {
                    routingConfiguration.setReturnValue(returnValue);
                }
                if (step != null) {
                    routingConfiguration.setStep(step);
                }
                routingConfigurations.add(routingConfiguration);
            } else if (exception != null) {
                try {
                    Class clazz = Class.forName(exception);
                    routingConfigurations.add(new RoutingConfiguration(clazz, step));
                } catch (ClassNotFoundException e) {
                    logger.log(Level.SEVERE, "Could not find exception class=" + exception + ", skipping exception routing");
                }
            }
		}
    }
}
