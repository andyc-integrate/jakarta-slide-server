package org.apache.slide.projector.processor.core;

import java.text.DateFormat;
import java.util.Locale;
import java.util.Map;

import org.apache.slide.projector.ContentType;
import org.apache.slide.projector.Context;
import org.apache.slide.projector.Processor;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.Store;
import org.apache.slide.projector.descriptor.DateValueDescriptor;
import org.apache.slide.projector.descriptor.LocaleValueDescriptor;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.RequiredEnvironmentDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.ResultEntryDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.descriptor.StringValueDescriptor;
import org.apache.slide.projector.i18n.DefaultMessage;
import org.apache.slide.projector.i18n.LocalizedMessage;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.processor.SimpleProcessor;
import org.apache.slide.projector.value.DateValue;
import org.apache.slide.projector.value.LocaleValue;
import org.apache.slide.projector.value.StringValue;

public class DateFormatter implements Processor {
	private final static String DATE_FORMAT = "dateFormat";
	private final static String TIME_FORMAT = "timeFormat";
    private final static String DATE = "date";
    private final static String LOCALE = "locale";
    private final static String HIDE = "hide";
    private final static String SHORT = "short";
    private final static String MEDIUM = "medium";
    private final static String LONG = "long";
    private final static String FULL = "full";
    private final static String[] STYLES = new String[] { HIDE, SHORT, MEDIUM, LONG, FULL }; 
    
    private final static ParameterDescriptor[] parameterDescriptors = new ParameterDescriptor[]{
    		new ParameterDescriptor(DATE, new ParameterMessage("dateFormatter/parameter/date"), new DateValueDescriptor()),
    		new ParameterDescriptor(DATE_FORMAT, new ParameterMessage("dateFormatter/parameter/dateFormat"), new StringValueDescriptor(STYLES), new StringValue(SHORT)),
    		new ParameterDescriptor(TIME_FORMAT, new ParameterMessage("dateFormatter/parameter/timeFormat"), new StringValueDescriptor(STYLES), new StringValue(SHORT)),
    };

    
    private final static ResultDescriptor resultDescriptor = new ResultDescriptor(
            new StateDescriptor[]{ StateDescriptor.OK_DESCRIPTOR },
            new ResultEntryDescriptor[]{new ResultEntryDescriptor(
                    SimpleProcessor.OUTPUT, new LocalizedMessage("dateFormatter/result/output"),
                    ContentType.PLAIN_TEXT, true)});

    private final static RequiredEnvironmentDescriptor[] requiredEnvironmentDescriptors = new RequiredEnvironmentDescriptor[] {
    		new RequiredEnvironmentDescriptor(LOCALE, Store.SESSION, new DefaultMessage("message/requiredEnvironment/locale"), new LocaleValueDescriptor(), new LocaleValue(Locale.getDefault()))
    };

	public Result process(Map parameters, Context context) throws Exception {
		DateValue date = (DateValue)parameters.get(DATE); 
		String dateFormat = parameters.get(DATE_FORMAT).toString();
		String timeFormat = parameters.get(TIME_FORMAT).toString();
		Locale locale = ((LocaleValue)context.getStore(Store.SESSION).get(LOCALE)).getLocale();
		String formattedDate = getDateFormatter(dateFormat, timeFormat, locale).format(date.getDate());
		return new Result(StateDescriptor.OK, SimpleProcessor.OUTPUT, new StringValue(formattedDate));
	}

	public ParameterDescriptor[] getParameterDescriptors() {
		return parameterDescriptors; 
	}

	public ResultDescriptor getResultDescriptor() {
		return resultDescriptor;
	}
	
	private DateFormat getDateFormatter(String dateFormat, String timeFormat, Locale locale) {
		if ( dateFormat.equals(HIDE) ) {
			return DateFormat.getTimeInstance(getStyleByName(timeFormat), locale);
		} else if ( timeFormat.equals(HIDE) ) {
			return DateFormat.getDateInstance(getStyleByName(dateFormat), locale);
		} else {
			return DateFormat.getDateTimeInstance(getStyleByName(dateFormat), getStyleByName(timeFormat), locale);
		}
	}
	
	private int getStyleByName(String styleName) {
		if ( styleName.equals(SHORT) ) {
			return DateFormat.SHORT;
		} else if ( styleName.equals(MEDIUM) ) {
			return DateFormat.MEDIUM;
		} else if ( styleName.equals(LONG) ) {
			return DateFormat.LONG;
		}
		return DateFormat.FULL;
	}
}