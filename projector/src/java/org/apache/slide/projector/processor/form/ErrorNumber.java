package org.apache.slide.projector.processor.form;

import java.util.Map;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.processor.TemplateRenderer;

/**
 * @version $Revision: 1.2 $
 */

public class ErrorNumber extends TemplateRenderer {
    private final static String FRAGMENT = "error-number";

    public ErrorNumber() {
        setRequiredFragments(new String[] { FRAGMENT });
    }

    public Result process(Map parameter, Context context) throws Exception {
        return new Result(OK, OUTPUT, renderFragment(FRAGMENT, parameter));
    }
}