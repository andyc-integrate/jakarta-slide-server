/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/projector/src/java/org/apache/slide/projector/processor/form/Form.java,v 1.2 2006-01-22 22:45:14 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:45:14 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.projector.processor.form;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.slide.projector.ConfigurationException;
import org.apache.slide.projector.Constants;
import org.apache.slide.projector.ContentType;
import org.apache.slide.projector.Context;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.Store;
import org.apache.slide.projector.URI;
import org.apache.slide.projector.descriptor.AnyValueDescriptor;
import org.apache.slide.projector.descriptor.BooleanValueDescriptor;
import org.apache.slide.projector.descriptor.LocaleValueDescriptor;
import org.apache.slide.projector.descriptor.MapValueDescriptor;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.ResultEntryDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.descriptor.StringValueDescriptor;
import org.apache.slide.projector.descriptor.URIValueDescriptor;
import org.apache.slide.projector.engine.ProcessorManager;
import org.apache.slide.projector.i18n.DefaultMessage;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.processor.process.Process;
import org.apache.slide.projector.value.ArrayValue;
import org.apache.slide.projector.value.BooleanValue;
import org.apache.slide.projector.value.LocaleValue;
import org.apache.slide.projector.value.MapValue;
import org.apache.slide.projector.value.NullValue;
import org.apache.slide.projector.value.StreamableValue;
import org.apache.slide.projector.value.StringValue;
import org.apache.slide.projector.value.Value;

/**
 * The Form class
 * 
 */
public class Form extends ControlComposer {
	public final static String CONTROL_IDENTIFIER = "control:";
    public final static String TRIGGER_IDENTIFIER = "trigger:";

    private ParameterDescriptor[] parameterDescriptors;
    
    private final static ResultDescriptor resultDescriptor = new ResultDescriptor(
            new StateDescriptor[] {
                new StateDescriptor(DEFAULT_STATE, new DefaultMessage("form/state/default")),
                new StateDescriptor(VALID_STATE, new DefaultMessage("form/state/valid")),
                new StateDescriptor(INVALID_STATE, new DefaultMessage("form/state/invalid"))
            },
            new ResultEntryDescriptor[]{
                new ResultEntryDescriptor(OUTPUT, new DefaultMessage("form/output"), ContentType.DYNAMIC, false)
            });

    public Result process(Map parameter, Context context) throws Exception {
    	Object formResult = context.getStore(Store.FORM).get(FormHandler.RESULT);
    	if ( formResult instanceof Result ) {
    		return (Result)formResult;  
    	}
    	Object a = parameter.get(ACTION); 
    	URI actionUri = (URI)a;
    	Locale locale = ((LocaleValue)parameter.get(LOCALE)).getLocale();
        List informations = context.getInformations();
        parameter.put(HANDLER, ProcessorManager.getInstance().process(ProcessorManager.URL, parameter.get(HANDLER), context));
        List controlDescriptions = new ArrayList();
        List triggerDescriptions = new ArrayList();
        for (int i = 0; i < parameterDescriptors.length; i++) {
        	String parameterName = parameterDescriptors[i].getName(); 
        	Value value = (Value)parameter.get(parameterName);
            if (parameterName.startsWith(CONTROL_IDENTIFIER)) {
            	((MapValue)value).getMap().put(CONTROL_NAME, parameterName);
            	controlDescriptions.add(value);
            } else if (parameterName.startsWith(TRIGGER_IDENTIFIER)) {
            	((MapValue)value).getMap().put(TRIGGER_NAME, parameterName);
            	triggerDescriptions.add(value);
            }
        }
        parameter.put(ControlComposer.CONTROL_DESCRIPTIONS, new ArrayValue((Value[])controlDescriptions.toArray(new Value[controlDescriptions.size()])));
        parameter.put(ControlComposer.TRIGGER_DESCRIPTIONS, new ArrayValue((Value[])triggerDescriptions.toArray(new Value[triggerDescriptions.size()])));
        ProcessorManager.prepareValues(super.getParameterDescriptors(), parameter, context);
        Result controlComposerResult = super.process(parameter, context);
        Value[] generatedControls = ((ArrayValue)controlComposerResult.getResultEntries().get(GENERATED_CONTROLS)).getArray();
        for ( int i = 0; i < generatedControls.length; i++ ) {
        	Iterator j = ((MapValue)generatedControls[i]).getMap().entrySet().iterator();
        	Map.Entry entry = (Map.Entry)j.next();
        	parameter.put(entry.getKey(), entry.getValue());
        }
        Value[] generatedTriggers = ((ArrayValue)controlComposerResult.getResultEntries().get(GENERATED_TRIGGERS)).getArray();
        for ( int i = 0; i < generatedTriggers.length; i++ ) {
        	Iterator j = ((MapValue)generatedTriggers[i]).getMap().entrySet().iterator();
        	Map.Entry entry = (Map.Entry)j.next();
        	parameter.put(entry.getKey(), entry.getValue());
        }
    	parameter.put(RENDERED_ERRORS, controlComposerResult.getResultEntries().get(RENDERED_ERRORS));
        Template template = defaultTemplate;
        String state = controlComposerResult.getState();
        if ( state == VALID_STATE && validTemplate != null ) {
            template = validTemplate;
        } else if ( state == INVALID_STATE && invalidTemplate != null ) {
            template = invalidTemplate;
        }
        return new Result(state, OUTPUT, renderFragment(template, parameter));
    }

    public void configure(StreamableValue config) throws ConfigurationException {
        super.configure(config);
        ParameterDescriptor[] parentParameterDescriptors = super.getParameterDescriptors();
        int counter = 0;
        List parameterList = new ArrayList();
        for (int i = 0; i < parentParameterDescriptors.length; i++) {
            if (parentParameterDescriptors[i].getName().startsWith(CONTROL_IDENTIFIER)) {
                parameterList.add(new ParameterDescriptor(parentParameterDescriptors[i].getName(),
                        new ParameterMessage("form/control"),
                        new MapValueDescriptor(new ParameterDescriptor[] {
                            new ParameterDescriptor(CONTROL, new ParameterMessage("form/control"), new URIValueDescriptor()),
                            new ParameterDescriptor(CONTROL_CONTAINER, new ParameterMessage("form/controlContainer"),  new URIValueDescriptor(), NullValue.NULL),
                            new ParameterDescriptor(Control.ACTION, new ParameterMessage("control/action"), new URIValueDescriptor(), NullValue.NULL),
                            new ParameterDescriptor(Control.PARAMETER, new ParameterMessage("control/parameter"), new StringValueDescriptor())
                        })));
            } else if (parentParameterDescriptors[i].getName().startsWith(TRIGGER_IDENTIFIER)) {
                    parameterList.add(new ParameterDescriptor(parentParameterDescriptors[i].getName(),
                            new ParameterMessage("form/trigger"),
                            new MapValueDescriptor(new ParameterDescriptor[] {
                                new ParameterDescriptor(TRIGGER, new ParameterMessage("form/trigger"), new URIValueDescriptor()),
                                new ParameterDescriptor(TRIGGER_CONTAINER, new ParameterMessage("form/triggerContainer"),  new URIValueDescriptor(), NullValue.NULL),
                                new ParameterDescriptor(Trigger.ACTION, new ParameterMessage("trigger/action"), new URIValueDescriptor(), NullValue.NULL),
	        					new ParameterDescriptor(Trigger.VALIDATE, new ParameterMessage("trigger/validate"), new BooleanValueDescriptor(), BooleanValue.TRUE),
                                new ParameterDescriptor(Process.STEP, new ParameterMessage("trigger/step"),  new AnyValueDescriptor(), NullValue.NULL)
                            })));
            } else if (!parentParameterDescriptors[i].getName().equals(FRAGMENT) 
            		&& !parentParameterDescriptors[i].getName().equals(CONTROL_DESCRIPTIONS)
            		&& !parentParameterDescriptors[i].getName().equals(TRIGGER_DESCRIPTIONS)
					&& !parentParameterDescriptors[i].getName().equals(RENDERED_ERRORS)
					&& !parentParameterDescriptors[i].getName().equals(HANDLER)
					&& !parentParameterDescriptors[i].getName().equals(METHOD)) {
                parameterList.add(parentParameterDescriptors[i]);
            }
        }
        parameterList.add(new ParameterDescriptor(HANDLER, new ParameterMessage("form/handler"), new URIValueDescriptor(), Constants.DEFAULT_FORM_HANDLER));
        parameterList.add(new ParameterDescriptor(METHOD, new ParameterMessage("form/method"), new StringValueDescriptor(methods), new StringValue(POST)));
        parameterList.add(new ParameterDescriptor(LOCALE, new ParameterMessage("form/locale"), new LocaleValueDescriptor()));
        parameterList.add(new ParameterDescriptor(ACTION, new ParameterMessage("form/action"), new URIValueDescriptor()));
        parameterDescriptors = (ParameterDescriptor[])parameterList.toArray(new ParameterDescriptor[parameterList.size()]);
    }

    public ParameterDescriptor[] getParameterDescriptors() {
        return parameterDescriptors;
    }

    public ResultDescriptor getResultDescriptor() {
        return resultDescriptor;
    }
}