package org.apache.slide.projector.processor.core;

import java.util.Locale;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.ProcessException;
import org.apache.slide.projector.Processor;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.descriptor.AnyValueDescriptor;
import org.apache.slide.projector.descriptor.LocaleValueDescriptor;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.ResultEntryDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.i18n.DefaultMessage;
import org.apache.slide.projector.i18n.ErrorMessage;
import org.apache.slide.projector.i18n.MessageNotFoundException;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.value.LocaleValue;
import org.apache.slide.projector.value.ObjectValue;
import org.apache.slide.projector.value.StringValue;

public class ExceptionRenderer implements Processor {
    private static Logger logger = Logger.getLogger(ExceptionRenderer.class.getName());

    final static String OK = "ok";

    public final static String EXCEPTION = "exception";
    public final static String CLASS = "class";
    public final static String TITLE = "title";
    public final static String STACK_TRACE = "stackTrace";
    public final static String TEXT = "text";
    public final static String SUMMARY = "summary";
    public final static String DETAILS = "details";
    public final static String LOCALE = "locale";

    public Result process(Map parameter, Context context) throws Exception {
        Locale locale = (((LocaleValue)parameter.get(LOCALE)).getLocale());
        ObjectValue exception = (ObjectValue)parameter.get(EXCEPTION);
        Throwable throwable = (Throwable)exception.getObject();
        Result result = new Result(OK);
        String title = null, text = null, summary = null;
        StringBuffer details = new StringBuffer();
        if ( throwable instanceof ProcessException ) {
            try {
                title = ((ProcessException)throwable).getErrorMessage().getTitle(locale);
                text = ((ProcessException)throwable).getErrorMessage().getText(locale);
                summary = ((ProcessException)throwable).getErrorMessage().getSummary(locale);
            } catch ( MessageNotFoundException e ) {
                title = ((ProcessException)throwable).getErrorMessage().getId();
                text = ((ProcessException)throwable).getErrorMessage().getId();
                summary = ((ProcessException)throwable).getErrorMessage().getId();
            }
            appendNestedDetails(details, throwable, locale);
        } else {
            title = throwable.getLocalizedMessage();
            text = throwable.getLocalizedMessage();
            summary = throwable.getLocalizedMessage();
            appendNestedDetails(details, throwable, locale);
        }
        if ( title == null ) title = "";
        if ( text == null ) text = "";
        if ( summary == null ) summary = "";
        result.addResultEntry(TITLE, new StringValue(title));
        result.addResultEntry(TEXT, new StringValue(text));
        result.addResultEntry(SUMMARY, new StringValue(summary));
        result.addResultEntry(DETAILS, new StringValue(details.toString()));
        result.addResultEntry(CLASS, new StringValue(throwable.getClass().getName()));
        StackTraceElement []trace = throwable.getStackTrace();
        StringBuffer buffer = new StringBuffer(256);
        for ( int i = 0; i < trace.length; i++ ) {
            buffer.append(trace[i].toString());
            buffer.append(" ");
        }
        result.addResultEntry(STACK_TRACE, new StringValue(buffer.toString()));
        return result;
    }

    private void appendNestedDetails(StringBuffer details, Throwable throwable, Locale locale) {
        if ( details.length() > 0 ) details.append(' ');
        if ( throwable instanceof ProcessException ) {
            ErrorMessage message = ((ProcessException)throwable).getErrorMessage();
            details.append(message.getDetails(locale, ""));
        } else {
            details.append(throwable.getMessage());
        }
        if ( throwable.getCause() != null ) appendNestedDetails(details, throwable.getCause(), locale);
    }

    public ParameterDescriptor[] getParameterDescriptors() {
        return new ParameterDescriptor[]{
            new ParameterDescriptor(EXCEPTION, new ParameterMessage("exceptionRenderer/exception"), new AnyValueDescriptor()),
            new ParameterDescriptor(LOCALE, new ParameterMessage("exceptionRenderer/locale"), new LocaleValueDescriptor(), new LocaleValue(Locale.getDefault()))
        };
    }


    public ResultDescriptor getResultDescriptor() {
        return new ResultDescriptor(new StateDescriptor[]{ StateDescriptor.OK_DESCRIPTOR },
                new ResultEntryDescriptor[]{
                    new ResultEntryDescriptor(TITLE, new DefaultMessage("exceptionRenderer/result/title"), "text/plain", true),
                    new ResultEntryDescriptor(TEXT, new DefaultMessage("exceptionRenderer/result/text"), "text/plain", true),
                    new ResultEntryDescriptor(SUMMARY, new DefaultMessage("exceptionRenderer/result/summary"), "text/plain", true),
                    new ResultEntryDescriptor(DETAILS, new DefaultMessage("exceptionRenderer/result/details"), "text/plain", true),
                    new ResultEntryDescriptor(CLASS, new DefaultMessage("exceptionRenderer/result/class"), "text/plain", true),
                    new ResultEntryDescriptor(STACK_TRACE, new DefaultMessage("exceptionRenderer/result/stackTrace"), "text/plain", true)
                });
    }
}
