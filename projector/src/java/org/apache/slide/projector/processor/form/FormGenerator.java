package org.apache.slide.projector.processor.form;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.slide.projector.ConfigurationException;
import org.apache.slide.projector.Constants;
import org.apache.slide.projector.Context;
import org.apache.slide.projector.HttpContext;
import org.apache.slide.projector.ProcessException;
import org.apache.slide.projector.Processor;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.URI;
import org.apache.slide.projector.descriptor.ArrayValueDescriptor;
import org.apache.slide.projector.descriptor.BooleanValueDescriptor;
import org.apache.slide.projector.descriptor.LocaleValueDescriptor;
import org.apache.slide.projector.descriptor.NumberValueDescriptor;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ProcessorDescriptor;
import org.apache.slide.projector.descriptor.StringValueDescriptor;
import org.apache.slide.projector.descriptor.URIValueDescriptor;
import org.apache.slide.projector.descriptor.ValueDescriptor;
import org.apache.slide.projector.engine.ProcessorManager;
import org.apache.slide.projector.i18n.DefaultMessage;
import org.apache.slide.projector.i18n.ErrorMessage;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.processor.process.Process;
import org.apache.slide.projector.value.ArrayValue;
import org.apache.slide.projector.value.BooleanValue;
import org.apache.slide.projector.value.LocaleValue;
import org.apache.slide.projector.value.MapValue;
import org.apache.slide.projector.value.NullValue;
import org.apache.slide.projector.value.StreamableValue;
import org.apache.slide.projector.value.StringValue;
import org.apache.slide.projector.value.URIValue;
import org.apache.slide.projector.value.Value;

/**
 * @version $Revision: 1.3 $
 */
public class FormGenerator extends ControlComposer {
    public final static String CONTROLS = "controls";
    public final static String TRIGGERS = "triggers";
    public final static String TARGET_STEP = "targetStep";

    protected final static String TRIGGER_IMAGE = "image";
    protected final static URI TEXTFIELD = new URIValue("textfield");
    protected final static URI TEXTAREA = new URIValue("textarea");
    protected final static URI LISTBOX = new URIValue("listbox");
    protected final static URI COMBOBOX = new URIValue("combobox");
    protected final static URI CHECKBOX = new URIValue("checkbox");
    protected final static URI ERRORS_TABLE = new URIValue("errors");
    protected final static URI TRIGGER_URI = new URIValue("textTrigger");
    protected final static URI DEFAULT_TRIGGER_IMAGE = new URIValue("/files/contelligent/images/ok.gif");
    protected final static URI DEFAULT_CONTROL_CONTAINER = new URIValue("bigControl");
    protected final static URI DEFAULT_TRIGGER_CONTAINER = new URIValue("triggerContainer");
    protected final static URI DEFAULT_ERROR_RENDERER = new URIValue("errors");

    protected final static String PROCESSOR_NAME = "processor-name";
    protected final static String PROCESSOR_TITLE = "processor-title";
    protected final static String PROCESSOR_TEXT = "processor-text";
    protected final static String PROCESSOR_SMALL_ICON = "processor-small-icon";
    protected final static String PROCESSOR_LARGE_ICON = "processor-large-icon";

    protected final static String STYLE = "style";
    protected final static String ORANGE_STYLE = "style.html";
    protected final static String LAUNCH_PROCESSOR = "trigger:launch";
    
    private ParameterDescriptor[] parameterDescriptors;

    public FormGenerator() {
    	super();
    }

    public Result process(Map parameter, Context context) throws Exception {
        URI actionUri = (URIValue)parameter.get(Control.ACTION);
        Processor action = ProcessorManager.getInstance().getProcessor(actionUri);
        Locale locale = ((LocaleValue)parameter.get(LOCALE)).getLocale();
        ParameterDescriptor []actionParameterDescriptors = action.getParameterDescriptors();
        StringBuffer buffer = new StringBuffer();
        List controlDescriptors = new ArrayList();
        // Collect controls to compose...
        for (int i = 0; i < actionParameterDescriptors.length; i++) {
        	String parameterName = actionParameterDescriptors[i].getName(); 
        	MapValue controlDescriptor = new MapValue();
        	controlDescriptor.getMap().put(Control.ACTION, actionUri);
        	controlDescriptor.getMap().put(Control.PARAMETER, parameterName);
        	controlDescriptor.getMap().put(CONTROL, getControlURI(actionParameterDescriptors[i].getValueDescriptor()));
        	controlDescriptor.getMap().put(CONTROL_CONTAINER, DEFAULT_CONTROL_CONTAINER);
        	controlDescriptor.getMap().put(CONTROL_NAME, parameterName);
        	controlDescriptors.add(controlDescriptor);
        }
        parameter.put(CONTROL_DESCRIPTIONS, new ArrayValue((Value[])controlDescriptors.toArray(new Value[controlDescriptors.size()])));

        // Finally add testing trigger...
    	MapValue triggerDescriptor = new MapValue();
    	triggerDescriptor.getMap().put(Trigger.ACTION, actionUri);
    	triggerDescriptor.getMap().put(Trigger.VALIDATE, BooleanValue.TRUE);
    	triggerDescriptor.getMap().put(Trigger.INVOLVED_PARAMETERS, NullValue.NULL);
    	triggerDescriptor.getMap().put(Process.STEP, parameter.get(TARGET_STEP));
    	triggerDescriptor.getMap().put(TRIGGER, TRIGGER_URI);
    	triggerDescriptor.getMap().put(TRIGGER_NAME, LAUNCH_PROCESSOR);
    	triggerDescriptor.getMap().put(TRIGGER_IMAGE, ProcessorManager.getInstance().process(ProcessorManager.BINARY, DEFAULT_TRIGGER_IMAGE, "path", context));
    	triggerDescriptor.getMap().put(TRIGGER_CONTAINER, DEFAULT_TRIGGER_CONTAINER);
        parameter.put(TRIGGER_DESCRIPTIONS, new ArrayValue(new Value[] { triggerDescriptor }));
        parameter.put(ERRORS_PROCESSOR, DEFAULT_ERROR_RENDERER);
        
        parameter.put(HANDLER, ProcessorManager.getInstance().process(ProcessorManager.URL, Constants.DEFAULT_FORM_HANDLER, context));
        parameter.put(METHOD, new StringValue(POST));
        Result controlComposerResult = super.process(parameter, context);
        StringBuffer controlBuffer = new StringBuffer();
        Value []generatedControls = ((ArrayValue)controlComposerResult.getResultEntries().get(ControlComposer.GENERATED_CONTROLS)).getArray();
        for ( int i = 0; i < generatedControls.length; i++ ) {
        	Iterator j = ((MapValue)generatedControls[i]).getMap().values().iterator();
        	StringValue renderedControl = (StringValue)j.next();
        	buffer.append(renderedControl.toString());
        }
        StringValue composedControls = new StringValue(buffer.toString());
        parameter.put(CONTROLS, composedControls);
        StringBuffer triggerBuffer = new StringBuffer();
        Value []generatedTriggers = ((ArrayValue)controlComposerResult.getResultEntries().get(ControlComposer.GENERATED_TRIGGERS)).getArray();
        for ( int i = 0; i < generatedTriggers.length; i++ ) {
        	Iterator j = ((MapValue)generatedTriggers[i]).getMap().values().iterator();
        	StringValue renderedTrigger = (StringValue)j.next();
        	triggerBuffer.append(renderedTrigger.toString());
        }
        StringValue composedTriggers= new StringValue(triggerBuffer.toString());
        parameter.put(TRIGGERS, composedTriggers);
        ProcessorDescriptor processorDescriptor = ProcessorManager.getInstance().getProcessorDescriptor(actionUri);
        parameter.put(PROCESSOR_NAME, processorDescriptor.getName());
        parameter.put(PROCESSOR_TITLE, ((DefaultMessage)processorDescriptor.getDescription()).getTitle(locale, processorDescriptor.getName()));
        parameter.put(PROCESSOR_TEXT, ((DefaultMessage)processorDescriptor.getDescription()).getText(locale, ""));
        parameter.put(PROCESSOR_LARGE_ICON, ProcessorManager.getInstance().process(ProcessorManager.BINARY, processorDescriptor.getLargeIcon(), "path", context));
        parameter.put(PROCESSOR_SMALL_ICON, ProcessorManager.getInstance().process(ProcessorManager.BINARY, processorDescriptor.getSmallIcon(), "path", context));
        parameter.put(TITLE, processorDescriptor.getName());
        parameter.put(STYLE, ((HttpContext)context).getContextPath() + ORANGE_STYLE);
        parameter.put(ERRORS, controlComposerResult.getResultEntries().get(ControlComposer.RENDERED_ERRORS)); 
        parameter.put(ERRORS_TITLE, "Fehler:");
        Template template = defaultTemplate;
        String state = controlComposerResult.getState();
        if ( state == VALID_STATE && validTemplate != null ) {
            template = validTemplate;
        } else if ( state == INVALID_STATE && invalidTemplate != null ) {
            template = invalidTemplate;
        }
        return new Result(state, OUTPUT, renderFragment(template, parameter));
    }

    public void configure(StreamableValue config) throws ConfigurationException {
        super.configure(config);
        parameterDescriptors = new ParameterDescriptor[] {
            new ParameterDescriptor(ACTION, new ParameterMessage("formGenerator/action"), new URIValueDescriptor()),
            new ParameterDescriptor(LOCALE, new ParameterMessage("formGenerator/locale"), new LocaleValueDescriptor(), new LocaleValue(Locale.getDefault())),
            new ParameterDescriptor(TARGET_STEP, new ParameterMessage("formGenerator/targetStep"), new StringValueDescriptor())
        };
        try {
            defaultTemplate = getRequiredFragment(DEFAULT_FORM);
        } catch ( ProcessException exception ) {
            throw new ConfigurationException(new ErrorMessage("form/defaultFragmentMissing"));
        }
        validTemplate = getOptionalFragment(VALID_FORM);
        invalidTemplate = getOptionalFragment(INVALID_FORM);
    }

    public ParameterDescriptor[] getParameterDescriptors() {
        return parameterDescriptors;
    }

    public static URI getControlURI(ValueDescriptor descriptor) {
        if ( descriptor instanceof ArrayValueDescriptor ) {
            return LISTBOX;
        }
        if ( descriptor instanceof StringValueDescriptor ) {
            if ( ((StringValueDescriptor)descriptor).isEnumrable() ) {
                return COMBOBOX;
            } else if ( ((StringValueDescriptor)descriptor).getMaximumLength() < 256 ) {
                return TEXTFIELD;
            } else {
                return TEXTAREA;
            }
        }
        if ( descriptor instanceof BooleanValueDescriptor ) {
            return CHECKBOX;
        }
        if ( descriptor instanceof NumberValueDescriptor ) {
            if ( ((NumberValueDescriptor)descriptor).isConstrained() ) {
                return COMBOBOX;
            }
            return TEXTFIELD;
        }
        return TEXTFIELD;
    }
}