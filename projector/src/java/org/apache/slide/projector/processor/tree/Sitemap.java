/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/projector/src/java/org/apache/slide/projector/processor/tree/Sitemap.java,v 1.2 2006-01-22 22:45:14 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:45:14 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.projector.processor.tree;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.slide.projector.ConfigurableProcessor;
import org.apache.slide.projector.ConfigurationException;
import org.apache.slide.projector.Context;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.ResultEntryDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.i18n.DefaultMessage;
import org.apache.slide.projector.processor.SimpleProcessor;
import org.apache.slide.projector.value.ArrayValue;
import org.apache.slide.projector.value.StreamableValue;
import org.apache.slide.projector.value.StringValue;
import org.apache.slide.projector.value.Value;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.AttributesImpl;

import de.zeigermann.xml.simpleImporter.DefaultSimpleImportHandler;
import de.zeigermann.xml.simpleImporter.SimpleImporter;
import de.zeigermann.xml.simpleImporter.SimplePath;

/**
 * The Sitemap class
 * 
 */
public class Sitemap implements ConfigurableProcessor {
    private static Logger logger = Logger.getLogger(Sitemap.class.getName());

    private final static ParameterDescriptor[] parameterDescriptors = new ParameterDescriptor[0];
    private static ResultDescriptor resultDescriptors;

    private static Result result = new Result(StateDescriptor.OK);

    public void configure(StreamableValue config) throws ConfigurationException {
        try {
            InputStream configuration = config.getInputStream();
            SimpleImporter importer = new SimpleImporter();
            ConfigurationHandler handler = new ConfigurationHandler();
            importer.addSimpleImportHandler(handler);
            importer.parse(new InputSource(configuration));
            Page rootPage = handler.getRootPage();
            List resources = new ArrayList();
            buildArray(resources, rootPage);
            Value[] array = new Value[resources.size()];
            ArrayValue arrayResource = new ArrayValue((Value [])resources.toArray(array));
            result.addResultEntry(SimpleProcessor.OUTPUT, arrayResource);
            resultDescriptors = new ResultDescriptor(
                    new StateDescriptor[] { StateDescriptor.OK_DESCRIPTOR },
                    new ResultEntryDescriptor[] {
                        new ResultEntryDescriptor(SimpleProcessor.OUTPUT, new DefaultMessage("sitemap/output"), ArrayValue.CONTENT_TYPE, false)
                    });
        } catch (Exception exception) {
            logger.log(Level.SEVERE, "Error while parsing sitemap configuration", exception);
        }
    }

    private void buildArray(List resources, Page page) {
        for ( Iterator j = page.getChildren().iterator(); j.hasNext(); ) {
            Page child = (Page)j.next();
            if ( child.getId() != null ) {
                resources.add(new StringValue(child.getId()));
            }
            if ( child.getChildren().size() > 0 ) {
                List childrenResources = new ArrayList();
                buildArray(childrenResources, child);
                Value[] array = new Value[childrenResources.size()];
                ArrayValue arrayResource = new ArrayValue((Value [])childrenResources.toArray(array));
                resources.add(arrayResource);
            }
        }
    }

    public Result process(Map parameter, Context context) throws Exception {
        return result;
    }

    public ParameterDescriptor[] getParameterDescriptors() {
        return parameterDescriptors;
    }

    public ResultDescriptor getResultDescriptor() {
        return resultDescriptors;
    }

    class Page {
        private List children = new ArrayList();
        private String id;

        public Page(String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }

        public void addChild(Page page) {
            children.add(page);
        }

        public List getChildren() {
            return children;
        }
    }

    class ConfigurationHandler extends DefaultSimpleImportHandler {
        private Stack pageStack = new Stack();
        private Page rootPage;

        public Page getRootPage() {
            return rootPage;
        }

        public void startElement(SimplePath path, String name, AttributesImpl attributes, String leadingCDdata) {
            if ( path.matches("page") ) {
                String id = attributes.getValue(org.apache.slide.projector.processor.tree.TreeRenderer.ID);
                Page page = new Page(id);
                Page parentPage = (Page)pageStack.peek();
                parentPage.addChild(page);
                pageStack.push(page);
            } else if ( path.matches("sitemap" ) ) {
                Page page = new Page(null);
                pageStack.push(page);
                rootPage = page;
            }
        }

        public void endElement(SimplePath path, String name) {
            if ( path.matches("page") ) {
                pageStack.pop();
            }
        }
    }
}
