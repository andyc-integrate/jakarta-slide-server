package org.apache.slide.projector.processor.form;

import java.util.Map;

import org.apache.slide.projector.ConfigurationException;
import org.apache.slide.projector.ContentType;
import org.apache.slide.projector.Context;
import org.apache.slide.projector.ProcessException;
import org.apache.slide.projector.Processor;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.URI;
import org.apache.slide.projector.descriptor.AnyValueDescriptor;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.ResultEntryDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.descriptor.StringValueDescriptor;
import org.apache.slide.projector.descriptor.URIValueDescriptor;
import org.apache.slide.projector.engine.ProcessorManager;
import org.apache.slide.projector.i18n.DefaultMessage;
import org.apache.slide.projector.i18n.ErrorMessage;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.processor.TemplateRenderer;
import org.apache.slide.projector.util.ProcessorHelper;
import org.apache.slide.projector.value.NullValue;
import org.apache.slide.projector.value.StreamableValue;
import org.apache.slide.projector.value.StringValue;
import org.apache.slide.projector.value.URIValue;

/**
 * @version $Revision: 1.2 $
 */

public class Control extends TemplateRenderer {
    private final static String NAME = " control";

    public final static String OPTIONAL_VALID_CONTROL = "valid optional";
    public final static String OPTIONAL_INVALID_CONTROL = "invalid optional";
    public final static String OPTIONAL_CONTROL = "optional";
    public final static String REQUIRED_CONTROL = "required";
    public final static String REQUIRED_VALID_CONTROL = "valid required";
    public final static String REQUIRED_INVALID_CONTROL = "invalid required";

    public final static String ACTION = "action";
    public final static String PARAMETER = "parameter";
    public final static String VALUE = "value";
    public final static String STATE = "state";

    protected Template optionalFragment, requiredFragment, validOptionalFragment, invalidOptionalFragment, validRequiredFragment, invalidRequiredFragment;

    private ParameterDescriptor[] parameterDescriptors;
    private final static ResultDescriptor resultDescriptor = new ResultDescriptor(
            new StateDescriptor[] { StateDescriptor.OK_DESCRIPTOR },
            new ResultEntryDescriptor[]{
                new ResultEntryDescriptor(OUTPUT, new DefaultMessage("control/output"), ContentType.DYNAMIC, true)
            });

    public Control() {
        setRequiredFragments(new String[] { OPTIONAL_CONTROL+getName(), REQUIRED_CONTROL+getName() });
        setOptionalFragments(new String[] { OPTIONAL_VALID_CONTROL+getName(), OPTIONAL_INVALID_CONTROL+getName(),
                                            REQUIRED_VALID_CONTROL+getName(), REQUIRED_INVALID_CONTROL+getName() });
    }

    public Result process(Map parameter, Context context) throws Exception {
        String state = parameter.get(STATE).toString();
        Template fragment = optionalFragment;
        if ( state.equals(REQUIRED_CONTROL) ) {
            fragment = requiredFragment;
        }
        if ( state.equals(OPTIONAL_VALID_CONTROL) ) {
            fragment = validOptionalFragment;
        } else if ( state.equals(OPTIONAL_INVALID_CONTROL) ) {
            fragment = invalidOptionalFragment;
        } else if ( state.equals(REQUIRED_VALID_CONTROL) ) {
            fragment = validRequiredFragment;
        } else if ( state.equals(REQUIRED_INVALID_CONTROL) ) {
            fragment = invalidRequiredFragment;
        }
        return new Result(state, OUTPUT, renderFragment(fragment, parameter));
    }

    public void configure(StreamableValue config) throws ConfigurationException {
        super.configure(config);
        ParameterDescriptor[] parentParameterDescriptors = super.getParameterDescriptors();
        parameterDescriptors = new ParameterDescriptor[parentParameterDescriptors.length + 3];
        int counter = 0;
        for ( int i = 0; i < parentParameterDescriptors.length; i++ ) {
            if (!parentParameterDescriptors[i].getName().equals(FRAGMENT)) {
                parameterDescriptors[counter] = parentParameterDescriptors[i];
                counter++;
            }
        }
        parameterDescriptors[parentParameterDescriptors.length - 1] =
                new ParameterDescriptor(ACTION, new ParameterMessage("control/action"), new URIValueDescriptor());
        parameterDescriptors[parentParameterDescriptors.length ] =
                new ParameterDescriptor(PARAMETER, new ParameterMessage("control/parameter"), new URIValueDescriptor());
        parameterDescriptors[parentParameterDescriptors.length + 1] =
                new ParameterDescriptor(VALUE, new ParameterMessage("control/value"), new AnyValueDescriptor(), new NullValue());
        parameterDescriptors[parentParameterDescriptors.length + 2] =
                new ParameterDescriptor(STATE, new ParameterMessage("control/state"), new StringValueDescriptor(new String[] {OPTIONAL_VALID_CONTROL, OPTIONAL_INVALID_CONTROL, OPTIONAL_CONTROL, REQUIRED_CONTROL, REQUIRED_VALID_CONTROL, REQUIRED_INVALID_CONTROL}), new StringValue(OPTIONAL));

        try {
            optionalFragment = getRequiredFragment(OPTIONAL_CONTROL + getName());
            requiredFragment = getRequiredFragment(REQUIRED_CONTROL + getName());
        } catch ( ProcessException exception ) {
            throw new ConfigurationException(new ErrorMessage("control/requiredFragmentMissing", new String[] { OPTIONAL+getName()}), exception);
        }
        validOptionalFragment = getOptionalFragment(OPTIONAL_VALID_CONTROL + getName(), optionalFragment);
        invalidOptionalFragment = getOptionalFragment(OPTIONAL_INVALID_CONTROL + getName(), optionalFragment);
        validRequiredFragment = getOptionalFragment(REQUIRED_VALID_CONTROL + getName(), requiredFragment);
        invalidRequiredFragment = getOptionalFragment(REQUIRED_INVALID_CONTROL + getName(), requiredFragment);
    }

    public ParameterDescriptor[] getParameterDescriptors() {
        return parameterDescriptors;
    }

    public ResultDescriptor getResultDescriptor() {
        return resultDescriptor;
    }

    static ParameterDescriptor getParameterDescriptor(Map parameter, Context context) throws ProcessException {
        URI actionUri = (URIValue)parameter.get(ACTION);
        Processor action = ProcessorManager.getInstance().getProcessor(actionUri);
        String parameterName = parameter.get(PARAMETER).toString();
        ParameterDescriptor parameterDescriptor = ProcessorHelper.getParameterDescriptor(action, parameterName);
        if ( parameterDescriptor == null ) throw new ProcessException(new ErrorMessage("control/actionParameterNotFound", new Object[] { parameterName, actionUri }));
        return parameterDescriptor;
    }

    protected String getName() {
        return NAME;
    }
}