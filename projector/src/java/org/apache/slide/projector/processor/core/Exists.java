package org.apache.slide.projector.processor.core;

import java.util.Map;
import java.util.logging.Logger;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.Processor;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.descriptor.AnyValueDescriptor;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.i18n.DefaultMessage;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.value.NullValue;
import org.apache.slide.projector.value.Value;

public class Exists implements Processor {
    private static Logger logger = Logger.getLogger(Exists.class.getName());

    public final static String INPUT = "input";

    public final static String TRUE = "true";
    public final static String FALSE = "false";

    private final static ResultDescriptor resultDescriptor = new ResultDescriptor(new StateDescriptor[]{
            new StateDescriptor(TRUE, new DefaultMessage("exists/state/true")),
            new StateDescriptor(FALSE, new DefaultMessage("exists/state/false"))
        });
	private final static ParameterDescriptor[] parameterDescriptor = new ParameterDescriptor[]{ new ParameterDescriptor(INPUT, new ParameterMessage("exists/input"), new AnyValueDescriptor(), NullValue.NULL) };
 
    
    public Result process(Map parameter, Context context) throws Exception {
        Value input = (Value)parameter.get(INPUT);
        if ( input instanceof NullValue ) {
            return new Result(FALSE);
        }
        return new Result(TRUE);
    }

    public ParameterDescriptor[] getParameterDescriptors() {
    	return parameterDescriptor;
    }
    	
    public ResultDescriptor getResultDescriptor() {
        return resultDescriptor;
    }
}