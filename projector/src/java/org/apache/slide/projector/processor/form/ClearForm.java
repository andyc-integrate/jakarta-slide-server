package org.apache.slide.projector.processor.form;

import java.util.Map;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.Processor;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.Store;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.store.FormStore;

public class ClearForm implements Processor {
	public Result process(Map parameter, Context context) throws Exception {
		Store formStore = context.getStore(Store.FORM);
		if ( formStore != null ) {
			((FormStore)formStore).clear();
		}
		return new Result(StateDescriptor.OK);
	}

	public ParameterDescriptor[] getParameterDescriptors() {
		return ParameterDescriptor.NO_PARAMETERS;
	}

	public ResultDescriptor getResultDescriptor() {
		return ResultDescriptor.OK;
	}
}
