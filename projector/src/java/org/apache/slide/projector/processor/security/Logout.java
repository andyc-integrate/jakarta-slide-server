package org.apache.slide.projector.processor.security;

import java.util.Map;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.Processor;
import org.apache.slide.projector.Projector;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;

/**
 * @version $Revision: 1.2 $
 */

public class Logout implements Processor {
    private final static ParameterDescriptor[] parameterDescriptors = new ParameterDescriptor[0];
    private final static ResultDescriptor resultDescirptor = new ResultDescriptor(new StateDescriptor[] {
            StateDescriptor.OK_DESCRIPTOR
         });

    public Result process(Map parameter, Context context) throws Exception {
        context.setCredentials(Projector.getCredentials());
        return Result.OK;
    }

    public ParameterDescriptor[] getParameterDescriptors() {
        return parameterDescriptors;
    }

    public ResultDescriptor getResultDescriptor() {
    	return resultDescirptor;
    }
}