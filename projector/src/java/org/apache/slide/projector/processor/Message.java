package org.apache.slide.projector.processor;

import java.util.Locale;
import java.util.Map;

import org.apache.slide.projector.ContentType;
import org.apache.slide.projector.Context;
import org.apache.slide.projector.EnvironmentConsumer;
import org.apache.slide.projector.Processor;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.Store;
import org.apache.slide.projector.descriptor.ArrayValueDescriptor;
import org.apache.slide.projector.descriptor.LocaleValueDescriptor;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.RequiredEnvironmentDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.ResultEntryDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.descriptor.StringValueDescriptor;
import org.apache.slide.projector.i18n.DefaultMessage;
import org.apache.slide.projector.i18n.LocalizedMessage;
import org.apache.slide.projector.i18n.MessageManager;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.value.ArrayValue;
import org.apache.slide.projector.value.LocaleValue;
import org.apache.slide.projector.value.StringValue;
import org.apache.slide.projector.value.Value;

public class Message implements Processor, EnvironmentConsumer {
    private final static String ID = "id";
    private final static String KEY = "key";
    private final static String ARGUMENTS = "arguments";
    private final static String LOCALE = "locale";

    private final static StringValue DEFAULT_KEY = new StringValue("text");

    private final static ParameterDescriptor[] parameterDescriptors = new ParameterDescriptor[]{
    		new ParameterDescriptor(ID, new ParameterMessage("message/parameter/id"), new StringValueDescriptor()),
    		new ParameterDescriptor(KEY, new ParameterMessage("message/parameter/key"), new StringValueDescriptor(), DEFAULT_KEY),
    		new ParameterDescriptor(ARGUMENTS, new ParameterMessage("message/parameter/arguments"), new ArrayValueDescriptor(new StringValueDescriptor()), new ArrayValue(new StringValue[] { new StringValue("") }))
    };

    private final static RequiredEnvironmentDescriptor[] requiredEnvironmentDescriptors = new RequiredEnvironmentDescriptor[] {
    		new RequiredEnvironmentDescriptor(LOCALE, Store.SESSION, new DefaultMessage("message/requiredEnvironment/locale"), new LocaleValueDescriptor(), new LocaleValue(Locale.getDefault()))
    };
    
    private final static ResultDescriptor resultDescriptor = new ResultDescriptor(
            new StateDescriptor[]{ StateDescriptor.OK_DESCRIPTOR },
            new ResultEntryDescriptor[]{new ResultEntryDescriptor(
                    SimpleProcessor.OUTPUT, new LocalizedMessage("message/result/output"),
                    ContentType.PLAIN_TEXT, true)});

    public Result process(Map parameter, Context context) throws Exception {
        String id = parameter.get(ID).toString();
        String key = parameter.get(KEY).toString();
        Value[] arguments = ((ArrayValue)parameter.get(ARGUMENTS)).getArray();
        Locale locale = ((LocaleValue)context.getStore(Store.SESSION).get(LOCALE)).getLocale();
        return new Result(StateDescriptor.OK, SimpleProcessor.OUTPUT, new StringValue(MessageManager.getText(id, key, arguments, locale, id)));
    }

    public ParameterDescriptor[] getParameterDescriptors() {
        return parameterDescriptors;
    }

    public ResultDescriptor getResultDescriptor() {
        return resultDescriptor;
    }

	public RequiredEnvironmentDescriptor[] getRequiredEnvironmentDescriptors() {
		return requiredEnvironmentDescriptors;
	}
}