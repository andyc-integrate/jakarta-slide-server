package org.apache.slide.projector.processor.security;

import java.util.Map;

import org.apache.commons.httpclient.Credentials;
import org.apache.slide.projector.Context;
import org.apache.slide.projector.Information;
import org.apache.slide.projector.Processor;
import org.apache.slide.projector.Projector;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.descriptor.StringValueDescriptor;
import org.apache.slide.projector.i18n.DefaultMessage;
import org.apache.slide.projector.i18n.ErrorMessage;
import org.apache.slide.projector.i18n.ParameterMessage;

/**
 * @version $Revision: 1.2 $
 */

public class Login implements Processor {
    private final static String USERNAME = "username";
    private final static String PASSWORD = "password";
    
    private final static String LOGIN_OK = "loginOk";
    private final static String LOGIN_FAILED = "loginFailed";
    public final static String ERRORS = "errors";

    public Result process(Map parameter, Context context) throws Exception {
        String username = parameter.get(USERNAME).toString();
        String password = parameter.get(PASSWORD).toString();
        Credentials credentials = Projector.getRepository().login(username, password);
        if ( credentials == null ) {
            context.addInformation(new Information(Information.ERROR, new ErrorMessage("login/invalidLogin"), new String[] { USERNAME, PASSWORD }));
            return new Result(LOGIN_FAILED);
        }
        context.setCredentials(credentials);
        return new Result(LOGIN_OK);
    }

    public ParameterDescriptor[] getParameterDescriptors() {
        return new ParameterDescriptor[] {
            new ParameterDescriptor(USERNAME, new ParameterMessage("login/user"), new StringValueDescriptor(1,20)),
            new ParameterDescriptor(PASSWORD, new ParameterMessage("login/password"), new StringValueDescriptor(4,12)),
        };
    }

    public ResultDescriptor getResultDescriptor() {
        return new ResultDescriptor(new StateDescriptor[] {
           new StateDescriptor(LOGIN_OK, new DefaultMessage("login/state/ok")),
           new StateDescriptor(LOGIN_FAILED, new DefaultMessage("login/state/failed"))
        });
    }
}