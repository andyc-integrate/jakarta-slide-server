package org.apache.slide.projector.processor.core;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.descriptor.ArrayValueDescriptor;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultEntryDescriptor;
import org.apache.slide.projector.descriptor.StringValueDescriptor;
import org.apache.slide.projector.i18n.DefaultMessage;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.processor.SimpleProcessor;
import org.apache.slide.projector.value.ArrayValue;
import org.apache.slide.projector.value.PrintableValue;
import org.apache.slide.projector.value.StringValue;
import org.apache.slide.projector.value.Value;

public class StringArrayRenderer extends SimpleProcessor {
    public Value process(Value input, Context context) throws Exception {
        Value []array = ((ArrayValue)input).getArray();
        StringBuffer buffer = new StringBuffer(1024);
        for ( int i = 0; i < array.length; i++ ) {
        	if ( array[i] instanceof PrintableValue ) {
        		((PrintableValue)array[i]).print(buffer);
        	}
        }
        return new StringValue(buffer.toString());
    }

    public ParameterDescriptor getParameterDescriptor() {
        return new ParameterDescriptor(INPUT, new ParameterMessage("arrayRenderer/input"), new ArrayValueDescriptor(new StringValueDescriptor()));
    }

    public ResultEntryDescriptor getResultEntryDescriptor() {
        return new ResultEntryDescriptor(OUTPUT, new DefaultMessage("arrayRenderer/output"), "*", true);
    }
}