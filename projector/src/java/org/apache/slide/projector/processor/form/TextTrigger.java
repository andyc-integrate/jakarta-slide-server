package org.apache.slide.projector.processor.form;

/**
 * @version $Revision: 1.2 $
 */

public class TextTrigger extends Trigger {
    private final static String FRAGMENT = "text trigger";

    public TextTrigger() {
        super();
    }
    
    protected String getName() {
    	return FRAGMENT;
    }
}