package org.apache.slide.projector.processor.form;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.Information;
import org.apache.slide.projector.ProcessException;
import org.apache.slide.projector.Processor;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.Store;
import org.apache.slide.projector.URI;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.URIValueDescriptor;
import org.apache.slide.projector.engine.ProcessorManager;
import org.apache.slide.projector.i18n.ErrorMessage;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.processor.process.Process;
import org.apache.slide.projector.store.FormStore;
import org.apache.slide.projector.value.BooleanValue;
import org.apache.slide.projector.value.MapValue;
import org.apache.slide.projector.value.NullValue;
import org.apache.slide.projector.value.StringValue;
import org.apache.slide.projector.value.URIValue;
import org.apache.slide.projector.value.Value;

public class Test implements Processor {
    private final static URI FORM_PROCESSOR = new URIValue("formGenerator");
    private final static URI RESULT_RENDERER = new URIValue("formResult");
    private final static String PROCESSOR = "processor";

    private final static String FORM_STEP = "form";
    private final static String PROCESS_STEP = "process";
    private final static String RESULT_STEP = "result";
    
    private final static ParameterDescriptor[] parameterDescriptors = new ParameterDescriptor[] {
			new ParameterDescriptor(PROCESSOR, new ParameterMessage("test/parameter/processor"), new URIValueDescriptor(), NullValue.NULL),
	};
	
	public Result process(Map parameter, Context context) throws Exception {
		URI processorUri = ProcessorManager.getInstance().getURI(this); 
		context.setBookmark(processorUri);
		Value uri = (Value)parameter.get(PROCESSOR); 
        if ( uri == null || uri == NullValue.NULL ) {
        	uri = (URI)context.getStore(Store.SESSION).get(PROCESSOR);
        }
        if ( uri == null ) {
        	throw new ProcessException(new ErrorMessage("test/noProcessorSpecified"));
        }
        context.setProcess((URI)uri);
    	context.getStore(Store.SESSION).put(PROCESSOR, uri);
    	Value stepResource = (Value)context.getStore(Store.FORM).get(Process.STEP);
    	String step;
    	if ( stepResource == null ) {
    		step = FORM_STEP;
    	} else {
    		step = stepResource.toString();
    	}
		context.setStep(step);
		Processor formProcessor = ProcessorManager.getInstance().getProcessor(FORM_PROCESSOR);
        Processor resultRenderer = ProcessorManager.getInstance().getProcessor(RESULT_RENDERER);
        Processor processor = ProcessorManager.getInstance().getProcessor((URI)uri);
    	parameter.put(Control.ACTION, uri);
        parameter.put(ControlComposer.LOCALE, ProcessorManager.getInstance().process(ProcessorManager.LOCALE_RESOLVER, context.getStore(Store.REQUEST_HEADER).get("accept-language"), context));
        parameter.put(FormGenerator.TARGET_STEP, PROCESS_STEP);
        Result result = null;
        if ( step.equals(FORM_STEP) ) {
        	result = formProcessor.process(parameter, context);
        } else if ( step.equals(PROCESS_STEP) ) {
        	parameter.putAll(((MapValue)((FormStore)context.getStore(Store.FORM)).getDomain()).getMap());
        	Result processorResult = ProcessorManager.process(processor, parameter, context);
        	if ( hasErrors(context.getInformations()) ) {
            	context.setStep(FORM_STEP);
        		((FormStore)context.getStore(Store.FORM)).put(ControlComposer.VALIDATE, BooleanValue.TRUE);
        		result = formProcessor.process(parameter, context);
        	} else {
        		Map resultParameters = new HashMap();
        		resultParameters.put("state", new StringValue(processorResult.getState()));
        		result = ProcessorManager.process(resultRenderer, resultParameters, context);
        		((FormStore)context.getStore(Store.FORM)).clear();
        	}
        } else {
    		((FormStore)context.getStore(Store.FORM)).clear();
        	return Result.OK;
        }
        return result;
	}

    protected boolean hasErrors(List informations) {
        for ( Iterator i = informations.iterator(); i.hasNext(); ) {
        	Information info = (Information)i.next();
        	if ( info.getSeverity() == Information.ERROR ) {
                return true;
            }
        }
        return false;
    }

	public ParameterDescriptor[] getParameterDescriptors() {
		return parameterDescriptors;
	}

	public ResultDescriptor getResultDescriptor() {
		return null;
	}
}