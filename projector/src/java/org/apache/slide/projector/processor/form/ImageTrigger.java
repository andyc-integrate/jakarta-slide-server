package org.apache.slide.projector.processor.form;

/**
 * @version $Revision: 1.2 $
 */

public class ImageTrigger extends Trigger {
    private final static String FRAGMENT = "image trigger";

    public ImageTrigger() {
        super();
    }
    
    protected String getName() {
    	return FRAGMENT;
    }
}