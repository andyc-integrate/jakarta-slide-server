package org.apache.slide.projector.processor.security;

import java.util.Map;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.Processor;
import org.apache.slide.projector.Projector;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.i18n.DefaultMessage;

/**
 * @version $Revision: 1.2 $
 */

public class Authenticated implements Processor {
    private final static String TRUE = "true";
    private final static String FALSE = "false";

    private final static ParameterDescriptor[] parameterDescriptors = new ParameterDescriptor[0];
    private final static ResultDescriptor resultDescirptor = new ResultDescriptor(new StateDescriptor[] {
            new StateDescriptor(TRUE, new DefaultMessage("authenticated/state/true")),
            new StateDescriptor(FALSE, new DefaultMessage("authenticated/state/false"))
         });

    public Result process(Map parameter, Context context) throws Exception {
        if ( context.getCredentials() == Projector.getCredentials() ) {
        	return new Result(FALSE);
        }
        return new Result(TRUE);
    }

    public ParameterDescriptor[] getParameterDescriptors() {
        return parameterDescriptors;
    }

    public ResultDescriptor getResultDescriptor() {
    	return resultDescirptor;
    }
}