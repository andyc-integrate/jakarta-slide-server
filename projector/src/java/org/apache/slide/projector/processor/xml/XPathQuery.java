package org.apache.slide.projector.processor.xml;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.slide.projector.ConfigurableProcessor;
import org.apache.slide.projector.ConfigurationException;
import org.apache.slide.projector.ContentType;
import org.apache.slide.projector.Context;
import org.apache.slide.projector.ProcessException;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultEntryDescriptor;
import org.apache.slide.projector.descriptor.StringValueDescriptor;
import org.apache.slide.projector.i18n.DefaultMessage;
import org.apache.slide.projector.i18n.ErrorMessage;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.processor.SimpleProcessor;
import org.apache.slide.projector.value.ArrayValue;
import org.apache.slide.projector.value.ElementValue;
import org.apache.slide.projector.value.StreamableValue;
import org.apache.slide.projector.value.StringValue;
import org.apache.slide.projector.value.Value;
import org.apache.slide.projector.value.XMLValue;
import org.jdom.Attribute;
import org.jdom.CDATA;
import org.jdom.Comment;
import org.jdom.Element;
import org.jdom.ProcessingInstruction;
import org.jdom.Text;
import org.jdom.xpath.XPath;

/**
 * @version $Revision: 1.2 $
 */

public class XPathQuery extends SimpleProcessor implements ConfigurableProcessor {
    private Element rootElement;

    public Value process(Value input, Context context) throws Exception {
        XPath xPath = XPath.newInstance(((StringValue)input).toString());
        List nodeList = xPath.selectNodes(rootElement);
        return createValueFromNodeList(nodeList);
    }

    public ParameterDescriptor getParameterDescriptor() {
        return new ParameterDescriptor(INPUT, new ParameterMessage("xPathQuery/parameter/input"), new StringValueDescriptor());
    }

    public ResultEntryDescriptor getResultEntryDescriptor() {
        return new ResultEntryDescriptor(OUTPUT, new DefaultMessage("xPathQuery/result/output"), ContentType.DYNAMIC, true);
    }

    public void configure(StreamableValue config) throws ConfigurationException {
        if ( config instanceof XMLValue ) {
            rootElement = ((XMLValue)config).getRootElement();
        } else {
            throw new ConfigurationException(new ErrorMessage("xpathQuery/xmlResourceRequired"));
        }
    }

    public static Value createValueFromNodeList(List nodeList) throws ProcessException {
    	if ( nodeList.size() > 1 ) {
    		List resources = new ArrayList();
    		for ( Iterator i = nodeList.iterator(); i.hasNext(); ) {
    			resources.add(createValueFromNode(i.next()));
    		}
    		Value[] array = new Value[resources.size()];
    		return new ArrayValue((Value [])resources.toArray(array));
    	} else {
    		return createValueFromNode(nodeList.get(0));
    	}
    }
    
    public static Value createValueFromNode(Object node) throws ProcessException {
    	if ( node instanceof Element ) {
    		return new ElementValue((Element)node);
    	} else if ( node instanceof Attribute ) {
    		return new StringValue(((Attribute)node).getValue());
    	} else if ( node instanceof Text ) {
    		return new StringValue(((Text)node).getText());
    	} else if ( node instanceof Comment ) {
    		return new StringValue(((Comment)node).getText());
    	} else if ( node instanceof CDATA ) {
    		return new StringValue(((CDATA)node).toString());
    	} else if ( node instanceof ProcessingInstruction ) {
    		return new StringValue(((ProcessingInstruction)node).getData());
        }
        throw new ProcessException(new ErrorMessage("unconvertableJDomNode"));
    }
}