package org.apache.slide.projector.processor.core;

import java.util.Map;
import java.util.logging.Logger;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.ProcessException;
import org.apache.slide.projector.Processor;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.Store;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.descriptor.StringValueDescriptor;
import org.apache.slide.projector.i18n.ErrorMessage;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.util.StoreHelper;
import org.apache.slide.projector.value.StringValue;

public class Dispose implements Processor {
    private static Logger logger = Logger.getLogger(Dispose.class.getName());

    public final static String STORE = "store";
    public final static String KEY = "key";

    private final static ParameterDescriptor[] parameterDescriptor = new ParameterDescriptor[]{
            new ParameterDescriptor(STORE, new ParameterMessage("dispose/parameter/store"), new StringValueDescriptor(Store.stores)),
            new ParameterDescriptor(KEY, new ParameterMessage("dispose/parameter/key"), new StringValueDescriptor())
        };

    private final static ResultDescriptor resultDescriptor = new ResultDescriptor(new StateDescriptor[]{ StateDescriptor.OK_DESCRIPTOR });

    public Result process(Map parameter, Context context) throws Exception {
        String name = ((StringValue)parameter.get(STORE)).toString();
        String key = ((StringValue)parameter.get(KEY)).toString();
        Store store = context.getStore(StoreHelper.getStoreByName(name));
        if ( store == null ) {
            throw new ProcessException(new ErrorMessage("storeNotAvailable", new String[] {name}));
        }
        store.dispose(key);
        return new Result( StateDescriptor.OK );
    }

    public ParameterDescriptor[] getParameterDescriptors() {
        return parameterDescriptor;
    }


    public ResultDescriptor getResultDescriptor() {
        return resultDescriptor;
    }
}