/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/projector/src/java/org/apache/slide/projector/processor/tree/TreeLocalizer.java,v 1.3 2006-04-19 15:06:55 peter-cvs Exp $
 * $Revision: 1.3 $
 * $Date: 2006-04-19 15:06:55 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.projector.processor.tree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.EnvironmentConsumer;
import org.apache.slide.projector.Processor;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.Store;
import org.apache.slide.projector.descriptor.LocaleValueDescriptor;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.RequiredEnvironmentDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.ResultEntryDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.descriptor.StringValueDescriptor;
import org.apache.slide.projector.descriptor.TreeValueDescriptor;
import org.apache.slide.projector.engine.ProcessorManager;
import org.apache.slide.projector.i18n.DefaultMessage;
import org.apache.slide.projector.i18n.MessageManager;
import org.apache.slide.projector.i18n.MessageNotFoundException;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.processor.SimpleProcessor;
import org.apache.slide.projector.value.ArrayValue;
import org.apache.slide.projector.value.LocaleValue;
import org.apache.slide.projector.value.MapValue;
import org.apache.slide.projector.value.StringValue;
import org.apache.slide.projector.value.Value;

/**
 * The TreeLocalizer class
 * 
 */
public class TreeLocalizer implements Processor, EnvironmentConsumer  {
    private static Logger logger = Logger.getLogger(TreeLocalizer.class.getName());

    public final static String LOCALE = "locale";
    
    protected final static String IMAGE_IDENTIFIER = "image";

    private final static ParameterDescriptor[] parameterDescriptors = new ParameterDescriptor[] {
        new ParameterDescriptor(SimpleProcessor.INPUT, new ParameterMessage("treeLocalizer/input"), new TreeValueDescriptor(new StringValueDescriptor()))
    };
    
    private final static ResultDescriptor resultDescriptor = new ResultDescriptor(
            new StateDescriptor[] { StateDescriptor.OK_DESCRIPTOR },
            new ResultEntryDescriptor[] {
                new ResultEntryDescriptor(SimpleProcessor.OUTPUT, new DefaultMessage("treeLocalizer/output"), ArrayValue.CONTENT_TYPE, false)
            });

    private final static RequiredEnvironmentDescriptor[] requiredEnvironmentDescriptors = new RequiredEnvironmentDescriptor[] {
    		new RequiredEnvironmentDescriptor(LOCALE, Store.SESSION, new DefaultMessage("treeLocalized/requiredEnvironment/locale"), new LocaleValueDescriptor(), new LocaleValue(Locale.getDefault()))
    };

    public Result process(Map parameter, Context context) throws Exception {
        Value []resources = ((ArrayValue)parameter.get(SimpleProcessor.INPUT)).getArray();
        Locale locale = ((LocaleValue)context.getStore(Store.SESSION).get(LOCALE)).getLocale();
        return new Result(StateDescriptor.OK, SimpleProcessor.OUTPUT, localize(resources, locale, context));
    }

    private ArrayValue localize(Value []resources, Locale locale, Context context) throws Exception {
        List localizedResources = new ArrayList();
        for ( int i = 0; i < resources.length; i++ ) {
            if ( resources[i] instanceof StringValue ) {
                Map localizedMap = new HashMap();
                try {
                	localizedMap.put(TreeRenderer.ID, ProcessorManager.getInstance().process(ProcessorManager.URL, resources[i], context));
                	Map entries = MessageManager.getEntries(((StringValue)resources[i]).toString(), locale);
                    for ( Iterator j = entries.entrySet().iterator(); j.hasNext(); ) {
                        Map.Entry entry = (Map.Entry)j.next();
                        String key = (String)entry.getKey();
                        String value = (String)entry.getValue();
                        if ( key.startsWith(IMAGE_IDENTIFIER) ) {
                        	key = key.substring(IMAGE_IDENTIFIER.length()+1);
                            localizedMap.put(key, ProcessorManager.getInstance().process(ProcessorManager.BINARY, value, "path", context));
                        } else {
                            localizedMap.put(key, new StringValue(value));
                        }
                    }
                } catch ( MessageNotFoundException exception ) {
                    logger.log(Level.SEVERE, "No message found while localizing tree", exception);
                }
                localizedResources.add(new MapValue(localizedMap));
            } else if ( resources[i] instanceof ArrayValue ) {
                localizedResources.add(localize(((ArrayValue)resources[i]).getArray(), locale, context));
            }
        }
        Value[] localizedResourceArray = new Value[localizedResources.size()];
        return new ArrayValue((Value [])localizedResources.toArray(localizedResourceArray));
    }

    public ParameterDescriptor[] getParameterDescriptors() {
        return parameterDescriptors;
    }

    public ResultDescriptor getResultDescriptor() {
        return resultDescriptor;
    }

    public RequiredEnvironmentDescriptor[] getRequiredEnvironmentDescriptors() {
		return requiredEnvironmentDescriptors;
	}
}
