package org.apache.slide.projector.processor.resolver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultEntryDescriptor;
import org.apache.slide.projector.descriptor.StringValueDescriptor;
import org.apache.slide.projector.i18n.DefaultMessage;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.processor.SimpleProcessor;
import org.apache.slide.projector.value.LocaleValue;
import org.apache.slide.projector.value.StringValue;
import org.apache.slide.projector.value.Value;

/**
 * @version $Revision: 1.2 $
 */
public class LocaleResolver extends SimpleProcessor {
    private final static ParameterDescriptor parameterDescriptor = new ParameterDescriptor(INPUT, new ParameterMessage("localeResolver/locale"), new StringValueDescriptor(), new StringValue(Locale.getDefault().toString()));
    private final static ResultEntryDescriptor resultEntryDescriptor =  new ResultEntryDescriptor(SimpleProcessor.OUTPUT, new DefaultMessage("locale/result/output"), LocaleValue.CONTENT_TYPE, false);

    public Value process(Value input, Context context) throws Exception {
        List acceptedLanguages = new ArrayList();
        StringTokenizer tokenizer = new StringTokenizer(input.toString(), ",");
        while ( tokenizer.hasMoreTokens() ) {
            String localeToken = tokenizer.nextToken();
            double q = 1.0;
            int qIndex = localeToken.indexOf(";q=");
            if ( qIndex > 0 ) {
                q = Double.parseDouble(localeToken.substring(qIndex+3));
                localeToken = localeToken.substring(0, qIndex);
            }
            // FIXME: Only add values that are supported by the current webapp (configure globally)
            acceptedLanguages.add(new AcceptedLanguage(q, new Locale(localeToken)));
        }
        Collections.sort(acceptedLanguages);
        return new LocaleValue(((AcceptedLanguage)acceptedLanguages.get(0)).getLocale());
    }

    public ParameterDescriptor getParameterDescriptor() {
        return parameterDescriptor;
    }

    public ResultEntryDescriptor getResultEntryDescriptor() {
        return resultEntryDescriptor;
    }

    class AcceptedLanguage implements Comparable {
        double q;
        Locale locale;

        public AcceptedLanguage(double q, Locale locale) {
            this.q = q;
            this.locale = locale;
        }

        public Locale getLocale() {
            return locale;
        }

        public double getQ() {
            return q;
        }

        public int compareTo(Object a) {
            if ( ((AcceptedLanguage)a).getQ() < q ) return -1;
            if ( ((AcceptedLanguage)a).getQ() > q ) return 1;
            return 0;
        }
    }
}