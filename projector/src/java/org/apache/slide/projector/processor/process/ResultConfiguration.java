package org.apache.slide.projector.processor.process;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.slide.projector.ContentType;
import org.apache.slide.projector.Context;
import org.apache.slide.projector.HttpContext;
import org.apache.slide.projector.ProcessException;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.Store;
import org.apache.slide.projector.descriptor.ResultEntryDescriptor;
import org.apache.slide.projector.i18n.ErrorMessage;
import org.apache.slide.projector.util.StoreHelper;
import org.apache.slide.projector.value.MapValue;
import org.apache.slide.projector.value.StreamableValue;
import org.apache.slide.projector.value.Value;

class ResultConfiguration {
    private static Logger logger = Logger.getLogger(ResultConfiguration.class.getName());

    private int store = Store.NONE;
    private String name, key, domain;
    private boolean presentable;
    private long timeout = -1;

    public ResultConfiguration(String name, String store, String domain, String key, boolean presentable, long timeout) {
        this(name, StoreHelper.getStoreByName(store), domain, key, presentable, timeout);
    }

    public ResultConfiguration(String name, int store, String domain, String key, boolean presentable, long timeout) {
        this.name = name;
        this.store = store;
        this.domain = domain;
        this.key = key;
        this.presentable = presentable;
        this.timeout = timeout;
    }

    public int getStore() {
        return store;
    }

    public String getDomain() {
    	return domain;
    }
    
    public String getKey() {
        return key;
    }

    public boolean isPresentable() {
        return presentable;
    }

    public String getName() {
        return name;
    }

    protected void storeValue(Value value, Store stepStore, Result result, ResultEntryDescriptor[] resultEntryDescriptors, Context context) throws ProcessException {
    	if ( value == null ) {
    		throw new ProcessException(new ErrorMessage("valueToStoreNotFound"));
    	}
    	if (presentable ) {
            if ( value instanceof StreamableValue ) {
                if ( context instanceof HttpContext ) {
                    ((HttpContext)context).setPresentableResource((StreamableValue)value);
                } else {
                    logger.log(Level.FINE, "Result can only be shown in http-context!");
                }
            } else {
        		throw new ProcessException(new ErrorMessage("unpresentableValue"));
            }
        }
        Store resultStore = stepStore;
        if ( store == Store.OUTPUT ) {
        	// check if result is defined
        	boolean resultDefined = false;
        	for ( int i = 0; i < resultEntryDescriptors.length; i++ ) {
        		if ( resultEntryDescriptors[i].getName().equals(key) ) {
        			// check if result matches description
        			if ( ContentType.matches(resultEntryDescriptors[i].getContentType(), value.getContentType()) ) {
        				result.addResultEntry(key, value);
        				resultDefined = true;
        				break;
        			} else {
                		throw new ProcessException(new ErrorMessage("contentTypeMismatch", new String[] { key, value.getContentType(), resultEntryDescriptors[i].getContentType()}));
        			}
        		}
        	}
        	if ( !resultDefined ) {
        		throw new ProcessException(new ErrorMessage("undefinedResultKey", new String[] { key }));
        	}
        } else {
            if ( store != Store.NONE ) {
            	resultStore = context.getStore(store);
            }
            if ( resultStore == null ) {
        		throw new ProcessException(new ErrorMessage("storeNotAvailable", new String[] { key, Store.stores[store] }));
            } else {
                try {
                    String evaluatedKey = Process.evaluateKey(key, context);
                    if ( evaluatedKey != null ) {
                    	if ( domain != null ) {
                    		Map map; 
                    		MapValue mapResource = (MapValue)resultStore.get(domain);
                			if ( mapResource == null ) {
                				map = new HashMap();
                				mapResource = new MapValue(map);
                			} else {
                				map = mapResource.getMap();
                			}
                			map.put(evaluatedKey, value);
                			evaluatedKey = domain;
                			value = mapResource;
                    	}
                    	if ( timeout != -1 ) {
                    		resultStore.put(evaluatedKey, value, timeout);
                    	} else {
                    		resultStore.put(evaluatedKey, value);
                    	}
                    }
                } catch ( IOException e ) {
            		throw new ProcessException(new ErrorMessage("storingResultFailed"), e);
                }
            }
        }
    }
}