package org.apache.slide.projector.processor.table;

import java.util.HashMap;
import java.util.Map;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.Processor;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.Store;
import org.apache.slide.projector.URI;
import org.apache.slide.projector.descriptor.NumberValueDescriptor;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.descriptor.StringValueDescriptor;
import org.apache.slide.projector.engine.ProcessorManager;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.util.StoreHelper;
import org.apache.slide.projector.value.MapValue;
import org.apache.slide.projector.value.NullValue;
import org.apache.slide.projector.value.Value;

/**
 * @version $Revision: 1.2 $
 */

public class TableHandler implements Processor {
    final static String URL = "tableHandler";

    final static String OK = "id";

    final static String ID = "id";
    final static String STORE = "store";
    final static String TARGET_POSITION = "targetPosition";
    final static String CURRENT_POSITION = "currentPosition";
    final static String SORTED_BY= "sortedBy";
    final static String ORDER= "order";
    final static String SIZE= "size";
    final static String EXPAND = "expand";
    final static String EXPANDED = "expanded";
    final static String COLLAPSE = "collapse";
    final static String COLLAPSED = "collapsed";

    final static String ASCENDING= "ascending";
    final static String DESCENDING= "descending";

    final static ParameterDescriptor[] parameterDescirptors = new ParameterDescriptor[] {
        new ParameterDescriptor(ID, new ParameterMessage("tableHandler/id"), new StringValueDescriptor()),
        new ParameterDescriptor(TARGET_POSITION, new ParameterMessage("tableHandler/targetPosition"), new NumberValueDescriptor(), NullValue.NULL),
        new ParameterDescriptor(SORTED_BY, new ParameterMessage("tableHandler/sortedBy"), new StringValueDescriptor(), NullValue.NULL),
        new ParameterDescriptor(ORDER, new ParameterMessage("tableHandler/order"), new StringValueDescriptor(new String[] {ASCENDING, DESCENDING}), NullValue.NULL),
        new ParameterDescriptor(STORE, new ParameterMessage("tableHandler/store"), new StringValueDescriptor(Store.stores)),
        new ParameterDescriptor(EXPAND, new ParameterMessage("tableHandler/expand"), new StringValueDescriptor(), NullValue.NULL),
        new ParameterDescriptor(COLLAPSE, new ParameterMessage("tableHandler/collapse"), new StringValueDescriptor(), NullValue.NULL)
    };

    public Result process(Map parameter, Context context) throws Exception {
        String id = parameter.get(ID).toString();
        Store store = context.getStore(StoreHelper.getStoreByName(parameter.get(STORE).toString()));
        MapValue idResource = (MapValue)store.get(id);
        Value targetPosition = (Value)parameter.get(TARGET_POSITION);
        Value sortedBy = (Value)parameter.get(SORTED_BY);
        Value order = (Value)parameter.get(ORDER);
        Value expand = (Value)parameter.get(EXPAND);
        Value collapse = (Value)parameter.get(COLLAPSE);
        Map tableMap;
        if ( idResource == null) {
            tableMap = new HashMap();
            idResource = new MapValue(tableMap);
            store.put(id, idResource);
        } else {
            tableMap = idResource.getMap();
        }
        if ( targetPosition != NullValue.NULL ) {
        	tableMap.put(CURRENT_POSITION, targetPosition);
        }
        if ( sortedBy != NullValue.NULL ) {
            tableMap.put(SORTED_BY, sortedBy);
        }
        if ( order != NullValue.NULL ) {
            tableMap.put(ORDER, order);
        }
        if ( collapse != NullValue.NULL || expand != NullValue.NULL ) {
        	MapValue sizeValue = (MapValue)tableMap.get(SIZE);
        	Map size;
        	if ( sizeValue == null ) { 
        		size = new HashMap();
        		tableMap.put(SIZE, new MapValue(size));
        	} else {
        		size = sizeValue.getMap();
        	}
        	if ( collapse != NullValue.NULL ) {
        		size.put(collapse.toString(), COLLAPSED);
        	}
        	if ( expand != NullValue.NULL ) {
        		size.put(expand.toString(), EXPANDED);
        	}
        }
        URI bookmark = (URI)context.getBookmark();
        if ( bookmark != null ) {
        	return ProcessorManager.getInstance().process(bookmark, parameter, context);
        } else {
        	return Result.OK;
        }
    }

    public ParameterDescriptor[] getParameterDescriptors() {
        return parameterDescirptors;
    }

    public ResultDescriptor getResultDescriptor() {
        return new ResultDescriptor(new StateDescriptor[] { StateDescriptor.OK_DESCRIPTOR });
    }
}