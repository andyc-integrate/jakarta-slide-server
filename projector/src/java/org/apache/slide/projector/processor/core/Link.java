package org.apache.slide.projector.processor.core;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.slide.projector.Constants;
import org.apache.slide.projector.Context;
import org.apache.slide.projector.HttpContext;
import org.apache.slide.projector.ProcessException;
import org.apache.slide.projector.Processor;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.URI;
import org.apache.slide.projector.descriptor.BooleanValueDescriptor;
import org.apache.slide.projector.descriptor.MapValueDescriptor;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.descriptor.StringValueDescriptor;
import org.apache.slide.projector.descriptor.URIValueDescriptor;
import org.apache.slide.projector.i18n.ErrorMessage;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.processor.SimpleProcessor;
import org.apache.slide.projector.processor.process.Process;
import org.apache.slide.projector.value.BooleanValue;
import org.apache.slide.projector.value.MapValue;
import org.apache.slide.projector.value.NullValue;
import org.apache.slide.projector.value.StringValue;
import org.apache.slide.projector.value.Value;

public class Link implements Processor {
	private final static String PROCESSOR = "processor";
	private final static String CONTINUE = "continue";
	private final static String PARAMETERS = "parameterS";
	
	private final static ParameterDescriptor[] parameterDescriptors = new ParameterDescriptor[] {
			new ParameterDescriptor(PROCESSOR, new ParameterMessage("link/parameter/processor"), new URIValueDescriptor()),
			new ParameterDescriptor(Process.STEP, new ParameterMessage("link/parameter/step"), new StringValueDescriptor(), new NullValue()),
			new ParameterDescriptor(CONTINUE, new ParameterMessage("link/parameter/continue"), new BooleanValueDescriptor(), BooleanValue.TRUE),
			new ParameterDescriptor(PARAMETERS, new ParameterMessage("link/parameter/parameters"), MapValueDescriptor.ANY, new MapValue(new HashMap()))
	};
	private final static ResultDescriptor resultDescriptor = new ResultDescriptor(new StateDescriptor[] { StateDescriptor.OK_DESCRIPTOR });
	
	public Result process(Map parameter, Context context) throws Exception {
		URI uri = (URI)parameter.get(PROCESSOR); 
		boolean continueProcess = ((BooleanValue)parameter.get(CONTINUE)).booleanValue();
		MapValue parameterMap = (MapValue)parameter.get(PARAMETERS);
		Value step = (Value)parameter.get(Process.STEP);
		StringBuffer linkBuffer = new StringBuffer(128);
		if ( context instanceof HttpContext ) {
            linkBuffer.append(((HttpContext)context).getContextPath()).append(uri);
            boolean first = true;
            if ( continueProcess ) {
            	parameterMap.getMap().put(Constants.PROCESS_ID_PARAMETER, context.getProcessId());
            }
            if ( step != null ) {
            	parameterMap.getMap().put(Process.STEP, step);
            }
            for ( Iterator i = parameterMap.getMap().entrySet().iterator(); i.hasNext(); ) {
            	Map.Entry entry = (Map.Entry)i.next();
            	if ( first ) {
            		linkBuffer.append('?');
            		first = false;
            	} else {
            		linkBuffer.append('&');
            	}
        		linkBuffer.append(entry.getKey());
        		linkBuffer.append('=');
        		linkBuffer.append(entry.getValue());
            }
		} else {
            throw new ProcessException(new ErrorMessage("httpContextRequired"));
        }
        return new Result(StateDescriptor.OK, SimpleProcessor.OUTPUT, new StringValue(linkBuffer.toString()));
	}

	public ParameterDescriptor[] getParameterDescriptors() {
		return parameterDescriptors; 
	}

	public ResultDescriptor getResultDescriptor() {
		return resultDescriptor;
	}
}
