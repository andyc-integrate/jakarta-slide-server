/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/projector/src/java/org/apache/slide/projector/processor/core/Event.java,v 1.2 2006-01-22 22:45:14 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:45:14 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.projector.processor.core;

import java.util.Map;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.Processor;
import org.apache.slide.projector.Projector;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.descriptor.BooleanValueDescriptor;
import org.apache.slide.projector.descriptor.MapValueDescriptor;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.value.BooleanValue;
import org.apache.slide.projector.value.MapValue;

/**
 * The Event class
 * 
 */
public class Event implements Processor {
    public final static String INFORMATION = "information";
    public final static String VETOABLE = "vetoable";

    private final static ParameterDescriptor[] parameterDescriptors = new ParameterDescriptor[] {
        new ParameterDescriptor(INFORMATION, new ParameterMessage("event/information"), MapValueDescriptor.ANY),
        new ParameterDescriptor(VETOABLE, new ParameterMessage("event/vetoable"), new BooleanValueDescriptor(), new BooleanValue(false))
    };
    private final static ResultDescriptor resultDescriptor = new ResultDescriptor(new StateDescriptor[] { StateDescriptor.OK_DESCRIPTOR });

    public Result process(Map parameter, Context context) throws Exception {
        Map information = ((MapValue)parameter.get(INFORMATION)).getMap();
        boolean vetoable = ((BooleanValue)parameter.get(VETOABLE)).booleanValue();
        if ( vetoable ) {
            Projector.getRepository().fireVetoableEvent(information, context.getCredentials());
        } else {
            Projector.getRepository().fireEvent(information, context.getCredentials());
        }
        return new Result(StateDescriptor.OK);
    }

    public ParameterDescriptor[] getParameterDescriptors() {
        return parameterDescriptors;
    }

    public ResultDescriptor getResultDescriptor() {
        return resultDescriptor;
    }
}
