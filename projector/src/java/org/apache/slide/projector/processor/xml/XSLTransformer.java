package org.apache.slide.projector.processor.xml;

import java.io.IOException;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;

import org.apache.slide.projector.ConfigurableProcessor;
import org.apache.slide.projector.ConfigurationException;
import org.apache.slide.projector.Context;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultEntryDescriptor;
import org.apache.slide.projector.descriptor.XMLValueDescriptor;
import org.apache.slide.projector.i18n.DefaultMessage;
import org.apache.slide.projector.i18n.ErrorMessage;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.processor.SimpleProcessor;
import org.apache.slide.projector.value.DocumentValue;
import org.apache.slide.projector.value.StreamableValue;
import org.apache.slide.projector.value.Value;
import org.jdom.Document;
import org.jdom.transform.JDOMResult;
import org.jdom.transform.JDOMSource;

/**
 * @version $Revision: 1.2 $
 */

public class XSLTransformer extends SimpleProcessor implements ConfigurableProcessor {
    private Transformer transformer;

    public Value process(Value input, Context context) throws Exception {
    	Document document = ((DocumentValue)input).getDocument();
        JDOMResult result = new JDOMResult();
        transformer.transform(new JDOMSource(document), result);
        return new DocumentValue(result.getDocument());
    }

    public ParameterDescriptor getParameterDescriptor() {
        return new ParameterDescriptor(INPUT, new ParameterMessage("xslTransformer/parameter/input"), new XMLValueDescriptor());
    }

    public ResultEntryDescriptor getResultEntryDescriptor() {
        return new ResultEntryDescriptor(OUTPUT, new DefaultMessage("xslTransformer/result/output"), "text/xml", true);
    }

    public void configure(StreamableValue config) throws ConfigurationException {
        try {
            transformer = TransformerFactory.newInstance().newTransformer(new StreamSource(config.getInputStream()));
        } catch (TransformerConfigurationException e) {
            throw new ConfigurationException(new ErrorMessage("xslTransformer/creationgFailed"), e);
        } catch (IOException e) {
            throw new ConfigurationException(new ErrorMessage("xslTransformer/ioException"), e);
        }
    }
}