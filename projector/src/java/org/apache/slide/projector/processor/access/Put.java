/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/projector/src/java/org/apache/slide/projector/processor/access/Put.java,v 1.2 2006-01-22 22:45:14 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:45:14 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.projector.processor.access;

import java.util.Map;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.Processor;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.Store;
import org.apache.slide.projector.descriptor.AnyValueDescriptor;
import org.apache.slide.projector.descriptor.NumberValueDescriptor;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.descriptor.StringValueDescriptor;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.util.StoreHelper;
import org.apache.slide.projector.value.NullValue;
import org.apache.slide.projector.value.NumberValue;
import org.apache.slide.projector.value.Value;

/**
 * The Event class
 * 
 */
public class Put implements Processor {
    public final static String STORE = "store";
    public final static String KEY = "key";
    public final static String VALUE = "value";
    public final static String TIMEOUT = "timeout";

    private final static ParameterDescriptor[] parameterDescriptors = new ParameterDescriptor[] {
        new ParameterDescriptor(STORE, new ParameterMessage("put/parameter/store"), new StringValueDescriptor(Store.stores)),
        new ParameterDescriptor(KEY, new ParameterMessage("put/parameter/key"), new StringValueDescriptor()),
        new ParameterDescriptor(VALUE, new ParameterMessage("put/parameter/value"), new AnyValueDescriptor()),
        new ParameterDescriptor(TIMEOUT, new ParameterMessage("put/parameter/timeout"), new NumberValueDescriptor(), NullValue.NULL)
    };
    private final static ResultDescriptor resultDescriptor = new ResultDescriptor(new StateDescriptor[] { StateDescriptor.OK_DESCRIPTOR });

    public Result process(Map parameter, Context context) throws Exception {
    	String storeName = parameter.get(STORE).toString();
    	String key = parameter.get(KEY).toString();
    	Value value = (Value)parameter.get(VALUE);
    	Value timeoutValue = (Value)parameter.get(TIMEOUT);
    	Store store = context.getStore(StoreHelper.getStoreByName(storeName));
    	if ( timeoutValue == NullValue.NULL ) {
    		store.put(key, value);
    	} else {
    		long timeout = ((NumberValue)timeoutValue).getNumber().longValue();
    		store.put(key, value, timeout);
    	}
        return Result.OK;
    }

    public ParameterDescriptor[] getParameterDescriptors() {
        return parameterDescriptors;
    }

    public ResultDescriptor getResultDescriptor() {
        return resultDescriptor;
    }
}
