package org.apache.slide.projector.processor.process;

import java.util.logging.Logger;

import org.apache.slide.projector.descriptor.ValueFactoryManager;
import org.apache.slide.projector.value.Value;
import org.jdom.Element;

public class ParameterConfiguration {
    private static Logger logger = Logger.getLogger(ParameterConfiguration.class.getName());

    private String name;
    private Value value;

    public ParameterConfiguration(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void configure(Element element) {
    	this.value = ValueFactoryManager.getInstance().loadValue(element);
    }
    
    public Value getValue() {
    	return value;
    }
}