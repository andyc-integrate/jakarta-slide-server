package org.apache.slide.projector.processor.security;

import java.util.Map;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.Processor;
import org.apache.slide.projector.Projector;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.URI;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.ResultEntryDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.descriptor.URIValueDescriptor;
import org.apache.slide.projector.i18n.DefaultMessage;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.value.ArrayValue;

/**
 * @version $Revision: 1.2 $
 */

public class ListRoles implements Processor {
    private final static String USER = "user";

    private final static String ROLES = "roles";
    private final static String OK = "ok";
    private final static String FAILED = "failed";

    private final static ParameterDescriptor [] parameterDescriptor = new ParameterDescriptor[] {
            new ParameterDescriptor(USER, new ParameterMessage("listRoles/parameter/user"), new URIValueDescriptor())
        };

    private final static ResultDescriptor resultDescriptor = new ResultDescriptor(
    		new StateDescriptor[] { StateDescriptor.OK_DESCRIPTOR },
			new ResultEntryDescriptor[] {
    			new ResultEntryDescriptor(ROLES, new DefaultMessage("listRoles/result/roles"), ArrayValue.CONTENT_TYPE, false)
    		});

    public Result process(Map parameter, Context context) throws Exception {
        URI user = (URI)parameter.get(USER);
        ArrayValue roles = Projector.getRepository().listRoles(user, context.getCredentials());
        return new Result(StateDescriptor.OK, ROLES, roles);
    }

    public ParameterDescriptor[] getParameterDescriptors() {
    	return parameterDescriptor;
    }

    public ResultDescriptor getResultDescriptor() {
    	return resultDescriptor;
    }
}