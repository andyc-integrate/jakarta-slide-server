package org.apache.slide.projector.processor.core;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.HttpContext;
import org.apache.slide.projector.Processor;
import org.apache.slide.projector.Projector;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.Store;
import org.apache.slide.projector.URI;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.ResultEntryDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.descriptor.URIValueDescriptor;
import org.apache.slide.projector.engine.Job;
import org.apache.slide.projector.engine.ProcessorManager;
import org.apache.slide.projector.engine.Scheduler;
import org.apache.slide.projector.expression.EventExpression;
import org.apache.slide.projector.i18n.DefaultMessage;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.processor.SimpleProcessor;
import org.apache.slide.projector.value.InputStreamValue;
import org.apache.slide.projector.value.MultipleStreamableValue;
import org.apache.slide.projector.value.StreamableValue;
import org.apache.slide.projector.value.StringValue;
import org.apache.slide.projector.value.URIValue;
import org.apache.slide.projector.value.Value;

/**
 * @version $Revision: 1.3 $
 */

public class CachedContent implements Processor {
    private static Logger logger = Logger.getLogger(CachedContent.class.getName());

    public final static String URL = "path";

    public Result process(Map parameter, Context context) throws Exception {
        URI uri = (URI)parameter.get(SimpleProcessor.INPUT);
        String url = "";
        if ( context instanceof HttpContext ) {
            url = ((HttpContext)context).getContextPath()+"/"+ProcessorManager.getInstance().getURI(this)+"?input="+uri;
        }
        // check for cached result
        Map resultEntries = new HashMap();
        Value output = (Value)context.getStore(Store.CACHE).get(uri.toString());
        if ( output == null ) {
            output = Projector.getRepository().getResource(uri, context.getCredentials());
            if ( output instanceof InputStreamValue ) {
                output = new MultipleStreamableValue((StreamableValue)output); 
            }
            // cache result...
            context.getStore(Store.CACHE).put(uri.toString(), output);
            // ...and add dispose condition
            Map jobParameter = new HashMap();
            jobParameter.put(Dispose.STORE, new StringValue(Store.stores[Store.CACHE]));
            jobParameter.put(Dispose.KEY, new StringValue(uri.toString()));
            EventExpression expression = new EventExpression("Update");
            expression.addProperty(EventExpression.DEPTH, "0");
            expression.addProperty(EventExpression.URI, uri.toString());
            Scheduler.getInstance().registerJob(new Job(context.getProcessId()+":"+context.getStep(), new URIValue("dispose"), expression, expression, jobParameter, false, false));
			Scheduler.getInstance().saveJobs();
        }
        resultEntries.put(SimpleProcessor.OUTPUT, output);
        // build path to activate this processor on top level
        resultEntries.put(URL, new StringValue(url));
        return new Result(StateDescriptor.OK, resultEntries);
    }

    public ParameterDescriptor[] getParameterDescriptors() {
        return new ParameterDescriptor[]{ new ParameterDescriptor(SimpleProcessor.INPUT, new ParameterMessage("cachedContent/parameter/input"), new URIValueDescriptor()) }; }

    public ResultDescriptor getResultDescriptor() {
        return new ResultDescriptor(new StateDescriptor[]{ StateDescriptor.OK_DESCRIPTOR },
                new ResultEntryDescriptor[] {
                    new ResultEntryDescriptor(SimpleProcessor.OUTPUT, new DefaultMessage("cachedContent/result/output"), "*", true),
                    new ResultEntryDescriptor(URL, new DefaultMessage("cachedContent/result/path"), "text/path", false),
        }
        );
    }
}
