/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/projector/src/java/org/apache/slide/projector/processor/security/AddRole.java,v 1.2 2006-01-22 22:45:14 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:45:14 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.projector.processor.security;

import java.util.Map;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.Processor;
import org.apache.slide.projector.Projector;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.URI;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.descriptor.URIValueDescriptor;
import org.apache.slide.projector.i18n.ParameterMessage;

/**
 * The Event class
 * 
 */
public class AddRole implements Processor {
    public final static String USER = "user";
    public final static String ROLE = "role";

    private final static ParameterDescriptor[] parameterDescriptors = new ParameterDescriptor[] {
        new ParameterDescriptor(USER, new ParameterMessage("addRole/parameter/user"), new URIValueDescriptor()),
        new ParameterDescriptor(ROLE, new ParameterMessage("addRole/parameter/role"), new URIValueDescriptor())
    };
    private final static ResultDescriptor resultDescriptor = new ResultDescriptor(new StateDescriptor[] { StateDescriptor.OK_DESCRIPTOR });

    public Result process(Map parameter, Context context) throws Exception {
    	URI user = (URI)parameter.get(USER);
    	URI role = (URI)parameter.get(ROLE);
    	Projector.getRepository().addRole(user, role, context.getCredentials());
    	return Result.OK;
    }

    public ParameterDescriptor[] getParameterDescriptors() {
        return parameterDescriptors;
    }

    public ResultDescriptor getResultDescriptor() {
        return resultDescriptor;
    }
}