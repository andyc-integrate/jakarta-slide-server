package org.apache.slide.projector.processor;

import java.util.Map;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.Processor;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.descriptor.NumberValueDescriptor;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.ResultEntryDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.i18n.LocalizedMessage;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.value.NumberValue;

public class Calculator implements Processor {
    private final static String FIRST_NUMBER = "firstNumber";
    private final static String SECOND_NUMBER = "secondNumber";
    private final static String CALCULATED_RESULT = "calculatedResult";

    private final static ParameterDescriptor[] parameterDescriptors = new ParameterDescriptor[]{new ParameterDescriptor(
            FIRST_NUMBER, new ParameterMessage(
                    "calculator/parameter/firstNumber"),
            new NumberValueDescriptor(new Integer(0), new Integer(5)))};
    private final static ResultDescriptor resultDescriptor = new ResultDescriptor(
            new StateDescriptor[]{new StateDescriptor(StateDescriptor.OK,
                    new LocalizedMessage("calculator/state/ok")),},
            new ResultEntryDescriptor[]{new ResultEntryDescriptor(
                    CALCULATED_RESULT, new LocalizedMessage(
                            "calculator/result/calculatedResult"),
                    NumberValue.CONTENT_TYPE, true)});

    public Result process(Map parameter, Context context) throws Exception {
        int firstInt = ((NumberValue) parameter.get(FIRST_NUMBER))
                .getNumber().intValue();
        int secondInt = ((NumberValue) parameter.get(SECOND_NUMBER))
                .getNumber().intValue();
        int calculatedResult = firstInt + secondInt;
        return new Result(StateDescriptor.OK, CALCULATED_RESULT,
                new NumberValue(new Integer(calculatedResult)));
    }

    public ParameterDescriptor[] getParameterDescriptors() {
        return parameterDescriptors;
    }

    public ResultDescriptor getResultDescriptor() {
        return resultDescriptor;
    }
}