/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/projector/src/java/org/apache/slide/projector/processor/query/PropertyQuery.java,v 1.2 2006-01-22 22:45:14 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:45:14 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.projector.processor.query;

import java.util.HashMap;
import java.util.Map;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.Processor;
import org.apache.slide.projector.Projector;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.ResultEntryDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.descriptor.URIValueDescriptor;
import org.apache.slide.projector.i18n.DefaultMessage;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.value.ArrayValue;
import org.apache.slide.projector.value.MapValue;
import org.apache.slide.projector.value.URIValue;

/**
 * The Query class
 * 
 */
public class PropertyQuery implements Processor {
    private final static String EMPTY = "empty";
    private final static String URI = "uri";
    private final static String ARRAY = "array";
    private final static String MAP = "map";

    private static ResultDescriptor resultDescriptor = new ResultDescriptor(
            new StateDescriptor[] {
                StateDescriptor.OK_DESCRIPTOR,
                new StateDescriptor(EMPTY, new DefaultMessage("propertyQuery/state/empty"))},
            new ResultEntryDescriptor[] {
                new ResultEntryDescriptor(ARRAY, new ParameterMessage("propertyQuery/array"), ArrayValue.CONTENT_TYPE, false),
                new ResultEntryDescriptor(MAP, new ParameterMessage("propertyQuery/map"), MapValue.CONTENT_TYPE, false)
            });

    private static ParameterDescriptor []parameterDescriptors = new ParameterDescriptor[] {
        new ParameterDescriptor(URI, new DefaultMessage("propertyQuery/uri"), new URIValueDescriptor())
    };

    public Result process(Map parameter, Context context) throws Exception {
    	ArrayValue properties = Projector.getRepository().getProperties((URIValue)parameter.get(URI), context.getCredentials());
        if ( properties.getArray().length == 0 ) return new Result(EMPTY);
        Result result= new Result(StateDescriptor.OK, ARRAY, properties);
        Map properyMap = new HashMap();
        for ( int i = 0; i < properties.getArray().length; i++ ) {
            properyMap.put(((MapValue)properties.getArray()[i]).getMap().get("name").toString(), ((MapValue)properties.getArray()[i]).getMap().get("value"));
        }
        result.addResultEntry(MAP, new MapValue(properyMap));
        return result;
    }

    public ParameterDescriptor[] getParameterDescriptors() {
        return parameterDescriptors;
    }

    public ResultDescriptor getResultDescriptor() {
        return resultDescriptor;
    }
}