package org.apache.slide.projector.processor.form;

import org.apache.slide.projector.ConfigurationException;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.StringValueDescriptor;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.value.StreamableValue;

public class ControlContainer extends Control {
    private final static String CONTROL = "control";

	private ParameterDescriptor[] parameterDescriptors;

    public ParameterDescriptor[] getParameterDescriptors() {
        return parameterDescriptors;
    }

    public void configure(StreamableValue config) throws ConfigurationException {
        super.configure(config);
        ParameterDescriptor[] parentParameterDescriptors = super.getParameterDescriptors();
        parameterDescriptors = new ParameterDescriptor[parentParameterDescriptors.length - 2];
        int counter = 0;
        for ( int i = 0; i < parentParameterDescriptors.length; i++ ) {
        	if (!parentParameterDescriptors[i].getName().equals(ACTION) 
        			&& !parentParameterDescriptors[i].getName().equals(PARAMETER) 
					&& !parentParameterDescriptors[i].getName().equals(VALUE)) {
                parameterDescriptors[counter] = parentParameterDescriptors[i];
                counter++;
            }
        }
        parameterDescriptors[parentParameterDescriptors.length - 3] =
                new ParameterDescriptor(CONTROL, new ParameterMessage("controlContainer/control"), new StringValueDescriptor());
    }
}