/*
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.projector.processor.query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.Processor;
import org.apache.slide.projector.Projector;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.URI;
import org.apache.slide.projector.descriptor.ArrayValueDescriptor;
import org.apache.slide.projector.descriptor.BooleanValueDescriptor;
import org.apache.slide.projector.descriptor.MapValueDescriptor;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.ResultEntryDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.descriptor.StringValueDescriptor;
import org.apache.slide.projector.descriptor.URIValueDescriptor;
import org.apache.slide.projector.i18n.DefaultMessage;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.processor.SimpleProcessor;
import org.apache.slide.projector.processor.xml.XPathQuery;
import org.apache.slide.projector.value.ArrayValue;
import org.apache.slide.projector.value.BooleanValue;
import org.apache.slide.projector.value.DocumentValue;
import org.apache.slide.projector.value.MapValue;
import org.apache.slide.projector.value.MultipleStreamableValue;
import org.apache.slide.projector.value.NullValue;
import org.apache.slide.projector.value.StreamableValue;
import org.apache.slide.projector.value.Value;
import org.jdom.xpath.XPath;

/**
 */
public class ResultResolver implements Processor {
	private final static String URI_ENTRY = "uri";
	private final static String INSTRUCTIONS = "instructions";
	private final static String INCLUDE_CONTENT = "includeContent";
	private final static String CONTENT_ENTRY = "content";

	private final static ParameterDescriptor[] parameterDescriptors = 
		new ParameterDescriptor[] {
			new ParameterDescriptor(
					SimpleProcessor.INPUT, 
					new ParameterMessage("resultResolver/paramter/input"), 
					new ArrayValueDescriptor(new MapValueDescriptor(
							new ParameterDescriptor(URI_ENTRY, 
							new ParameterMessage("resultResolver/parameter/input/uri"), 
							new URIValueDescriptor())))),
			new ParameterDescriptor(
					INSTRUCTIONS, 
					new ParameterMessage("resultResolver/paramter/instructions"), 
					new MapValueDescriptor(new ParameterDescriptor(MapValueDescriptor.ALL, 
									new ParameterMessage("resultResolver/parameter/instruction/instruction"), 
									new StringValueDescriptor())), NullValue.NULL),
			new ParameterDescriptor(
					INCLUDE_CONTENT, 
					new ParameterMessage("resultResolver/paramter/include-content"), 
					new BooleanValueDescriptor(), BooleanValue.TRUE)
	};
	
	private final static ResultDescriptor resultDescriptor = 
		new ResultDescriptor(new StateDescriptor[] { StateDescriptor.OK_DESCRIPTOR },
				new ResultEntryDescriptor[] {
				new ResultEntryDescriptor(SimpleProcessor.OUTPUT, 
						new DefaultMessage("resultResolver/result/output"),
						ArrayValue.CONTENT_TYPE, false)	});
	
	public Result process(Map parameter, Context context) throws Exception {
		Value []values = ((ArrayValue)parameter.get(SimpleProcessor.INPUT)).getArray();
		boolean includeContent = ((BooleanValue)parameter.get(INCLUDE_CONTENT)).booleanValue();
		Value instructions = (Value)parameter.get(INSTRUCTIONS);
		List arrayEntries = new ArrayList();
		for ( int i = 0; i < values.length; i++ ) {
			Map map = ((MapValue)values[i]).getMap();
			Map resultMap = new HashMap();
			resultMap.putAll(map);
			URI uri = (URI)map.get(URI_ENTRY);
			Value content = Projector.getRepository().getResource(uri, context.getCredentials()); 
			if ( content != null ) {
				if ( instructions instanceof MapValue ) {
					if ( includeContent ) {
						content = new MultipleStreamableValue((StreamableValue) content);
					}
					DocumentValue documentValue = new DocumentValue((StreamableValue)content);
					Map instructionMap = ((MapValue)instructions).getMap();
					for ( Iterator j = instructionMap.entrySet().iterator(); j.hasNext(); ) {
						Map.Entry entry = (Map.Entry)j.next();
						String key = (String) entry.getKey();
						XPath xPath = XPath.newInstance(entry.getValue().toString());
						List nodeList = xPath.selectNodes(documentValue.getRootElement());
						resultMap.put(key, XPathQuery.createValueFromNodeList(nodeList));
					}
				}
				if ( includeContent ) {
					resultMap.put(CONTENT_ENTRY, content);
				}
			}
			arrayEntries.add(new MapValue(resultMap));
		}
		return new Result(StateDescriptor.OK, SimpleProcessor.OUTPUT, new ArrayValue((Value [])arrayEntries.toArray(new Value[arrayEntries.size()])));
	}
	
	public ParameterDescriptor[] getParameterDescriptors() {
		return parameterDescriptors;
	}

	public ResultDescriptor getResultDescriptor() {
		return resultDescriptor;
	}
}