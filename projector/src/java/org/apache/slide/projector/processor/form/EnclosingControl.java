package org.apache.slide.projector.processor.form;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.slide.projector.ConfigurationException;
import org.apache.slide.projector.Context;
import org.apache.slide.projector.ProcessException;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.descriptor.ArrayValueDescriptor;
import org.apache.slide.projector.descriptor.BooleanValueDescriptor;
import org.apache.slide.projector.descriptor.NumberValueDescriptor;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.StringValueDescriptor;
import org.apache.slide.projector.descriptor.ValueDescriptor;
import org.apache.slide.projector.i18n.ErrorMessage;
import org.apache.slide.projector.value.ArrayValue;
import org.apache.slide.projector.value.NumberValue;
import org.apache.slide.projector.value.StreamableValue;
import org.apache.slide.projector.value.StringValue;
import org.apache.slide.projector.value.Value;

/**
 * @version $Revision: 1.2 $
 */

public class EnclosingControl extends Control {
    protected final static int MAX_OPTIONS = 100;

    private final static String OPTIONS = "options";
    private final static String ENTRY_POSTFIX = " entry";
    private final static String SELECTED_ENTRY_POSTFIX = " selected entry";

    protected Template optionalEntryFragment, optionalSelectedEntryFragment, optionalValidEntryFragment, optionalValidSelectedEntryFragment, optionalInvalidEntryFragment, optionalInvalidSelectedEntryFragment;
    protected Template requiredEntryFragment, requiredSelectedEntryFragment, requiredValidEntryFragment, requiredValidSelectedEntryFragment, requiredInvalidEntryFragment, requiredInvalidSelectedEntryFragment;

    public EnclosingControl() {
        setRequiredFragments(new String[] { OPTIONAL+getName(), OPTIONAL+getName()+ENTRY_POSTFIX, OPTIONAL+getName()+SELECTED_ENTRY_POSTFIX,
                                            REQUIRED+getName(), REQUIRED+getName()+ENTRY_POSTFIX, REQUIRED+getName()+SELECTED_ENTRY_POSTFIX });
        setOptionalFragments(new String[] { OPTIONAL_VALID_CONTROL+getName(), OPTIONAL_INVALID_CONTROL+getName(), OPTIONAL_VALID_CONTROL+getName()+ENTRY_POSTFIX, OPTIONAL_INVALID_CONTROL+getName()+ENTRY_POSTFIX,
                                            OPTIONAL_VALID_CONTROL+getName()+SELECTED_ENTRY_POSTFIX, OPTIONAL_INVALID_CONTROL+getName()+SELECTED_ENTRY_POSTFIX,
                                            REQUIRED_VALID_CONTROL+getName(), REQUIRED_INVALID_CONTROL+getName(), REQUIRED_VALID_CONTROL+getName()+ENTRY_POSTFIX, REQUIRED_INVALID_CONTROL+getName()+ENTRY_POSTFIX,
                                            REQUIRED_VALID_CONTROL+getName()+SELECTED_ENTRY_POSTFIX, REQUIRED_INVALID_CONTROL+getName()+SELECTED_ENTRY_POSTFIX});
    }

    public Result process(Map parameter, Context context) throws Exception {
        ParameterDescriptor parameterDescriptor = getParameterDescriptor(parameter, context);
        String state = parameter.get(STATE).toString();
        Object value = parameter.get(VALUE);
        String[] options = getOptions(parameterDescriptor.getValueDescriptor());
        StringBuffer renderedEntries = new StringBuffer();
        Template template = optionalEntryFragment;
        for (int j = 0; j < options.length; j++) {
            if (valueMatches(options[j], value)) {
                if ( state.equals(OPTIONAL_CONTROL) ) {
                  template = optionalSelectedEntryFragment;
                } else if ( state.equals(OPTIONAL_VALID_CONTROL) ) {
                    template = optionalValidSelectedEntryFragment;
                } else if ( state.equals(OPTIONAL_INVALID_CONTROL) ) {
                    template = optionalInvalidSelectedEntryFragment;
                } else if ( state.equals(REQUIRED_CONTROL) ) {
                  template = requiredSelectedEntryFragment;
                } else if ( state.equals(REQUIRED_VALID_CONTROL) ) {
                    template = requiredValidSelectedEntryFragment;
                } else if ( state.equals(REQUIRED_INVALID_CONTROL) ) {
                    template = requiredInvalidSelectedEntryFragment;
                }
            } else {
                if ( state.equals(OPTIONAL_CONTROL) ) {
                  template = optionalEntryFragment;
                } else if ( state.equals(OPTIONAL_VALID_CONTROL) ) {
                    template = optionalValidEntryFragment;
                } else if ( state.equals(OPTIONAL_INVALID_CONTROL) ) {
                    template = optionalInvalidEntryFragment;
                } else if ( state.equals(REQUIRED_CONTROL) ) {
                  template = requiredEntryFragment;
                } else if ( state.equals(REQUIRED_VALID_CONTROL) ) {
                    template = requiredValidEntryFragment;
                } else if ( state.equals(REQUIRED_INVALID_CONTROL) ) {
                    template = requiredInvalidEntryFragment;
                }
            }
            parameter.put(VALUE, new StringValue(options[j]));
            template.evaluate(renderedEntries, parameter);
        }
        parameter.put(OPTIONS, new StringValue(renderedEntries.toString()));
        return super.process(parameter, context);
    }

    public void configure(StreamableValue config) throws ConfigurationException {
        super.configure(config);
        try {
            optionalEntryFragment = getRequiredFragment(OPTIONAL_CONTROL + getName() + ENTRY_POSTFIX);
            optionalSelectedEntryFragment = getRequiredFragment(OPTIONAL_CONTROL + getName() + SELECTED_ENTRY_POSTFIX);
            requiredEntryFragment = getRequiredFragment(REQUIRED_CONTROL + getName() + ENTRY_POSTFIX);
            requiredSelectedEntryFragment = getRequiredFragment(REQUIRED_CONTROL + getName() + SELECTED_ENTRY_POSTFIX);
        } catch ( ProcessException exception ) {
            throw new ConfigurationException(new ErrorMessage("control/requiredFragmentMissing", new String[] { OPTIONAL+getName()}), exception);
        }
        optionalValidEntryFragment = getOptionalFragment(OPTIONAL_VALID_CONTROL + getName() + ENTRY_POSTFIX, optionalEntryFragment);
        optionalInvalidEntryFragment = getOptionalFragment(OPTIONAL_INVALID_CONTROL + getName() + ENTRY_POSTFIX, optionalEntryFragment);
        optionalValidSelectedEntryFragment = getOptionalFragment(OPTIONAL_VALID_CONTROL + getName() + SELECTED_ENTRY_POSTFIX, optionalSelectedEntryFragment);
        optionalInvalidSelectedEntryFragment = getOptionalFragment(OPTIONAL_INVALID_CONTROL + getName() + SELECTED_ENTRY_POSTFIX, optionalSelectedEntryFragment);
        requiredValidEntryFragment = getOptionalFragment(REQUIRED_VALID_CONTROL + getName() + ENTRY_POSTFIX, requiredEntryFragment);
        requiredInvalidEntryFragment = getOptionalFragment(REQUIRED_INVALID_CONTROL + getName() + ENTRY_POSTFIX, requiredEntryFragment);
        requiredValidSelectedEntryFragment = getOptionalFragment(REQUIRED_VALID_CONTROL + getName() + SELECTED_ENTRY_POSTFIX, requiredSelectedEntryFragment);
        requiredInvalidSelectedEntryFragment = getOptionalFragment(REQUIRED_INVALID_CONTROL + getName() + SELECTED_ENTRY_POSTFIX, requiredSelectedEntryFragment);
    }

    protected String[] getOptions(ValueDescriptor valueDescriptor) {
        if (valueDescriptor instanceof ArrayValueDescriptor ) {
            valueDescriptor = ((ArrayValueDescriptor)valueDescriptor).getEntryValueDescriptor();
        }
        if (valueDescriptor instanceof StringValueDescriptor) {
            return ((StringValueDescriptor)valueDescriptor).getAllowedValues();
        } else if (valueDescriptor instanceof BooleanValueDescriptor) {
            return new String[]{BooleanValueDescriptor.TRUE, BooleanValueDescriptor.FALSE};
        } else if (valueDescriptor instanceof NumberValueDescriptor) {
            NumberValueDescriptor numberDescriptor = (NumberValueDescriptor)valueDescriptor;
            if (numberDescriptor.isConstrained()) {
                List allowedValues = new ArrayList();
                if (numberDescriptor.getAllowedValues() != null) {
                    Number[] numbers = numberDescriptor.getAllowedValues();
                    for (int i = 0; i < numbers.length; i++) {
                        allowedValues.add(String.valueOf(numbers[i]));
                    }
                } else {
                    int counter = 0;
                    for (int i = numberDescriptor.getMinimum().intValue(); i <= numberDescriptor.getMaximum().intValue(); i++) {
                        allowedValues.add(String.valueOf(i));
                        counter++;
                        if (counter == MAX_OPTIONS) break;
                    }
                }
                String[] options = new String[allowedValues.size()];
                return (String[])allowedValues.toArray(options);
            }
        }
        return new String[0];
    }

    protected boolean valueMatches(String value, Object resource) {
        if (resource instanceof StringValue) {
            return value.equals(resource.toString());
        } else if (resource instanceof NumberValue) {
            return value.equals(String.valueOf(((NumberValue)resource).getNumber()));
        } else if (resource instanceof ArrayValue) {
            Value[] array = ((ArrayValue)resource).getArray();
            for (int i = 0; i < array.length; i++) {
                if (array[i] instanceof StringValue) {
                    if (array[i].toString().equals(value)) return true;
                }
            }
        }
        return false;
    }
}