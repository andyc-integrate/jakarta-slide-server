package org.apache.slide.projector.processor.core;

import org.apache.slide.projector.ContentType;
import org.apache.slide.projector.Context;
import org.apache.slide.projector.descriptor.AnyValueDescriptor;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultEntryDescriptor;
import org.apache.slide.projector.i18n.DefaultMessage;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.processor.SimpleProcessor;
import org.apache.slide.projector.value.Value;

public class Echo extends SimpleProcessor {
    public Value process(Value input, Context context) throws Exception {
        return input;
    }

    public ParameterDescriptor getParameterDescriptor() {
        return new ParameterDescriptor(INPUT, new ParameterMessage("echo/parameter/input"), new AnyValueDescriptor());
    }

    public ResultEntryDescriptor getResultEntryDescriptor() {
        return new ResultEntryDescriptor(OUTPUT, new DefaultMessage("echo/result/output"), ContentType.DYNAMIC, true);
    }
}