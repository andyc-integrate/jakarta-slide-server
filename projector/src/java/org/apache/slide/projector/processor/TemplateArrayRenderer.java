package org.apache.slide.projector.processor;

import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.slide.projector.ConfigurationException;
import org.apache.slide.projector.Context;
import org.apache.slide.projector.ProcessException;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.descriptor.ArrayValueDescriptor;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.ResultEntryDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.i18n.DefaultMessage;
import org.apache.slide.projector.i18n.ErrorMessage;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.value.ArrayValue;
import org.apache.slide.projector.value.StreamableValue;
import org.apache.slide.projector.value.StringValue;

public class TemplateArrayRenderer extends TemplateRenderer {
    private static Logger logger = Logger.getLogger(TemplateArrayRenderer.class.getName());

    private ParameterDescriptor[] parameterDescriptors;

    public void configure(StreamableValue config) throws ConfigurationException {
        super.configure(config);
        parameterDescriptors = new ParameterDescriptor[parameterDescriptions.size()];
        int counter = 0;
        for ( Iterator i = parameterDescriptions.iterator(); i.hasNext(); ) {
            ParameterDescriptor entryDescriptor = (ParameterDescriptor)i.next();
            if ( entryDescriptor.getName() == FRAGMENT ) {
                parameterDescriptors[counter] = entryDescriptor;
            } else {
                parameterDescriptors[counter] = new ParameterDescriptor(entryDescriptor.getName(), new ParameterMessage("templateArrayRenderer/parameter"), new ArrayValueDescriptor(entryDescriptor.getValueDescriptor()));
            }
            counter++;
        }
    }

    public Result process(Map parameter, Context context) throws Exception {
        String fragment = ((StringValue)parameter.get(FRAGMENT)).toString();
        Template template = getRequiredFragment(fragment);
        parameter.remove(FRAGMENT);
        if ( template == null ) throw new ProcessException(new ErrorMessage("templateArrayRenderer/fragmentNotFound", new String[] { fragment }));
        StringBuffer buffer = new StringBuffer(template.getLength());
        for ( int i = 0; i < getMaxIndex(parameter); i++ ) {
            template.evaluate(buffer, parameter, i);
        }
        return new Result(OK, OUTPUT, new StringValue(buffer.toString(), template.getContentType(), false));
    }

    public ParameterDescriptor[] getParameterDescriptors() {
        return parameterDescriptors;
    }

    public ResultDescriptor getResultDescriptor() {
        return new ResultDescriptor(
                new StateDescriptor[] { StateDescriptor.OK_DESCRIPTOR },
                new ResultEntryDescriptor[]{
                    new ResultEntryDescriptor(OUTPUT, new DefaultMessage("templateArrayRenderer/result/output"), "*", true)
                });
    }

    protected int getMaxIndex(Map parameter) {
        int maxIndex = 0;
        for ( Iterator i = parameter.values().iterator(); i.hasNext(); ) {
            ArrayValue resurce = (ArrayValue)i.next();
            if ( resurce.getArray().length > maxIndex ) maxIndex = resurce.getArray().length;
        }
        return maxIndex;
    }
}