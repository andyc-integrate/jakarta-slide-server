package org.apache.slide.projector.processor.form;

import java.util.Map;

import org.apache.slide.projector.ConfigurationException;
import org.apache.slide.projector.Context;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.URI;
import org.apache.slide.projector.descriptor.ArrayValueDescriptor;
import org.apache.slide.projector.descriptor.BooleanValueDescriptor;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.StringValueDescriptor;
import org.apache.slide.projector.descriptor.URIValueDescriptor;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.processor.TemplateRenderer;
import org.apache.slide.projector.processor.process.Process;
import org.apache.slide.projector.value.ArrayValue;
import org.apache.slide.projector.value.BooleanValue;
import org.apache.slide.projector.value.StreamableValue;
import org.apache.slide.projector.value.StringValue;
import org.apache.slide.projector.value.Value;

/**
 * @version $Revision: 1.2 $
 */

public abstract class Trigger extends TemplateRenderer {
	public final static String INSTRUCTION = "instruction";
    public final static String ACTION = "action";
    public final static String VALIDATE = "validate";
    public final static String WIZARD = "wizard";
	public final static String INVOLVED_PARAMETERS = "involvedParameters";
	public final char SEPARATOR = ';';
	
    private ParameterDescriptor[] parameterDescriptors;

    public Trigger() {
        setRequiredFragments(new String[] { getName() });
    }

    public Result process(Map parameter, Context context) throws Exception {
    	Value []involvedParameters = (Value[])((ArrayValue)parameter.get(INVOLVED_PARAMETERS)).getArray();
    	String targetStep = parameter.get(Process.STEP).toString();
    	BooleanValue validate = (BooleanValue)parameter.get(VALIDATE);
    	BooleanValue wizard = (BooleanValue)parameter.get(WIZARD);
    	URI actionUri = (URI)parameter.get(ACTION);
		StringBuffer buffer = new StringBuffer(128);
		buffer.append(actionUri).append(SEPARATOR).append(validate).append(SEPARATOR).append(wizard).append(SEPARATOR).append(context.getStep()).append(SEPARATOR).append(targetStep).append(SEPARATOR).append(context.getProcess());
    	if ( validate.booleanValue() ) {
    		for ( int i = 0; i < involvedParameters.length; i++ ) {
    			buffer.append(SEPARATOR).append(involvedParameters[i]);
    		}
    	}
		String instruction = buffer.toString();
		parameter.put(INSTRUCTION, new StringValue(instruction));
        return new Result(OK, OUTPUT, renderFragment(getName(), parameter));
    }
    
    public void configure(StreamableValue config) throws ConfigurationException {
        super.configure(config);
        ParameterDescriptor[] parentParameterDescriptors = super.getParameterDescriptors();
        parameterDescriptors = new ParameterDescriptor[parentParameterDescriptors.length + 4];
        int counter = 0;
        for ( int i = 0; i < parentParameterDescriptors.length; i++ ) {
            if (!parentParameterDescriptors[i].getName().equals(FRAGMENT)) {
                parameterDescriptors[counter] = parentParameterDescriptors[i];
                counter++;
            }
        }
        parameterDescriptors[parentParameterDescriptors.length - 1] =
                new ParameterDescriptor(ACTION, new ParameterMessage("trigger/action"), new URIValueDescriptor());
        parameterDescriptors[parentParameterDescriptors.length ] =
            new ParameterDescriptor(Process.STEP, new ParameterMessage("trigger/step"), new StringValueDescriptor());
        parameterDescriptors[parentParameterDescriptors.length + 1] =
            new ParameterDescriptor(VALIDATE, new ParameterMessage("trigger/validate"), new BooleanValueDescriptor(), BooleanValue.TRUE);
        parameterDescriptors[parentParameterDescriptors.length + 2] =
            new ParameterDescriptor(INVOLVED_PARAMETERS, new ParameterMessage("trigger/involvedParameters"), new ArrayValueDescriptor(new StringValueDescriptor()));
        parameterDescriptors[parentParameterDescriptors.length + 3] =
            new ParameterDescriptor(WIZARD, new ParameterMessage("trigger/wizard"), new BooleanValueDescriptor(), BooleanValue.FALSE);
    }

    public ParameterDescriptor[] getParameterDescriptors() {
        return parameterDescriptors;
    }

    protected abstract String getName();
}