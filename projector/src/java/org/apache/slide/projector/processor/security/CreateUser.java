package org.apache.slide.projector.processor.security;

import java.util.Map;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.Information;
import org.apache.slide.projector.Processor;
import org.apache.slide.projector.Projector;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.ResultEntryDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.descriptor.StringValueDescriptor;
import org.apache.slide.projector.i18n.DefaultMessage;
import org.apache.slide.projector.i18n.ErrorMessage;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.repository.UserExistsException;
import org.apache.slide.projector.value.NullValue;
import org.apache.slide.projector.value.URIValue;
import org.apache.slide.projector.value.Value;

/**
 * @version $Revision: 1.2 $
 */

public class CreateUser implements Processor {
    private final static String USERNAME = "username";
    private final static String PASSWORD = "password";
    private final static String RETYPE_PASSWORD = "retypePassword";

    private final static String USER = "user";
    private final static String OK = "ok";
    private final static String FAILED = "failed";

    private final static ParameterDescriptor [] parameterDescriptor = new ParameterDescriptor[] {
            new ParameterDescriptor(USERNAME, new ParameterMessage("create-user/username"), new StringValueDescriptor(1,24)),
            new ParameterDescriptor(PASSWORD, new ParameterMessage("create-user/password"), new StringValueDescriptor(1,24)),
            new ParameterDescriptor(RETYPE_PASSWORD, new ParameterMessage("create-user/retyped-password"), new StringValueDescriptor(1,24)),
        };

    private final static ResultDescriptor resultDescriptor = new ResultDescriptor(
    		new StateDescriptor[] {
    				new StateDescriptor(OK, new DefaultMessage("create-user/state/ok")),
					new StateDescriptor(FAILED, new DefaultMessage("create-user/state/failed"))
    		}, new ResultEntryDescriptor[] {
    				new ResultEntryDescriptor(USER, new DefaultMessage("createUser/result/user"), URIValue.CONTENT_TYPE, false)
    		});

    public Result process(Map parameter, Context context) throws Exception {
        String username = parameter.get(USERNAME).toString();
        String retypedPassword = parameter.get(RETYPE_PASSWORD).toString();
        String password = parameter.get(PASSWORD).toString();
        String state = OK;
        // Check spelling
        Value user = NullValue.NULL;
        if ( !password.equals(retypedPassword) ) {
            context.addInformation(new Information(Information.ERROR, new ErrorMessage("register/passwordsNotIdentical"), new String[] { PASSWORD, RETYPE_PASSWORD }));
            state = FAILED;
        } else {
            try {
            	user = Projector.getRepository().createUser(username, password, context.getCredentials());	
            } catch ( UserExistsException exception ) {
                context.addInformation(new Information(Information.ERROR, exception.getErrorMessage(), new String[] { USERNAME }));
                state = FAILED;
            }
        }
        if ( user == null ) {
            context.addInformation(new Information(Information.ERROR, new ErrorMessage("register/failed"), new String[0]));
            state = FAILED;
        }
        return new Result(state, USER, user);
    }

    public ParameterDescriptor[] getParameterDescriptors() {
    	return parameterDescriptor;
    }

    public ResultDescriptor getResultDescriptor() {
    	return resultDescriptor;
    }
}