package org.apache.slide.projector.processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.slide.projector.ConfigurationException;
import org.apache.slide.projector.Context;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.descriptor.MapValueDescriptor;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.ResultEntryDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.i18n.DefaultMessage;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.value.MapValue;
import org.apache.slide.projector.value.NullValue;
import org.apache.slide.projector.value.StreamableValue;
import org.apache.slide.projector.value.StringValue;

public class TemplateMapRenderer extends TemplateRenderer {
    private static Logger logger = Logger.getLogger(TemplateMapRenderer.class.getName());

    private ParameterDescriptor[] parameterDescriptors;

    public void configure(StreamableValue config) throws ConfigurationException {
        super.configure(config);
        MapValueDescriptor mapValueDescriptor = new MapValueDescriptor();
        List parameterDescriptorList = new ArrayList();
        for ( Iterator i = parameterDescriptions.iterator(); i.hasNext(); ) {
            ParameterDescriptor entryDescriptor = (ParameterDescriptor)i.next();
            // add all map parameters to optional parameters 
            if ( entryDescriptor.getName() == FRAGMENT ) {
                parameterDescriptorList.add(entryDescriptor);
            } else {
                if ( entryDescriptor.isRequired() ) {
                    mapValueDescriptor.addEntryDescriptor(new ParameterDescriptor(entryDescriptor.getName(), new ParameterMessage("templateMapRenderer/mapEntry"), entryDescriptor.getValueDescriptor()));
                } else {
                    mapValueDescriptor.addEntryDescriptor(new ParameterDescriptor(entryDescriptor.getName(), new ParameterMessage("templateMapRenderer/mapEntry"), entryDescriptor.getValueDescriptor(), entryDescriptor.getDefaultValue() ));
                    parameterDescriptorList.add(new ParameterDescriptor(entryDescriptor.getName(), entryDescriptor.getDescription(), entryDescriptor.getValueDescriptor(), NullValue.NULL));
                }
            }
        }
        parameterDescriptorList.add(new ParameterDescriptor(SimpleProcessor.INPUT, new ParameterMessage("templateMapRenderer/input"), mapValueDescriptor));
        parameterDescriptors = (ParameterDescriptor[])parameterDescriptorList.toArray(new ParameterDescriptor[parameterDescriptorList.size()]);
    }

    public Result process(Map parameter, Context context) throws Exception {
        String fragment = ((StringValue)parameter.get(FRAGMENT)).toString();
        Template template = getRequiredFragment(fragment);
        StringBuffer buffer = new StringBuffer(template.getLength());
        Map parameterMap = new HashMap();
        parameterMap.putAll(parameter);
        for ( Iterator j = ((MapValue)parameter.get(SimpleProcessor.INPUT)).getMap().entrySet().iterator(); j.hasNext(); ) {
        	Map.Entry entry = (Map.Entry)j.next();
        	if ( !(entry.getValue() instanceof NullValue) ) {
        		parameterMap.put(entry.getKey(), entry.getValue());
        	}
        }
        template.evaluate(buffer, parameterMap);
        return new Result(OK, OUTPUT, new StringValue(buffer.toString(), template.getContentType(), false));
    }

    public ParameterDescriptor[] getParameterDescriptors() {
        return parameterDescriptors;
    }

    public ResultDescriptor getResultDescriptor() {
        return new ResultDescriptor(
                new StateDescriptor[] { StateDescriptor.OK_DESCRIPTOR },
                new ResultEntryDescriptor[]{
                    new ResultEntryDescriptor(OUTPUT, new DefaultMessage("templateMapRenderer/result/output"), "*", true)
                });
    }
}