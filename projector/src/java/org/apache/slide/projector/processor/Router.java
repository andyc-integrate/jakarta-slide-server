/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/projector/src/java/org/apache/slide/projector/processor/Router.java,v 1.2 2006-01-22 22:45:14 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:45:14 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.projector.processor;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.slide.projector.ConfigurableProcessor;
import org.apache.slide.projector.ConfigurationException;
import org.apache.slide.projector.Context;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.descriptor.StringValueDescriptor;
import org.apache.slide.projector.i18n.DefaultMessage;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.value.StreamableValue;
import org.apache.slide.projector.value.StringValue;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.AttributesImpl;

import de.zeigermann.xml.simpleImporter.DefaultSimpleImportHandler;
import de.zeigermann.xml.simpleImporter.SimpleImporter;
import de.zeigermann.xml.simpleImporter.SimplePath;

/**
 * The Router class
 * 
 */
public class Router implements ConfigurableProcessor {
    private static Logger logger = Logger.getLogger(Router.class.getName());

    private static ResultDescriptor resultDescriptor;
    private static ParameterDescriptor []parameterDescriptors;
    private Map routes = new HashMap();

    public Result process(Map parameter, Context context) throws Exception {
        String value = ((StringValue)parameter.get(SimpleProcessor.INPUT)).toString();
        return new Result((String)routes.get(value));
    }

    public ParameterDescriptor[] getParameterDescriptors() {
        return parameterDescriptors;
    }

    public ResultDescriptor getResultDescriptor() {
        return resultDescriptor;
    }

    public void configure(StreamableValue config) throws ConfigurationException {
        try {
            InputStream configuration = config.getInputStream();
            SimpleImporter importer = new SimpleImporter();
            ConfigurationHandler handler = new ConfigurationHandler();
            importer.addSimpleImportHandler(handler);
            importer.parse(new InputSource(configuration));
            parameterDescriptors = new ParameterDescriptor[] {
                new ParameterDescriptor(SimpleProcessor.INPUT, new ParameterMessage(handler.getDescription()), handler.getValueDesriptor())
            };
            resultDescriptor = new ResultDescriptor(handler.getStateDescriptors());
        } catch (Exception exception) {
            logger.log(Level.SEVERE, "Error while parsing router configuration", exception);
        }
    }

    public class ConfigurationHandler extends DefaultSimpleImportHandler {
        private StringValueDescriptor valueDesriptor = new StringValueDescriptor();
        private List states = new ArrayList();
        private String description;

        public void startElement(SimplePath path, String name, AttributesImpl attributes, String leadingCDdata) {
            if (path.matches("routes")) {
                description = attributes.getValue("description");
            } else if (path.matches("route")) {
                String value = attributes.getValue("value");
                String description = attributes.getValue("description");
                String state = attributes.getValue("state");
                valueDesriptor.addAllowedValue(value);
                states.add(new StateDescriptor(value, new DefaultMessage(description)));
                routes.put(value, state);
            }
        }

        public StringValueDescriptor getValueDesriptor() {
            return valueDesriptor;
        }

        public String getDescription() {
            return description;
        }

        public StateDescriptor[] getStateDescriptors() {
            return (StateDescriptor[])states.toArray(new StateDescriptor[0]);
        }
    }
}