package org.apache.slide.projector.processor.core;

import org.apache.slide.projector.ContentType;
import org.apache.slide.projector.Context;
import org.apache.slide.projector.HttpContext;
import org.apache.slide.projector.ProcessException;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultEntryDescriptor;
import org.apache.slide.projector.descriptor.StringValueDescriptor;
import org.apache.slide.projector.i18n.DefaultMessage;
import org.apache.slide.projector.i18n.ErrorMessage;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.processor.SimpleProcessor;
import org.apache.slide.projector.value.StringValue;
import org.apache.slide.projector.value.Value;

public class URL extends SimpleProcessor {
    public Value process(Value input, Context context) throws Exception {
        Value output;
        if ( context instanceof HttpContext ) {
            output = new StringValue(((HttpContext)context).getContextPath() + "/" + ((StringValue)input).toString());
        } else {
            throw new ProcessException(new ErrorMessage("httpContextRequired"));
        }
        return output;
    }

    public ParameterDescriptor getParameterDescriptor() {
        return new ParameterDescriptor(INPUT, new ParameterMessage("path/parameter/input"), new StringValueDescriptor());
    }

    public ResultEntryDescriptor getResultEntryDescriptor() {
        return new ResultEntryDescriptor(OUTPUT, new DefaultMessage("path/result/output"), ContentType.DYNAMIC, true);
    }
}