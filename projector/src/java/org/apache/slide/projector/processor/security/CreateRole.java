package org.apache.slide.projector.processor.security;

import java.util.Map;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.Information;
import org.apache.slide.projector.Processor;
import org.apache.slide.projector.Projector;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.ResultEntryDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.descriptor.StringValueDescriptor;
import org.apache.slide.projector.i18n.DefaultMessage;
import org.apache.slide.projector.i18n.ErrorMessage;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.repository.RoleExistsException;
import org.apache.slide.projector.value.NullValue;
import org.apache.slide.projector.value.URIValue;
import org.apache.slide.projector.value.Value;

/**
 * @version $Revision: 1.2 $
 */

public class CreateRole implements Processor {
    private final static String ROLENAME = "rolename";
    
    private final static String ROLE = "role";
    private final static String OK = "ok";
    private final static String FAILED = "failed";

    private final static ParameterDescriptor [] parameterDescriptor = new ParameterDescriptor[] {
            new ParameterDescriptor(ROLENAME, new ParameterMessage("createRole/parameter/rolename"), new StringValueDescriptor(1,24)),
        };

    private final static ResultDescriptor resultDescriptor = new ResultDescriptor(
    		new StateDescriptor[] {
    				new StateDescriptor(OK, new DefaultMessage("createRole/state/ok")),
					new StateDescriptor(FAILED, new DefaultMessage("createRole/state/failed"))
    		}, new ResultEntryDescriptor[] {
    				new ResultEntryDescriptor(ROLE, new DefaultMessage("createRole/result/role"), URIValue.CONTENT_TYPE, false)
    		});

    public Result process(Map parameter, Context context) throws Exception {
        String rolename = parameter.get(ROLENAME).toString();
        String state = OK;
        Value role = NullValue.NULL;
        try {
        	role = Projector.getRepository().createRole(rolename, context.getCredentials());	
        } catch ( RoleExistsException exception ) {
        	context.addInformation(new Information(Information.ERROR, exception.getErrorMessage(), new String[] { ROLENAME }));
        	state = FAILED;
        }
        if ( role == null ) {
            context.addInformation(new Information(Information.ERROR, new ErrorMessage("createRole/failed"), new String[0]));
            state = FAILED;
        }
        return new Result(state, ROLE, role);
    }

    public ParameterDescriptor[] getParameterDescriptors() {
    	return parameterDescriptor;
    }

    public ResultDescriptor getResultDescriptor() {
    	return resultDescriptor;
    }
}