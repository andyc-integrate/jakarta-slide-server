/*
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.projector.processor.core;

import java.util.Map;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.Processor;
import org.apache.slide.projector.Result;
import org.apache.slide.projector.URI;
import org.apache.slide.projector.descriptor.BooleanValueDescriptor;
import org.apache.slide.projector.descriptor.MapValueDescriptor;
import org.apache.slide.projector.descriptor.ParameterDescriptor;
import org.apache.slide.projector.descriptor.ResultDescriptor;
import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.descriptor.URIValueDescriptor;
import org.apache.slide.projector.descriptor.XMLValueDescriptor;
import org.apache.slide.projector.engine.Job;
import org.apache.slide.projector.engine.ProcessorManager;
import org.apache.slide.projector.engine.Scheduler;
import org.apache.slide.projector.expression.Expression;
import org.apache.slide.projector.expression.ExpressionFactory;
import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.value.BooleanValue;
import org.apache.slide.projector.value.MapValue;
import org.apache.slide.projector.value.NullValue;
import org.apache.slide.projector.value.URIValue;
import org.apache.slide.projector.value.Value;
import org.apache.slide.projector.value.XMLValue;

/**
*/
public class Thread implements Processor {
	// parameters
	public final static String PROCESSOR = "processor";
	public final static String REPEAT = "repeat";
	public final static String CONDITION = "condition";
	public final static String REMAINING_CONDITION = "remainingCondition";
	public final static String PARAMETERS = "parameters";
	public final static String PERSISTENT = "persistent";
	
	private final static ParameterDescriptor[] parameterDescriptors = new ParameterDescriptor[] {
			new ParameterDescriptor(PROCESSOR, new ParameterMessage("job/parameter/processor"), new URIValueDescriptor()),
			new ParameterDescriptor(REPEAT, new ParameterMessage("job/parameter/repeat"), new BooleanValueDescriptor(), BooleanValue.FALSE),
			new ParameterDescriptor(PERSISTENT, new ParameterMessage("job/parameter/persistent"), new BooleanValueDescriptor(), BooleanValue.TRUE),
			new ParameterDescriptor(CONDITION, new ParameterMessage("job/parameter/condition"), new XMLValueDescriptor()),
			new ParameterDescriptor(REMAINING_CONDITION, new ParameterMessage("job/parameter/remainingCondition"), new XMLValueDescriptor(), NullValue.NULL),
			new ParameterDescriptor(PARAMETERS, new ParameterMessage("job/parameter/parameters"), MapValueDescriptor.ANY)
	};
	
	private final static ResultDescriptor resultDescriptor = new ResultDescriptor(new StateDescriptor[] {
		StateDescriptor.OK_DESCRIPTOR
	}); 
	
	public Result process(Map parameters, Context context) throws Exception {
			Map jobParameters = ((MapValue)parameters.get(PARAMETERS)).getMap();
			URI jobUri = (URIValue)parameters.get(PROCESSOR);
			XMLValue initialCondition = (XMLValue)parameters.get(CONDITION);
			boolean repeatJob = ((BooleanValue)parameters.get(REPEAT)).booleanValue();
			Value remainingConditionValue = (Value)parameters.get(REMAINING_CONDITION);
			XMLValue remainingCondition = null;
			if ( remainingConditionValue == NullValue.NULL ) {
				remainingCondition = initialCondition;
			} else {
				remainingCondition = (XMLValue)remainingConditionValue;
			}
			boolean persistentJob = ((BooleanValue)parameters.get(PERSISTENT)).booleanValue();
			Expression remainingExpression = ExpressionFactory.create(remainingCondition.getRootElement());
			Expression initialExpression = ExpressionFactory.create(initialCondition.getRootElement());
			Processor jobProcessor = ProcessorManager.getInstance().getProcessor(jobUri);
			ProcessorManager.prepareValues(jobProcessor.getParameterDescriptors(), jobParameters, context);
			String processorId = context.getProcessId();
			Job job;
			if ( processorId == null ) {
				job = new Job(context.getStep(), jobUri, initialExpression, remainingExpression, jobParameters, repeatJob, persistentJob); 
			} else {
				job = new Job(context.getProcessId()+":"+context.getStep(), jobUri, initialExpression, remainingExpression, jobParameters, repeatJob, persistentJob); 
			}
			Scheduler.getInstance().registerJob(job);
			Scheduler.getInstance().saveJobs();
			return Result.OK;
	}
	
	public ParameterDescriptor[] getParameterDescriptors() {
		return parameterDescriptors;
	}
	
	public ResultDescriptor getResultDescriptor() {
		return resultDescriptor;
	}
}