/*
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.projector;

import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.slide.projector.i18n.ErrorMessage;
import org.apache.slide.projector.repository.Configurable;
import org.apache.slide.projector.repository.Repository;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

public class Projector {
    private final static Logger logger = Logger.getLogger(Projector.class.getName());

    private static Credentials credentials;
    private static Repository repository;
	private static String projectorDir, applicationsDir, workDir;
	
	public static void configure(InputStream configuration) throws ConfigurationException {
		logger.log(Level.INFO, "Starting projector");
        SAXBuilder saxBuilder = new SAXBuilder();
        try {
        	Document document = saxBuilder.build(configuration);
        	Element credentialsElement = document.getRootElement().getChild("credentials");
        	String user = credentialsElement.getAttributeValue("user");
        	String password = credentialsElement.getAttributeValue("password");
            credentials = new UsernamePasswordCredentials(user, password);
        	Element repositoryElement = document.getRootElement().getChild("repository");
        	String repositoryClassname = repositoryElement.getAttributeValue("class");
        	Class repositoryClass = Class.forName(repositoryClassname);
        	Method getInstanceMethod = repositoryClass.getMethod("getInstance", new Class[0]);
        	repository = (Repository)getInstanceMethod.invoke(null, null);
        	Element configurationElement = repositoryElement.getChild("configuration");
        	if ( repository instanceof Configurable ) {
        		((Configurable)repository).configure(configurationElement);
        	}
        	Element pathsElement = repositoryElement.getChild("paths");
        	projectorDir = pathsElement.getChildText("projector"); 
        	applicationsDir = pathsElement.getChildText("applications"); 
        	workDir = pathsElement.getChildText("work"); 
        } catch (Exception e) {
        	throw new ConfigurationException(new ErrorMessage("projector/configurationFailed"), e);
        }
	}
	
	public static Repository getRepository() {
        return repository;
	}
	
	public static String getApplicationsDir() {
		return applicationsDir;
	}
	
	public static String getProjectorDir() {
		return projectorDir;
	}
	
	public static String getWorkDir() {
		return workDir;
	}

	public static Credentials getCredentials() {
		return credentials;
	}
}