package org.apache.slide.projector;

import java.io.IOException;
import java.util.Map;

import org.apache.commons.httpclient.Credentials;
import org.apache.slide.projector.store.Cache;
import org.apache.slide.projector.store.ProcessStore;
import org.apache.slide.projector.store.RepositoryStore;

public class SystemContext extends AbstractContext {
	protected final static String BOOKMARK = "_bookmark_";
    protected Store stepStore;
	
	private Credentials credentials = Projector.getCredentials();
    private Cache contextStore = new Cache();
	private RepositoryStore repositoryStore = new RepositoryStore(this);
    private ProcessStore transientProcessStore = new ProcessStore(Cache.getInstance());
    private ProcessStore persistentProcessStore = new ProcessStore(repositoryStore);
    private Cache inputParameterStore = new Cache();
    
    public Store getStore(int store) {
        switch ( store ) {
            case Store.CACHE : {
                return Cache.getInstance();
            }
            case Store.CONTEXT : {
                return contextStore;
            }
            case Store.REPOSITORY : {
                return repositoryStore;
            }
            case Store.TRANSIENT_PROCESS : {
                return transientProcessStore;
            }
            case Store.PERSISTENT_PROCESS : {
                return persistentProcessStore;
            }
            case Store.INPUT : {
                return inputParameterStore;
            }
            case Store.STEP : {
                return stepStore;
            }
        }
        return null;
    }

    public void setCredentials(Credentials credentials) {
    	this.credentials = credentials;
    }
    
    public Credentials getCredentials() {
        return credentials;
    }

    public void setProcessId(String processId) {
    	transientProcessStore.setProcessId(processId);
    	persistentProcessStore.setProcessId(processId);
    }
    
    public String getProcessId() {
    	return transientProcessStore.getProcessId();
    }

    public void setBookmark(URI processor) throws IOException {
    	transientProcessStore.put(BOOKMARK, processor);
    }
    
    public URI getBookmark() throws IOException {
    	return (URI)transientProcessStore.get(BOOKMARK);
    }

    public void setStepStore(Store store) {
    	this.stepStore = store;
    }

    public void setInputParameters(Map parameters) {
    	inputParameterStore.setMap(parameters);
    }
}