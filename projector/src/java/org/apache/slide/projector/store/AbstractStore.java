package org.apache.slide.projector.store;

import java.io.IOException;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.slide.projector.Store;
import org.apache.slide.projector.engine.Scheduler;

public abstract class AbstractStore implements Store {
    private final static Logger logger = Logger.getLogger(AbstractStore.class.getName());
	
	public void put(final String key, Object value, long timeout) throws IOException {
		put(key, value);
		Scheduler.getTimer().schedule(new TimerTask() {
			public void run() {
				try {
					dispose(key);
				} catch ( IOException exception ) {
					logger.log(Level.SEVERE, "Could not dispose object with key="+key, exception);
				}
			}
		}, timeout);
	}
}
