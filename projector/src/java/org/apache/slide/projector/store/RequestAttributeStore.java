/*
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.projector.store;

import javax.servlet.http.HttpServletRequest;

public class RequestAttributeStore extends AbstractStore {
    protected HttpServletRequest request;

    public RequestAttributeStore(HttpServletRequest request) {
        this.request = request;
    }

    public void put(String key, Object value) {
        request.setAttribute(key, value);
    }

    public Object get(String key) {
        return request.getAttribute(key);
    }

    public void dispose(String key) {
        request.removeAttribute(key);
    }
}