package org.apache.slide.projector.store;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Cache extends AbstractStore {
    private static Cache cache = new Cache();
    private Map map;

    public static Cache getInstance() {
        return cache;
    }

    public Cache() {
    	map = new HashMap();
    }
    
    public void setMap(Map map) {
    	this.map = map;
    }
    
    public Map getMap() {
    	return map;
    }
    
    public void put(String key, Object value) throws IOException {
        map.put(key, value);
    }

    public Object get(String key) throws IOException {
        return map.get(key);
    }

    public void dispose(String key) throws IOException {
        map.remove(key);
    }
}