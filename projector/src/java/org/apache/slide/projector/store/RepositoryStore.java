/*
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.projector.store;

import java.io.IOException;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.Projector;
import org.apache.slide.projector.descriptor.ValueFactoryManager;
import org.apache.slide.projector.value.StreamableValue;
import org.apache.slide.projector.value.StringValue;
import org.apache.slide.projector.value.URIValue;
import org.apache.slide.projector.value.Value;

import de.zeigermann.xml.XMLStringWriter;

public class RepositoryStore extends AbstractStore {
	private Context context;
	
	public RepositoryStore(Context context) {
		this.context = context;
	}
	
	public void put(String key, Object value) throws IOException {
        if ( value instanceof StreamableValue) {
            Projector.getRepository().setResource(new URIValue(key), (StreamableValue)value, context.getCredentials());
        } else if ( value instanceof Value ) {
            XMLStringWriter writer = XMLStringWriter.create();
            writer.writeXMLDeclaration();
            ValueFactoryManager.getInstance().saveValue((Value)value, writer);
            Projector.getRepository().setResource(new URIValue(key), new StringValue(writer.toString()), context.getCredentials());
        } else {
        	throw new IOException("Could not write value to repository! Given value is '"+value+"'");
        }
    }

    public Object get(String key) throws IOException {
        return Projector.getRepository().getResource(new URIValue(key), context.getCredentials());
    }

    public void dispose(String key) throws IOException {
        Projector.getRepository().removeResource(new URIValue(key), context.getCredentials());
    }
}