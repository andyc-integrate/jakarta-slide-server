/*
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.projector.store;

import java.io.IOException;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.Store;
import org.apache.slide.projector.value.MapValue;

public class FormStore extends AbstractStore {
	private Context context;
	private Store store;
	
	public FormStore(Context context, Store store) {
		this.context = context;
		this.store = store;
	}

	public void put(String key, Object value) throws IOException {
		MapValue domain = getDomain();
		domain.getMap().put(key, value);
	}

	public Object get(String key) throws IOException {
		return getDomain().getMap().get(key);
	}

	public void dispose(String key) throws IOException {
		getDomain().getMap().remove(key);
	}

	public void clear() throws IOException {
		String domain = context.getProcess().toString();
		store.dispose(domain);
	}
	
	public MapValue getDomain() throws IOException {
		String domain = context.getProcess().toString();
		MapValue mapResource = (MapValue)store.get(domain);
		if ( mapResource == null ) {
			mapResource = new MapValue();
			store.put(domain, mapResource);
			return mapResource;
		} else {
			return mapResource;
		}
	}
}