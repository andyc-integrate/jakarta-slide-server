/*
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.projector.store;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.slide.projector.Projector;
import org.apache.slide.projector.Store;
import org.apache.slide.projector.value.MapValue;

public class ProcessStore extends AbstractStore {
    private final static String STORAGE_PREFIX = "process/";

    private Store store;
	private String processId, storageKey;
	private Map map;
	private MapValue mapResource;
	
	public ProcessStore(Store store) {
		this.store = store;
	}
	
	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
		this.storageKey = Projector.getWorkDir()+STORAGE_PREFIX+processId;
	}

	public void put(String key, Object value) throws IOException {
		getMap().put(key, value);
		store.put(storageKey, mapResource);
	}

	public Object get(String key) throws IOException {
		return getMap().get(key);
	}

	public void dispose(String key) throws IOException {
		getMap().remove(key);
		store.put(storageKey, mapResource);
	}
	
	private Map getMap() throws IOException {
		if ( mapResource != null ) return mapResource.getMap();
		mapResource = (MapValue)store.get(storageKey);
		if ( mapResource == null ) {
			map = new HashMap();
			mapResource = new MapValue(map);
			return map;
		} else {
			return mapResource.getMap();
		}
	}
}