package org.apache.slide.projector;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.Credentials;
import org.apache.slide.projector.store.Cache;
import org.apache.slide.projector.store.CookieStore;
import org.apache.slide.projector.store.FormStore;
import org.apache.slide.projector.store.RequestAttributeStore;
import org.apache.slide.projector.store.RequestHeaderStore;
import org.apache.slide.projector.store.RequestParameterStore;
import org.apache.slide.projector.store.SessionStore;
import org.apache.slide.projector.value.StreamableValue;

public class HttpContext extends SystemContext {
    private final static String CREDENTIALS = "credentials";

    private StreamableValue resource;
    private SessionStore sessionStore;
    private RequestAttributeStore requestAttributeStore;
    private RequestParameterStore requestParameterStore;
    private RequestHeaderStore requestHeaderStore;
    private CookieStore cookieStore;
    private FormStore formStore;
    private String contextPath;

    public HttpContext(HttpServletRequest request, HttpServletResponse response) {
        this.contextPath = request.getContextPath()+request.getServletPath();
        sessionStore = new SessionStore(request);
        formStore = new FormStore(this, sessionStore);
        requestAttributeStore = new RequestAttributeStore(request);
        requestParameterStore = new RequestParameterStore(request);
        requestHeaderStore = new RequestHeaderStore(request);
        cookieStore = new CookieStore(request, response);
    }

    public void setCredentials(Credentials credentials) {
        sessionStore.put(CREDENTIALS, credentials);
    }

    public Credentials getCredentials() {
        Credentials credentials = (Credentials)sessionStore.get(CREDENTIALS);
        if ( credentials == null ) {
            credentials = Projector.getCredentials();            
        }
        return credentials;
    }

    public Store getStore(int store) {
        switch ( store ) {
            case Store.SESSION : {
                return sessionStore;
            }
            case Store.REQUEST_PARAMETER : {
                return requestParameterStore;
            }
            case Store.REQUEST_ATTRIBUTE : {
                return requestAttributeStore;
            }
            case Store.REQUEST_HEADER : {
                return requestHeaderStore;
            }
            case Store.COOKIE : {
                return cookieStore;
            }
            case Store.CACHE : {
                return Cache.getInstance();
            }
            case Store.FORM : {
                return formStore;
            }
        }
        return super.getStore(store);
    }

    public String getContextPath() {
        return contextPath;
    }

    public void setPresentableResource(StreamableValue resource) {
        this.resource = resource;
    }

    public StreamableValue getPresentableResource() {
        return resource;
    }

    public void setBookmark(URI processor) throws IOException {
    	sessionStore.put(BOOKMARK, processor);
    }
    
    public URI getBookmark() throws IOException {
    	return (URI)sessionStore.get(BOOKMARK);
    }
}