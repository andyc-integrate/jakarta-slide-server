package org.apache.slide.projector.i18n;

import java.util.Locale;

public class DefaultMessage extends LocalizedMessage {
    private final static String TITLE = "title";
    private final static String TEXT = "text";

    public DefaultMessage(String messageId) {
        super(messageId);
    }

    public DefaultMessage(String messageId, Object[] arguments) {
        super(messageId, arguments);
    }

    public String getTitle(Locale locale) throws MessageNotFoundException {
        return getText(TITLE, locale);
    }

    public String getTitle(Locale locale, String defaultTitle) {
        return getText(TITLE, defaultTitle, locale);
    }

    public String getText(Locale locale) throws MessageNotFoundException  {
        return getText(TEXT, locale);
    }

    public String getText(Locale locale, String defaultText) {
        return getText(TEXT, defaultText, locale);
    }
}
