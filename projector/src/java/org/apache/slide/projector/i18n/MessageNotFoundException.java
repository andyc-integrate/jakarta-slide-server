package org.apache.slide.projector.i18n;

import org.apache.slide.projector.ProcessException;

/**
 * @version $Revision: 1.2 $
 */

public class MessageNotFoundException extends ProcessException {
    public MessageNotFoundException(ErrorMessage errorMessage, Throwable throwable) {
        super(errorMessage, throwable);
    }

    public MessageNotFoundException(ErrorMessage errorMessage) {
        super(errorMessage);
    }
}
