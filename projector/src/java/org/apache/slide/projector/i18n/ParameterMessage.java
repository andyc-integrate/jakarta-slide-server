package org.apache.slide.projector.i18n;

import java.util.Locale;

public class ParameterMessage extends DefaultMessage {
    public final static String NO_MESSAGE_AVAILABLE = "noParameterDescriptionAvailable";

    private final static String PROMPT = "prompt";

    public ParameterMessage(String messageId) {
        super(messageId);
    }

    public ParameterMessage(String messageId, Object[] arguments) {
        super(messageId, arguments);
    }

    public String getPrompt(Locale locale) throws MessageNotFoundException {
        return getText(PROMPT, locale);
    }

    public String getPrompt(Locale locale, String defaultPrompt) {
        return getText(PROMPT, defaultPrompt, locale);
    }
}
