package org.apache.slide.projector.i18n;

import java.util.Locale;

public class ErrorMessage extends DefaultMessage {
    private final static String SUMMARY = "summary";
    private final static String DETAILS = "details";

    public ErrorMessage(String messageId) {
        super(messageId);
    }

    public ErrorMessage(String messageId, Object[] arguments) {
        super(messageId, arguments);
    }

    public String getSummary(Locale locale) throws MessageNotFoundException {
        return getText(SUMMARY, locale);
    }

    public String getSummary(Locale locale, String defaultSummary) {
        return getText(SUMMARY, defaultSummary, locale);
    }

    public String getDetails(Locale locale) throws MessageNotFoundException {
        return getText(DETAILS, locale);
    }

    public String getDetails(Locale locale, String defaultDetails) {
        return getText(DETAILS, defaultDetails, locale);
    }
}
