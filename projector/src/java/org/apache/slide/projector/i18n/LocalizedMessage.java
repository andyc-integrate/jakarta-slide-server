package org.apache.slide.projector.i18n;

import org.apache.slide.projector.value.MessageValue;

public class LocalizedMessage extends MessageValue {
    public LocalizedMessage(String messageId) {
        super(messageId);
    }

    public LocalizedMessage(String messageId, Object[] arguments) {
        super(messageId, arguments);
    }
}