package org.apache.slide.projector;

import org.apache.slide.projector.i18n.ErrorMessage;

public class ConfigurationException extends ProcessException {
    public ConfigurationException(ErrorMessage errorMessage, Throwable throwable) {
        super(errorMessage, throwable);
    }

    public ConfigurationException(ErrorMessage errorMessage) {
        super(errorMessage);
    }
}