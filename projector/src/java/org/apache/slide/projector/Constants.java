package org.apache.slide.projector;

import org.apache.slide.projector.value.URIValue;

public interface Constants {
    public static final String PROCESS_ID_PARAMETER = "_process_id_";
    public static final String STEP_PARAMETER = "step";
	public static final int PROCESS_ID_LENGTH = 12;

	// FIXME: Use Classpath from applications
//    public final static String CLASSES_DIR = PROJECTOR_DIR + "classes/";

    public static final URI DEFAULT_FORM_HANDLER = new URIValue("formHandler");
}