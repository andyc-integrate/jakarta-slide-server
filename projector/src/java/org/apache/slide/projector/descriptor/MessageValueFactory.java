package org.apache.slide.projector.descriptor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.slide.projector.value.MessageValue;
import org.apache.slide.projector.value.Value;
import org.jdom.Element;

import de.zeigermann.xml.XMLStringWriter;
import de.zeigermann.xml.XMLWriter;

public class MessageValueFactory implements ValueFactory {
	public final static String ID = "id";
	public final static String ARGUMENTS = "arguments";

	public Class getValueClass() {
		return MessageValue.class;
	}
	
	public String getElementName() {
    	return "message";
    }
    
	public Value load(Element element) {
		String messageId = element.getAttributeValue(ID);
		List children = new ArrayList();
		for ( Iterator i = element.getChildren().iterator(); i.hasNext(); ) {
			Element childElement = (Element)i.next();
			children.add(ValueFactoryManager.getInstance().loadValue(childElement));			
		}
		return new MessageValue(messageId, (Value[])children.toArray(new Value[children.size()]));
	}

    public void save(Value value, XMLStringWriter writer) {
    	MessageValue messageValue = (MessageValue)value;
        writer.writeStartTag(XMLWriter.createStartTag(getElementName(), ID, messageValue.getId()));
        Value []arguments = messageValue.getArguments();
        for ( int i = 0; i < arguments.length; i++ ) {
        	ValueFactoryManager.getInstance().saveValue(arguments[i], writer);
        }
        writer.writeEndTag(XMLWriter.createEndTag(getElementName()));
    }

    public ValueDescriptor loadDescriptor(Element element) {
    	return new MessageValueDescriptor();
    }
}