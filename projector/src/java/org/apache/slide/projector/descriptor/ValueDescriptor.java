package org.apache.slide.projector.descriptor;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.value.Value;

public interface ValueDescriptor {
	public Value valueOf(Object value, Context context) throws ValueCastException;
	
	/**
     *
     * @param value the value to be validated
     * @return the validated value
     */
    public void validate(Value value, Context context) throws ValidationException;
}