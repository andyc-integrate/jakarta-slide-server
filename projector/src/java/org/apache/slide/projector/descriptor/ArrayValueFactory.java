package org.apache.slide.projector.descriptor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.slide.projector.value.ArrayValue;
import org.apache.slide.projector.value.Value;
import org.jdom.Element;

import de.zeigermann.xml.XMLStringWriter;
import de.zeigermann.xml.XMLWriter;

public class ArrayValueFactory implements ValueFactory {
	public Class getValueClass() {
		return ArrayValue.class;
	}
	
	public String getElementName() {
    	return "array";
    }
    
	public Value load(Element element) {
		List children = new ArrayList();
		for ( Iterator i = element.getChildren().iterator(); i.hasNext(); ) {
			Element childElement = (Element)i.next();
			children.add(ValueFactoryManager.getInstance().loadValue(childElement));			
		}
		return new ArrayValue((Value [])children.toArray(new Value[children.size()]));
	}

    public void save(Value value, XMLStringWriter writer) {
    	Value[] array = ((ArrayValue)value).getArray();
    	writer.writeStartTag(XMLWriter.createStartTag(getElementName()));
        for ( int i = 0; i < array.length; i++ ) {
            ValueFactoryManager.getInstance().saveValue(array[i], writer);
        }
        writer.writeEndTag(XMLWriter.createEndTag(getElementName()));
    }

    public ValueDescriptor loadDescriptor(Element element) {
    	Element allowedArrayValueElement = (Element)element.getChildren().iterator().next();
    	ValueDescriptor allowedArrayValue = ValueFactoryManager.getInstance().loadValueDescriptor(allowedArrayValueElement);
    	return new ArrayValueDescriptor(allowedArrayValue);
    }
}