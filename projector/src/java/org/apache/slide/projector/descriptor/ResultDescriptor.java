package org.apache.slide.projector.descriptor;


public class ResultDescriptor {
	public final static ResultDescriptor OK = new ResultDescriptor(new StateDescriptor[] { StateDescriptor.OK_DESCRIPTOR }); 
		
	protected StateDescriptor[] stateDescriptors;
    protected ResultEntryDescriptor[] resultEntryDescriptors;

    public ResultDescriptor(StateDescriptor[] stateDescriptors, ResultEntryDescriptor[] resultEntryDescriptors) {
        this.stateDescriptors = stateDescriptors;
        this.resultEntryDescriptors = resultEntryDescriptors;
    }

    public ResultDescriptor(StateDescriptor[] stateDescriptors) {
        this.stateDescriptors = stateDescriptors;
        this.resultEntryDescriptors = new ResultEntryDescriptor[0];
    }

    public StateDescriptor[] getStateDescriptors() {
        return stateDescriptors;
    }

    public ResultEntryDescriptor[] getResultEntryDescriptors() {
        return resultEntryDescriptors;
    }
}