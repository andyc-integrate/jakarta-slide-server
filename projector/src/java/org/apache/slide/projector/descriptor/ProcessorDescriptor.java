package org.apache.slide.projector.descriptor;

import org.apache.slide.projector.Processor;
import org.apache.slide.projector.URI;
import org.apache.slide.projector.i18n.LocalizedMessage;

/**
 * @version $Revision: 1.2 $
 */

public class ProcessorDescriptor extends Descriptor {
    private URI uri, configuration, smallIcon, largeIcon;
    private Processor processor;
    private boolean bookmark;

    public ProcessorDescriptor(URI uri) {
        super(null, null);
        this.uri = uri;
    }

    public void setName(String name) {
    	this.name = name;
    }

    public void setDescription(LocalizedMessage description) {
    	this.description = description;
    }
    
    public Processor getProcessor() {
        return processor;
    }

    public void setProcessor(Processor processor) {
        this.processor = processor;
    }

    public void setConfiguration(URI configuration) {
        this.configuration = configuration;
    }

    public void setSmallIcon(URI smallIcon) {
        this.smallIcon = smallIcon;
    }

    public void setLargeIcon(URI largeIcon) {
        this.largeIcon = largeIcon;
    }

    public URI getUri() {
        return uri;
    }

    public URI getConfiguration() {
        return configuration;
    }

    public URI getSmallIcon() {
        return smallIcon;
    }

    public URI getLargeIcon() {
        return largeIcon;
    }

    public void setBookmark(boolean bookmark) {
        this.bookmark = bookmark;
    }

    public boolean isBookmark() {
        return bookmark;
    }
}
