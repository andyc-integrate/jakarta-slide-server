package org.apache.slide.projector.descriptor;

import org.apache.slide.projector.i18n.LocalizedMessage;
import org.apache.slide.projector.value.Value;

public class ParameterDescriptor extends Descriptor {
	public final static ParameterDescriptor[] NO_PARAMETERS = new ParameterDescriptor[0];
	
    protected ValueDescriptor valueDescriptor;
    protected Value defaultValue;

    public ParameterDescriptor(String name, LocalizedMessage description, ValueDescriptor valueDescriptor) {
        super(name, description);
        this.valueDescriptor = valueDescriptor;
    }

    public ParameterDescriptor(String name, LocalizedMessage description, ValueDescriptor valueDescriptor, Value defaultValue) {
        super(name, description);
        this.valueDescriptor = valueDescriptor;
        this.defaultValue  = defaultValue;
    }

    public boolean isRequired() {
        return ( defaultValue == null );
    }

    public void setValueDescriptor(ValueDescriptor valueDescriptor) {
        this.valueDescriptor = valueDescriptor;
    }

    public ValueDescriptor getValueDescriptor() {
        return valueDescriptor;
    }

    public void setDefaultValue(Value defaultValue) {
        this.defaultValue = defaultValue;
    }

    public Value getDefaultValue() {
        return defaultValue;
    }
    
    public boolean equals(Object o) {
    	if ( o instanceof ParameterDescriptor && ((ParameterDescriptor)o).getName().equals(getName())) return true;
    	return false;
    }
}