package org.apache.slide.projector.descriptor;

import org.apache.slide.projector.i18n.LocalizedMessage;

public class Descriptor {
    protected String name;
    protected LocalizedMessage description;

    public Descriptor(String name, LocalizedMessage description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public LocalizedMessage getDescription() {
        return description;
    }
}
