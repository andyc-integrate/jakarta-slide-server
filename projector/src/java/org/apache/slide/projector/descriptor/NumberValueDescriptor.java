package org.apache.slide.projector.descriptor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.i18n.ErrorMessage;
import org.apache.slide.projector.value.NumberValue;
import org.apache.slide.projector.value.Value;

public class NumberValueDescriptor implements ValueDescriptor {
	protected boolean constrained;
    protected List allowedValues = new ArrayList();
    protected Number minimum, maximum;

    public NumberValueDescriptor() {
        this.constrained = false;
    }

    public NumberValueDescriptor(Number minimum, Number maximum) {
        constrained = true;
        this.minimum = minimum;
        this.maximum = maximum;
    }

    public NumberValueDescriptor(Number[] allowedValues) {
        this.constrained = true;
        this.allowedValues = new ArrayList(Arrays.asList(allowedValues));
    }

    public boolean isConstrained() {
        return constrained;
    }

    public void addAllowedValue(Number value) {
        allowedValues.add(value);
    }

    public Number[] getAllowedValues() {
        if ( allowedValues.isEmpty() ) return null;
        return (Number [])allowedValues.toArray(new Number[0]);
    }

    public void setMinimum(Number minimum) {
    	this.minimum = minimum;
    }
    
    public Number getMinimum() {
        return minimum;
    }

    public void setMaximum(Number maximum) {
    	this.maximum = maximum;
    }
    
    public Number getMaximum() {
        return maximum;
    }

    public Value valueOf(Object value, Context context) throws ValueCastException {
        if ( value instanceof NumberValue ) {
            return (NumberValue)value;
        } else {
        	try {
            	return new NumberValue(new BigDecimal(StringValueDescriptor.ANY.valueOf(value, null).toString()));
        	} catch ( ValueCastException exception ) {
                throw new ValueCastException(new ErrorMessage("uncastableNumberValue", new Object[] { value }), exception);
        	}
        }
    }

    public void validate(Value value, Context context) throws ValidationException {
    	if ( constrained ) {
    		Number number = ((NumberValue)value).getNumber();
    		if ( getAllowedValues() != null ) {
    			for ( Iterator i = allowedValues.iterator(); i.hasNext(); ) {
    				Number allowedNumber = (Number)i.next();
    				if ( allowedNumber.equals(number) ) {
    					return;
    				}
    			}
    			throw new ValidationException(new ErrorMessage("invalidNumber", new Object[] { value }));
    		} else {
    			if ( minimum.longValue() >  number.longValue() || maximum.longValue() < number.longValue()) {
    				throw new ValidationException(new ErrorMessage("invalidNumberRange", new Object[] { number, minimum, maximum }));
    			}
    		}
    	}
    }
}