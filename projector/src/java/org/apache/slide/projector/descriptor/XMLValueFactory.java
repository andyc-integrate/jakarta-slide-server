package org.apache.slide.projector.descriptor;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.slide.projector.value.ElementValue;
import org.apache.slide.projector.value.Value;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import de.zeigermann.xml.XMLStringWriter;
import de.zeigermann.xml.XMLWriter;

public class XMLValueFactory implements ValueFactory {
    private final static XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
    private final static Logger logger = Logger.getLogger(XMLValueFactory.class.getName());
    
	public Class getValueClass() {
		return ElementValue.class;
	}
	
	public String getElementName() {
    	return "xml";
    }
    
    public ValueDescriptor loadDescriptor(Element element) {
    	AnyValueDescriptor valueDescriptor = new AnyValueDescriptor();
    	List allowedValueElements = element.getChildren("allowed-content-type");
    	for ( Iterator i = allowedValueElements.iterator(); i.hasNext(); ) {
    		Element allowedValueElement = (Element)i.next(); 
            String allowedValue = allowedValueElement.getTextTrim(); 
            valueDescriptor.addAllowedContentType(allowedValue);
    	}
    	return valueDescriptor;
    }

    public Value load(Element element) {
    	Iterator iterator = element.getChildren().iterator();
    	if ( iterator.hasNext() ) {
    		return new ElementValue((Element)iterator.next());
    	}
		return new ElementValue(element);
	}
    
    public void save(Value value, XMLStringWriter writer) {
    	ElementValue elementValue = (ElementValue)value;
    	try {
    		xmlOutputter.output(elementValue.getRootElement(), writer);
    	} catch (IOException e) {
    		logger.log(Level.SEVERE, "Could not persist ElementValue", e);
    	}
    	writer.writeEndTag(XMLWriter.createEndTag(getElementName()));
    }
}