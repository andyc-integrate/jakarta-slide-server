package org.apache.slide.projector.descriptor;

import java.util.Locale;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.i18n.ErrorMessage;
import org.apache.slide.projector.value.LocaleValue;
import org.apache.slide.projector.value.Value;

public class LocaleValueDescriptor implements ValueDescriptor {
	public void validate(Value value, Context context) throws ValidationException {
	}
	
	public Value valueOf(Object value, Context context) throws ValueCastException {
		if ( value instanceof LocaleValue ) {
            return (LocaleValue)value;
		} 
		try {
			return new LocaleValue(new Locale(StringValueDescriptor.ANY.valueOf(value, null).toString()));
		} catch ( ValueCastException exception ) {
			throw new ValueCastException(new ErrorMessage("uncastableLocaleValue", new Object[] { value }));
		}
	}
}