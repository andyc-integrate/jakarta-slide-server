package org.apache.slide.projector.descriptor;

import java.util.Iterator;
import java.util.List;

import org.apache.slide.projector.value.StringValue;
import org.apache.slide.projector.value.Value;
import org.jdom.Element;

import de.zeigermann.xml.XMLEncode;
import de.zeigermann.xml.XMLStringWriter;
import de.zeigermann.xml.XMLWriter;
import de.zeigermann.xml.simpleImporter.ConversionHelpers;

public class StringValueFactory implements ValueFactory {
	public Class getValueClass() {
		return StringValue.class;
	}
	
	public String getElementName() {
    	return "string";
    }
    
	public Value load(Element element) {
		return new StringValue(element.getText());
	}
	
    public void save(Value value, XMLStringWriter writer) {
        writer.writeElementWithPCData(XMLWriter.createStartTag(getElementName()), XMLEncode.xmlEncodeText(value.toString()), XMLWriter.createEndTag(getElementName()));
    }
	
	public ValueDescriptor loadDescriptor(Element element) {
    	StringValueDescriptor valueDescriptor = new StringValueDescriptor();
        String mininum = element.getAttributeValue("minimum-length");
        String maximum = element.getAttributeValue("maximum-length");
        if ( mininum != null ) valueDescriptor.setMinimumLength(Integer.parseInt(mininum));
        if ( maximum != null ) valueDescriptor.setMaximumLength(Integer.parseInt(maximum));
    	List allowedStringElements = element.getChildren("allowed-value");
    	for ( Iterator i = allowedStringElements.iterator(); i.hasNext(); ) {
    		Element allowedStringElement = (Element)i.next(); 
            boolean isDefault = ConversionHelpers.getBoolean(allowedStringElement.getAttributeValue("default"), false);
            String allowedValue = allowedStringElement.getTextTrim(); 
            valueDescriptor.addAllowedValue(allowedValue);
    	}
    	return valueDescriptor;
    }
}