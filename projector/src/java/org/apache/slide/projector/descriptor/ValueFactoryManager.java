/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/projector/src/java/org/apache/slide/projector/descriptor/ValueFactoryManager.java,v 1.2 2006-01-22 22:45:14 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:45:14 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.projector.descriptor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.slide.projector.value.Value;
import org.jdom.Element;

import de.zeigermann.xml.XMLStringWriter;

/**
 * The AdapterManager
 * 
 */
public class ValueFactoryManager {
	private final static ValueFactoryManager valueDescriptorManager = new ValueFactoryManager();
	private List registeredDescriptorFactories = new ArrayList();
	
	public static ValueFactoryManager getInstance() {
		return valueDescriptorManager;
	}
	
	public void registerDescriptorFactory(ValueFactory factory) {
		registeredDescriptorFactories.add(factory);
	}
	
	public ValueDescriptor loadValueDescriptor(Element element) {
		for ( Iterator i = registeredDescriptorFactories.iterator(); i.hasNext(); ) {
			ValueFactory adapter = (ValueFactory)i.next();
	    	if ( element.getName().equals(adapter.getElementName()) ) {
				return adapter.loadDescriptor(element);  
	    	}
		}
		return null;
	}
	
	public Value loadValue(Element element) {
		for ( Iterator i = registeredDescriptorFactories.iterator(); i.hasNext(); ) {
			ValueFactory adapter = (ValueFactory)i.next();
	    	if ( element.getName().equals(adapter.getElementName()) ) {
				return adapter.load(element);  
	    	}
		}
		return null;
	}
	
	public void saveValue(Value value, XMLStringWriter xmlStringWriter) {
		for ( Iterator i = registeredDescriptorFactories.iterator(); i.hasNext(); ) {
			ValueFactory adapter = (ValueFactory)i.next();
	    	if ( value.getClass().equals(adapter.getValueClass()) ) {
				adapter.save(value, xmlStringWriter);  
	    	}
		}
	}
}