package org.apache.slide.projector.descriptor;

import java.util.Iterator;
import java.util.List;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.i18n.ErrorMessage;
import org.apache.slide.projector.value.AnyValue;
import org.apache.slide.projector.value.ArrayValue;
import org.apache.slide.projector.value.NullValue;
import org.apache.slide.projector.value.Value;

public class ArrayValueDescriptor implements ValueDescriptor {
	protected ValueDescriptor entryValueDescriptor;

    public ArrayValueDescriptor() {
    }

    public ArrayValueDescriptor(ValueDescriptor entryValueDescriptor) {
        this.entryValueDescriptor = entryValueDescriptor;
    }

    public void setEntryValueDescriptor(ValueDescriptor entryValueDescriptor) {
        this.entryValueDescriptor = entryValueDescriptor;
    }

    public ValueDescriptor getEntryValueDescriptor() {
        return entryValueDescriptor;
    }

    public Value valueOf(Object value, Context context) throws ValueCastException {
    	Value[] array = null;
    	if ( value instanceof String[] ) {
    		array = new Value[((String[])value).length];
    		for ( int i = 0; i < array.length; i++ ) {
    			array[i] = entryValueDescriptor.valueOf(((String [])value)[i], context);
    		}
    	} else if ( value instanceof List ) {
        	array = new Value[((List)value).size()];
    		int counter = 0;
    		for ( Iterator i = ((List)value).iterator(); i.hasNext(); ) {
    			Object entry = i.next();
    			if ( entry instanceof AnyValue ) {
    				try {
    					array[counter] = entryValueDescriptor.valueOf(((AnyValue)entry).load(context), context);
					} catch (Exception e) {
			    		throw new ValueCastException(new ErrorMessage("uncastableArrayValue", new Object[] { entry }));
					}
    			} else {
    				array[counter] = entryValueDescriptor.valueOf(entry, context);
    			}
    			counter++;
    		}
        } else if ( value instanceof ArrayValue ) {
        	array = new Value[((ArrayValue)value).getArray().length];
    		for ( int i = 0; i < array.length; i++ ) {
    			Object entry = ((ArrayValue)value).getArray()[i];
    			if ( entry instanceof AnyValue ) {
    				try {
    					array[i] = entryValueDescriptor.valueOf(((AnyValue)entry).load(context), context);
					} catch (Exception e) {
			    		throw new ValueCastException(new ErrorMessage("uncastableArrayValue", new Object[] { value }));
					}
    			} else {
    				array[i] = entryValueDescriptor.valueOf(entry, context);
    			}
    		}
    	} else {
    		throw new ValueCastException(new ErrorMessage("uncastableArrayValue", new Object[] { value }));
    	}
		return new ArrayValue(array);
    }
    
    public void validate(Value value, Context context) throws ValidationException {
    	try {
    		Value[] array = ((ArrayValue)value).getArray();
    		for ( int i = 0; i < array.length; i++ ) {
    			if ( !(array[i] instanceof NullValue) ) {
    				entryValueDescriptor.validate(array[i], context);
    			}
    		}
    	} catch ( ValidationException exception ) {
    		throw new ValidationException(new ErrorMessage("invalidNestedValue", new Object[] { value }), exception);
    	}
    }
}