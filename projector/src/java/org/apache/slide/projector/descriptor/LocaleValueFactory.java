package org.apache.slide.projector.descriptor;

import java.util.Locale;

import org.apache.slide.projector.value.LocaleValue;
import org.apache.slide.projector.value.Value;
import org.jdom.Element;

import de.zeigermann.xml.XMLEncode;
import de.zeigermann.xml.XMLStringWriter;
import de.zeigermann.xml.XMLWriter;

public class LocaleValueFactory implements ValueFactory {
	public Class getValueClass() {
		return LocaleValue.class;
	}
	
	public String getElementName() {
    	return "locale";
    }

	public Value load(Element element) {
		return new LocaleValue(new Locale(element.getText()));
	}
    
	public void save(Value value, XMLStringWriter writer) {
		writer.writeElementWithPCData(XMLWriter.createStartTag(getElementName()), XMLEncode.xmlEncodeText(((LocaleValue)value).getLocale().toString()), XMLWriter.createEndTag(getElementName()));
	}
    
    public ValueDescriptor loadDescriptor(Element element) {
    	return new LocaleValueDescriptor();
    }
}