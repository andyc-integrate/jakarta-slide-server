package org.apache.slide.projector.descriptor;

import org.apache.slide.projector.value.Value;
import org.jdom.Element;

import de.zeigermann.xml.XMLStringWriter;

public interface ValueFactory {
	public String getElementName();

	public Class getValueClass();
	
	public Value load(Element element);
	
    public void save(Value value, XMLStringWriter writer);
	
	public ValueDescriptor loadDescriptor(Element element);
}