package org.apache.slide.projector.descriptor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.i18n.ErrorMessage;
import org.apache.slide.projector.value.PrintableValue;
import org.apache.slide.projector.value.StringValue;
import org.apache.slide.projector.value.Value;

public class StringValueDescriptor implements ValueDescriptor {
    public final static String UNSET = "[unset]";

    public final static StringValueDescriptor ANY = new StringValueDescriptor();
    public final static StringValueDescriptor EMPTY = new StringValueDescriptor(0);
    public final static StringValueDescriptor NOT_EMPTY = new StringValueDescriptor(1,Integer.MAX_VALUE);
    
    protected boolean constrained;
    protected List allowedValues = new ArrayList();
    protected int minimumLength = -1, maximumLength =-1;
    protected Value defaultValue;

    public StringValueDescriptor() {
        this.constrained = false;
    }

    public StringValueDescriptor(String[] allowedValues) {
        this.constrained = true;
        this.allowedValues = new ArrayList(Arrays.asList(allowedValues));
    }

    public StringValueDescriptor(int maximumLength) {
        this.maximumLength = maximumLength;
        this.constrained = true;
    }

    public StringValueDescriptor(int minimumLength, int maximumLength) {
        this.minimumLength = minimumLength;
        this.maximumLength = maximumLength;
        this.constrained = true;
    }

    public int getMinimumLength() {
        return minimumLength;
    }

    public void setMinimumLength(int minimumLength) {
        this.minimumLength = minimumLength;
        constrained = true;
    }

    public int getMaximumLength() {
        return maximumLength;
    }

    public void setMaximumLength(int maximumLength) {
        this.maximumLength = maximumLength;
        constrained = true;
    }

    public void addAllowedValue(String value) {
        constrained = true;
        allowedValues.add(value);
    }

    public boolean isConstrained() {
        return constrained;
    }

    public boolean isEnumrable() {
        return ( allowedValues.size() > 0 );
    }

    public String[] getAllowedValues() {
        return (String [])allowedValues.toArray(new String [0]);
    }

    public Value valueOf(Object value, Context context) throws ValueCastException {
        if (value instanceof StringValue) {
            return (StringValue)value;
        } else if (value instanceof String) {
            return new StringValue((String)value);
        } else if (value instanceof String[]) {
            return new StringValue(((String [])value)[0]);
        } else if (value instanceof PrintableValue) {
            return new StringValue(((PrintableValue)value).print(new StringBuffer(256)).toString());
        }
        throw new ValueCastException(new ErrorMessage("uncastableStringValue", new Object[] { value }));
    }

    public void validate(Value value, Context context) throws ValidationException {
    	String valueAsString = value.toString();
    	if ( constrained ) {
    		if ( isEnumrable() ) {
    			for ( Iterator i = allowedValues.iterator(); i.hasNext(); ) {
    				if (valueAsString.equals((String)i.next())) {
    					return;
    				}
    			}
    			throw new ValidationException(new ErrorMessage("invalidStringValue", new String[] { valueAsString }));
    		} else {
    			if ( minimumLength != -1 && valueAsString.length() < minimumLength ) throw new ValidationException(new ErrorMessage("stringTooShort", new Object[] { new Integer(minimumLength) }));
    			if ( maximumLength != -1 && valueAsString.length() > maximumLength ) throw new ValidationException(new ErrorMessage("stringTooLong", new Object[] { new Integer(maximumLength) }));
    		}
    	}
    }
}