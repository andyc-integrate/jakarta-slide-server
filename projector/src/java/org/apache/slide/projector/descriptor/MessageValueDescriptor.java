package org.apache.slide.projector.descriptor;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.i18n.ErrorMessage;
import org.apache.slide.projector.value.MessageValue;
import org.apache.slide.projector.value.Value;

public class MessageValueDescriptor implements ValueDescriptor {
	public Value valueOf(Object value, Context context) throws ValueCastException {
        if ( value instanceof MessageValue ) {
            return (MessageValue)value;
        } else {
        	try {
        		return new MessageValue((StringValueDescriptor.ANY.valueOf(value, null).toString()));
        	} catch ( ValueCastException exception ) {
                throw new ValueCastException(new ErrorMessage("uncastableMessageValue", new Object[] { value }));
        	}
        }
    }

	public void validate(Value value, Context context) throws ValidationException {
    }
}