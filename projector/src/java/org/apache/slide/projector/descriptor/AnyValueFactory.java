package org.apache.slide.projector.descriptor;

import java.util.Iterator;
import java.util.List;

import org.apache.slide.projector.Store;
import org.apache.slide.projector.URI;
import org.apache.slide.projector.util.StoreHelper;
import org.apache.slide.projector.value.AnyValue;
import org.apache.slide.projector.value.URIValue;
import org.apache.slide.projector.value.Value;
import org.jdom.Element;

import de.zeigermann.xml.XMLStringWriter;
import de.zeigermann.xml.XMLWriter;

public class AnyValueFactory implements ValueFactory {
	private final static String PROCESSOR = "processor";
	private final static String RESULT = "result";
	private final static String STORE = "store";
	private final static String KEY = "key";
	private final static String DOMAIN = "domain";
	
	public Class getValueClass() {
		return AnyValue.class;
	}
	
	public String getElementName() {
    	return "value";
    }

	public Value load(Element element) {
        String processor = element.getAttributeValue("processor");
        String result = element.getAttributeValue("result");
        String store = element.getAttributeValue("store");
		int storeId = StoreHelper.getStoreByName(store);
        String key = element.getAttributeValue("key");
        String domain = element.getAttributeValue("domain");
        URI processorUri = ( processor == null ? null : new URIValue(processor) );
        Iterator children = element.getChildren().iterator();
    	Value input = null; 
        if ( children.hasNext() ) {
        	Element childElement = (Element)children.next();
        	input = ValueFactoryManager.getInstance().loadValue(childElement);
        }
        return new AnyValue(processor, input, result, storeId, key, domain);
	}
	    
	public void save(Value value, XMLStringWriter writer) {
		AnyValue anyValue = (AnyValue)value;
		if ( anyValue.getProcessorUri() == null ) {
			writer.writeEmptyElement(XMLWriter.createEmptyTag(getElementName(), new String[] { STORE, KEY, DOMAIN }, new String[] { Store.stores[anyValue.getStoreId()], anyValue.getKey(), anyValue.getDomain() }));
		} else {
			if ( anyValue.getInput() == null ) {
				writer.writeStartTag(XMLWriter.createStartTag(getElementName(), new String[] { PROCESSOR, RESULT, STORE, KEY, DOMAIN }, new String[] { anyValue.getProcessorUri().toString(), anyValue.getResult(), Store.stores[anyValue.getStoreId()], anyValue.getKey(), anyValue.getDomain() }));
			} else {
				writer.writeStartTag(XMLWriter.createStartTag(getElementName(), new String[] { PROCESSOR, RESULT }, new String[] { anyValue.getProcessorUri().toString(), anyValue.getResult() }));
				ValueFactoryManager.getInstance().saveValue(anyValue.getInput(), writer);
				writer.writeEndTag(XMLWriter.createEndTag(getElementName()));
			}
		}
	}

	public ValueDescriptor loadDescriptor(Element element) {
    	AnyValueDescriptor valueDescriptor = new AnyValueDescriptor();
    	List allowedValueElements = element.getChildren("allowed-content-type");
    	for ( Iterator i = allowedValueElements.iterator(); i.hasNext(); ) {
    		Element allowedValueElement = (Element)i.next(); 
            String allowedValue = allowedValueElement.getTextTrim(); 
            valueDescriptor.addAllowedContentType(allowedValue);
    	}
    	return valueDescriptor;
    }
}