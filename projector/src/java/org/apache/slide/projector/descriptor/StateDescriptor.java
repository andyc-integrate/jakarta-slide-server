package org.apache.slide.projector.descriptor;

import org.apache.slide.projector.i18n.DefaultMessage;
import org.apache.slide.projector.i18n.LocalizedMessage;

/**
 * @version $Revision: 1.2 $
 */

public class StateDescriptor extends Descriptor {
	public final static String OK = "ok";
	public final static StateDescriptor OK_DESCRIPTOR = new StateDescriptor(OK,
			new DefaultMessage("state/ok"));

	public StateDescriptor(String state, LocalizedMessage description) {
		super(state, description);
	}

	public String getState() {
		return getName();
	}
}