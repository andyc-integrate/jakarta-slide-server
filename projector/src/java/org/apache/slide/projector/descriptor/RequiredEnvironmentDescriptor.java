package org.apache.slide.projector.descriptor;

import org.apache.slide.projector.i18n.LocalizedMessage;
import org.apache.slide.projector.value.Value;

public class RequiredEnvironmentDescriptor extends ParameterDescriptor {
    protected int store;

    public RequiredEnvironmentDescriptor(String name, int store, LocalizedMessage description, ValueDescriptor valueDescriptor) {
        super(name, description, valueDescriptor);
        this.store = store;
    }

    public RequiredEnvironmentDescriptor(String name, int store, LocalizedMessage description, ValueDescriptor valueDescriptor, Value defaultValue) {
        super(name, description, valueDescriptor, defaultValue);
        this.store = store;
    }

    public int getStore() {
        return store;
    }
}