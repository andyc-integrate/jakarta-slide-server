package org.apache.slide.projector.descriptor;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.i18n.ErrorMessage;
import org.apache.slide.projector.value.BooleanValue;
import org.apache.slide.projector.value.Value;

public class BooleanValueDescriptor implements ValueDescriptor {
	public final static String TRUE = Boolean.toString(true);
    public final static String FALSE = Boolean.toString(false);

    public Value valueOf(Object value, Context context) throws ValueCastException {
        if ( value instanceof BooleanValue ) {
            return (BooleanValue)value;
        } 
        try {
            return getBooleanResource(StringValueDescriptor.ANY.valueOf(value, null).toString());
        } catch ( ValueCastException exception ) {
            throw new ValueCastException(new ErrorMessage("uncastableBooleanValue", new Object[] { value }));
        }
    }
    
    public void validate(Value value, Context context) throws ValidationException {
    }

    public static BooleanValue getBooleanResource(String value) {
        if ( value != null && value.equals(TRUE) ) {
            return BooleanValue.TRUE;
        } else {
            return BooleanValue.FALSE;
        }
    }
}