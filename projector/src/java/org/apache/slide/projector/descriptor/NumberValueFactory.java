package org.apache.slide.projector.descriptor;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import org.apache.slide.projector.value.NumberValue;
import org.apache.slide.projector.value.Value;
import org.jdom.Element;

import de.zeigermann.xml.XMLEncode;
import de.zeigermann.xml.XMLStringWriter;
import de.zeigermann.xml.XMLWriter;
import de.zeigermann.xml.simpleImporter.ConversionHelpers;

public class NumberValueFactory implements ValueFactory {
	public Class getValueClass() {
		return NumberValue.class;
	}
	
	public String getElementName() {
    	return "number";
    }

    public Value load(Element element) {
		return new NumberValue(new BigDecimal(element.getText()));
	}

    public void save(Value value, XMLStringWriter writer) {
        writer.writeElementWithPCData(XMLWriter.createStartTag(getElementName()), XMLEncode.xmlEncodeText(String.valueOf(((NumberValue)value).getNumber())), XMLWriter.createEndTag(getElementName()));
    }
    
    public ValueDescriptor loadDescriptor(Element element) {
    	NumberValueDescriptor valueDescriptor = new NumberValueDescriptor();
        String mininum = element.getAttributeValue("minimum");
        String maximum = element.getAttributeValue("maximum");
        if ( mininum != null ) valueDescriptor.setMinimum(new BigDecimal(mininum));
        if ( maximum != null ) valueDescriptor.setMaximum(new BigDecimal(maximum));
    	List allowedValueElements = element.getChildren("allowed-value");
    	for ( Iterator i = allowedValueElements.iterator(); i.hasNext(); ) {
    		Element allowedValueElement = (Element)i.next(); 
            boolean isDefault = ConversionHelpers.getBoolean(allowedValueElement.getAttributeValue("default"), false);
            Number allowedValue = new BigDecimal(allowedValueElement.getTextTrim()); 
            valueDescriptor.addAllowedValue(allowedValue);
    	}
    	return valueDescriptor;
    }
}