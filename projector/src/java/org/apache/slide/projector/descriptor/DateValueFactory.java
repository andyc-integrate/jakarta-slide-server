package org.apache.slide.projector.descriptor;

import java.util.Date;

import org.apache.slide.projector.value.DateValue;
import org.apache.slide.projector.value.Value;
import org.jdom.Element;

import de.zeigermann.xml.XMLEncode;
import de.zeigermann.xml.XMLStringWriter;
import de.zeigermann.xml.XMLWriter;

public class DateValueFactory implements ValueFactory {
	public Class getValueClass() {
		return DateValue.class;
	}
	
	public String getElementName() {
    	return "date";
    }

	public Value load(Element element) {
		return new DateValue(new Date(Long.valueOf(element.getText()).longValue()));
	}

    public void save(Value value, XMLStringWriter writer) {
    	DateValue dateValue = (DateValue)value;
        writer.writeElementWithPCData(XMLWriter.createStartTag(getElementName()), XMLEncode.xmlEncodeText(dateValue.toString()), XMLWriter.createEndTag(getElementName()));
    }

    public ValueDescriptor loadDescriptor(Element element) {
    	DateValueDescriptor valueDescriptor = new DateValueDescriptor();
        String earliest = element.getAttributeValue("earliest");
        String latest = element.getAttributeValue("latest");
        if ( earliest != null ) valueDescriptor.setEarliest(new Date(Long.valueOf(earliest).longValue()));
        if ( latest != null ) valueDescriptor.setLatest(new Date(Long.valueOf(latest).longValue()));
    	return valueDescriptor;
    }
}