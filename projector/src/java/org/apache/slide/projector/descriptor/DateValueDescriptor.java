package org.apache.slide.projector.descriptor;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.i18n.ErrorMessage;
import org.apache.slide.projector.value.DateValue;
import org.apache.slide.projector.value.Value;

public class DateValueDescriptor implements ValueDescriptor {
	protected boolean constrained;
    protected List allowedValues = new ArrayList();
    protected Date earliest, latest;

    public DateValueDescriptor() {
        this.constrained = false;
    }

    public DateValueDescriptor(Date earliest, Date latest) {
        constrained = true;
        this.earliest = earliest;
        this.latest = latest;
    }

    public boolean isConstrained() {
        return constrained;
    }

    public void setEarliest(Date earliest) {
        this.earliest = earliest;
    }

    public void setLatest(Date latest) {
        this.latest = latest;
    }

    public Value valueOf(Object value, Context context) throws ValueCastException {
    	Date date = null;
    	if ( value instanceof DateValue ) {
            return (DateValue)value;
        } else {
        	try {
        		String valueAsString = StringValueDescriptor.ANY.valueOf(value, null).toString();
        		try {
        			date = new Date(Long.valueOf(valueAsString).longValue());
        		} catch ( NumberFormatException exception ) {
        			try {
        				date = DateFormat.getInstance().parse(valueAsString);
        			} catch (ParseException e) {
        				try {
        					date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").parse(valueAsString);
        				} catch (ParseException pe) {
        					throw new ValueCastException(new ErrorMessage("uncastableDateValue", new Object[] { value }), pe);
        				}
        			}
        		}
        	} catch ( ValueCastException exception ) {
        		throw new ValueCastException(new ErrorMessage("uncastableDateValue", new Object[] { value }), exception);
        	}
        }
        return new DateValue(date);
    }

    public void validate(Value value, Context context) throws ValidationException {
        Date date = ((DateValue)value).getDate();
        if ( constrained && ( date.after(latest) || date.before(earliest) ) ) {
        	throw new ValidationException(new ErrorMessage("invalidDate", new Object[] { date, earliest, latest }));
        }
    }
}