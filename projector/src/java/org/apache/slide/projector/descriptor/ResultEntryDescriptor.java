package org.apache.slide.projector.descriptor;

import org.apache.slide.projector.i18n.LocalizedMessage;

public class ResultEntryDescriptor extends Descriptor {
    protected String contentType;
    protected boolean presentable;

    public ResultEntryDescriptor(String name, LocalizedMessage description, String contentType, boolean presentable) {
        super(name, description);
        this.contentType = contentType;
        this.presentable = presentable;
    }

    public boolean isPresentable() {
        return presentable;
    }

    public String getContentType() {
        return contentType;
    }
}