package org.apache.slide.projector.descriptor;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.slide.projector.i18n.ParameterMessage;
import org.apache.slide.projector.value.MapValue;
import org.apache.slide.projector.value.Value;
import org.jdom.Element;

import de.zeigermann.xml.XMLStringWriter;
import de.zeigermann.xml.XMLWriter;

public class MapValueFactory implements ValueFactory {
	public Class getValueClass() {
		return MapValue.class;
	}

	public String getElementName() {
    	return "map";
    }

	public Value load(Element element) {
		Map map = new HashMap();
		for ( Iterator i = element.getChildren().iterator(); i.hasNext(); ) {
			Element childElement = (Element)i.next();
			String key = childElement.getAttributeValue("key");
			Element entryElement = (Element)childElement.getChildren().iterator().next();
			map.put(key, ValueFactoryManager.getInstance().loadValue(entryElement));			
		}
		return new MapValue(map);
	}

    public void save(Value value, XMLStringWriter writer) {
    	Map map = ((MapValue)value).getMap();
        writer.writeStartTag(XMLWriter.createStartTag(getElementName()));
        for ( Iterator i = map.entrySet().iterator(); i.hasNext(); ) {
            Map.Entry entry = (Map.Entry)i.next();
            if ( entry.getValue() instanceof Value ) {
            	writer.writeStartTag(XMLWriter.createStartTag("entry", "key", (String)entry.getKey()));
            	ValueFactoryManager.getInstance().saveValue((Value)entry.getValue(), writer);
            	writer.writeEndTag(XMLWriter.createEndTag("entry"));
            }
        }
        writer.writeEndTag(XMLWriter.createEndTag(getElementName()));
    }

    public ValueDescriptor loadDescriptor(Element element) {
		MapValueDescriptor valueDescriptor = new MapValueDescriptor();
		for ( Iterator i = element.getChildren().iterator(); i.hasNext(); ) {
			Element entry = (Element)i.next();
			String key = entry.getAttributeValue("key");
			String description = entry.getAttributeValue("description");
    		Element allowedEntryValueElement = (Element)element.getChildren().iterator().next();
   			ValueDescriptor allowedEntryValue = ValueFactoryManager.getInstance().loadValueDescriptor(allowedEntryValueElement);
   			valueDescriptor.addEntryDescriptor(new ParameterDescriptor(key, new ParameterMessage(description), allowedEntryValue));
		}
		return valueDescriptor;
    }
}