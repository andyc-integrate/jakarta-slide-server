package org.apache.slide.projector.descriptor;

import org.apache.slide.projector.value.BooleanValue;
import org.apache.slide.projector.value.Value;
import org.jdom.Element;

import de.zeigermann.xml.XMLEncode;
import de.zeigermann.xml.XMLStringWriter;
import de.zeigermann.xml.XMLWriter;

public class BooleanValueFactory implements ValueFactory {
	public Class getValueClass() {
		return BooleanValue.class;
	}
	
	public String getElementName() {
    	return "boolean";
    }
    
	public Value load(Element element) {
		return new BooleanValue(Boolean.valueOf(element.getText()).booleanValue());
	}
    
    public void save(Value value, XMLStringWriter writer) {
        writer.writeElementWithPCData(XMLWriter.createStartTag(getElementName()), XMLEncode.xmlEncodeText(String.valueOf(((BooleanValue)value).booleanValue())), XMLWriter.createEndTag(getElementName()));
    }

    public ValueDescriptor loadDescriptor(Element element) {
    	return new BooleanValueDescriptor();
    }
}