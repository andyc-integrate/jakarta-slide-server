package org.apache.slide.projector.descriptor;

import org.apache.slide.projector.value.URIValue;
import org.apache.slide.projector.value.Value;
import org.jdom.Element;

import de.zeigermann.xml.XMLEncode;
import de.zeigermann.xml.XMLStringWriter;
import de.zeigermann.xml.XMLWriter;

public class URIValueFactory implements ValueFactory {
	public Class getValueClass() {
		return URIValue.class;
	}
	
	public String getElementName() {
    	return "uri";
    }

	public Value load(Element element) {
		return new URIValue(element.getText());
	}

    public void save(Value value, XMLStringWriter writer) {
        writer.writeElementWithPCData(XMLWriter.createStartTag(getElementName()), XMLEncode.xmlEncodeText(((URIValue)value).toString()), XMLWriter.createEndTag(getElementName()));
    }
    
    public ValueDescriptor loadDescriptor(Element element) {
    	return new URIValueDescriptor();
    }
}