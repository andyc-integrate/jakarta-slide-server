package org.apache.slide.projector.descriptor;

import org.apache.slide.projector.Context;
import org.apache.slide.projector.i18n.ErrorMessage;
import org.apache.slide.projector.value.URIValue;
import org.apache.slide.projector.value.Value;

public class URIValueDescriptor implements ValueDescriptor {
	public Value valueOf(Object value, Context context) throws ValueCastException {
        if ( value instanceof URIValue ) {
            return (URIValue)value;
        } else {
        	try {
                return new URIValue(StringValueDescriptor.ANY.valueOf(value, null).toString());
            } catch ( ValueCastException exception ) {
            	throw new ValueCastException(new ErrorMessage("uncastableUriValue", new Object[] { value }), exception);
            }
        }
	}
	
	public void validate(Value value, Context context) throws ValidationException {
    }    
}