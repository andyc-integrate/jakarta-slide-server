package org.apache.slide.projector.descriptor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.slide.projector.ContentType;
import org.apache.slide.projector.Context;
import org.apache.slide.projector.i18n.ErrorMessage;
import org.apache.slide.projector.value.ArrayValue;
import org.apache.slide.projector.value.ObjectValue;
import org.apache.slide.projector.value.StringValue;
import org.apache.slide.projector.value.Value;

public class AnyValueDescriptor implements ValueDescriptor {
	protected List allowedContentTypes = new ArrayList();
    private boolean constrained = false;

    public AnyValueDescriptor() {
    }

    public AnyValueDescriptor(String allowedContentType) {
        allowedContentTypes.add(allowedContentType);
        constrained = true;
    }

    public void addAllowedContentType(String allowedContentType) {
        allowedContentTypes.add(allowedContentType);
        constrained = true;
    }

    public String[] getAllowedContentTypes() {
        return (String [])allowedContentTypes.toArray(new String[0]);
    }

    public Value valueOf(Object value, Context context) throws ValueCastException {
    	if ( value instanceof Value ) {
            return (Value)value;
        } else if ( value instanceof String) {
            return new StringValue((String)value);
        } else if ( value instanceof String[] ) {
            // convert to string resource
            Value []elements = new Value[((String[])value).length];
            for ( int i = 0; i < ((String[])value).length; i++ ) {
                elements[i] = new StringValue(((String [])value)[i]);                                
            }
            return new ArrayValue(elements);
        } else {
        	return new ObjectValue(value); 
        }
    }
    
    public void validate(Value value, Context context) throws ValidationException {
         if ( constrained ) {
            for ( Iterator i = allowedContentTypes.iterator(); i.hasNext(); ) {
                if ( ContentType.matches((String)i.next(), ((Value)value).getContentType()) ) {
                    return;
                }
            }
            throw new ValidationException(new ErrorMessage("invalidValue", new String[] { ((Value)value).getContentType(), ContentType.getContentTypesAsString(allowedContentTypes) }));
        }
    }
}