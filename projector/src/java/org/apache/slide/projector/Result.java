package org.apache.slide.projector;

import java.util.HashMap;
import java.util.Map;

import org.apache.slide.projector.descriptor.StateDescriptor;
import org.apache.slide.projector.value.Value;

public class Result {
    public final static Result OK = new Result(StateDescriptor.OK);

    protected String state;
    protected Map resultEntries;

    public Result(String state) {
        this.state = state;
        resultEntries = new HashMap();
    }

    public Result(String state, String key, Value resource) {
        this(state);
        addResultEntry(key, resource);
    }

    public Result(String state, Map resultEntries) {
        this.state = state;
        this.resultEntries = resultEntries;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public Map getResultEntries() {
        return resultEntries;
    }

    public void addResultEntry(String key, Value resource) {
        resultEntries.put(key, resource);
    }
}