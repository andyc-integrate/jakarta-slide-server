package org.apache.slide.projector;

import java.util.ArrayList;
import java.util.List;

/**
 * @version $Revision: 1.2 $
 */

public abstract class AbstractContext implements Context {
    private List informations = new ArrayList();
    private int informationNumber = 0;
    private URI process;
    private String step;

    public void addInformation(Information info) {
        if ( !informations.contains(info) ) {
            informationNumber++;
            info.setNumber(informationNumber);
            informations.add(info);
        }
    }

    public List getInformations() {
        return informations;
    }

    public void setProcess(URI process) {
    	this.process = process;
    }

    public URI getProcess() {
    	return process;
    }

    public void setStep(String step) {
    	this.step = step;
    }
    
    public String getStep() {
    	return step;
    }
}