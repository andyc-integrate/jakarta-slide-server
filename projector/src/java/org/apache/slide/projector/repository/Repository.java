package org.apache.slide.projector.repository;

import java.io.IOException;
import java.util.Map;

import org.apache.commons.httpclient.Credentials;
import org.apache.slide.projector.URI;
import org.apache.slide.projector.value.ArrayValue;
import org.apache.slide.projector.value.StreamableValue;
import org.apache.slide.projector.value.Value;
import org.apache.webdav.lib.Subscriber;

/**
 * The Connector interface
 * 
 */
public interface Repository {
	public Credentials login(String user, String password) throws IOException;
		
	public URI createUser(String user, String password, Credentials credentials) throws UserExistsException, IOException;
	
	public void deleteUser(URI user, Credentials credentials) throws IOException;
	
	public URI createRole(String role, Credentials credentials) throws RoleExistsException, IOException;
	
	public void deleteRole(URI role, Credentials credentials) throws IOException;
	
    public ArrayValue listRoles(URI user, Credentials credentials) throws IOException;

    public void addRole(URI user, URI role, Credentials credentials) throws UserExistsException, IOException;
	
	public void removeRole(URI user, URI role, Credentials credentials) throws UserExistsException, IOException;
	
	public void changePassword(URI uri, String oldPassword, String newPassword, Credentials credentials) throws IOException;
	
	public Value getResource(URI uri, Credentials credentials) throws IOException;

    public ArrayValue getProperties(URI uri, Credentials credentials) throws IOException;

    public void setResource(URI uri, StreamableValue resource, Credentials credentials) throws IOException;

    public void removeResource(URI uri, Credentials credentials) throws IOException;

    public ArrayValue getChildren(URI uri, Credentials credentials) throws IOException;
    
    public Value[] search(String query, Credentials credentials) throws IOException;

    public void subscribe(String method, URI uri, int depth, Subscriber listener, Credentials credentials);

    public void subscribe(String method, URI uri, int depth, int lifetime, int notificationDelay, Subscriber listener, Credentials credentials);

    public void unsubscribe(URI uri, Subscriber listener, Credentials credentials);

    public void fireEvent(Map information, Credentials credentials) throws IOException;

    public void fireVetoableEvent(Map information, Credentials credentials) throws IOException;
}
