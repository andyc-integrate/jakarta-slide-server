/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/jaas/org/apache/slide/jaas/spi/SlideGroup.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.jaas.spi;

import java.security.Principal;
import java.security.acl.Group;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;


public final class SlideGroup implements Group {
    
    private final HashSet m_members = new HashSet();
    
    public boolean addMember(Principal user) {
        return m_members.add(user);
    }
    
    public boolean isMember(Principal member) {
        return m_members.contains(member);
    }
    
    public Enumeration members() {
        class MembersEnumeration implements Enumeration {
            private Iterator m_iter;
            public MembersEnumeration(Iterator iter) {
                m_iter = iter;
            }
            public boolean hasMoreElements () {
                return m_iter.hasNext();
            }
            public Object nextElement () {
                return m_iter.next();
            }
        }

        return new MembersEnumeration(m_members.iterator());
    }

    public boolean removeMember(Principal user) {
        return m_members.remove(user);
    }
    
    public String getName() {
        return "roles";
    }

}
