/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/stores/org/apache/slide/store/impl/rdbms/RDBMSAdapter.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2003 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.store.impl.rdbms;

import java.sql.Connection;
import java.util.Enumeration;
import java.util.Hashtable;

import org.apache.slide.common.ServiceAccessException;
import org.apache.slide.common.ServiceParameterErrorException;
import org.apache.slide.common.ServiceParameterMissingException;
import org.apache.slide.common.Uri;
import org.apache.slide.content.NodeRevisionContent;
import org.apache.slide.content.NodeRevisionDescriptor;
import org.apache.slide.content.NodeRevisionDescriptors;
import org.apache.slide.content.NodeRevisionNumber;
import org.apache.slide.content.RevisionAlreadyExistException;
import org.apache.slide.content.RevisionDescriptorNotFoundException;
import org.apache.slide.content.RevisionNotFoundException;
import org.apache.slide.lock.LockTokenNotFoundException;
import org.apache.slide.lock.NodeLock;
import org.apache.slide.security.NodePermission;
import org.apache.slide.structure.ObjectAlreadyExistsException;
import org.apache.slide.structure.ObjectNode;
import org.apache.slide.structure.ObjectNotFoundException;
import org.apache.slide.util.logger.Logger;

/**
 * 
 * 
 * @version $Revision: 1.2 $
 */
public interface RDBMSAdapter {
    
    
    // ---------------------------------------------------------------- Methods
    
    public void setParameters(Hashtable parameters)
            throws ServiceParameterErrorException, 
            ServiceParameterMissingException;
    
    public Logger getLogger();

    /**
     * 
     */
    public ObjectNode retrieveObject(Connection conn, Uri uri)
        throws ServiceAccessException, ObjectNotFoundException;
    
    
    /**
     * 
     */
    public void storeObject(Connection conn, Uri uri, ObjectNode object)
        throws ServiceAccessException, ObjectNotFoundException;
    
    
    /**
     * 
     */
    public void createObject(Connection conn, Uri uri, ObjectNode object)
        throws ServiceAccessException, ObjectAlreadyExistsException;
    
    
    /**
     * 
     */
    public void removeObject(Connection conn, Uri uri, ObjectNode object)
        throws ServiceAccessException, ObjectNotFoundException;
    
    
    /**
     * 
     */
    public void grantPermission
        (Connection conn, Uri uri, NodePermission permission)
        throws ServiceAccessException;
    
    
    /**
     * 
     */
    public void revokePermission
        (Connection conn, Uri uri, NodePermission permission)
        throws ServiceAccessException;
    
    
    /**
     * 
     */
    public void revokePermissions(Connection conn, Uri uri)
        throws ServiceAccessException;
    
    
    /**
     * 
     */
    public Enumeration enumeratePermissions(Connection conn, Uri uri)
        throws ServiceAccessException;
    
    
    /**
     * 
     */
    public void putLock(Connection conn, Uri uri, NodeLock lock)
        throws ServiceAccessException;
    
    
    /**
     * 
     */
    public void renewLock(Connection conn, Uri uri, NodeLock lock)
        throws ServiceAccessException, LockTokenNotFoundException;
    
    
    /**
     * 
     */
    public void removeLock(Connection conn, Uri uri, NodeLock lock)
        throws ServiceAccessException, LockTokenNotFoundException;
    
    
    /**
     * 
     */
    public void killLock(Connection conn, Uri uri, NodeLock lock)
        throws ServiceAccessException, LockTokenNotFoundException;
    
    
    /**
     * 
     */
    public Enumeration enumerateLocks(Connection conn, Uri uri)
        throws ServiceAccessException;
    
    
    /**
     * 
     */
    public NodeRevisionDescriptors retrieveRevisionDescriptors
        (Connection conn, Uri uri)
        throws ServiceAccessException, RevisionDescriptorNotFoundException;
    
    
    /**
     * 
     */
    public void createRevisionDescriptors
        (Connection conn, Uri uri, NodeRevisionDescriptors revisionDescriptors)
        throws ServiceAccessException;
    
    
    /**
     * 
     */
    public void storeRevisionDescriptors
        (Connection conn, Uri uri, NodeRevisionDescriptors revisionDescriptors)
        throws ServiceAccessException, RevisionDescriptorNotFoundException;
    
    
    /**
     * 
     */
    public void removeRevisionDescriptors(Connection conn, Uri uri)
        throws ServiceAccessException;
    
    
    /**
     * 
     */
    public NodeRevisionDescriptor retrieveRevisionDescriptor
        (Connection conn, Uri uri, NodeRevisionNumber nrn)
        throws ServiceAccessException, RevisionDescriptorNotFoundException;
    
    
    /**
     * 
     */
    public void createRevisionDescriptor
        (Connection conn, Uri uri, NodeRevisionDescriptor revisionDescriptor)
        throws ServiceAccessException;
    
    
    /**
     * 
     */
    public void storeRevisionDescriptor
        (Connection conn, Uri uri, NodeRevisionDescriptor revisionDescriptor)
        throws ServiceAccessException, RevisionDescriptorNotFoundException;
    
    
    /**
     * 
     */
    public void removeRevisionDescriptor
        (Connection conn, Uri uri, NodeRevisionNumber nrn)
        throws ServiceAccessException;
    
    
    /**
     * 
     */
    public NodeRevisionContent retrieveRevisionContent
        (Connection conn, Uri uri, NodeRevisionDescriptor revisionDescriptor, boolean temporaryConnection)
        throws ServiceAccessException, RevisionNotFoundException;
    
    
    /**
     * 
     */
    public void createRevisionContent
        (Connection conn, Uri uri, NodeRevisionDescriptor revisionDescriptor,
         NodeRevisionContent revisionContent)
        throws ServiceAccessException, RevisionAlreadyExistException;
    
    
    /**
     * 
     */
    public void storeRevisionContent
        (Connection conn, Uri uri, NodeRevisionDescriptor revisionDescriptor,
         NodeRevisionContent revisionContent)
        throws ServiceAccessException, RevisionNotFoundException;
    
    
    /**
     * 
     */
    public void removeRevisionContent
        (Connection conn, Uri uri, NodeRevisionDescriptor revisionDescriptor)
        throws ServiceAccessException;
    
    
}

