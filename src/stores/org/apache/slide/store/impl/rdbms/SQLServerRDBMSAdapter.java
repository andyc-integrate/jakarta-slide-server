/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/stores/org/apache/slide/store/impl/rdbms/SQLServerRDBMSAdapter.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2003 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.store.impl.rdbms;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.slide.common.Service;
import org.apache.slide.common.ServiceAccessException;
import org.apache.slide.macro.ConflictException;
import org.apache.slide.util.logger.Logger;

/**
 * Adapter for MS SQLServer. Actually does not change a thing from StandardRDBMSAdapter.
 * 
 * @version $Revision: 1.2 $
 */

public class SQLServerRDBMSAdapter extends StandardRDBMSAdapter implements SequenceAdapter {

    protected static final String LOG_CHANNEL = SQLServerRDBMSAdapter.class.getName();

    protected static String normalizeSequenceName(String sequenceName) {
        return sequenceName.replace('-', '_').toUpperCase() + "_SEQ";
    }

    public SQLServerRDBMSAdapter(Service service, Logger logger) {
        super(service, logger);
    }

    protected ServiceAccessException createException(SQLException e, String uri) {

        switch (e.getErrorCode()) {
            case 1205 : // thread was deadlock victim
                getLogger().log(e.getErrorCode() + ": Deadlock resolved on " + uri, LOG_CHANNEL, Logger.WARNING);
                return new ServiceAccessException(service, new ConflictException(uri));

            case 547 : // referential integraty constaint was violated (like in storeObject on table URI )
            case 2627 : // primary key constraint violation (like in storeContent on table VERSION_CONTENT)
                getLogger().log(e.getErrorCode() + ": Low isolation conflict for " + uri, LOG_CHANNEL, Logger.WARNING);
                return new ServiceAccessException(service, new ConflictException(uri));

            default :
                getLogger().log(
                    "SQL error " + e.getErrorCode() + " on " + uri + ": " + e.getMessage(),
                    LOG_CHANNEL,
                    Logger.ERROR);

                return new ServiceAccessException(service, e);
        }

    }

    public boolean isSequenceSupported(Connection conn) {
        return true;
    }

    public boolean createSequence(Connection conn, String sequenceName) throws ServiceAccessException {

        // XXX DUMMY is a keyword in Sybase, so spell it wrong
        String query =
            "CREATE TABLE dbo."
                + normalizeSequenceName(sequenceName)
                + " (ID id_type IDENTITY UNIQUE NOT NULL, DUMY bit NOT NULL)";

        PreparedStatement statement = null;

        try {
            statement = conn.prepareStatement(query);
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new ServiceAccessException(service, e);
        } finally {
            close(statement);
        }

    }

    public long nextSequenceValue(Connection conn, String sequenceName) throws ServiceAccessException {
        String query = "INSERT INTO dbo." + normalizeSequenceName(sequenceName) + " (DUMY) VALUES(1)";

        String selectQuery = "SELECT @@identity";

        PreparedStatement statement = null;
        PreparedStatement selectStatement = null;
        ResultSet res = null;

        try {
            statement = conn.prepareStatement(query);
            statement.executeUpdate();

            selectStatement = conn.prepareStatement(selectQuery);
            res = selectStatement.executeQuery();
            if (!res.next()) {
                throw new ServiceAccessException(service, "Could not increment sequence " + sequenceName);
            }
            long value = res.getLong(1);
            return value;
        } catch (SQLException e) {
            throw new ServiceAccessException(service, e);
        } finally {
            close(statement);
            close(selectStatement, res);
        }
    }

    public boolean sequenceExists(Connection conn, String sequenceName) throws ServiceAccessException {
        String selectQuery = getExistsQuery(sequenceName);

        PreparedStatement selectStatement = null;
        ResultSet res = null;

        try {
            selectStatement = conn.prepareStatement(selectQuery);
            res = selectStatement.executeQuery();
            return res.next();
        } catch (SQLException e) {
            throw new ServiceAccessException(service, e);
        } finally {
            close(selectStatement, res);
        }
    }

    protected String getExistsQuery(String sequenceName) {
        String selectQuery =
            "SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].["
                + normalizeSequenceName(sequenceName)
                + "]') and OBJECTPROPERTY(id, N'IsUserTable') = 1";
        return selectQuery;
    }

}
