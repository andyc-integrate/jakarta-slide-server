/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/stores/org/apache/slide/store/impl/rdbms/JDBCAwareInputStream.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.store.impl.rdbms;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Wrapper for an input stream that has come from a JDBC connection. This 
 * wrapper also closes the underlying JDBC result set - freeing precious system 
 * resources - or at least Oracle cursors  ;-)
 *
 * @version $Revision: 1.2 $
 */
class JDBCAwareInputStream
    extends FilterInputStream {                                                 
    
    
    // ---------------------------------------------------------- Instance Data
    
    
    /**
     * The JDBC statement that will be closed along with its result set when the 
     * input stream is told to close itself.
     */
    private Statement stmt = null;
    private ResultSet rs = null;
    private Connection connection = null;
    
    
    // ----------------------------------------------------------- Constructors
    
    
    /**
     * Creates an input stream that closes a statmenet, a resultset and a connection
     * when the stream itself is closed.
     *  
     */
    public JDBCAwareInputStream(InputStream in, Statement stmt, ResultSet rs, Connection connection) {
        super(in);
        
        this.stmt = stmt;
        this.rs = rs;
        this.connection = connection;
    }
    
    
    // --------------------------------------------- InputStream Implementation
    
    
    /**
     * Overridden to close the associated JDBC statement, result set and connection together with the 
     * input stream.
     */
    public void close() throws IOException {
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            throw new IOException(e.getMessage());
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException e) {
                throw new IOException(e.getMessage());
            } finally {
                try {
                    if (connection != null) {
                        try {
                            connection.commit();
                        } catch (SQLException e) {
                            throw new IOException(e.getMessage());
                        } finally {
                            try {
                                connection.close();
                            } catch (SQLException e) {
                                throw new IOException(e.getMessage());
                            }
                        }
                    }
                } finally {
                    super.close();
                }
            }
        }
    }
}

