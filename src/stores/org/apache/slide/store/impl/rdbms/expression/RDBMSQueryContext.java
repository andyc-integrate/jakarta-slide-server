/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/stores/org/apache/slide/store/impl/rdbms/expression/RDBMSQueryContext.java,v 1.2 2006-01-22 22:49:06 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:06 $
 *
 * ====================================================================
 *
 * Copyright 1999-2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.store.impl.rdbms.expression;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.slide.search.basic.IBasicResultSet;

/**
 */
public final class RDBMSQueryContext {

    private final Collection _criteria;
    private final Collection _joins;
    private final Map _selects;
    private final IBasicResultSet _results;

    public RDBMSQueryContext(IBasicResultSet results) {
        _criteria = new ArrayList();
        _joins = new HashSet();
        _selects = new HashMap();
        _results = results;
    }

    public Collection criteria() {
        return _criteria;
    }

    public Collection joins() {
        return _joins;
    }

    public Map selects() {
        return _selects;
    }

    public IBasicResultSet results() {
        return _results;
    }

}
