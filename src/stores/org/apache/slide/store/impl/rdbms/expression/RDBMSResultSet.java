/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/stores/org/apache/slide/store/impl/rdbms/expression/RDBMSResultSet.java,v 1.2 2006-01-22 22:49:06 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:06 $
 *
 * ====================================================================
 *
 * Copyright 1999-2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.store.impl.rdbms.expression;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import org.apache.slide.common.SlideRuntimeException;
import org.apache.slide.search.BadQueryException;
import org.apache.slide.search.basic.IBasicResultSet;

/**
 */
public class RDBMSResultSet extends HashSet implements IBasicResultSet {

    private boolean _initialized;
    private final RDBMSExpressionFactory _factory;

    public RDBMSResultSet(RDBMSExpressionFactory factory) {
        _factory = factory;
    }

    private void initialize() {
        try {
            super.addAll(_factory.getRequestedResourcePool().getPool());
        }
        catch (BadQueryException e) {
            throw new SlideRuntimeException(e.toString(), true);
        }
        _initialized = true;
    }
    
    public boolean isPartialResultSet() {
        return _factory.getRequestedResourcePool().partialResult();
    }

    public void clear() {
        if (!_initialized) {
            _initialized = true;
        }
        super.clear();
    }

    public boolean contains(Object arg0) {
        if (!_initialized) {
            initialize();
        }
        return super.contains(arg0);
    }

    public boolean isEmpty() {
        if (!_initialized) {
            initialize();
        }
        return super.isEmpty();
    }

    public Iterator iterator() {
        if (!_initialized) {
            initialize();
        }
        return super.iterator();
    }

    public boolean remove(Object arg0) {
        if (!_initialized) {
            initialize();
        }
        return super.remove(arg0);
    }
    
    public int size() {
        if (!_initialized) {
            initialize();
        }
        return super.size();
    }
    
    public boolean removeAll(Collection arg0) {
        if (!_initialized) {
            initialize();
        }
        return super.removeAll(arg0);
    }
    
    public boolean containsAll(Collection arg0) {
        if (!_initialized) {
            initialize();
        }
        return super.containsAll(arg0);
    }

    public boolean retainAll(Collection arg0) {
        if (!_initialized) {
            initialize();
        }
        return super.retainAll(arg0);
    }

    public Object[] toArray() {
        if (!_initialized) {
            initialize();
        }
        return super.toArray();
    }

    public Object[] toArray(Object[] arg0) {
        if (!_initialized) {
            initialize();
        }
        return super.toArray(arg0);
    }

}