/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/stores/org/apache/slide/store/impl/rdbms/expression/RDBMSNotIsCollectionExpression.java,v 1.2 2006-01-22 22:49:06 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:06 $
 *
 * ====================================================================
 *
 * Copyright 1999-2004 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.store.impl.rdbms.expression;

import org.jdom.a.Element;

/**
 */
public class RDBMSNotIsCollectionExpression extends RDBMSCompareExpression {

    public RDBMSNotIsCollectionExpression(Element element, RDBMSQueryContext context) {
        super(element, context);
    }

    protected String compile() {
        return "(p" + _tableIndex + ".PROPERTY_NAME = 'resourcetype' AND " +
               "p" + _tableIndex + ".PROPERTY_NAMESPACE = 'DAV:' AND " +
               "p" + _tableIndex + ".PROPERTY_VALUE NOT LIKE '%<collection/>%')";
    }

    protected String getPropertyName() {
        return "resourcetype";
    }

    protected String getPropertyNamespace() {
        return "DAV:";
    }

}
