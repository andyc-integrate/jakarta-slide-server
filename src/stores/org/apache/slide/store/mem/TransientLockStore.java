/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/stores/org/apache/slide/store/mem/TransientLockStore.java,v 1.2 2006-01-22 22:49:06 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:06 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.store.mem;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.apache.slide.common.ServiceAccessException;
import org.apache.slide.common.Uri;
import org.apache.slide.lock.LockTokenNotFoundException;
import org.apache.slide.lock.NodeLock;
import org.apache.slide.store.LockStore;


/**
 */
public class TransientLockStore extends AbstractTransientStore 
   implements LockStore 
{
   // Note: we don't clone Locks because ExtendedStore clones

   public void putLock(Uri uri, NodeLock lock) throws ServiceAccessException {
      debug("putLock {0} {1}", uri, lock.getLockId());
      List list = (List)get(uri.toString());
      if (list != null) {
         list = new ArrayList(list);
      } else {
         list = new ArrayList();
      }
      list.add(lock);
      put(uri.toString(), list);
   }
   
   public void renewLock(Uri uri, NodeLock lock)
      throws ServiceAccessException, LockTokenNotFoundException 
   {
      debug("renewLock {0} {1}", uri, lock.getLockId());
      List list = (List)get(uri.toString());
      if (list == null || !list.contains(lock)) {
         throw new LockTokenNotFoundException(lock);
      }
      list = new ArrayList(list);
      list.remove(lock);
      list.add(lock);
      put(uri.toString(), list);
   }

   public void removeLock(Uri uri, NodeLock lock)
      throws ServiceAccessException, LockTokenNotFoundException 
   {
      debug("removeLock {0} {1}", uri, lock.getLockId());
      List list = (List)get(uri.toString());
      if (list == null) {
         throw new LockTokenNotFoundException(lock);
      }
      if (!list.contains(lock)) {
         throw new LockTokenNotFoundException(lock);
      } else {
         if (list.size() == 1) {
            remove(uri.toString());
         } else {
            list = new ArrayList(list);
            list.remove(lock);
            put(uri.toString(), list);
         }
      }
   }
   
   public void killLock(Uri uri, NodeLock lock)
      throws ServiceAccessException, LockTokenNotFoundException 
   {
      debug("killLock {0} {1}", uri, lock.getLockId());
      removeLock(uri, lock);
   }

   public Enumeration enumerateLocks(Uri uri) throws ServiceAccessException 
   {
      debug("enumerateLocks {0}", uri);

      List list = (List)get(uri.toString());
      if (list != null) {
         return new IteratorEnum(list.iterator());
      } else {
         return EMPTY_ENUM;
      }
   }
}
