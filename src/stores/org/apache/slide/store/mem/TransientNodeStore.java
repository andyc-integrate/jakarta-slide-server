/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/stores/org/apache/slide/store/mem/TransientNodeStore.java,v 1.2 2006-01-22 22:49:06 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:06 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.store.mem;

import org.apache.slide.common.ServiceAccessException;
import org.apache.slide.common.Uri;
import org.apache.slide.store.NodeStore;
import org.apache.slide.structure.ObjectAlreadyExistsException;
import org.apache.slide.structure.ObjectNode;
import org.apache.slide.structure.ObjectNotFoundException;


/**
 * NodeStore that stores ObjectNodes in transient memory. 
 * 
 */
public class TransientNodeStore extends AbstractTransientStore implements
      NodeStore
{
   // Note: we don't clone ObjectNodes because ExtendedStore clones

   public ObjectNode retrieveObject(Uri uri) throws ServiceAccessException,
         ObjectNotFoundException
   {
      debug("retrieveObject {0}", uri);
      ObjectNode node = (ObjectNode)get(uri.toString());
      if (node != null) {
         return node;
      } else {
         throw new ObjectNotFoundException(uri);
      }
   }

   public void storeObject(Uri uri, ObjectNode object)
         throws ServiceAccessException, ObjectNotFoundException
   {
      debug("storeObejct {0}", uri);
      ObjectNode node = (ObjectNode)get(uri.toString());
      if (node != null) {
         put(uri.toString(), object);
      } else {
         throw new ObjectNotFoundException(uri);
      }
   }

   public void createObject(Uri uri, ObjectNode object)
         throws ServiceAccessException, ObjectAlreadyExistsException
   {
      debug("createObject {0}", uri);
      ObjectNode node = (ObjectNode)get(uri.toString());
      if (node == null) {
         put(uri.toString(), object); 
      } else {
         throw new ObjectAlreadyExistsException(uri.toString());
      }
   }

   public void removeObject(Uri uri, ObjectNode object)
         throws ServiceAccessException, ObjectNotFoundException
   {
      debug("removeObject {0}", uri);
      ObjectNode node = (ObjectNode)get(uri.toString());
      if (node != null) {
         remove(uri.toString());
      } else {
         throw new ObjectNotFoundException(uri);
      }
   }
}
