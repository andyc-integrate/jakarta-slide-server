/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/stores/org/apache/slide/store/mem/TransientSecurityStore.java,v 1.2 2006-01-22 22:49:06 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:06 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.store.mem;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.apache.slide.common.ServiceAccessException;
import org.apache.slide.common.Uri;
import org.apache.slide.security.NodePermission;
import org.apache.slide.store.SecurityStore;


/**
 */
public class TransientSecurityStore extends AbstractTransientStore implements
      SecurityStore
{
   // ExtendendStore always caches, so we don't
   
   public void grantPermission(Uri uri, NodePermission permission)
         throws ServiceAccessException
   {
      debug("grantPermission {0} {1}", uri, permission);
      List list = (List)get(uri.toString());
      if (list != null) {
         list = new ArrayList(list);
      } else {
         list = new ArrayList();
      }
      list.add(permission);
      put(uri.toString(), list);
   }

   public void revokePermission(Uri uri, NodePermission permission)
         throws ServiceAccessException
   {
      debug("revokePermission {0} {1}", uri, permission);
      List list = (List)get(uri.toString());
      if (list != null) {
         list = new ArrayList(list);
         if (list.remove(permission)) {
            if (list.size() > 0) {
               put(uri.toString(), list);
            } else {
               remove(uri.toString());
            }
         } 
      }
   }

   public void revokePermissions(Uri uri) throws ServiceAccessException
   {
      debug("revokePermissions {0}", uri);
      List list = (List)get(uri.toString());
      if (list != null) {
         remove(uri.toString());
      }
   }

   public Enumeration enumeratePermissions(Uri uri)
         throws ServiceAccessException
   {
      debug("enumeratePermissions {0}", uri);
      List list = (List)get(uri.toString());
      if (list != null) {
         return new IteratorEnum(list.iterator());
      } else {
         return EMPTY_ENUM;
      }
   }
}
