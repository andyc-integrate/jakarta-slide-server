/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/stores/org/apache/slide/store/mem/TransientDescriptorsStore.java,v 1.2 2006-01-22 22:49:06 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:06 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.store.mem;

import org.apache.slide.common.ServiceAccessException;
import org.apache.slide.common.Uri;
import org.apache.slide.content.NodeRevisionDescriptors;
import org.apache.slide.content.RevisionDescriptorNotFoundException;
import org.apache.slide.store.RevisionDescriptorsStore;


/**
 */
public class TransientDescriptorsStore extends AbstractTransientStore implements
      RevisionDescriptorsStore
{
   // Note: cloning is done by the ExtendedStore
   
   public NodeRevisionDescriptors retrieveRevisionDescriptors(Uri uri)
         throws ServiceAccessException, RevisionDescriptorNotFoundException
   {
      NodeRevisionDescriptors descriptors = 
            (NodeRevisionDescriptors)get(uri.toString());
      if (descriptors != null) {
         return descriptors;
      } else {
         throw new RevisionDescriptorNotFoundException(uri.toString());
      }
   }

   public void createRevisionDescriptors(Uri uri,
         NodeRevisionDescriptors revisionDescriptors)
         throws ServiceAccessException
   {
      put(uri.toString(), revisionDescriptors);
   }

   public void storeRevisionDescriptors(Uri uri,
         NodeRevisionDescriptors revisionDescriptors)
         throws ServiceAccessException, RevisionDescriptorNotFoundException
   {
      NodeRevisionDescriptors descriptors = 
            (NodeRevisionDescriptors)get(uri.toString());
      if (descriptors != null) {
         put(uri.toString(), revisionDescriptors);
      } else {
         throw new RevisionDescriptorNotFoundException(uri.toString());
      }
   }

   public void removeRevisionDescriptors(Uri uri) throws ServiceAccessException
   {
      remove(uri.toString());
   }
}
