/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/stores/org/apache/slide/store/mem/TransientContentStore.java,v 1.2 2006-01-22 22:49:06 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:06 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.store.mem;

import org.apache.slide.common.ServiceAccessException;
import org.apache.slide.common.Uri;
import org.apache.slide.content.NodeRevisionContent;
import org.apache.slide.content.NodeRevisionDescriptor;
import org.apache.slide.content.RevisionAlreadyExistException;
import org.apache.slide.content.RevisionNotFoundException;
import org.apache.slide.store.ContentStore;


/**
 */
public class TransientContentStore extends AbstractTransientStore implements
      ContentStore
{
   // FIXME: should we clone the content byte array?

   public NodeRevisionContent retrieveRevisionContent(Uri uri,
         NodeRevisionDescriptor revisionDescriptor)
         throws ServiceAccessException, RevisionNotFoundException
   {
      VersionedUriKey key = new VersionedUriKey(uri, 
            revisionDescriptor.getRevisionNumber());
      debug("retrieveRevisionContent {0}", key);
      
      byte[] content = (byte[])get(key);
      if (content != null) {
         NodeRevisionContent revisionContent = new NodeRevisionContent();
         revisionContent.setContent(content);
         return revisionContent;
      } else {
         throw new RevisionNotFoundException(uri.toString(), 
               revisionDescriptor.getRevisionNumber());
      }
   }

   public void createRevisionContent(Uri uri,
         NodeRevisionDescriptor revisionDescriptor,
         NodeRevisionContent revisionContent) throws ServiceAccessException,
         RevisionAlreadyExistException
   {
      VersionedUriKey key = new VersionedUriKey(uri, 
            revisionDescriptor.getRevisionNumber());
      debug("createRevisionContent {0}", key);
      
      byte[] content = (byte[])get(key);
      if (content == null) {
         put(key, revisionContent.getContentBytes());
      } else {
         throw new RevisionAlreadyExistException(uri.toString(), 
               revisionDescriptor.getRevisionNumber());
      }
   }
   
   public void storeRevisionContent(Uri uri,
         NodeRevisionDescriptor revisionDescriptor,
         NodeRevisionContent revisionContent) throws ServiceAccessException,
         RevisionNotFoundException
   {
      VersionedUriKey key = new VersionedUriKey(uri, 
            revisionDescriptor.getRevisionNumber());
      debug("storeRevisionContent {0}", key);
     
      byte[] content = (byte[])get(key);
      if (content != null) {
         put(key, revisionContent.getContentBytes());
      } else {
         throw new RevisionNotFoundException(uri.toString(), 
               revisionDescriptor.getRevisionNumber());
      }
   }

   public void removeRevisionContent(Uri uri,
         NodeRevisionDescriptor revisionDescriptor)
         throws ServiceAccessException
   {
      VersionedUriKey key = new VersionedUriKey(uri, 
            revisionDescriptor.getRevisionNumber());
      debug("storeRevisionContent {0}", key);
      
      remove(key);
   }
}
