/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/stores/org/apache/slide/search/basic/sample/BasicQuerySample.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.search.basic.sample;

import org.apache.slide.search.SearchToken;
import org.apache.slide.search.basic.BasicQueryImpl;
import org.apache.slide.search.basic.IBasicExpressionFactory;
import org.apache.slide.search.basic.IBasicQuery;

/**
 * This class is meant as an example for a store specific implementation
 * for Basic SEARCH.
 *
 * @version $Revision: 1.2 $
 */
public class BasicQuerySample extends BasicQueryImpl implements IBasicQuery {
    
    /**
	 * Constructor
	 */
	public BasicQuerySample () {
    }
    
	/**
	 * Constructor
	 */
    public BasicQuerySample (SearchToken searchToken) {
        super (searchToken);
    }
 
        
	/**
	 * Method getExpressionFactory
	 *
	 * @return   an IBasicExpressionFactory
	 *
	 */
    public IBasicExpressionFactory getExpressionFactory() {
        return new BasicExpressionFactorySample ();
    }
}


