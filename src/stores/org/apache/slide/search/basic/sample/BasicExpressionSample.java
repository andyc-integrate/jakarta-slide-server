/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/stores/org/apache/slide/search/basic/sample/BasicExpressionSample.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */



package org.apache.slide.search.basic.sample;

import org.apache.slide.search.basic.*;

import java.util.Collection;
import java.util.Iterator;
import org.apache.slide.common.SlideException;
import org.apache.slide.search.BadQueryException;
import org.apache.slide.search.RequestedResource;
import org.apache.slide.search.SearchException;
import org.apache.slide.structure.ObjectNode;
import org.apache.slide.structure.SubjectNode;

/**
 * A very basic sample for a store specific Expression. Depending on the
 * complexity of the concrete store specific implementation, iut might be
 * a good idea to have an Expression class for each DAV: expression
 * (SQLEqExpression, SQLOrExpression, ...)
 *
 * @version $Revision: 1.2 $
 */
public class BasicExpressionSample implements IBasicExpression {
    
    
    /** an example for an executable command */
    String theExecutableCommand;
    
    /** backptr to the factory */
    IBasicExpressionFactory factory;
    
    /**
     * constructor for a compare expression like gt, eq, ...
     * For your concrete implementation you are free, which parameters have to
     * be passed, let the factory give you everything you need.
     */
    BasicExpressionSample (String command, IBasicExpressionFactory factory){
        theExecutableCommand = command;
        this.factory = factory;
    }
    
    /**
     * contructor for a merge expression
     */
    BasicExpressionSample (String mergeOperator,
                           Collection children,
                           IBasicExpressionFactory factory)
        throws BadQueryException
    {
        this.factory = factory;
        Iterator it = children.iterator();
        BasicExpressionSample firstChild = (BasicExpressionSample)it.next();
        
        if (firstChild == null)
            throw new BadQueryException (mergeOperator + " needs at least one nested element");
        
        theExecutableCommand = firstChild.theExecutableCommand;
        
        // create the executable command
        while (it.hasNext()) {
            BasicExpressionSample exp = (BasicExpressionSample)it.next();
            theExecutableCommand += " " + mergeOperator + " " + exp.theExecutableCommand;
        }
    }
    
    /**
     * fake executer. The executable command is printed and a fake result is created.
     *
     * @return   an IBasicResultSet
     *
     * @throws   SearchException
     *
     */
    public IBasicResultSet execute() throws SearchException {
        
        IBasicResultSet result = new BasicResultSetImpl (true);
        
        // here the miracle happens. The command is executed, and ObjectNodes
        // are created from all results, that match the query.
        System.out.println("now execute: " + theExecutableCommand);
        
        //
        // fake one result
        //
        ObjectNode node = new SubjectNode("/"); // this will return the root folder
        RequestedResource resource = null;
        IBasicQuery query = factory.getQuery();
        
        try {
            resource = new ComparableResourceImpl
                (node, query.getSearchToken(), query.getScope(),
                 factory.getPropertyProvider());
        }
        catch (SlideException e) {
            throw new SearchException (e);
        }
        
        result.add (resource);
        
        
        return  result;
    }
    
    public void setFactory (IBasicExpressionFactory factory) {
        this.factory = factory;
    }
    
    public IBasicExpressionFactory getFactory() {
        return this.factory;
    }
    
    
}

