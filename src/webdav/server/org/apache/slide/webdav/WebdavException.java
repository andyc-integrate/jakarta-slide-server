/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/WebdavException.java,v 1.2 2006-01-22 22:49:06 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:06 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav;

import org.apache.slide.common.SlideException;
import org.apache.slide.webdav.util.WebdavStatus;

/**
 * Exception class used by the WebDAV server classes. A WebDAV exception is 
 * always associated with a status code, which can be sent to the client as 
 * response.
 *
 */
public class WebdavException
    extends SlideException {
    
    
    // ----------------------------------------------------- Instance Variables
    
    
    /**
     * HTTP status code associated with the exception.
     */
    protected int statusCode;
    
    
    // ----------------------------------------------------------- Constructors
    
    
    /**
     * Constructor. Creation of the exception is logged.
     * 
     * @param statusCode the status code
     */
    public WebdavException(int statusCode) {
        this(statusCode, false); // logging=false
    }
    
    
    /**
     * Constructor. Logging when the exception is created can be disabled.
     *
     * @param statusCode    the status code
     * @param logging       should the creation of this exception be logged ?
     */
    public WebdavException(int statusCode, boolean logging) {
        super(WebdavStatus.getStatusText(statusCode), logging);
        this.statusCode = statusCode;
    }
    
    
    // --------------------------------------------------------- Public Methods
    
    
    /**
     * Returns the HTTP/WebDAV status code associated with the exception.
     *
     * @return the status code
     */
    public int getStatusCode() {
        return statusCode;
    }
    
    
}

