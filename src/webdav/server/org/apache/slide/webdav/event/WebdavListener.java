/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/event/WebdavListener.java,v 1.2 2006-01-22 22:55:20 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:20 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.event;

import java.util.EventListener;

import org.apache.slide.event.VetoException;

/**
 * Webdav listener interface
 *
 * @version $Revision: 1.2 $
 */
public interface WebdavListener extends EventListener {
    void get(WebdavEvent event) throws VetoException;

    void put(WebdavEvent event) throws VetoException;

    void propFind(WebdavEvent event) throws VetoException;

    void propPatch(WebdavEvent event) throws VetoException;

    void bind(WebdavEvent event) throws VetoException;

    void rebind(WebdavEvent event) throws VetoException;

    void unbind(WebdavEvent event) throws VetoException;

    void mkcol(WebdavEvent event) throws VetoException;

    void copy(WebdavEvent event) throws VetoException;

    void move(WebdavEvent event) throws VetoException;

    void delete(WebdavEvent event) throws VetoException;

    void lock(WebdavEvent event) throws VetoException;

    void unlock(WebdavEvent event) throws VetoException;

    void acl(WebdavEvent event) throws VetoException;

    void report(WebdavEvent event) throws VetoException;

    void search(WebdavEvent event) throws VetoException;

    void versionControl(WebdavEvent event) throws VetoException;

    void options(WebdavEvent event) throws VetoException;

    void update(WebdavEvent event) throws VetoException;

    void checkin(WebdavEvent event) throws VetoException;

    void checkout(WebdavEvent event) throws VetoException;

    void uncheckout(WebdavEvent event) throws VetoException;

    void label(WebdavEvent event) throws VetoException;

    void mkworkspace(WebdavEvent event) throws VetoException;

    void subscribe(WebdavEvent event) throws VetoException;

    void unsubscribe(WebdavEvent event) throws VetoException;

    void poll(WebdavEvent event) throws VetoException;
}