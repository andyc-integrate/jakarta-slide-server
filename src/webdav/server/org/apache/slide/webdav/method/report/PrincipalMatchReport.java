/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/method/report/PrincipalMatchReport.java,v 1.2 2006-01-22 22:55:20 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:20 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.method.report;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.slide.common.NamespaceAccessToken;
import org.apache.slide.common.PropertyParseException;
import org.apache.slide.common.RequestedProperties;
import org.apache.slide.common.RequestedPropertiesImpl;
import org.apache.slide.common.RequestedProperty;
import org.apache.slide.common.ServiceAccessException;
import org.apache.slide.common.SlideException;
import org.apache.slide.common.SlideToken;
import org.apache.slide.content.NodeRevisionNumber;
import org.apache.slide.search.RequestedResource;
import org.apache.slide.search.SearchQuery;
import org.apache.slide.search.basic.Literals;
import org.apache.slide.structure.SubjectNode;
import org.apache.slide.webdav.WebdavServletConfig;
import org.apache.slide.webdav.util.AclConstants;
import org.apache.slide.webdav.util.PreconditionViolationException;
import org.apache.slide.webdav.util.ViolatedPrecondition;
import org.apache.slide.webdav.util.WebdavStatus;
import org.apache.slide.webdav.util.WebdavUtils;
import org.jdom.a.Element;
import org.jdom.a.Namespace;
import org.jdom.a.output.XMLOutputter;


/**
 * DAV:principal-match report worker.
 *
 */
public class PrincipalMatchReport extends AbstractReport implements AclConstants {

    private RequestedProperties requestedProperties = null;
    private SubjectNode matchSubjectNode = null;

    /**
     * Constructor
     *
     * @param    slideToken          a  SlideToken
     * @param    token               a  NamespaceAccessToken
     * @param    config              a  WebdavServletConfig
     * @param    servletPath         a String, the result of HttpRequest.getServletPath()
     * @param    contextPath         a  String , the result of HttpRequest.getContextPath()
     */
    public PrincipalMatchReport(SlideToken slideToken, NamespaceAccessToken token, WebdavServletConfig config, String slideContextPath) {
        super(slideToken, token, config, slideContextPath);
    }

    /**
     * Initialize report worker with specified report element
     *
     * @param    resourcePath        a  String
     * @param    principalMatchElm   an Element
     *
     * @throws   PreconditionViolationException
     */
    public void init(String resourcePath, Element principalMatchElm) throws PreconditionViolationException {
        if (principalMatchElm.getChildren().size() == 0) {
            throw new PreconditionViolationException(
                new ViolatedPrecondition("at-least-one-child-element",
                                         WebdavStatus.SC_BAD_REQUEST,
                                         "DAV:principal-match element must have at least one child"),
                resourcePath
            );
        }
        List principalPropertyElmL = principalMatchElm.getChildren(E_PRINCIPAL_PROPERTY, DNSP);
        List selfElmL = principalMatchElm.getChildren(E_SELF, DNSP);
        if (principalPropertyElmL.size()+selfElmL.size() != 1) {
            throw new PreconditionViolationException(
                new ViolatedPrecondition("either-one-principal-property-or-one-self-element",
                                         WebdavStatus.SC_BAD_REQUEST,
                                         "DAV:principal-match element must contain either a DAV:principal-property or a DAV:self element"),
                resourcePath
            );
        }
        if (principalPropertyElmL.size() == 1) {
            Element principalPropertyElm = (Element)principalPropertyElmL.get(0);
            if (principalPropertyElm.getChildren(E_OWNER, DNSP).size() > 0) {
                matchSubjectNode = SubjectNode.OWNER;
            }
            else {
                // TODO: support other properties
            }
        }
        else {
            matchSubjectNode = SubjectNode.SELF;
        }
        List propElmL = principalMatchElm.getChildren(E_PROP, DNSP);
        if (propElmL.size() > 1) {
            throw new PreconditionViolationException(
                new ViolatedPrecondition("at-most-one-prop-element",
                                         WebdavStatus.SC_BAD_REQUEST,
                                         "DAV:principal-match element must have at most one DAV:prop child"),
                resourcePath
            );
        }

        if (propElmL.size() == 1) {
            Element propElm = (Element)propElmL.get(0);
            try {
                this.requestedProperties = new RequestedPropertiesImpl(propElm);
            }
            catch (PropertyParseException e) {
                throw new PreconditionViolationException(
                    new ViolatedPrecondition("invalid-prop",
                                             WebdavStatus.SC_BAD_REQUEST,
                                             e.getMessage()),
                    resourcePath
                );
            }
        }
    }

    /**
     * Execute report and add results to given multistatus element
     *
     * @param    resourcePath        a  String
     * @param    multistatusElm      an Element
     * @param    depth               an int
     *
     * @throws   SlideException
     * @throws   IOException
     */
    public void execute(String resourcePath, Element multistatusElm, int depth) throws SlideException, IOException {
        SubjectNode currentUserNode = (SubjectNode)security.getPrincipal(slideToken);
        Element queryElm = getQueryElement(resourcePath, currentUserNode);
        new XMLOutputter(org.jdom.a.output.Format.getPrettyFormat()).output(queryElm, System.out);

        String absUri = WebdavUtils.getAbsolutePath (resourcePath, slideContextPath, config);

        SearchQuery query =
            search.createSearchQuery(queryElm.getNamespaceURI(),
                                     queryElm,
                                     slideToken,
                                     config.getDepthLimit(),
                                     absUri);

        Iterator result = query.execute().iterator();
        while (result.hasNext()) {
            RequestedResource r = (RequestedResource)result.next();
            multistatusElm.addContent(getResponseElement(slideToken, r.getUri(), new NodeRevisionNumber(), requestedProperties));
        }

    }

    private Element getQueryElement(String resourcePath, SubjectNode currentUserNode) throws SlideException {
        if (matchSubjectNode.equals(SubjectNode.OWNER)) {
            return getOwnerQueryElement(resourcePath, currentUserNode);
        }
        else if (matchSubjectNode.equals(SubjectNode.SELF)) {
            return getSelfQueryElement(resourcePath, currentUserNode);
        }
        else {
            // TODO: other properties
            return null;
        }
    }

    private Element getSelfQueryElement(String resourcePath, SubjectNode currentUserNode) {
        Element result = new Element(Literals.BASICSEARCH, DNSP);
        // select
        Element selectElm = new Element(Literals.SELECT, DNSP);
        result.addContent(selectElm);
        Element propElm = new Element(E_PROP, DNSP);
        selectElm.addContent(propElm);
        Iterator props = requestedProperties.getRequestedProperties();
        while (props.hasNext()) {
            RequestedProperty p = (RequestedProperty)props.next();
            Namespace nsp = (DNSP.getURI().equals(p.getNamespace()))
                ? DNSP
                : Namespace.getNamespace(p.getNamespace());
            propElm.addContent(new Element(p.getName(), nsp));
        }
        // from
        Element fromElm = new Element(Literals.FROM, DNSP);
        result.addContent(fromElm);
        Element scopeElm = new Element(Literals.SCOPE, DNSP);
        fromElm.addContent(scopeElm);
        Element hrefElm = new Element(E_HREF, DNSP);
        hrefElm.setText(WebdavUtils.getAbsolutePath (resourcePath, slideContextPath, config));

        scopeElm.addContent(hrefElm);
        // where
        Element whereElm = new Element(Literals.WHERE, DNSP);
        result.addContent(whereElm);
        Element propcontainsElm = new Element("propcontains", Namespace.getNamespace("http://jakarta.apache.org/slide/"));
        whereElm.addContent(propcontainsElm);
        propElm = new Element(E_PROP, DNSP);
        propElm.addContent(new Element(P_GROUP_MEMBER_SET, DNSP));
        Element literalElm = new Element(Literals.LITERAL, DNSP);
        literalElm.addContent("/"+currentUserNode.getPath().lastSegment()+"<");
        propcontainsElm.addContent(propElm);
        propcontainsElm.addContent(literalElm);
        return result;
    }

    private Element getOwnerQueryElement(String resourcePath, SubjectNode currentUserNode) {
        Element result = new Element(Literals.BASICSEARCH, DNSP);
        // select
        Element selectElm = new Element(Literals.SELECT, DNSP);
        result.addContent(selectElm);
        Element propElm = new Element(E_PROP, DNSP);
        selectElm.addContent(propElm);
        Iterator props = requestedProperties.getRequestedProperties();
        while (props.hasNext()) {
            RequestedProperty p = (RequestedProperty)props.next();
            Namespace nsp = (DNSP.getURI().equals(p.getNamespace()))
                ? DNSP
                : Namespace.getNamespace(p.getNamespace());
            propElm.addContent(new Element(p.getName(), nsp));
        }
        // from
        Element fromElm = new Element(Literals.FROM, DNSP);
        result.addContent(fromElm);
        Element scopeElm = new Element(Literals.SCOPE, DNSP);
        fromElm.addContent(scopeElm);
        Element hrefElm = new Element(E_HREF, DNSP);
        hrefElm.setText(WebdavUtils.getAbsolutePath (resourcePath, slideContextPath, config));

        scopeElm.addContent(hrefElm);
        // where
        Element whereElm = new Element(Literals.WHERE, DNSP);
        result.addContent(whereElm);
        Element eqElm = new Element(Literals.EQ, DNSP);
        whereElm.addContent(eqElm);
        propElm = new Element(E_PROP, DNSP);
        propElm.addContent(new Element(P_OWNER, DNSP));
        Element literalElm = new Element(Literals.LITERAL, DNSP);
        literalElm.addContent(currentUserNode.getPath().lastSegment());
        eqElm.addContent(propElm);
        eqElm.addContent(literalElm);
        return result;
    }

    /**
     * Method checkPreconditions
     * @param    resourcePath        a  String
     * @param    depth               an int
     * @throws   PreconditionViolationException
     * @throws   ServiceAccessException
     */
    public void checkPreconditions(String resourcePath, int depth) throws PreconditionViolationException, ServiceAccessException {
        if (depth != 0) {
            throw new PreconditionViolationException(
                new ViolatedPrecondition("depth-must-be-zero",
                                         WebdavStatus.SC_BAD_REQUEST,
                                         "This report is only defined for depth=0."),
                resourcePath
            );
        }
    }
}
