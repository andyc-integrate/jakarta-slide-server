/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/method/DeleteMethod.java,v 1.2 2006-01-22 22:55:20 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:20 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.method;

import java.io.IOException;

import org.apache.slide.common.NamespaceAccessToken;
import org.apache.slide.common.ServiceAccessException;
import org.apache.slide.common.SlideException;
import org.apache.slide.content.NodeRevisionDescriptor;
import org.apache.slide.content.NodeRevisionDescriptors;
import org.apache.slide.content.RevisionDescriptorNotFoundException;
import org.apache.slide.event.EventDispatcher;
import org.apache.slide.macro.DeleteListener;
import org.apache.slide.macro.DeleteMacroException;
import org.apache.slide.structure.ObjectNotFoundException;
import org.apache.slide.util.Configuration;
import org.apache.slide.webdav.WebdavException;
import org.apache.slide.webdav.WebdavServletConfig;
import org.apache.slide.webdav.event.WebdavEvent;
import org.apache.slide.webdav.util.DeltavConstants;
import org.apache.slide.webdav.util.PreconditionViolationException;
import org.apache.slide.webdav.util.PropertyHelper;
import org.apache.slide.webdav.util.UriHandler;
import org.apache.slide.webdav.util.VersioningHelper;
import org.apache.slide.webdav.util.ViolatedPrecondition;
import org.apache.slide.webdav.util.WebdavStatus;
import org.apache.slide.webdav.util.resourcekind.AbstractResourceKind;
import org.apache.slide.webdav.util.resourcekind.CheckedOutVersionControlled;
import org.apache.slide.webdav.util.resourcekind.ResourceKind;
import org.apache.slide.webdav.util.resourcekind.Working;
import org.jdom.a.JDOMException;

/**
 * DELETE method.
 *
 */
public class DeleteMethod extends AbstractMultistatusResponseMethod implements DeltavConstants, DeleteListener, WriteMethod {


    // ----------------------------------------------------- Instance Variables


    /**
     * Id of the resosurce or collection which is to be deleted.
     */
    private String toDelete;

    /**
     * The NodeRevisionDescriptors of the history of the deleted version.
     */
    protected NodeRevisionDescriptors historyNrds = null;

    /**
     * Indicates if the resource to delete is a collection.
     */
    protected boolean isCollection = false;

    /**
     * The UriHandler used by method {@link #beforeDelete beforeDelete()} and
     * {@link #afterDelete afterDelete()}.
     */
    protected UriHandler uriHandler = null;

    /**
     * The VersioningHelper used by this instance.
     */
    protected VersioningHelper versioningHelper = null;


    // ----------------------------------------------------------- Constructors


    /**
     * Constructor.
     *
     * @param token     the token for accessing the namespace
     * @param config    configuration of the WebDAV servlet
     */
    public DeleteMethod(NamespaceAccessToken token,
                        WebdavServletConfig config) {
        super(token, config);
    }


    // ------------------------------------------------------ Protected Methods


    /**
     * Parse request.
     *
     * @exception WebdavException Does not happen
     */
    protected void parseRequest()
        throws WebdavException {
        versioningHelper =
            VersioningHelper.getVersioningHelper(slideToken, token, req, resp,
                                                 config);
        toDelete = requestUri;
        if (toDelete == null) {
            toDelete = "/";
        }
    }


    /**
     * Execute request.
     *
     * @exception WebdavException Unrecoverable error while deleting
     */
    protected void executeRequest()
        throws WebdavException {

        // Prevent dirty reads
        slideToken.setForceStoreEnlistment(true);

        // check lock-null resources
        try {
            if (isLockNull(toDelete)) {
                int statusCode = WebdavStatus.SC_NOT_FOUND;
                sendError( statusCode, "lock-null resource", new Object[]{toDelete} );
                throw new WebdavException( statusCode );
            }
        }
        catch (ServiceAccessException e) {
            int statusCode = getErrorCode( e );
            sendError( statusCode, e );
            throw new WebdavException( statusCode );
        }

        isCollection = isCollection(toDelete);
        try {
            if ( WebdavEvent.DELETE.isEnabled() ) EventDispatcher.getInstance().fireVetoableEvent(WebdavEvent.DELETE, new WebdavEvent(this));

            macro.delete(slideToken, toDelete, null, this);
            resp.setStatus(WebdavStatus.SC_NO_CONTENT);
        } catch (DeleteMacroException dme) {
            // If it's not a collection, we don't want to give a 207,
            // because it's silly, and it confuses many clients (such as
            // MS Web Folders).
            if (generateMultiStatusResponse(isCollection, dme, requestUri)) {
                String errorMessage = generateErrorMessage(dme);
                // Write it on the servlet writer
                resp.setStatus(WebdavStatus.SC_MULTI_STATUS);
                try {
                    resp.setContentType(TEXT_XML_UTF_8);
                    resp.getWriter().write(errorMessage);
                } catch(IOException ex) {
                    // Critical error ... Servlet container is dead or something
                    int statusCode = WebdavStatus.SC_INTERNAL_SERVER_ERROR;
                    sendError( statusCode, ex );
                    throw new WebdavException( statusCode );
                }
            } else {
                // Returning 207 on non-collection requests is generally
                // considered bad. So let's not do it, since this way
                // makes clients generally behave better.
                SlideException exception = (SlideException)dme.enumerateExceptions().nextElement();
                if (exception instanceof PreconditionViolationException) {
                    try {
                        sendPreconditionViolation((PreconditionViolationException)exception);
                    } catch(IOException ex) {
                        // Critical error ... Servlet container is dead or something
                        int statusCode = WebdavStatus.SC_INTERNAL_SERVER_ERROR;
                        sendError( statusCode, ex );
                        throw new WebdavException( statusCode );
                    }
                }
                else {
                    int statusCode = getErrorCode( exception );
                    sendError( statusCode, exception );
                    throw new WebdavException( statusCode );
                }
            }
            //
            // make sure the transaction is aborted
            // throw any WebDAV exception to indicate the transaction wants to be aborted
            //
            throw new WebdavException(WebdavStatus.SC_ACCEPTED, false);
        } catch( SlideException x ) {
            int statusCode = getErrorCode((SlideException)x);
            sendError( statusCode, x );
            throw new WebdavException( statusCode );
        }
    }

    // ------------------------------------------------------ Interface DeleteListener

    /**
     * This method is called prior to deleting the resource associated by
     * the given <code>targetUri</code>. The deletion can be prohibited by
     * throwing a SlideException.
     *
     * @param      targetUri       the Uri of the resource that will be deleted.
     *
     * @throws     SlideException  this Exception will be passed to the caller
     *                             of the Macro helper (contained in the
     *                             MacroDeleteException.
     */
    public void beforeDelete(String targetUri) throws SlideException {

        if(Configuration.useVersionControl()) {

            uriHandler = UriHandler.getUriHandler(targetUri);
            if (uriHandler.isVersionUri() && !isCollection) {
                // delete of version is only allowed if the history collection is deleted
                throw new PreconditionViolationException(new ViolatedPrecondition(C_NO_VERSION_DELETE,
                                                                                  WebdavStatus.SC_FORBIDDEN),
                                                         targetUri);
            }

            // if resource being removed is a checked-out VCR or a WR,
            // remove its URI in the DAV:checkout-set property of the VR
            NodeRevisionDescriptors nrds = content.retrieve(slideToken, targetUri);
            NodeRevisionDescriptor nrd = content.retrieve(slideToken, nrds);
            ResourceKind rk = AbstractResourceKind.determineResourceKind(token, nrds, nrd);
            if(rk instanceof CheckedOutVersionControlled || rk instanceof Working) {
                String vrUri =
                    VersioningHelper.getUriOfAssociatedVR(token, slideToken, content, targetUri);
                try {
                    NodeRevisionDescriptors vrNrds = content.retrieve(slideToken, vrUri);
                    NodeRevisionDescriptor vrNrd = content.retrieve(slideToken, vrNrds);
                    PropertyHelper.removeHrefFromProperty(vrNrd, P_CHECKOUT_SET, targetUri);
                    content.store(slideToken, vrNrds.getUri(), vrNrd, null);
                }
                catch( ObjectNotFoundException e ) {
                    // Nothing to do if the VR is gone, e.g. because the VHR was deleted.
                }
                catch( RevisionDescriptorNotFoundException e ) {
                    // Nothing to do if the VR is gone, e.g. because the VHR was deleted.
                }
                catch( JDOMException e ) {
                    throw new SlideException(
                        "Unable to update DAV:checkout-set of "+vrUri+": "+e.getMessage() );
                }
            }
        }
    }


    /**
     * This method is called after deleting the resource associated by
     * the given <code>targetUri</code>.
     *
     * @param      targetUri       the Uri of the resource that will be deleted.
     *
     * @throws     SlideException  this Exception will be passed to the caller
     *                             of the Macro helper (contained in the
     *                             MacroDeleteException.
     */
    public void afterDelete(String targetUri) throws SlideException {
    }



}

