/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/method/UnsubscribeMethod.java,v 1.2 2006-01-22 22:55:20 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:20 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.method;


import org.apache.slide.common.NamespaceAccessToken;
import org.apache.slide.event.EventDispatcher;
import org.apache.slide.webdav.WebdavException;
import org.apache.slide.webdav.WebdavServletConfig;
import org.apache.slide.webdav.event.NotificationTrigger;
import org.apache.slide.webdav.event.Subscriber;
import org.apache.slide.webdav.event.WebdavEvent;
import org.apache.slide.webdav.util.NotificationConstants;
import org.apache.slide.webdav.util.WebdavStatus;


/**
 * Unsubscribe Method.
 *
 */
public class UnsubscribeMethod extends AbstractWebdavMethod implements NotificationConstants {
    protected static final String LOG_CHANNEL = UnsubscribeMethod.class.getName();

    /**
     * Constructor.
     *
     * @param token     the token for accessing the namespace
     * @param config    configuration of the WebDAV servlet
     */
    public UnsubscribeMethod(NamespaceAccessToken token,
                         WebdavServletConfig config) {
        super(token, config);
    }
    
    protected void parseRequest() throws WebdavException {
    }

    protected void executeRequest() throws WebdavException {
        try {
            if ( WebdavEvent.UNSUBSCRIBE.isEnabled() ) EventDispatcher.getInstance().fireVetoableEvent(WebdavEvent.UNSUBSCRIBE, new WebdavEvent(this));
            int []subscriptionIDs = requestHeaders.getSubscriptionId();
            for ( int i = 0; i < subscriptionIDs.length; i++ ) {
                Subscriber subscriber = NotificationTrigger.getInstance().getSubscriber(subscriptionIDs[i]);
                NotificationTrigger.getInstance().removeSubscriber(subscriber);
            }
            resp.setStatus(WebdavStatus.SC_OK);
        } catch (Exception e) {
            int statusCode = getErrorCode( (Exception)e );
            sendError( statusCode, e );
            throw new WebdavException( statusCode );
        }
    }
}