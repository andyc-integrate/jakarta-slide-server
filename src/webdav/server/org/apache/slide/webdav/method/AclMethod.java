/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/method/AclMethod.java,v 1.2 2006-01-22 22:55:20 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:20 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.method;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import org.apache.slide.common.NamespaceAccessToken;
import org.apache.slide.common.ServiceAccessException;
import org.apache.slide.common.SlideException;
import org.apache.slide.common.Uri;
import org.apache.slide.content.NodeProperty;
import org.apache.slide.content.NodeRevisionDescriptor;
import org.apache.slide.content.NodeRevisionDescriptors;
import org.apache.slide.security.NodePermission;
import org.apache.slide.structure.ActionNode;
import org.apache.slide.structure.ObjectNode;
import org.apache.slide.structure.SubjectNode;
import org.apache.slide.webdav.WebdavException;
import org.apache.slide.webdav.WebdavServletConfig;
import org.apache.slide.webdav.event.WebdavEvent;
import org.apache.slide.webdav.util.AclConstants;
import org.apache.slide.webdav.util.PreconditionViolationException;
import org.apache.slide.webdav.util.ViolatedPrecondition;
import org.apache.slide.webdav.util.WebdavConstants;
import org.apache.slide.webdav.util.WebdavStatus;
import org.apache.slide.event.EventDispatcher;
import org.jdom.a.Element;
import org.jdom.a.JDOMException;


/**
 * ACL method.
 */
public class AclMethod extends AbstractWebdavMethod implements AclConstants, WriteMethod {

    private String resourcePath;
    private Vector permissions;

    /**
     * Constructor.
     *
     * @param token     the token for accessing the namespace
     * @param config    configuration of the WebDAV servlet
     */
    public AclMethod(NamespaceAccessToken token, WebdavServletConfig config) {
        super(token, config);
    }

    /**
     * Parse WebDAV XML query.
     *
     * @exception WebdavException
     */
    protected void parseRequest() throws WebdavException {

        permissions = new Vector();

        resourcePath = requestUri;
        if (resourcePath == null) {
            resourcePath = "/";
        }

        try{
            Iterator aceIt = parseRequestContent(P_ACL).getChildren().iterator();
            while (aceIt.hasNext()) {
                Element aceElm = (Element)aceIt.next();
                permissions.addAll( createNodePermissionList(aceElm) );
            }
        }
        catch (IOException e) {  // TODO: merge exception handling into jdom access methods
            int statusCode = WebdavStatus.SC_INTERNAL_SERVER_ERROR;
            sendError( statusCode, e );
            throw new WebdavException( statusCode );
        }
        catch (JDOMException e) {  // TODO: merge exception handling into jdom access methods
            int statusCode = WebdavStatus.SC_BAD_REQUEST;
            sendError( statusCode, e );
            throw new WebdavException( statusCode );
        }
        catch (PreconditionViolationException e) {
            try {
                sendPreconditionViolation(e);
            } catch (IOException x) {}
            throw new WebdavException(e.getStatusCode());
        }
        catch (SlideException e) {
            int statusCode = WebdavStatus.SC_INTERNAL_SERVER_ERROR;
            sendError( statusCode, e );
            throw new WebdavException( statusCode );
        }
    }

    private List createNodePermissionList( Element aceElm ) throws PreconditionViolationException, SlideException, JDOMException {
        List result = new ArrayList();
        String objectUri = resourcePath;
        String subjectUri = null;
        String actionUri = null;
        boolean negative = false;
        boolean invert = false;

        // ACE principal
        Element principalElm = aceElm.getChild(E_PRINCIPAL, DNSP);
        if (principalElm == null) {
            Element invertElm = aceElm.getChild(E_INVERT, DNSP);
            if (invertElm != null) {
                invert = true;
                principalElm = invertElm.getChild(E_PRINCIPAL, DNSP);
            }
        }
        if (principalElm != null) {
            subjectUri = createSubjectUri(principalElm);
        }
        else {
            throw new PreconditionViolationException(
                new ViolatedPrecondition("missing-ace-principal", WebdavStatus.SC_BAD_REQUEST), resourcePath
            );
        }

        // ACE grant and deny
        Element grantDenyElm = null;
        Element grantElm = aceElm.getChild(E_GRANT, DNSP);
        Element denyElm = aceElm.getChild(E_DENY, DNSP);
        if (grantElm != null && denyElm == null) {
            grantDenyElm = grantElm;
        }
        else if (grantElm == null && denyElm != null) {
            negative = true;
            grantDenyElm = denyElm;
        }
        else if(grantElm != null && denyElm != null) {
            throw new PreconditionViolationException(
                new ViolatedPrecondition("only-grant-or-deny-allowed", WebdavStatus.SC_BAD_REQUEST), resourcePath
            );
        }
        else if(grantElm == null && denyElm == null) {
            throw new PreconditionViolationException(
                new ViolatedPrecondition("missing-grant-or-deny", WebdavStatus.SC_BAD_REQUEST), resourcePath
            );
        }
        Iterator privilegeIt = grantDenyElm.getChildren(E_PRIVILEGE, DNSP).iterator();

        while (privilegeIt.hasNext()) {
            Element privilegeElm = (Element)privilegeIt.next();
            actionUri = createActionUri(privilegeElm);
            if (actionUri == null) {
                throw new PreconditionViolationException(
                    new ViolatedPrecondition("not-supported-privilege", WebdavStatus.SC_BAD_REQUEST), resourcePath
                );
            }
            else {
                NodePermission np = new NodePermission(objectUri, subjectUri, actionUri, true, negative);
                np.setInvert(invert);
                result.add(np);
            }
        }
        return result;
    }

    private String createSubjectUri(Element principalElm) throws JDOMException, PreconditionViolationException {
        Element elm = principalElm.getChild(E_HREF, DNSP);
        if (elm != null) {
            return getSlidePath(elm.getTextTrim());
        }

        elm = principalElm.getChild(E_ALL, DNSP);
        if (elm != null) {
            return SubjectNode.ALL_URI;
        }

        elm = principalElm.getChild(E_AUTHENTICATED, DNSP);
        if (elm != null) {
            return SubjectNode.AUTHENTICATED_URI;
        }

        elm = principalElm.getChild(E_UNAUTHENTICATED, DNSP);
        if (elm != null) {
            return SubjectNode.UNAUTHENTICATED_URI;
        }

        elm = principalElm.getChild(E_SELF, DNSP);
        if (elm != null) {
            return SubjectNode.SELF_URI;
        }

        elm = principalElm.getChild(E_PROPERTY, DNSP);
        if (elm != null) {
            if (elm.getChild(E_OWNER, DNSP) != null) {
                return SubjectNode.OWNER_URI;
            }
            else {
                throw new PreconditionViolationException(
                    new ViolatedPrecondition("only-onwer-property-supported", WebdavStatus.SC_CONFLICT), resourcePath
                );
            }
        }

        throw new PreconditionViolationException(
            new ViolatedPrecondition("could-not-determine-principal", WebdavStatus.SC_BAD_REQUEST), resourcePath
        );
    }

    private String createActionUri(Element privilegeElm) throws JDOMException, PreconditionViolationException, SlideException {
        Element privilegeChildElm = (Element)privilegeElm.getChildren().get(0);
        String privilegeName = privilegeChildElm.getName();
        String privilegeNamespace = privilegeChildElm.getNamespaceURI();
        if (E_ALL.equals(privilegeName) && S_DAV.equals(privilegeNamespace)) {
            return ActionNode.ALL_URI;
        }
        else {
            ObjectNode actions = structure.retrieve(slideToken, token.getNamespaceConfig().getActionsPath());
            ObjectNode action = findAction(actions.getChildren().iterator(), privilegeName, privilegeNamespace);
            return action != null ? action.getUri() : null;
        }
    }

    /**
     * Find an action in the repository based on its simple name and its namespace.
     *
     * @param actionsIterator An iterator over the actions collection to search in.
     * @param privilegeName The simple name of the action.
     * @param privilegeNamespace The name space of the action.
     * @return The action having the simple name and namespace.
     * @throws SlideException There was a problem accessing the actions in the repository.
     */
    private ObjectNode findAction(Iterator actionsIterator, String privilegeName, String privilegeNamespace) throws SlideException {
        ObjectNode result = null;
        while (result == null && actionsIterator.hasNext()) {
            String aUriAsString = (String) actionsIterator.next();
            ObjectNode aNode = structure.retrieve(slideToken, aUriAsString);
            if (aNode.hasChildren()) {
                result = findAction(aNode.getChildren().iterator(), privilegeName, privilegeNamespace);
            }
            else {
                Uri aUri = token.getUri(slideToken, aUriAsString);
                NodeRevisionDescriptors nrds = aUri.getStore().retrieveRevisionDescriptors(aUri);
                NodeRevisionDescriptor latestNrd = aUri.getStore().retrieveRevisionDescriptor(aUri, nrds.getLatestRevision());
                NodeProperty aNamespaceAsNode = latestNrd.getProperty(AclConstants.P_PRIVILEGE_NAMESPACE, WebdavConstants.S_DAV);
                String aNamespace = aNamespaceAsNode == null ? null : aNamespaceAsNode.getValue().toString();
                String aUriLastSegment = aUriAsString.substring(aUriAsString.lastIndexOf('/') + 1);
                if (aUriLastSegment.equals(privilegeName)
                        && ((aNamespace != null && privilegeNamespace.equals(aNamespace))
                                || (aNamespace == null && privilegeNamespace.equals(WebdavConstants.S_DAV)))) {
                    result = aNode;
                }
            }
        }
        return result;
    }

    /**
     * Method checkPreconditions
     *
     * @throws   PreconditionViolationException
     *
     */
    private void checkPreconditions() throws PreconditionViolationException, ServiceAccessException {
        resp.setStatus( WebdavStatus.SC_OK );
    }

    /**
     * Execute the request.
     *
     * @exception WebdavException
     */
    protected void executeRequest() throws WebdavException, IOException {

        // Prevent dirty reads
        slideToken.setForceStoreEnlistment(true);

        // check lock-null resources
        try {
            if (isLockNull(resourcePath)) {
                int statusCode = WebdavStatus.SC_NOT_FOUND;
                sendError( statusCode, "lock-null resource", new Object[]{resourcePath} );
                throw new WebdavException( statusCode );
            }
        }
        catch (ServiceAccessException e) {
            int statusCode = getErrorCode( e );
            sendError( statusCode, e );
            throw new WebdavException( statusCode );
        }

        try {
            if ( WebdavEvent.ACL.isEnabled() ) EventDispatcher.getInstance().fireVetoableEvent(WebdavEvent.ACL, new WebdavEvent(this));

            checkPreconditions();
            security.setPermissions(slideToken, resourcePath, permissions.elements());
        }
        catch (PreconditionViolationException e) {
            sendPreconditionViolation(e);
            throw e;
        }
        catch (Exception e) {
            int statusCode = getErrorCode( e );
            sendError( statusCode, e );
            throw new WebdavException( statusCode );
        }
    }
}
