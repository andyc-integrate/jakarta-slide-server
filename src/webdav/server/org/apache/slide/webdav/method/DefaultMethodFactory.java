/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/method/DefaultMethodFactory.java,v 1.2 2006-01-22 22:55:20 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:20 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.method;

import org.apache.slide.common.NamespaceAccessToken;
import org.apache.slide.util.Configuration;
import org.apache.slide.webdav.WebdavMethod;
import org.apache.slide.webdav.WebdavMethodFactory;
import org.apache.slide.webdav.WebdavServlet;
import org.apache.slide.webdav.WebdavServletConfig;

/**
 * The default factory for WebDAVMethod implementations.
 *
 */
public class DefaultMethodFactory
    extends WebdavMethodFactory {
    
    
    // ----------------------------------------------------- Instance Variables
    
    
    /**
     * Configuration of the WebDAV servlet.
     */
    private WebdavServletConfig config;
    
    
    /**
     * The token for accessing the namespace.
     */
    private NamespaceAccessToken token;
    
    
    // ------------------------------------- WebdavMethodFactory Implementation
    
    
    // inherit javadocs
    public WebdavMethod createMethod(String name) {
        
        if ((config == null) || (token == null)) {
            throw new IllegalStateException();
        }
        
        if (name.equals("GET")) {
            return new GetMethod(token, config);
        } else if (name.equals("PROPFIND")) {
            return new PropFindMethod(token, config);
        } else if (name.equals("HEAD")) {
            return new HeadMethod(token, config);
        } else if (name.equals("LOCK")) {
            return new LockMethod(token, config);
        } else if (name.equals("UNLOCK")) {
            return new UnlockMethod(token, config);
        } else if (name.equals("OPTIONS")) {
            return new OptionsMethod(token, config);
        } else if (name.equals("PUT")) {
            return new PutMethod(token, config);
        } else if (name.equals("MKCOL")) {
            return new MkcolMethod(token, config);
        } else if (name.equals("POST")) {
            return new PostMethod(token, config);
        } else if (name.equals("COPY")) {
            return new CopyMethod(token, config);
        } else if (name.equals("MOVE")) {
            return new MoveMethod(token, config);
        } else if (name.equals("DELETE")) {
            return new DeleteMethod(token, config);
        } else if (name.equals("PROPPATCH")) {
            return new PropPatchMethod(token, config);
        } else if (name.equals("REPORT")) {
            return new ReportMethod(token, config);
        } else if (name.equals("SUBSCRIBE")) {
            return new SubscribeMethod(token, config);
        } else if (name.equals("UNSUBSCRIBE")) {
            return new UnsubscribeMethod(token, config);
        } else if (name.equals("POLL")) {
            return new PollMethod(token, config);
        } else if (name.equals("EVENT")) {
            return new EventMethod(token, config);
        } else {
            if (Configuration.useIntegratedSecurity()) {
                if (name.equals("ACL")) {
                    return new AclMethod(token, config);
                }
            }
            if (Configuration.useSearch()) {
                if (name.equals("SEARCH")) {
                    return new SearchMethod(token, config);
                }
            }
            if (Configuration.useVersionControl()) {
                if (name.equals("VERSION-CONTROL")) {
                    return new VersionControlMethod(token, config);
                } else if (name.equals("CHECKIN")) {
                    return new CheckinMethod(token, config);
                } else if (name.equals("CHECKOUT")) {
                    return new CheckoutMethod(token, config);
                } else if (name.equals("UNCHECKOUT")) {
                    return new UncheckoutMethod(token, config);
                } else if (name.equals("MKWORKSPACE")) {
                    return new MkworkspaceMethod(token, config);
                } else if (name.equals("LABEL")) {
                    return new LabelMethod(token, config);
                } else if (name.equals("UPDATE")) {
                    return new UpdateMethod(token, config);
                }
            }
            if (Configuration.useGlobalBinding()) {
                if (name.equals("BIND")) {
                    return new BindMethod(token, config);
                } else if (name.equals("UNBIND")) {
                    return new UnbindMethod(token, config);
                } else if (name.equals("REBIND")) {
                    return new RebindMethod(token, config);
                }
            }
        }
        
        return null;
    }
    
    /**
     * Returns the value of a boolean init parameter of the servlet.
     * Default value: true.
     */
    protected boolean getBooleanInitParameter( String name ) {
        return !"false".equalsIgnoreCase( config.getInitParameter(name) );
    }
    
    
    // inherit javadocs
    protected void setConfig(WebdavServletConfig config) {
        
        this.config = config;
        token = (NamespaceAccessToken)config.getServletContext().getAttribute
            (WebdavServlet.ATTRIBUTE_NAME);
    }
    
}

