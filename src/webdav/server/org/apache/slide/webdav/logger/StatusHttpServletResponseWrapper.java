/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/logger/StatusHttpServletResponseWrapper.java,v 1.2 2006-01-22 22:55:20 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:20 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


package org.apache.slide.webdav.logger;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.apache.slide.common.Domain;

/**
 * This class supports additional set-methods and a re-readable
 * inputstream to the interface javax.servlet.http.HttpServletResponse.
 *
 *            Christopher Lenz (cmlenz at apache.org)
 *
 * @version   0.1
 *
 * @invariant (inputStream != null)
 *
 * @see       javax.servlet.http.HttpServletResponse
 *
 */
public class StatusHttpServletResponseWrapper extends HttpServletResponseWrapper
{

    
    private int statusCode = -1;
    private String statusText = "";
    
    /**
     * This constructor creates an re-writable HttpServletResüpmse.
     *
     * @pre        (response != null)
     * @post
     *
     * @param      response   HttpServletResponse
     *
     * @time
     * @space
     */
    public StatusHttpServletResponseWrapper(HttpServletResponse response) {
        super(response);
        Domain.debug("Create XHttpServletResponseFacade");
    }


    public int getStatus() {
        return statusCode;
    }
    public String getStatusText() {
        return statusText;
    }
    public void setStatus(int sc) {
        statusCode = sc;
        super.setStatus(sc);
    }
    public void setStatus(int sc, String msg) {
        statusCode = sc;
        statusText = msg;
        super.setStatus(sc, msg);
    }
    public void sendError( int sc ) throws IOException {
        statusCode = sc;
        super.sendError(sc);
    }
    public void sendError( int sc, String msg ) throws IOException {
        statusCode = sc;
        statusText = msg;
        super.sendError(sc, msg);
    }
}
