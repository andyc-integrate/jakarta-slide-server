/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/logger/XServletInputStreamFacade.java,v 1.2 2006-01-22 22:55:20 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:20 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


package org.apache.slide.webdav.logger;

import java.io.IOException;

import javax.servlet.ServletInputStream;

import org.apache.slide.common.Domain;

/**
 * This class is to buffer an ServletInputStream.
 *
 */
public class XServletInputStreamFacade extends ServletInputStream {
    private ServletInputStream in;
    private XByteBuffer byteBuf;

    public XServletInputStreamFacade( ServletInputStream in ) {
        if ( Domain.isInitialized()) Domain.debug("Create XServletInputStreamFacade");
        this.in = in;
        byteBuf = new XByteBuffer();
    }



    public int read() throws IOException {
        if ( Domain.isInitialized())Domain.debug("ENTER: XServletInputStreamFacade:read()");
        int result = in.read();
        byteBuf.write(result);
        if ( Domain.isInitialized())Domain.debug("LEAVE: XServletInputStreamFacade:read() result = " + result );
        return result;
    }

    public int read(byte[] b) throws IOException {
        if ( Domain.isInitialized()) Domain.debug("ENTER: XServletInputStreamFacade:read(byte[] b)");
        int result = in.read(b);
        byteBuf.write( b, 0, result );
        if ( Domain.isInitialized()) Domain.debug("LEAVE: XServletInputStreamFacade:read(byte[] b) result = " + result );
        return result;
    }

    public int read(byte[] b, int off, int len) throws IOException {
        if ( Domain.isInitialized()) Domain.debug("ENTER: XServletInputStreamFacade:read(byte[] b, off="+off+" len="+len+" )");
        int result = in.read(b, off, len);
        byteBuf.write( b, off, result );
        if ( Domain.isInitialized()) Domain.debug("LEAVE: XServletInputStreamFacade:read(byte[] b, off="+off+" len="+len+" ) result = " + result );
        return result;
    }

    /**
     * See the general contract of the <code>skip</code>
     * method of <code>InputStream</code>.
     *
     * @param      n   the number of bytes to be skipped.
     * @return     the actual number of bytes skipped.
     * @exception  IOException  if an I/O error occurs.
     */
    public long skip(long n) throws IOException {
        return in.skip(n);
    }

    /**
     * Returns the number of bytes that can be read from this input
     * stream without blocking.
     * <p>
     * The <code>available</code> method of
     * <code>BufferedInputStream</code> returns the sum of the the number
     * of bytes remaining to be read in the buffer
     * (<code>count&nbsp;- pos</code>)
     * and the result of calling the <code>available</code> method of the
     * underlying input stream.
     *
     * @return     the number of bytes that can be read from this input
     *             stream without blocking.
     * @exception  IOException  if an I/O error occurs.
     * @see        java.io.FilterInputStream#in
     */
    public synchronized int available() throws IOException {
        return in.available();
    }

    /**
     * See the general contract of the <code>mark</code>
     * method of <code>InputStream</code>.
     *
     * @param   readlimit   the maximum limit of bytes that can be read before
     *                      the mark position becomes invalid.
     * @see     java.io.BufferedInputStream#reset()
     */
    public synchronized void mark(int readlimit) {
        in.mark(readlimit);
    }

    /**
     * See the general contract of the <code>reset</code>
     * method of <code>InputStream</code>.
     * <p>
     * If <code>markpos</code> is <code>-1</code>
     * (no mark has been set or the mark has been
     * invalidated), an <code>IOException</code>
     * is thrown. Otherwise, <code>pos</code> is
     * set equal to <code>markpos</code>.
     *
     * @exception  IOException  if this stream has not been marked or
     *               if the mark has been invalidated.
     * @see        java.io.BufferedInputStream#mark(int)
     */
    public synchronized void reset() throws IOException {
        in.reset();
    }

    /**
     * Tests if this input stream supports the <code>mark</code>
     * and <code>reset</code> methods. The <code>markSupported</code>
     * method of <code>BufferedInputStream</code> returns
     * <code>true</code>.
     *
     * @return  a <code>boolean</code> indicating if this stream type supports
     *          the <code>mark</code> and <code>reset</code> methods.
     * @see     java.io.InputStream#mark(int)
     * @see     java.io.InputStream#reset()
     */
    public boolean markSupported() {
        return in.markSupported();
    }

    /**
     * Closes this input stream and releases any system resources
     * associated with the stream.
     *
     * @exception  IOException  if an I/O error occurs.
     */
    public void close() throws IOException {
        in.close();
    }

    /**
     * Get content of the read input stream saved in the buffer.
     **/
    public String getBufferContent() {
        return byteBuf.getBufferContent();
    }

    /**
     * Get number of bytes read from the input stream saved in the buffer.
     **/
    public int getNumberOfBytesWritten() {
        return byteBuf.getNumberOfBytesWritten();
    }

}
