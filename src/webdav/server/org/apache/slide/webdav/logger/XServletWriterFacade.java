/*
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


package org.apache.slide.webdav.logger;

import java.io.PrintWriter;
import java.io.Writer;

/**
 *  Facade to the PrintWriter returned by Response.
 *  This will grow to include more support for JSPs ( and other templating
 *  systems ) buffering requirements, provide support for accounting
 *  and will allow more control over char-to-byte conversion ( if it proves
 *  that we spend too much time in that area ).
 *
 *  This will also help us control the multi-buffering ( since all writers have
 *  8k or more of un-recyclable buffers).
 *
 */
public class XServletWriterFacade extends PrintWriter {
    
    protected static final int DEFAULT_BUFFER_SIZE = 8*1024;
    int defaultBufferSize = DEFAULT_BUFFER_SIZE;
    int bytesWritten = 0;

    /** The buffer
     */
    public StringBuffer buffer;

    public XServletWriterFacade( Writer w ) {
        super( w );
//      buf = new byte[defaultBufferSize];
        buffer = new StringBuffer();
    }

    // -------------------- Write methods --------------------

    public void flush() {
    super.flush();
    }

    public void print( String str ) {
        buffer = buffer.append(str);
        super.print( str );
   }

    public void println( String str ) {
        buffer = buffer.append(str + "\n");
        super.println( str );
   }

    public void write( char buf[], int offset, int count ) {
        buffer = buffer.append( buf, offset, count );
        super.write( buf, offset, count );
    }

    public void write( String str ) {
        buffer = buffer.append(str);
        super.write( str );
    }

    public String getOutputBuffer() {
        return buffer.toString();
    }

}

