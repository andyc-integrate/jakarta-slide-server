/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/filter/XmlLogFilter.java,v 1.2 2006-01-22 22:55:20 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:20 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.filter;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.slide.webdav.logger.XHttpServletRequestFacade;
import org.apache.slide.webdav.logger.XHttpServletResponseFacade;
import org.apache.slide.webdav.logger.XMLTestCaseGenerator;

/**
 * A servlet filter for detailed XML logging.
 *
 * @version $Revision: 1.2 $
 */
public class XmlLogFilter implements Filter {
    
    FilterConfig config;
    boolean outputToFile = false;
    String outputFilePath = null;
    File outputFile = null;
    BufferedOutputStream fout = null;
    
    /**
     * Interface implementation
     *
     * @param    config              a  FilterConfig
     *
     * @throws   ServletException
     *
     */
    public void init(FilterConfig config) throws ServletException {
        this.config = config;
        // get the init parms
        String p;
        p = config.getInitParameter( "outputToFile" );
        if( p != null && !"".equals(p) ) {
            outputFilePath = p;
            outputFile = new File( outputFilePath );
            try {
                if( outputFile.canWrite() || outputFile.createNewFile() ) {
                    fout = new BufferedOutputStream( new FileOutputStream(outputFilePath, true) );
                    outputToFile = true;
                }
            }
            catch (IOException e) {}
        }
    }
    
    /**
     * Interface implementation
     *
     * @param    req                 a  ServletRequest
     * @param    resp                a  ServletResponse
     * @param    chain               a  FilterChain
     *
     * @throws   IOException
     * @throws   ServletException
     *
     */
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
        XHttpServletRequestFacade reqFac = new XHttpServletRequestFacade((HttpServletRequest)req);
        XHttpServletResponseFacade respFac = new XHttpServletResponseFacade((HttpServletResponse)resp);
        chain.doFilter( reqFac, respFac );
        logXML( reqFac, respFac );
    }
    
    /**
     * Log one line.
     *
     * @param    req                 a  XHttpServletRequestFacade
     * @param    resp                a  XHttpServletResponseFacade
     *
     * @throws   IOException
     *
     */
    private void logXML( XHttpServletRequestFacade req, XHttpServletResponseFacade resp ) throws IOException {
        String thread = Thread.currentThread().getName();
        XMLTestCaseGenerator xmlGen = new XMLTestCaseGenerator( req, resp );
        xmlGen.setThreadName( thread );
        
        if( outputToFile ) {
            fout.write( xmlGen.toString().getBytes("UTF-8") );
            fout.flush();
        }
    }
    
    /**
     * Interface implementation.
     *
     */
    public void destroy() {
        try {
            if( outputToFile )
                fout.close();
        }
        catch (IOException e) {}
    }
}

