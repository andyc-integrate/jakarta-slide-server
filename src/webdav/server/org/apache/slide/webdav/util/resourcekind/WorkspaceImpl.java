/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/util/resourcekind/WorkspaceImpl.java,v 1.2 2006-01-22 22:55:21 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:21 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.util.resourcekind;

import java.util.Set;


public class WorkspaceImpl extends AbstractResourceKind implements Workspace {
    
    protected static ResourceKind singleton = null;
    
    /**
     * Factory method.
     */
    static public ResourceKind getInstance() {
        if( singleton == null )
            singleton = new WorkspaceImpl();
        return singleton;
    }
    
    /**
     * Protected constructor
     */
    protected WorkspaceImpl() {
    }

    /**
     * Get the set properties supported by this resource kind.
     * @param filter Q_PROTECTED_ONLY or Q_COMPUTED_ONLY (no filtering if null)
     * @param excludedFeatures array of F_* constants (no filtering if null or empty)
     * @see org.apache.slide.webdav.util.WebdavConstants
     * @see org.apache.slide.webdav.util.DeltavConstants
     * @see org.apache.slide.webdav.util.AclConstants
     * @see org.apache.slide.webdav.util.DaslConstants
     */
    public Set getSupportedLiveProperties( String[] excludedFeatures ) {
        Set s = super.getSupportedLiveProperties( excludedFeatures );
        if( isSupportedFeature(F_WORKSPACE, excludedFeatures) ) {
            s.add( P_WORKSPACE_CHECKOUT_SET );
            if( isSupportedFeature(F_BASELINE, excludedFeatures) )
                s.add( P_BASELINE_CONTROLLED_COLLECTION_SET );
            if( isSupportedFeature(F_ACTIVITY, excludedFeatures) )
                s.add( P_CURRENT_ACTIVITY_SET );
        }
        return s;
    }

    /**
     * Get the set methods supported by this resource kind.
     */
    public Set getSupportedMethods() {
        Set s = super.getSupportedMethods();
        return s;
    }

    /**
     * Get the set reports supported by this resource kind.
     */
    public Set getSupportedReports() {
        Set s = super.getSupportedReports();
        return s;
    }
    
    /**
     *
     */
    public String toString() {
        return "workspace";
    }
}

