/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/util/DaslConstants.java,v 1.2 2006-01-22 22:55:20 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:20 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.util;

/**
 * DASL constants.
 *
 */
public interface DaslConstants extends WebdavConstants {

    /** Features */
    String F_SEARCHING_AND_LOCATING         = "searching-and-locating";
    
    /** Live Properties */

    /** Methods */
    String M_SEARCH                          = "SEARCH";

    /** Reposrts */

    /** XML Elements */
    String E_BASICSEARCH             = "basicsearch";
    String E_FROM                    = "from";
    String E_ISDEFINED               = "is-defined";
    String E_LITERAL                 = "literal";
    String E_SCOPE                   = "scope";
    String E_SEARCHREQUEST           = "searchrequest";
    String E_SELECT                  = "select";
    String E_WHERE                   = "where";

    
    /** Inofficial elements used by slide */
    String E_EXCLUDE                 = "exclude";
    String E_PROPCONTAINS            = "propcontains";

    /** XML Attributes */
    
    /** Resource Kinds */
}

