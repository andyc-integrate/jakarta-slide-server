/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/util/AclConstants.java,v 1.2 2006-01-22 22:55:20 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:20 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.slide.content.NodeProperty.NamespaceCache;
import org.apache.slide.search.basic.Literals;

/**
 * ACL constants.
 *
 */
public interface AclConstants extends WebdavConstants {
    
    /** Resource Kinds */
    String K_PRINCIPAL                       = "principal";

    /** Features */
    String F_ACCESS_CONTROL                  = "access-control";
    
    /** Live Properties */
    String P_ALTERNATE_URI_SET               = "alternate-URI-set";
    String P_PRINCIPAL_URL                   = "principal-URL";
    String P_GROUP_MEMBER_SET                = "group-member-set";
    String P_GROUP_MEMBERSHIP                = "group-membership";
    String P_OWNER                           = "owner";
    String P_MODIFICATIONUSER                = "modificationuser";
    String P_CREATIONUSER                    = "creationuser";
    String P_SUPPORTED_PRIVILEGE_SET         = "supported-privilege-set";
    String P_CURRENT_USER_PRIVILEGE_SET      = "current-user-privilege-set";
    String P_ACL                             = "acl";
    String P_ACL_RESTRICTIONS                = "acl-restrictions";
    String P_INHERITED_ACL_SET               = "inherited-acl-set";
    String P_PRINCIPAL_COLLECTION_SET        = "principal-collection-set";
    String P_PRIVILEGE_COLLECTION_SET        = "privilege-collection-set";
    String P_PRIVILEGE_MEMBER_SET            = "privilege-member-set";
    String P_PRIVILEGE_MEMBERSHIP            = "privilege-membership";
    String P_PRIVILEGE_NAMESPACE             = "privilege-namespace";

    String[] ACL_PROPERTIES = new String[] {
        P_ALTERNATE_URI_SET,
            P_PRINCIPAL_URL,
            P_GROUP_MEMBER_SET,
            P_GROUP_MEMBERSHIP,
        P_OWNER,
            P_CREATIONUSER,
        P_MODIFICATIONUSER,
        P_SUPPORTED_PRIVILEGE_SET,
        P_CURRENT_USER_PRIVILEGE_SET,
        P_ACL,
            P_ACL_RESTRICTIONS,
            P_INHERITED_ACL_SET,
            P_PRINCIPAL_COLLECTION_SET,
            P_PRIVILEGE_COLLECTION_SET,
            P_PRIVILEGE_MEMBER_SET,
            P_PRIVILEGE_MEMBERSHIP
    };
        
    List ACL_PROPERTY_LIST = Collections.unmodifiableList(Arrays.asList(ACL_PROPERTIES));
    
    /** Methods */
    String M_ACL                             = "ACL";

    /** Reports */
    String R_ACL_PRINCIPAL_PROP_SET          = "acl-principal-prop-set";
    String R_PRINCIPAL_MATCH                 = "principal-match";
    String R_PRINCIPAL_PROPERTY_SEARCH       = "principal-property-search";
    String R_PRINCIPAL_SEARCH_PROPERTY_SET   = "principal-search-property-set";

    /** XML Elements */
    String E_ACE                             = "ace";
    String E_ALL                             = "all";
    String E_AUTHENTICATED                   = "authenticated";
    String E_CASELESS_SUBSTRING              = "caseless-substring";
    String E_CREATE_OBJECT                   = "create-object";
    String E_CREATE_REVISION_CONTENT         = "create-revision-content";
    String E_CREATE_REVISION_METADATA        = "create-revision-metadata";
    String E_DENY                            = "deny";
    String E_DESCRIPTION                     = "description";
    String E_GRANT                           = "grant";
    String E_GRANT_PERMISSION                = "grant-premission";
    String E_INHERITED                       = "inherited";
    String E_INVERT                          = "invert";
    String E_LOCK_OBJECT                     = "lock-object";
    String E_MATCH                           = "match";
    String E_MODIFY_REVISION_CONTENT         = "modify-revision-content";
    String E_MODIFY_REVISION_METADATA        = "modify-revision-metadata";
    String E_NON_SEARCHABLE_PROPERTY         = "non-searchable-property";
    String E_PRINCIPAL                       = "principal";
    String E_PRINCIPAL_MATCH                 = "principal-match";
    String E_PRINCIPAL_PROPERTY              = "principal-property";
    String E_PRINCIPAL_SEARCH_PROPERTY       = "principal-search-property";
    String E_PRINCIPAL_SEARCH_PROPERTY_SET   = "principal-search-property-set";
    String E_PRINCIPAL_URL                   = "principal-URL";
    String E_PRIVILEGE                       = "privilege";
    String E_PROPERTY_SEARCH                 = "property-search";
    String E_PROTECTED                       = "protected";
    String E_READ                            = "read";
    String E_READ_ACL                        = "read-acl";
    String E_READ_LOCKS                      = "read-locks";
    String E_READ_OBJECT                     = "read-object";
    String E_READ_REVISION_CONTENT           = "read-revision-content";
    String E_READ_REVISION_METADATA          = "read-revision-metadata";
    String E_REMOVE_OBJECT                   = "remove-object";
    String E_REMOVE_REVISION_CONTENT         = "remove-revision-content";
    String E_REMOVE_REVISION_METADATA        = "remove-revision-metadata";
    String E_REQUIRED_PRINCIPAL              = "required-principal";
    String E_REVOKE_PERMISSION               = "revoke-premission";
    String E_SELF                            = "self";
    String E_SUBSTRING                       = "substring";
    String E_SUPPORTED_PRIVILEGE             = "supported-privilege";
    String E_UNAUTHENTICATED                 = "unauthenticated";
    String E_WRITE_ACL                       = "write-acl";
    

    /** Constant helpers */
    String C_HREF_OPEN                       = "<D:href>";
    String C_HREF_CLOSE                       = "</D:href>";
    String C_SEARCHREQUEST_OPEN              = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><searchrequest xmlns:D=\"DAV:\"><D:basicsearch>";
    String C_SEARCHREQUEST_CLOSE             = "</D:basicsearch></searchrequest>";
    String C_SELECT_OPEN                     = "<D:select>";
    String C_SELECT_CLOSE                    = "</D:select>";
    String C_WHERE_ISPRINCIPAL               = "<D:where><" + Literals.ISPRINCIPAL + " xmlns=\"" + NamespaceCache.SLIDE_URI + "\"/></D:where>";
    String C_PROP_PRINCIPAL_URL               = "<D:prop><D:href><D:principal-URL/></D:href></D:prop>";
    String C_LITERAL_OPEN                    = "<D:literal>";
    String C_LITERAL_CLOSE                   = "</D:literal>";
    String C_PROP_OPEN                       = "<D:prop>";
    String C_PROP_OPEN_NS_OPEN               = "<D:prop xmlns:Y=\"";
    String C_PROP_OPEN_NS_CLOSE              = "\">";
    String C_PROP_CLOSE                      = "</D:prop>";
    String C_X_PREFIX                        = "Y:";
    String C_PROPCONTAINS_OPEN               = "<X:propcontains xmlns:X=\"http://jakarta.apache.org/slide/\">";
    String C_PROPCONTAINS_CLOSE              = "</X:propcontains>";
    String C_WHERE_OPEN                      = "<D:where><D:and>";
    String C_WHERE_CLOSE                     = "</D:and></D:where>";
    String C_FROM_SCOPE_OPEN                 = "<D:from><D:scope><D:href>";
    String C_FROM_SCOPE_CLOSE                = "</D:href></D:scope></D:from>";
    String C_DISPLAYNAME                     = "displayname";

    
    /* The PRINCIPAL-SEARCH-PROPERTY-SET has to be in the following way:
     * C_PRINCIPAL_SEARCH_PROPERTY_SET   = "property1,namespace1,description1/property2,namspace2,description2/";
     */
    String C_PRINCIPAL_SEARCH_PROPERTY_SET   = "displayname,DAV:,Full name/";
    
    String C_SEARCH_PROPERTY_SET_SEPERATOR   = "/";

}




