/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/util/PreconditionViolationException.java,v 1.2 2006-01-22 22:55:21 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:21 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.webdav.util;

// import list
import org.apache.slide.webdav.WebdavException;

/**
 * This exception is thrown due to a violated precondition.
 *
 * @version $Revision: 1.2 $
 *
 **/
public class PreconditionViolationException extends WebdavException {
    
    /**
     *  The precondition that has been violated.
     */
    protected ViolatedPrecondition violatedPrecondition = null;
    
    /**
     * The URI of the resource associated with the precondition violation.
     */
    protected String objectUri = null;
    
    
    /**
     * Creates a PreconditionViolationException.
     *
     * @param      violatedPrecondition  the precondition that has been violated.
     * @param      objectUri             the URI of the resource associated with
     *                                   the precondition violation.
     */
    public PreconditionViolationException(ViolatedPrecondition violatedPrecondition,
                                         String objectUri) {
        super(violatedPrecondition.getStatusCode());
        this.violatedPrecondition = violatedPrecondition;
        this.objectUri = objectUri;
    }
    
    /**
     * Returns the precondition that has been violated.
     *
     * @return    precondition that has been violated.
     */
    public ViolatedPrecondition getViolatedPrecondition() {
        return violatedPrecondition;
    }
    
    /**
     * Returns the URI of the resource associated with the precondition violation.
     *
     * @return    the URI of the resource associated with the precondition violation.
     */
    public String getObjectUri() {
        return objectUri;
    }
    
}

