/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/util/WorkingresourcePathHandler.java,v 1.2 2006-01-22 22:55:21 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:21 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.slide.common.Domain;

public class WorkingresourcePathHandler extends UriHandler {
    
    final static String WORKINGRESOURCE_PATH =
        Domain.getParameter( I_WORKINGRESOURCEPATH, I_WORKINGRESOURCEPATH_DEFAULT );
    
    static WorkingresourcePathHandler workingresourcePathHandler =
        new WorkingresourcePathHandler( WORKINGRESOURCE_PATH );
    
    static boolean parameterized = (WORKINGRESOURCE_PATH.indexOf(I_STORE_PLACE_HOLDER_IN_PATH) >= 0);
    
    /**
     * Factory method.
     */
    public static WorkingresourcePathHandler getWorkingresourcePathHandler() {
        return workingresourcePathHandler;
        }
    
    /**
     * Get a resolved UriHandler for this WorkingresourcePathHandler.
     * @param namespaceName the namespace name
     * @param uh an URI required to determine the associated base store;
     *           usually the URI of the version resource being checked out.
     */
    public static UriHandler getResolvedWorkingresourcePathHandler( String namespaceName, UriHandler uh ) {
        if( parameterized ) {
            // requires URI to determine associated base store; usually URI of version being checked-out.
            // NOTE: if the workingresource path is parameterized but the history path is not ... we have trouble :-(
            return getResolvedWorkingresourcePathHandler( uh.getAssociatedBaseStoreName(namespaceName) );
        }
        else
            return workingresourcePathHandler;
    }
    
    /**
     * Get a resolved UriHandler for this WorkingresourcePathHandler.
     */
    public static UriHandler getResolvedWorkingresourcePathHandler( String storeName ) {
        if( parameterized ) {
        StringBuffer b;
        String rp = workingresourcePathHandler.toString();
        int k = rp.indexOf( I_STORE_PLACE_HOLDER_IN_PATH );
        if( k >= 0 ) {
            b = new StringBuffer( rp );
            while( k >= 0 ) {
                b.replace( k, k + I_STORE_PLACE_HOLDER_IN_PATH.length(), storeName );
                k = b.toString().indexOf(I_STORE_PLACE_HOLDER_IN_PATH);
            }
            rp = b.toString();
        }
        return new UriHandler(rp);
        }
        else
            return workingresourcePathHandler;
        }
    
    /**
     * Factory method.
     */
    public static String getWorkingresourcePath() {
        return WORKINGRESOURCE_PATH;
    }
    
    
    private Set resolvedWorkingresourcePaths = null;
    
    /**
     * Protected constructor
     */
    protected WorkingresourcePathHandler( String uri ) {
        super( uri );
    }
    
    /**
     * Return true if the specified URI is a valid workingresource path URI
     */
    public boolean isWorkingresourcePathUri( UriHandler uh ) {
        if( !parameterized )
            return equals( uh );
        
        if( !Domain.namespacesAreInitialized() )
            return false;
        
        if( resolvedWorkingresourcePaths == null )
            resolve();
        
        return resolvedWorkingresourcePaths.contains( uh );
}

    /**
     * Return the resolved workingresource paths
     */
    public List getResolvedWorkingresourcePaths() {
        List result;
        if( parameterized ) {
            resolve();
            result = new ArrayList( resolvedWorkingresourcePaths );
        }
        else {
            result = new ArrayList();
            result.add( WORKINGRESOURCE_PATH );
        }
        return result;
    }

    /**
     * Resolve this workingresource path handler
     */
    private void resolve() {
        resolvedWorkingresourcePaths = new HashSet();
        Iterator i = allStoreNames.iterator();
        while( i.hasNext() ) {
            String storeName = (String)i.next();
            UriHandler rpuh = getResolvedWorkingresourcePathHandler( storeName );
            if( allScopes.contains(rpuh) )
                resolvedWorkingresourcePaths.add( rpuh );
        }
}
}

