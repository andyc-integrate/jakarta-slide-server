/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/util/WorkspacePathHandler.java,v 1.2 2006-01-22 22:55:21 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:21 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.slide.common.Domain;

public class WorkspacePathHandler extends UriHandler {
    
    final static String WORKSPACE_PATH =
        Domain.getParameter( I_WORKSPACEPATH, I_WORKSPACEPATH_DEFAULT );

    static WorkspacePathHandler workspacePathHandler =
        new WorkspacePathHandler( WORKSPACE_PATH );

    static boolean parameterized = (WORKSPACE_PATH.indexOf(I_STORE_PLACE_HOLDER_IN_PATH) >= 0);

    
    /**
     * Factory method.
     */
    public static WorkspacePathHandler getWorkspacePathHandler() {
        return workspacePathHandler;
        }
    
    /**
     * Get a resolved UriHandler for this WorkspacePathHandler.
     */
    public static UriHandler getResolvedWorkspacePathHandler( String namespaceName, UriHandler uh ) {
        if( parameterized ) {
            return getResolvedWorkspacePathHandler( uh.getAssociatedBaseStoreName(namespaceName) );
        }
        else
            return workspacePathHandler;
    }
    
    /**
     * Get a resolved UriHandler for this WorkspacePathHandler.
     */
    public static UriHandler getResolvedWorkspacePathHandler( String storeName ) {
        if( parameterized ) {
        StringBuffer b;
        String rp = workspacePathHandler.toString();
        int k = rp.indexOf( I_STORE_PLACE_HOLDER_IN_PATH );
        if( k >= 0 ) {
            b = new StringBuffer( rp );
            while( k >= 0 ) {
                b.replace( k, k + I_STORE_PLACE_HOLDER_IN_PATH.length(), storeName );
                k = b.toString().indexOf(I_STORE_PLACE_HOLDER_IN_PATH);
            }
            rp = b.toString();
        }
        return new UriHandler(rp);
    }
        else
            return workspacePathHandler;
    }
    
    /**
     * Factory method.
     */
    public static String getWorkspacePath() {
        return WORKSPACE_PATH;
    }
    
    
    private Set resolvedWorkspacePaths = null;
    
    /**
     * Protected constructor
     */
    protected WorkspacePathHandler( String uri ) {
        super( uri );
    }
    
    /**
     * Return true if the specified URI is a valid workspace path URI
     */
    public boolean isWorkspacePathUri( UriHandler uh ) {
        if( !parameterized )
            return equals( uh );
        
        if( !Domain.namespacesAreInitialized() )
            return false;

        if( resolvedWorkspacePaths == null )
            resolve();

        return resolvedWorkspacePaths.contains( uh );
    }
    
    /**
     * Return the resolved workspace paths
     */
    public List getResolvedWorkspacePaths() {
        return getResolvedWorkspacePaths( null );
    }
    
    /**
     * Return the resolved workspace paths
     */
    public List getResolvedWorkspacePaths( String storeName ) {
        List result;
        if( parameterized ) {
            if( storeName != null ) {
                result = new ArrayList();
                result.add( getResolvedWorkspacePathHandler(storeName) );
            }
            else {
                resolve();
                result = new ArrayList( resolvedWorkspacePaths );
            }
        }
        else {
            result = new ArrayList();
            result.add( WORKSPACE_PATH );
        }
        return result;
    }

    /**
     * Resolve this workspace path handler
     */
    private void resolve() {
        resolvedWorkspacePaths = new HashSet();
        Iterator i = allStoreNames.iterator();
        while( i.hasNext() ) {
            String storeName = (String)i.next();
            UriHandler rpuh = getResolvedWorkspacePathHandler( storeName );
            if( allScopes.contains(rpuh) )
                resolvedWorkspacePaths.add( rpuh );
        }
    }
}




