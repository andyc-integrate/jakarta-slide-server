/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/util/BindConstants.java,v 1.3 2006-04-19 15:06:55 peter-cvs Exp $
 * $Revision: 1.3 $
 * $Date: 2006-04-19 15:06:55 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Bind constants.
 *
 */
public interface BindConstants extends WebdavConstants {

    /** URI schemes */
    String S_RESOURCE_ID                             = "urn:uuid:";

    /** Features */
    String F_BINDING                                 = "binding";

    /** Headers */

    /** XML Elements */
    String E_BIND                                    = "bind";
    String E_UNBIND                                  = "unbind";
    String E_REBIND                                  = "rebind";
    String E_PARENT                                  = "parent";
    String E_SEGMENT                                 = "segment";

    /** XML Attributes */

    /** Property filters */

    /** Live Properties */
    String P_RESOURCE_ID                             = "resource-id";
    String P_PARENT_SET                              = "parent-set";

    String[] BIND_PROPERTIES = new String[] {
        P_RESOURCE_ID,
            P_PARENT_SET
    };

    List BIND_PROPERTY_LIST = Collections.unmodifiableList(Arrays.asList(BIND_PROPERTIES));

    /** Methods */
    String M_BIND                                    = "BIND";
    String M_UNBIND                                  = "UNBIND";
    String M_REBIND                                  = "REBIND";

    /** Reports */

    /** Pre- and postconditions */
    String C_BIND_INTO_COLLECTION                    = "bind-into-collection";
    String C_BIND_SOURCE_EXISTS                      = "bind-source-exists";
    String C_BINDING_ALLOWED                         = "binding-allowed";
    String C_BINDING_DELETED                         = "binding-deleted";
    String C_CAN_OVERWRITE                           = "can-overwrite";
    String C_CROSS_SERVER_BINDING                    = "cross-server-binding";
    String C_CYCLE_ALLOWED                           = "cycle-allowed";
    String C_LOCKED_OVERWRITE_ALLOWED                = "locked-overwrite-allowed";
    String C_LOCKED_SOURCE_COLLECTION_UPDATE_ALLOWED = "locked-source-collection-update-allowed";
    String C_LOCKED_UPDATE_ALLOWED                   = "locked-update-allowed";
    String C_NAME_ALLOWED                            = "name-allowed";
    String C_NEW_BINDING                             = "new-binding";
    String C_PROTECTED_SOURCE_URL_DELETION_ALLOWED   = "protected-source-path-deletion-allowed";
    String C_PROTECTED_URL_DELETION_ALLOWED          = "protected-path-deletion-allowed";
    String C_PROTECTED_URL_MODIFICATION_ALLOWED      = "protected-path-modification-allowed";
    String C_REBIND_INTO_COLLECTION                  = "rebind-into-collection";
    String C_REBIND_SOURCE_EXISTS                    = "rebind-source-exists";
    String C_UNBIND_FROM_COLLECTION                  = "unbind-from-collection";
    String C_UNBIND_SOURCE_EXISTS                    = "unbind-source-exists";
}

