/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/util/ViolatedPrecondition.java,v 1.2 2006-01-22 22:55:21 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:21 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.webdav.util;

// import list

/**
 * This class encapsulates the status code and name of the precondition
 * that has been violated.
 *
 * @version $Revision: 1.2 $
 *
 **/
public class ViolatedPrecondition {

    /**
     * String constant for the message of the IllegalArgumentException
     * that might be thrown in {@link #ViolatedPrecondition the constructor}
     * if the parameter <code>precondition</code> is <code>null</code>.
     */
    public static final String PRECONDITION_MUST_NOT_BE_NULL = "Parameter 'precondition' must not be null";
    
    
    /**
     * The status of the violated precondition.
     * Must be either <code>403 (Forbidden)</code> or
     * <code>409 (Conflict)</code>.
     */
    protected int statusCode = WebdavStatus.SC_FORBIDDEN;
    
    /**
     * The precondition that has been violated.
     */
    protected String precondition = null;
    protected String explanation = null;

    /**
     * The String representation returned by {@link #toString toString()}.
     */
    protected String stringRepresentation = null;
    
    
    
    /**
     * Creates a ViolatedPrecondition.
     *
     * @pre      precondition != null
     * @pre      (statusCode == 403) || (statusCode == 409)
     *
     * @param      precondition  the name of the violated precondition. Must not
     *                           be <code>null</code>.
     * @param      statusCode    the status of the violated precondition.
     *                           Must be either <code>403 (Forbidden)</code> or
     *                           <code>409 (Conflict)</code>.
     *
     * @throws IllegalArgumentException if the <code>precondition</code> is
     *                                  <code>null</code>, or if the parameter
     *                                  <code>statusCode</code> is neither
     *                                  <code>403 (Forbidden)</code>
     *                                  nor <code>409 (Conflict)</code>.
     */
    public ViolatedPrecondition(String precondition, int statusCode) {
        this(precondition, statusCode, null);
    }
    
    public ViolatedPrecondition(String precondition, int statusCode, String explanation) {
        
        if (precondition == null) {
            throw new IllegalArgumentException(PRECONDITION_MUST_NOT_BE_NULL);
        }

        this.precondition = precondition;
        this.statusCode = statusCode;
        this.explanation = explanation;
    }
    
    /**
     * Returns the name of the violated precondition.
     *
     * @return     the name of the violated precondition.
     */
    public String getPrecondition() {
        return precondition;
    }
    
    /**
     * Returns the status of the violated precondition.
     *
     * @return     the status of the violated precondition.
     */
    public int getStatusCode() {
        return statusCode;
    }
    
    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }
    
    public String getExplanation() {
        return explanation;
    }
    
    /**
     * Returns <code>true</code> if the <code>other</code> Object is a
     * ViolatedPrecondition and if both their precondition and status code
     * are equal.
     *
     * @param      other  the Object to test for equality.
     *
     * @return     <code>true</code> if the other Object is equal to this one.
     */
    public boolean equals(Object other) {
        
        boolean isEqual = false;
        if (other instanceof ViolatedPrecondition) {
            ViolatedPrecondition otherViolatedPrecondition = (ViolatedPrecondition)other;
            isEqual = getPrecondition().equals(otherViolatedPrecondition.getPrecondition());
            isEqual &= ( getStatusCode() == otherViolatedPrecondition.getStatusCode() );
        }
        
        return isEqual;
    }
    
    /**
     * Returns the hash code. Due to definition equal objects must have
     * the same hash code.
     *
     * @return     the hash code.
     */
    public int hashCode() {
        return 13*getPrecondition().hashCode() + getStatusCode();
    }
    
    /**
     * Returns the String representation of this Object.
     *
     * @return     the String representation of this Object.
     */
    public String toString() {
        
        if (stringRepresentation == null) {
            StringBuffer buffer = new StringBuffer("ViolatedPrecondition[");
            buffer.append(getPrecondition());
            buffer.append(", ");
            buffer.append(getStatusCode());
            buffer.append(" ");
            buffer.append(WebdavStatus.getStatusText(getStatusCode()));
            buffer.append("]");
            stringRepresentation = buffer.toString();
        }
        return stringRepresentation;
    }
}

