/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/util/PropertyRetriever.java,v 1.2 2006-01-22 22:55:21 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:21 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.webdav.util;

// import list
import java.util.List;

import org.apache.slide.common.RequestedProperties;
import org.apache.slide.common.SlideException;
import org.apache.slide.content.NodeRevisionDescriptor;
import org.apache.slide.content.NodeRevisionDescriptors;
import org.apache.slide.content.NodeRevisionNumber;
import org.apache.slide.search.RequestedResource;
import org.apache.slide.webdav.util.resourcekind.ResourceKind;
import org.jdom.a.JDOMException;

/**
 * An implementation of this interface is used to retrieve the properties of a
 * resource as a list of <code>&lt;propstat&gt;</code> elements. The main code of this
 * class has been copied from the <code>PropFindMethod</code>. All methods
 * providing property information (<code>PropFindMethod</code>,
 * <code>ReportMethod</code>) should use an implementation of this interface.
 *
 * @version $Revision: 1.2 $
 *
 */
public interface PropertyRetriever {



    /**
     * Returns the requested properties of the last revision of the resource
     * identified by the given <code>uri</code> as list of <code>&lt;propstat&gt;</code>
     * JDOM Elements.
     *
     * @param      requestedProperties  the requested properties.
     * @param      uri                  the URI of the resource.
     * @param      contextPath         a  String , the result of HttpRequest.getContextPath()
     * @param      servletPath         a String, the result of HttpRequest.getServletPath()
     * @param      allpropSupportsDeltaV  indicates if the <code>DeltaV</code> specific
     *                                    properties should be included in case
     *                                    all properties are requested.
     *
     * @return     the requested properties as list of <code>&lt;propstat&gt;</code>
     *             JDOM Element.
     *
     * @throws     SlideException
     * @throws     JDOMException  if creating the JDOM Element fails.
     */
    public List getPropertiesOfObject(RequestedProperties requestedProperties, String uri, String servletContextPath, boolean allpropSupportsDeltaV) throws SlideException, JDOMException;

    /**
     * Returns the requested properties of the resource identified by the given
     * <code>uri</code> and <code>revisionNumber</code> as list of
     * <code>&lt;propstat&gt;</code> JDOM Elements.
     *
     * @param      requestedProperties  the requested properties.
     * @param      uri                  the URI of the resource.
     * @param      revisionNumber       the revision number of the requested resource.
     * @param      contextPath          a  String , the result of HttpRequest.getContextPath()
     * @param      servletPath          a String, the result of HttpRequest.getServletPath()
     * @param      allpropSupportsDeltaV  indicates if the <code>DeltaV</code> specific
     *                                    properties should be included in case
     *                                    all properties are requested.
     *
     * @return     the requested properties as list of <code>&lt;propstat&gt;</code>
     *             JDOM Element.
     *
     * @throws     SlideException
     * @throws     JDOMException  if creating the JDOM Element fails.
     */
    public List getPropertiesOfObject(RequestedProperties requestedProperties, String uri, NodeRevisionNumber revisionNumber, String servletContextPath, boolean allpropSupportsDeltaV) throws SlideException, JDOMException;

    /**
     * Returns the requested properties of the resource identified by the given
     * <code>uri</code> and <code>revisionNumber</code> as list of
     * <code>&lt;propstat&gt;</code> JDOM Elements.
     *
     * @param      requestedProperties  the requested properties.
     * @param      revisionDescriptors    the NodeRevisionDescriptors of the resource.
     * @param      revisionDescriptor     the NodeRevisionDescriptor of the resource.
     * @param      contextPath          a  String , the result of HttpRequest.getContextPath()
     * @param      servletPath          a String, the result of HttpRequest.getServletPath()
     * @param      allpropSupportsDeltaV  indicates if the <code>DeltaV</code> specific
     *                                    properties should be included in case
     *                                    all properties are requested.
     *
     * @return     the requested properties as list of <code>&lt;propstat&gt;</code>
     *             JDOM Element.
     *
     * @throws     SlideException
     * @throws     JDOMException  if creating the JDOM Element fails.
     */
    public List getPropertiesOfObject(RequestedProperties requestedProperties, NodeRevisionDescriptors revisionDescriptors, NodeRevisionDescriptor revisionDescriptor, String servletContextPath, boolean allpropSupportsDeltaV) throws SlideException, JDOMException;

    /**
     * Returns the requested properties of the <code><requestedResource/code>
     * as list of <code>&lt;propstat&gt;</code> JDOM Elements.
     *
     * @param      requestedProperties  the requested properties.
     * @param      requestedResource    the resource for which to return the properties.
     * @param      contextPath          a  String , the result of HttpRequest.getContextPath()
     * @param      servletPath          a String, the result of HttpRequest.getServletPath()
     * @param      allpropSupportsDeltaV  indicates if the <code>DeltaV</code> specific
     *                                    properties should be included in case
     *                                    all properties are requested.
     *
     * @return     the requested properties as list of <code>&lt;propstat&gt;</code>
     *             JDOM Element.
     *
     * @throws     SlideException
     * @throws     JDOMException  if creating the JDOM Element fails.
     */
    public List getPropertiesOfObject(RequestedProperties requestedProperties, RequestedResource requestedResource, String servletContextPath, boolean allpropSupportsDeltaV) throws SlideException, JDOMException;

    /**
     * Discover what properties exist or are supported by a resource.
     * @param    resourcePath          a  String
     * @param    liveOnly              only live properties
     * @return   a RequestedProperties
     * @throws   SlideException
     */
    public RequestedProperties getAllPropertyNames(String resourcePath, boolean liveOnly) throws SlideException;

    /**
     * Discover what properties are supported by the specified resourcetype.
     *
     * @param    resourceKind        a  ResourceKind
     *
     * @return   a RequestedProperties
     *
     * @throws   SlideException
     *
     */
    public RequestedProperties getAllPropertyNames(ResourceKind resourceKind) throws SlideException;
}


