/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/util/resourcekind/ResourceKind.java,v 1.2 2006-01-22 22:55:21 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:21 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.util.resourcekind;

import java.util.Set;


/**
 * Abstraction of a WebDAV-compliant resource kind.
 */
public interface ResourceKind {
    
    /**
     * Get the set live properties supported by this resource kind.
     */
    Set getSupportedLiveProperties();
    
    /**
     * Get the set live properties supported by this resource kind.
     * @param filter Q_PROTECTED_ONLY or Q_COMPUTED_ONLY (no filtering if null)
     * @see org.apache.slide.webdav.util.WebdavConstants
     * @see org.apache.slide.webdav.util.DeltavConstants
     * @see org.apache.slide.webdav.util.AclConstants
     * @see org.apache.slide.webdav.util.DaslConstants
     */
    Set getSupportedLiveProperties( String filter );
    
    /**
     * Get the set live properties supported by this resource kind.
     * @param excludedFeatures array of F_* constants (no filtering if null or empty)
     * @see org.apache.slide.webdav.util.WebdavConstants
     * @see org.apache.slide.webdav.util.DeltavConstants
     * @see org.apache.slide.webdav.util.AclConstants
     * @see org.apache.slide.webdav.util.DaslConstants
     */
    Set getSupportedLiveProperties( String[] excludedFeatures );
    
    /**
     * Get the set live properties supported by this resource kind.
     * @param filter Q_PROTECTED_ONLY or Q_COMPUTED_ONLY (no filtering if null)
     * @param excludedFeatures array of F_* constants (no filtering if null or empty)
     * @see org.apache.slide.webdav.util.WebdavConstants
     * @see org.apache.slide.webdav.util.DeltavConstants
     * @see org.apache.slide.webdav.util.AclConstants
     * @see org.apache.slide.webdav.util.DaslConstants
     */
    Set getSupportedLiveProperties( String filter, String[] excludedFeatures );
    
    /**
     * Get the set methods supported by this resource kind.
     */
    Set getSupportedMethods();
    
    /**
     * Return true, if the specified property is supported by this resource kind.
     */
    public boolean isSupportedLiveProperty( String prop );
    
    /**
     * Return true, if the specified method is supported by this resource kind.
     */
    boolean isSupportedMethod( String method );
    
    /**
     * Get the set reports supported by this resource kind.
     */
    Set getSupportedReports();
    
    /**
     * Some properties (e.g. <code>&lt;auto-version&gt;</code>) have a
     * restricted set of supported values.
     * If the value set of the given <code>property</code> is restricted and
     * the given <code>value</code> is not contained in that set, this method
     * returns <code>false</code>, otherwise <code>true</code>.
     *
     * @param      propertyName  the name of the property.
     * @param      value         the value to check.
     *
     * @return     <code>false</code> if the value is not allowed, otherwise
     *             <code>true</code>.
     */
    boolean isSupportedPropertyValue(String propertyName, Object value);
    
}

