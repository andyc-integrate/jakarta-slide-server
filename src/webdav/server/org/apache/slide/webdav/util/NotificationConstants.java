/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/webdav/server/org/apache/slide/webdav/util/NotificationConstants.java,v 1.2 2006-01-22 22:55:21 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:21 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.webdav.util;

/**
 * Notification constants.
 *
 */
public interface NotificationConstants extends WebdavConstants {
    /** Headers */
    String H_NOTIFICATION_TYPE        = "Notification-type";
    String H_NOTIFICATION_DELAY       = "Notification-delay";
    String H_SUBSCRIPTION_LIFETIME    = "Subscription-lifetime";
    String H_SUBSCRIPTION_ID          = "Subscription-ID";
    String H_SUBSCRIPTION_ID_RESPONSE = "Subscription-id";
    String H_CALL_BACK                = "Call-back";
    String H_CONTENT_LOCATION         = "Content-Location";

    /** XML Elements */
    String E_SUBSCRIPTION_ID          = "subscriptionID";
    String E_LISTENER                 = "li";
    String E_FIRE_EVENTS              = "fire-events";
    String E_EVENT                    = "event";
    String E_VETOABLE_EVENT           = "vetoable-event";
    String E_INFORMATION              = "information";

    /** Attributes */
    String A_INFORMATION_KEY          = "name";

    /** Namespace */
    String N_PREFIX                   = "N";
    String N_URI                      = "http://schemas.microsoft.com/Exchange/";
}
