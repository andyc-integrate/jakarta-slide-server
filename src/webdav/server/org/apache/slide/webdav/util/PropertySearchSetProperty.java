/*
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SearchPropertySet.java
 *
 */

package org.apache.slide.webdav.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.slide.content.NodeProperty.NamespaceCache;

public class PropertySearchSetProperty {
    
    private static HashMap propertyMap = null;
    
    private String property = null;
    
    private String description = null;
    
    private String namespace = null;
    
    /**
     * Returns an iterator over the principal-search-property-set.
     *
     * @return   iterator over the principal-search-property-set
     */
    public static Iterator getPropertySetIterator(){
        if (propertyMap == null) {
            loadProperties();
        }
        Collection values = propertyMap.values();
        return values.iterator();
    }
    
    private PropertySearchSetProperty (String prop, String namesp, String desc) {
        property = prop;
        description = desc;
        namespace = namesp;
    }
    
    /** loads the principal-search-properties-set
     */
    private static void loadProperties () {
        String propSet = AclConstants.C_PRINCIPAL_SEARCH_PROPERTY_SET;
        int index = 0;
        int doubleIndex = 0;
        int trippleIndex = 0;
        String currentProp = null;
        String currentTripple = null;
        String currentDesc = null;
        String namespace = null;
        propertyMap = new HashMap();
        int len = AclConstants.C_PRINCIPAL_SEARCH_PROPERTY_SET.length();
        while ((index != -1) && (index != (len-1))) {
            index = propSet.indexOf(AclConstants.C_SEARCH_PROPERTY_SET_SEPERATOR);
            if (index == -1) {
                currentTripple = propSet;
            } else {
                currentTripple = propSet.substring(0, index);
                propSet = propSet.substring(index+1);
            }
            
            if ((len > 0) && (!(propSet.equals(AclConstants.C_SEARCH_PROPERTY_SET_SEPERATOR)))) {
                trippleIndex = currentTripple.indexOf(",");
                if (trippleIndex == -1) {
                    currentProp = currentTripple;
                    namespace = NamespaceCache.DEFAULT_URI;
                    currentDesc = "";
                }else {
                    String currentDouble = currentTripple.substring(trippleIndex+1);
                    currentProp = currentTripple.substring(0,trippleIndex);
                    doubleIndex = currentDouble.indexOf(",");
                    if (doubleIndex == -1) {
                        namespace = currentDouble;
                        currentDesc = "";
                    }else {
                        namespace = currentDouble.substring(0,doubleIndex);
                        currentDesc = currentDouble.substring(doubleIndex+1);
                    }
                }
                String key = new String(currentProp + namespace);
                propertyMap.put(key,(new PropertySearchSetProperty(currentProp,namespace,currentDesc)));
                len = propSet.length();
            }
        }
    }
    
    /** Verifies if the property is in the search-property-set
     *
     * @return true if property is found, false else
     */
    public static boolean inSearchPropertySet(String property, String namespace) {
        if (propertyMap == null) {
            loadProperties();
        }
        String space;
        if (namespace == null) {
            space = NamespaceCache.DEFAULT_URI;
        } else {
            space = namespace;
        }
        return propertyMap.containsKey(property + space);
    }
    
    /** returns the property name of this property
     *
     * @return property name
     */
    public String getPropertyName() {
        return property;
    }

    /** returns the namespace of this property
     *
     * @return property namspace
     */
    public String getNamespace() {
        return namespace;
    }
    
    /** returns the description of this property
     *
     * @return property description
     */
    public String getDescription () {
        return description;
    }
}

