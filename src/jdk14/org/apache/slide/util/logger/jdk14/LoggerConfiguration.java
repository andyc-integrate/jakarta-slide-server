/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/jdk14/org/apache/slide/util/logger/jdk14/LoggerConfiguration.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.util.logger.jdk14;

import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.logging.FileHandler;
import java.util.logging.Handler;

import java.io.*;

/**
 * Configures Java 1.4 logger.
 *
 */
public class LoggerConfiguration {

    private final static Logger logger = Logger.getLogger(LoggerConfiguration.class.getName());

    public final static String DEFAULT_LOG_FILE = "Slide.log";
    public final static Level DEFAULT_LOG_LEVEL = Level.ALL;
    public final static String DEFAULT_FORMAT_STRING = SimpleLogfileFormatter.SIMPLE_FORMAT_STRING;
    //                new LogfileFormatter(DEFAULT_FORMAT_STRING));
    public final static Formatter DEFAULT_FORMATTER = new SimpleLogfileFormatter(DEFAULT_FORMAT_STRING);
    public final static Formatter DETAIL_FORMATTER = new LogfileFormatter(DEFAULT_FORMAT_STRING);

    private static boolean isConfigured = false;

    public final static void configure() {
        configure(DEFAULT_LOG_FILE, DEFAULT_LOG_LEVEL, Level.INFO);
    }

    /**
     * Configures standard Java1.4 logger
     * 
     */
    public final static void configure(String logFilePath, Level logLevel, Level logToConsoleThreshold) {
        try {
            if (isConfigured)
                return;
            isConfigured = true;
            // XXX first set new formatter to (default) console handler(s)
            Handler[] handlers = Logger.getLogger("").getHandlers();
            for (int i = 0; i < handlers.length; i++) {
                Handler handler = handlers[i];
                if (handler instanceof ConsoleHandler) {
                    handler.setFormatter(new ConsoleLogfileFormatter());
                    handler.setLevel(logToConsoleThreshold);
                    // could exit loop here, but keep on going for robustness (might be more than on handler)
                }
            }
            // set default logging
            Logger.getLogger("").setLevel(logLevel);
            Handler fileHandler = new FileHandler(logFilePath);
            fileHandler.setFormatter(DETAIL_FORMATTER);
            Logger.getLogger("").addHandler(fileHandler);
            logger.log(Level.INFO, "Logging Service configured");
        } catch (IOException ioe) {
            logger.log(Level.WARNING, "Could not set logging configuration", ioe);
        }
    }
}
