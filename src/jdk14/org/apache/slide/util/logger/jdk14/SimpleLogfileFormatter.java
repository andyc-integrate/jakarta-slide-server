/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/jdk14/org/apache/slide/util/logger/jdk14/SimpleLogfileFormatter.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.util.logger.jdk14;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 *
 */
public class SimpleLogfileFormatter extends java.util.logging.Formatter {

    public final static String SIMPLE_FORMAT_STRING = "MM dd HH:mm:ss";

    private SimpleDateFormat dateFormatter;

    /** Creates a new instance of LogfileFormatter */
    public SimpleLogfileFormatter(String dateFormatPattern) {
        dateFormatter = new SimpleDateFormat(dateFormatPattern);
    }

    public SimpleLogfileFormatter() {
        this(SIMPLE_FORMAT_STRING);
    }

    /**
     * Format the given log record and return the formatted string.
     * <p>
     * The resulting formatted String will normally include a
     * localized and formated version of the LogRecord's message field.
     * The Formatter.formatMessage convenience method can (optionally)
     * be used to localize and format the message field.
     *
     * @param record the log record to be formatted.
     * @return the formatted log record
     */
    public String format(LogRecord record) {
        // give it a special appearance if it is a warning or an error 
        boolean highlight = record.getLevel().intValue() > Level.INFO.intValue(); 

        StringBuffer message = new StringBuffer();
        Date logDate = new Date(record.getMillis());
        if (highlight) message.append("\n");
        message.append(record.getSequenceNumber());
        message.append(" - ");
        message.append(dateFormatter.format(logDate));
        message.append(", ");
        message.append(record.getLevel());
        message.append(" - ");
        message.append(record.getLoggerName());
        message.append(" - ");
        message.append(record.getSourceMethodName());
        message.append(":\n");
        message.append(formatMessage(record));
        message.append("\n");
        if (record.getThrown() != null) {
            try {
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                record.getThrown().printStackTrace(pw);
                pw.close();
                message.append(sw.toString());
            } catch (Exception ex) {
            }
        }
        if (highlight) message.append("\n");
        return message.toString();
    }
}
