/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/jdk14/org/apache/slide/util/logger/jdk14/Jdk14Logger.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.util.logger.jdk14;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Default logger implementation. Uses java.util.logging implementation provided by Java 1.4.
 *
 */
public class Jdk14Logger implements org.apache.slide.util.logger.Logger {

    public final static int DEFAULT_LEVEL = INFO;

    public Jdk14Logger() {
        LoggerConfiguration.configure();
    }

    public void log(Object data, Throwable throwable, String channel, int slideLevel) {
        Logger logger = Logger.getLogger(channel);
        Level level = getLevelForSlideLevel(slideLevel);
        logger.log(level, data.toString(), throwable);
    }

    public void log(Object data, String channel, int slideLevel) {
        Logger logger = Logger.getLogger(channel);
        Level level = getLevelForSlideLevel(slideLevel);
        if (data instanceof Throwable) {
            logger.log(level, data.toString(), (Throwable) data);
        } else {
            logger.log(level, data.toString());
        }
    }

    public void log(Object data, int level) {
        this.log(data, DEFAULT_CHANNEL, level);
    }

    public void log(Object data) {
        this.log(data, DEFAULT_CHANNEL, DEFAULT_LEVEL);
    }

    public int getLoggerLevel(String channel) {
        Logger logger = Logger.getLogger(channel);
        Level level = logger.getLevel();
        return getSlideLevelForLevel(level);
    }

    public int getLoggerLevel() {
        return getLoggerLevel(DEFAULT_CHANNEL);
    }

    public void setLoggerLevel(int slideLevel) {
        Logger.getLogger("").setLevel(getLevelForSlideLevel(slideLevel));
        setLoggerLevel(DEFAULT_CHANNEL, slideLevel);
    }

    public void setLoggerLevel(String channel, int slideLevel) {
        Logger logger = Logger.getLogger(channel);
        Level level = getLevelForSlideLevel(slideLevel);
        logger.setLevel(level);
    }

    public boolean isEnabled(String channel, int slideLevel) {
        Logger logger = Logger.getLogger(channel);
        Level level = getLevelForSlideLevel(slideLevel);
        return logger.isLoggable(level);
    }

    public boolean isEnabled(int level) {
        return isEnabled(DEFAULT_CHANNEL, level);
    }

    protected Level getLevelForSlideLevel(int slideLevel) {
        Level level = Level.ALL;
        switch (slideLevel) {
            case EMERGENCY :
                level = Level.SEVERE;
                break;
            case CRITICAL :
                level = Level.SEVERE;
                break;
            case ERROR :
                level = Level.SEVERE;
                break;
            case WARNING :
                level = Level.WARNING;
                break;
            case INFO :
                level = Level.INFO;
                break;
            case DEBUG :
                level = Level.FINE;
                break;
        }
        return level;
    }

    protected int getSlideLevelForLevel(Level level) {
        if (level.equals(Level.ALL))
            return DEBUG;
        if (level.equals(Level.FINEST))
            return DEBUG;
        if (level.equals(Level.FINER))
            return DEBUG;
        if (level.equals(Level.FINE))
            return DEBUG;
        if (level.equals(Level.CONFIG))
            return DEBUG;

        if (level.equals(Level.INFO))
            return INFO;

        if (level.equals(Level.WARNING))
            return WARNING;

        if (level.equals(Level.SEVERE))
            return ERROR;

        if (level.equals(Level.OFF))
            return -1;

        return DEFAULT_LEVEL;
    }

}
