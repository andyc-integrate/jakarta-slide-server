/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/wrappers/log4j/Log4jLogger.java,v 1.2 2006-01-22 22:55:21 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:21 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package log4j;

import org.apache.log4j.Category;
import org.apache.log4j.Level;
import org.apache.slide.util.logger.Logger;

/**
 * Log4j logger implementation.
 *
 */
public class Log4jLogger implements Logger {
    
    
    // ----------------------------------------------------- Instance Variables
    
    
    
    
    // ----------------------------------------------------- Static Initializer
    
    
    static {
//      BasicConfigurator.configure();
    }
    
    
    // ------------------------------------------------------------- Properties
    
    
    /**
     * Logger level setter.
     *
     * @param loggerLevel New logger level
     */
    public void setLoggerLevel(String channel, int loggerLevel) {
        Level level = toLevel(loggerLevel);
        Category cat = getCategory(channel);
        cat.setLevel(level);
    }
    
    /**
     * Logger level setter.
     *
     * @param loggerLevel New logger level
     */
    public void setLoggerLevel(int loggerLevel) {
        setLoggerLevel(DEFAULT_CHANNEL, loggerLevel);
    }
    
    
    /**
     * Logger level getter.
     *
     * @return int logger level
     */
    public int getLoggerLevel(String channel) {
        Category cat = getCategory(channel);
        return fromLevel(cat.getLevel());
    }
    
    
    /**
     * Logger level getter.
     *
     * @return int logger level
     */
    public int getLoggerLevel() {
        return getLoggerLevel(DEFAULT_CHANNEL);
    }
    
    
    
    
    // --------------------------------------------------------- Logger Methods
    
    
    /**
     * Log an object and an associated throwable thru the specified channel and with the specified level.
     *
     * @param data object to log
     * @param throwable throwable to be logged
     * @param channel channel name used for logging
     * @param level level used for logging
     */
    public void log(Object data, Throwable throwable, String channel, int level) {
        Category cat = getCategory(channel);
        cat.log(toLevel(level), data, throwable);
    }

    
    /**
     * Log an object thru the specified channel and with the specified level.
     *
     * @param data The object to log.
     * @param channel The channel name used for logging.
     * @param level The level used for logging.
     */
    public void log(Object data, String channel, int level) {
        Category cat = getCategory(channel);
		if (data instanceof Throwable)
            cat.log(toLevel(level), data, (Throwable)data);
        else	
            cat.log(toLevel(level), data);
    }
    
    
    /**
     * Log an object with the specified level.
     *
     * @param data The object to log.
     * @param level The level used for logging.
     */
    public void log(Object data, int level) {
        this.log(data, DEFAULT_CHANNEL, level);
    }
    
    
    /**
     * Log an object.
     *
     * @param data The object to log.
     */
    public void log(Object data) {
        this.log(data, DEFAULT_CHANNEL, Logger.DEBUG);
    }
    
        
    
    /**
     * Check if the channel with the specified level is enabled for logging.
     * This implementation ignores the channel specification
     *
     * @param channel The channel specification
     * @param level   The level specification
     */

    public boolean isEnabled(String channel, int level) {
        Category cat = getCategory(channel);
        return cat.isEnabledFor(toLevel(level));
    }
            
        
    
    /**
     * Check if the default channel with the specified level is enabled for logging.
     *
     * @param level   The level specification
     */

    public boolean isEnabled(int level) {
        return isEnabled(DEFAULT_CHANNEL, level);
    }
            
    
    
    
    // ------------------------------------------------------ Private/Protected Methods
    
    private Category getCategory(String channel) {
        Category cat = Category.exists(channel);
        if (cat == null) {
            cat = Category.getInstance(channel);
        }
        return cat;
    }
    
    
    /**
     * Convert Slide level to log4j level.
     */
    protected Level toLevel(int level) {
        Level result = null;
        
        switch (level) {
        case EMERGENCY:
            result = Level.FATAL;
            break;
        case CRITICAL:
            result = Level.FATAL;
            break;
        case ERROR:
            result = Level.ERROR;
            break;
        case WARNING:
            result = Level.WARN;
            break;
        case INFO:
            result = Level.INFO;
            break;
        case DEBUG:
            result = Level.DEBUG;
            break;
        }
        
        if (result == null)
            result = Level.toLevel(level);
        return result;
    }
    
    
    /**
     * Convert Slide level to log4j level.
     */
    protected int fromLevel(Level level) {
        int result = INFO;
        
        if (level.equals(Level.FATAL)){
            result = EMERGENCY;
        }
        else if (level.equals(Level.ERROR)){
            result = ERROR;
        }
        else if (level.equals(Level.WARN)){
            result = WARNING;
        }
        else if (level.equals(Level.INFO)){
            result = INFO;
        }
        else if (level.equals(Level.DEBUG)){
            result = DEBUG;
        }
        else {
            result = INFO;
        }
        
        return result;
    }

}
