/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/wrappers/wrappers/catalina/SlideRealm.java,v 1.2 2006-01-22 22:55:21 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:55:21 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package wrappers.catalina;

import java.security.Principal;
import java.io.File;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import org.apache.catalina.Container;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.Logger;
import org.apache.catalina.Realm;
import org.apache.catalina.realm.RealmBase;
import org.apache.catalina.util.LifecycleSupport;
import org.apache.catalina.util.StringManager;

import org.apache.slide.common.NamespaceAccessToken;
import org.apache.slide.common.Domain;
import org.apache.slide.common.SlideToken;
import org.apache.slide.common.SlideTokenImpl;
import org.apache.slide.common.SlideException;
import org.apache.slide.content.Content;
import org.apache.slide.content.NodeRevisionDescriptors;
import org.apache.slide.content.NodeRevisionDescriptor;
import org.apache.slide.content.NodeProperty;
import org.apache.slide.structure.ObjectNode;
import org.apache.slide.authenticate.CredentialsToken;
import org.apache.slide.authenticate.SecurityToken;
import org.apache.slide.security.Security;


/**
 * Implemetation of a Catalina realm which authenticates users defined
 * in a Slide namespace.
 * <p>
 * The namespace used will have the same name as the container to which the
 * realm is associated. If such a namespace doesn't exist, it falls back
 * to tomcat, webdav or default.
 *
 * @version $Revision: 1.2 $ $Date: 2006-01-22 22:55:21 $
 */

public final class SlideRealm
    extends RealmBase {


    // ----------------------------------------------------- Instance Variables


    /**
     * Descriptive information about this Realm implementation.
     */
    private static final String info =
    "wrappers.catalina.SlideRealm/1.2";


    /**
     * Set of access tokens.
     */
    private NamespaceAccessToken accessToken;


    /**
     * Content helper.
     */
    private Content contentHelper;


    /**
     * Security halper.
     */
    private Security securityHelper;


    /**
     * Users path.
     */
    private String usersPath;


    /**
     * Root credentials token.
     */
    private CredentialsToken rootCredentials;


    /**
     * Namepsace to which this realm will connect.
     */
    private String namespace;


    // ------------------------------------------------------------- Properties


    /**
     * Set the namespace name to which this realm will connect.
     */
    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }


    /**
     * Set the namespace access token used by this realm.
     */
    public void setAccessToken(NamespaceAccessToken accessToken) {
        this.accessToken = accessToken;
    }


    /**
     * Get name.
     */
    public String getName() {
        return "Slide realm";
    }


    // --------------------------------------------------------- Public Methods


    /**
     * Return <code>true</code> if the specified Principal has the specified
     * security role, within the context of this Realm; otherwise return
     * <code>false</code>.
     *
     * @param principal Principal for whom the role is to be checked
     * @param role Security role to be checked
     */
    public boolean hasRole(Principal principal, String role) {

        CredentialsToken credToken = new CredentialsToken(principal);
        SlideToken slideToken = new SlideTokenImpl(credToken);
        try {
            return securityHelper.hasRole(slideToken, role);
        } catch (SlideException e) {
            return (false);
        }

    }


    /**
     * Start the realm.
     */
    public void start() throws LifecycleException {

        super.start();

        if (namespace == null)
            namespace = container.getName();

        if (accessToken == null)
            accessToken = Domain.accessNamespace
                (new SecurityToken(container), namespace);

        if (accessToken == null)
            throw new IllegalStateException
                ("Invalid Slide Realm configuration : "
                 + "Couldn't access namespace " + namespace);

        contentHelper = accessToken.getContentHelper();
        securityHelper = accessToken.getSecurityHelper();

        usersPath = accessToken.getNamespaceConfig().getUsersPath();

    }


    // ------------------------------------------------------ Protected Methods


    /**
     * Return the password associated with the given principal's user name.
     */
    protected String getPassword(String username) {
        
        Principal userPrincipal = getPrincipal(username);
        CredentialsToken credToken = new CredentialsToken(userPrincipal);
        SlideToken slideToken = new SlideTokenImpl(credToken);
        
        // Fetch the Slide object representing the user.
        try {
            
            ObjectNode user = securityHelper.getPrincipal(slideToken);
            
        } catch (SlideException e) {
            return null;
        }
        
        String passwordValue = null;
        
        try {
            
            NodeRevisionDescriptors revisionDescriptors =
                contentHelper.retrieve(slideToken, usersPath + "/" + username);
            NodeRevisionDescriptor revisionDescriptor =
                contentHelper.retrieve(slideToken, revisionDescriptors);
            NodeProperty password =
                revisionDescriptor.getProperty
                ("password", NodeProperty.SLIDE_NAMESPACE);
            if (password != null) {
                passwordValue = (String) password.getValue();
            }
            
        } catch (SlideException e) {
            // Whatever happens doesn't really matter
            // The stack trace is displayed for now for debug purposes
        }
        
        if (passwordValue == null) {
            log("User " + username
                + " doesn't have his password property set : "
                + "can't authenticate");
        }
        
        return passwordValue;
        
    }


    /**
     * Return the Principal associated with the given user name.
     */
    protected Principal getPrincipal(String username) {
    return new SlideRealmPrincipal(username);
    }


}


/**
 * Private class representing an individual user's Principal object.
 */

final class SlideRealmPrincipal implements Principal, Serializable {

    /**
     * The username for this Principal.
     */
    private String username = null;


    /**
     * Construct a new MemoryRealmPrincipal instance.
     *
     * @param username The username for this Principal
     */
    public SlideRealmPrincipal(String username) {

    this.username = username;

    }


    /**
     * Return the name of this Principal.
     */
    public String getName() {

    return (username);

    }


}


