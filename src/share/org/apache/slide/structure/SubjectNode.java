/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/structure/SubjectNode.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.structure;

import java.util.Vector;

/**
 * Subject node class.
 *
 * @version $Revision: 1.2 $
 */
public class SubjectNode extends ObjectNode {
    
    /** generic subjects */
    public static final String ALL_URI = "all";
    public static final String OWNER_URI = "owner";
    public static final String SELF_URI = "self";
    public static final String UNAUTHENTICATED_URI = "unauthenticated";
    public static final String AUTHENTICATED_URI = "authenticated";
    
    public static final SubjectNode ALL = new SubjectNode(ALL_URI);
    public static final SubjectNode OWNER = new SubjectNode(OWNER_URI);
    public static final SubjectNode SELF = new SubjectNode(SELF_URI);
    public static final SubjectNode UNAUTHENTICATED = new SubjectNode(UNAUTHENTICATED_URI);
    public static final SubjectNode AUTHENTICATED = new SubjectNode(AUTHENTICATED_URI);
    
    
    // ----------------------------------------------------------- Constructors
    
    
    /**
     * Constructor.
     */
    public SubjectNode() {
        super();
    }
    
    
    /**
     * Default constructor.
     */
    public SubjectNode(String uri) {
        super(uri);
    }
    
    
    /**
     * Default constructor.
     */
    public SubjectNode(String uri, Vector children, Vector links) {
        super(uri, children, links);
    }
    
    
    /**
     * Contructor to be used by stores supporting binding.
     */
    public SubjectNode(String uuri, Vector bindings, Vector parentset, Vector links) {
        super(uuri, bindings, parentset, links);
    }
    
    public static SubjectNode getSubjectNode(String subjectUri) {
        if (SubjectNode.ALL_URI.equals(subjectUri)) {
            return SubjectNode.ALL;
        }
        else if (SubjectNode.AUTHENTICATED_URI.equals(subjectUri)) {
            return SubjectNode.AUTHENTICATED;
        }
        else if (SubjectNode.OWNER_URI.equals(subjectUri)) {
            return SubjectNode.OWNER;
        }
        else if (SubjectNode.SELF_URI.equals(subjectUri)) {
            return SubjectNode.SELF;
        }
        else if (SubjectNode.UNAUTHENTICATED_URI.equals(subjectUri)) {
            return SubjectNode.UNAUTHENTICATED;
        }
        return new SubjectNode(subjectUri);
    }
}
