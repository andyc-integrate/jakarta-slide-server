/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/structure/ObjectNotFoundException.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.structure;

import org.apache.slide.common.Uri;
import org.apache.slide.util.Messages;

/**
 * Object not found.
 *
 * @version $Revision: 1.2 $
 */
public class ObjectNotFoundException extends StructureException {
    
    
    // ----------------------------------------------------------- Constructors
    
    
    /**
     * Constructor.
     *
     * @param objectUri Uri of the object
     */
    public ObjectNotFoundException(Uri objectUri) {
        this(objectUri.toString());
    }
    
    
    /**
     * Constructor.
     *
     * @param objectUri Uri of the object
     */
    public ObjectNotFoundException(String objectUri) {
        super(Messages.format(ObjectNotFoundException.class.getName(),
                              objectUri));
        this.objectUri = objectUri;
    }
    
    
    // ----------------------------------------------------- Instance Variables
    
    
    /**
     * Object Uri.
     */
    private String objectUri;
    
    
    // ------------------------------------------------------------- Properties
    
    
    /**
     * Object Uri accessor.
     *
     * @return String object Uri
     */
    public String getObjectUri() {
        if (objectUri == null) {
            return new String();
        } else {
            return objectUri;
        }
    }
    
}
