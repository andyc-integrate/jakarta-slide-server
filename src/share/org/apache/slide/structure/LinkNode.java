/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/structure/LinkNode.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */ 

package org.apache.slide.structure;

import java.util.Vector;

import org.apache.slide.common.ObjectValidationFailedException;
import org.apache.slide.util.Messages;

/**
 * Link node class.
 * 
 * @version $Revision: 1.2 $
 */
public class LinkNode extends ObjectNode {
    
    
    // ----------------------------------------------------------- Constructors
    
    
    /**
     * Constructor.
     */
    public LinkNode() {
        super();
    }
    
    
    /**
     * Default constructor.
     */
    public LinkNode(String uri) {
        super(uri);
    }
    
    
    /**
     * Default constructor.
     */
    public LinkNode(String uri, Vector children, Vector links) {
        super(uri, children, links);
    }
    
    
    /**
     * Default constructor.
     */
    public LinkNode(String uri, Vector children, Vector links, 
                    String linkedUri) {
        super(uri, children, links);
        setLinkedUri(linkedUri);
    }
    
    
    // ----------------------------------------------------- Instance Variables
    
    
    /**
     * Linked Uri
     */
    protected String linkedUri;
    
    
    // ------------------------------------------------------------- Properties
    
    
    /**
     * Get linked Uri.
     * 
     * @return String Linked Uri
     */
    public String getLinkedUri() {
        return linkedUri;
    }
    
    
    /**
     * Set linked Uri.
     * 
     * @param linkedUri New linked Uri
     */
    void setLinkedUri(String linkedUri) {
        if (!linkedUri.equals(getUri())) {
            this.linkedUri = linkedUri;
        }
    }
    
    
    /**
     * Validate an ObjectNode.
     * 
     * @param expectedUri Uri
     */
    public void validate(String expectedUri) {
        super.validate(expectedUri);
        
        if (linkedUri == null)
            throw new ObjectValidationFailedException
                (uri, Messages.message
                 (LinkNode.class.getName() + ".nullLink"));
        
    }
    
    
}
