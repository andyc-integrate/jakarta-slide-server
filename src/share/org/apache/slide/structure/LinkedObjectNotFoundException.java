/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/structure/LinkedObjectNotFoundException.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */ 

package org.apache.slide.structure;

import org.apache.slide.common.Uri;
import org.apache.slide.util.Messages;

/**
 * Linked object not found.
 * 
 * @version $Revision: 1.2 $
 */
public class LinkedObjectNotFoundException extends StructureException {
    
    
    // ----------------------------------------------------------- Constructors
    
    
    /**
     * Constructor.
     * 
     * @param linkUri Uri of the link
     * @param targetUri Link target
     */
    public LinkedObjectNotFoundException(Uri linkUri, String targetUri) {
        super(Messages.format(LinkedObjectNotFoundException.class.getName(), 
                              linkUri.toString(), targetUri));
        this.linkUri = linkUri.toString();
        this.targetUri = targetUri;
    }
    
    
    // ----------------------------------------------------- Instance Variables
    
    
    /**
     * Link uri.
     */
    private String linkUri;
    
    
    /**
     * Target uri.
     */
    private String targetUri;
    
    
    // ------------------------------------------------------------- Properties
    
    
    /**
     * Link Uri accessor.
     * 
     * @return String link Uri
     */
    public String getLinkUri() {
        if (linkUri == null) {
            return new String();
        } else {
            return linkUri;
        }
    }
    
    
    /**
     * Target Uri accessor.
     * 
     * @return String target Uri
     */
    public String getTargetUri() {
        if (targetUri == null) {
            return new String();
        } else {
            return targetUri;
        }
    }
    
}
