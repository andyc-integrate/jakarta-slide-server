/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/lock/UnlockListener.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.lock;

import org.apache.slide.common.SlideException;

/**
 * An UnlockListener may be passed to the Lock helper in order to have more
 * control on the unlock operation. The UnlockListener will be notified
 * before and after unlocking any single resource.
 *
 * @version $Revision: 1.2 $
 **/
public interface UnlockListener {

    /**
     * This method is called before unlocking the resource.
     *
     * @param      uri the uri of the resource that will be unlocked.
     * @throws     SlideException
     */
    public void beforeUnlock( String uri ) throws SlideException;

    /**
     * This method is called after unlocking the resource.
     *
     * @param      uri the uri of the resource that has been unlocked.
     * @throws     SlideException
     */
    public void afterUnlock( String uri ) throws SlideException;
}

