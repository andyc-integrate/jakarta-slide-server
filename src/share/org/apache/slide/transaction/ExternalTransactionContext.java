/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/transaction/ExternalTransactionContext.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.transaction;

import javax.transaction.Status;
import javax.transaction.Transaction;

import java.util.*;

/**
 * Context for external transaction started an controlled by clients.
 * 
 */
public class ExternalTransactionContext {

    protected static Map transactions = Collections.synchronizedMap(new HashMap());

    public static void registerContext(Object txId, Transaction transaction) {
        ExternalTransactionContext context = new ExternalTransactionContext(transaction, txId);
        context.setStatus(Status.STATUS_ACTIVE);
        transactions.put(txId, context);
    }

    public static ExternalTransactionContext lookupContext(Object txId) {
        return (ExternalTransactionContext) transactions.get(txId);
    }

    public static void deregisterContext(Object txId) {
        transactions.remove(txId);
    }

    protected Transaction transaction;
    protected Object txId;
    protected volatile int status;

    protected ExternalTransactionContext(Transaction transaction, Object txId) {
        this.transaction = transaction;
        this.txId = txId;
    }

    /**
     * @return
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param i
     */
    public void setStatus(int i) {
        status = i;
    }
    /**
     * @return
     */
    public Transaction getTransaction() {
        return transaction;
    }

}
