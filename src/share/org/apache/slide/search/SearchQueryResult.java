/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/SearchQueryResult.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.search;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Aggregates a set containing the result items of a query. This set is either
 * a HashSet (if no ordering was requested) or a TreeSet (if orderby was
 * specified) May also contain a status and a response description
 * (For example: 507 partial result)
 *
 *
 * @version $Revision: 1.2 $
 */
public class SearchQueryResult {

    public final static int STATUS_OK             = 0;
    public final static int STATUS_BAD_QUERY      = 1;
    public final static int STATUS_INVALID_SCOPE  = 2;
    public final static int STATUS_PARTIAL_RESULT = 3;
    public final static int STATUS_UNPROCESSABLE_ENTITY = 4;
    public final static int STATUS_BAD_GATEWAY    = 5;
    public final static int STATUS_FORBIDDEN      = 6;

    private int status;
    private String description;
    private String href;

    private Set result;
    /**
     * Method setStatus
     *
     * @param    status              an int
     *
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * Method getStatus
     *
     * @return   an int
     *
     */
    public int getStatus() {
        return status;
    }

    /**
     * Method setDescription
     *
     * @param    description         a  String
     *
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Method getDescription
     *
     * @return   a String
     *
     */
    public String getDescription() {
        return description;
    }

    /**
     * Method setHref
     *
     * @param    href                a  String
     *
     */
    public void setHref (String href) {
        this.href = href;
    }
    /**
     * Method getHref
     *
     * @return   a String
     *
     */
    public String getHref () {
        return href;
    }

    /**
     * Method add
     *
     * @param    subResultSet        a  SearchQueryResult
     *
     */
    public void add (SearchQueryResult subResultSet) {
        result.addAll (subResultSet.getResultSet());
    }

    /**
     * Constructs an empty unorderred SearchQueryResult
     *
     */
    public SearchQueryResult () {
        this (null, null);
    }

    /**
     * Constructs an unordered SearchQueryResult
     *
     * @param result    the set containing the result items
     */
    public SearchQueryResult (Set result) {
        this (result, null);
    }

    /**
     * Constructs an empty orderred SearchQueryResult
     *
     */
    public SearchQueryResult (Comparator comparator) {
        this (null, comparator);
    }

    /**
     * Constructs an ordered SearchQueryResult
     *
     * @param result    the set containing the result items
     * @param comparator for ordering
     */
    public SearchQueryResult (Set result, Comparator comparator) {
        if (comparator == null) {
            if (result == null)
                this.result =  new HashSet ();
            else
            this.result = result;
        }
        else {
            this.result = new TreeSet (comparator);
            if (result != null)
            this.result.addAll (result);
        }
        status = STATUS_OK;
        description = "";
    }


    public SearchQueryResult (Set result, Comparator comparator, int limit) {
        this (result, comparator);
        if (this.result.size() > limit) {
            SortedSet tmp = new TreeSet (comparator);
            Iterator it = this.result.iterator();
            for (int i = 0; i < limit; i++) {
                tmp.add (it.next());
            }
            this.result = tmp;
        }
    }

    /**
     * Method iterator
     *
     * @return   iterator for iterating the result, RequestedResource
     *
     */
    public Iterator iterator () {
        return result.iterator();
    }

    /**
     * Method getResultSet. needed to add result subsets.
     *
     * @return   a Set
     *
     */
    Set getResultSet () {
        return result;
    }
}
