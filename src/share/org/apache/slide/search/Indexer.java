/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/Indexer.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2004 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


package org.apache.slide.search;

import org.apache.slide.common.Uri;
import org.apache.slide.content.NodeRevisionContent;
import org.apache.slide.content.NodeRevisionDescriptor;
import org.apache.slide.content.NodeRevisionNumber;

/**
 * An Indexer may create an index on a resource, update it or delete it.
 *
 * @version $Revision: 1.2 $
 */
public interface Indexer
{
    
    /**
     * Drops the index of the specified resource
     *
     * @param    uri        uri of the resource
     * @param    number     nodeRevisionNumber of the resource
     *
     * @throws   IndexException
     *
     */
    public void dropIndex (Uri uri, NodeRevisionNumber number)
        throws IndexException;
    
    
    /**
     * creates an index for a resource
     *
     * @param    uri                uri of the resource
     * @param    number             nodeRevisionNumber of the resource
     * @param    revisionContent    the content of the resource
     *
     * @throws   IndexException
     *
     */
    void createIndex (Uri uri, NodeRevisionDescriptor revisionDescriptor,
                      NodeRevisionContent revisionContent)
        throws IndexException;
    
    /**
     * updates an index for a resource
     *
     * @param    uri                uri of the resource
     * @param    number             nodeRevisionNumber of the resource
     * @param    revisionContent    the content of the resource
     *
     * @throws   IndexException
     *
     */
    void updateIndex (Uri uri, NodeRevisionDescriptor revisionDescriptor,
                      NodeRevisionContent revisionContent)
        throws IndexException;
}

