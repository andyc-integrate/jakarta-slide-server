/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/Search.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.search;

import org.apache.slide.common.ServiceAccessException;
import org.apache.slide.common.SlideToken;
import org.apache.slide.event.VetoException;
import org.jdom.a.Element;

/**
 * Search helper.
 *
 * @version $Revision: 1.2 $
 */
public interface Search {


    // ------------------------------------------------------ Interface Methods



    /**
     * Method search
     *
     * @param    token               a  SlideToken
     * @param    query               a  SearchQuery
     *
     * @return   the SearchQueryResult
     *
     * @throws   ServiceAccessException DataSource access error
     */
    SearchQueryResult search(SlideToken token, SearchQuery query)
        throws ServiceAccessException, VetoException;


    /**
     * Return the allowed query languages.
     */
    SearchLanguage[] getSupportedLanguages ();


    /**
     * retrieves the language bound to a specific grammarUri.
     *
     * @param    grammarUri           the URI specifying the grammar
     *
     * @return   the SearchLanguage or null, if not found.
     *
     * @throws   InvalidQueryException
     *
     */
    SearchLanguage getLanguage (String grammarUri)
        throws BadQueryException;


    /**
     * Creates a SearchQuery.
     *
     * @param    grammarUri            identifier for the SearchLanguage
     * @param    searchRequestElement  the JDOM element containing the query
     * @param    token                 the SlideToken
     * @param    maxDepth              may be 0, 1 or INFINIT
     *
     * @return   the SearchQuery
     *
     * @throws   BadQueryException
     */
    SearchQuery createSearchQuery (String grammarUri,
                                Element searchRequestElement,
                                SlideToken token,
                                int maxDepth)
        throws BadQueryException;

    /**
     * Creates a SearchQuery.
     *
     * @param    grammarUri            identifier for the SearchLanguage.
     * @param    searchRequestElement  the JDOM element containing the query
     * @param    token                 the SlideToken.
     * @param    maxDepth              may be 0, 1 or INFINITY.
     * @param    propertyProvider      the  PropertyProvider to use (may be
     *                                 <code>null</code>).
     *
     * @return   the SearchQuery
     *
     * @throws   BadQueryException
     */
    SearchQuery createSearchQuery (String grammarUri,
                                   Element searchRequestElement,
                                   SlideToken token,
                                   int maxDepth,
                                   PropertyProvider propertyProvider)
        throws BadQueryException;

    /**
     * Creates a SearchQuery.
     *
     * @param    grammarUri            identifier for the SearchLanguage.
     * @param    searchRequestElement  the JDOM element containing the query
     * @param    token                 the SlideToken.
     * @param    maxDepth              may be 0, 1 or INFINITY.
     * @param    requestUri            the  URI of the request.
     *
     * @return   the SearchQuery
     *
     * @throws   BadQueryException
     */
    SearchQuery createSearchQuery (String grammarUri,
                                   Element searchRequestElement,
                                   SlideToken token,
                                   int maxDepth,
                                   String requestUri)
        throws BadQueryException;

    /**
     * Creates a SearchQuery.
     *
     * @param    grammarUri            identifier for the SearchLanguage.
     * @param    searchRequestElement  the JDOM element containing the query
     * @param    token                 the SlideToken.
     * @param    maxDepth              may be 0, 1 or INFINITY.
     * @param    propertyProvider      the  PropertyProvider to use (may be
     *                                 <code>null</code>).
     * @param    requestUri            the  URI of the request.
     *
     * @return   the SearchQuery
     *
     * @throws   BadQueryException
     */
    SearchQuery createSearchQuery (String grammarUri,
                                   Element searchRequestElement,
                                   SlideToken token,
                                   int maxDepth,
                                   PropertyProvider propertyProvider,
                                   String requestUri)
        throws BadQueryException;
}
