/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/RequestedResource.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.search;

// import list
import org.apache.slide.common.SlideException;
import org.apache.slide.common.PropertyName;

import org.apache.slide.content.NodeProperty;

import java.util.Iterator;

/**
 * This interface provides access to the URI and the properties of a resource.
 *
 * @version $Revision: 1.2 $
 *
 **/
public interface RequestedResource {
    
    /**
     * Returns the URI of the resource.
     *
     * @return     the URI of the resource.
     *
     * @throws     SlideException
     */
    public String getUri() throws SlideException;
    
    /**
     * Returns the property with the given <code>name</code> and
     * <code>namespace</code>.
     *
     * @param      name       the name of the property.
     * @param      namespace  the namespace URI of the property.
     *
     * @return     the property with the given <code>name</code> and
     *             <code>namespace</code>.
     *
     * @throws     SlideException
     */
    public NodeProperty getProperty(String name, String namespace) throws SlideException;
    
    /**
     * Returns the property with the given <code>propertyName</code>.
     *
     * @param      propertyName       the PropertyName of the property.
     *
     * @return     the property with the given <code>npropertyNameame</code>.
     *
     * @throws     SlideException
     */
    public NodeProperty getProperty(PropertyName propertyName) throws SlideException;
    
    /**
     * Returns an Iterator of PropertyName of all properties.
     *
     * @return     an Iterator of PropertyName.
     *
     * @throws     SlideException
     */
    public Iterator getAllPropertiesNames() throws SlideException;
    
    /**
     * Returns all properties as an Iterator of NodeProperty objects.
     *
     * @return     all properties as an Iterator of NodeProperty objects.
     *
     * @throws     SlideException
     */
    public Iterator getAllProperties() throws SlideException;
    
}


