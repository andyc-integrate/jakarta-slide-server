/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/CompareHint.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.search;

/**
 * Gives some hints to a comparator, that compares resources. You can specify,
 * if compare is case sensitive, ascending or descending, and the resource's
 * property, on which the compare shall take place.
 *
 * @version $Revision: 1.2 $
 */
import org.apache.slide.common.PropertyName;
//import org.apache.slide.search.basic.ComparedProperty;

public class CompareHint {

    /** the name of the property, which shall be compared */
    protected String propName;

    /** the property's namespace */
    protected String propNamespace;

    /** if the associated comparator is used for sort, ascending or descending? */
    private boolean ascending;

    /** is compare case sensitive or not */
    private boolean caseSensitive;

    private PropertyName comparedProperty;


    /**
     * Method getPropName
     *
     * @return   the property's name
     *
     */
    public String getPropName () {
        return propName;
    }

    /**
     * Method getPropNamespace
     *
     * @return   the property's namespace
     *
     */
    public String getPropNamespace () {
        return propNamespace;
    }

    /**
     * Method isAscending
     *
     * @return   a boolean
     *
     */
    public boolean isAscending () {
        return ascending;
    }

    /**
     * Method isCaseSensitive
     *
     * @return   a boolean
     *
     */
    public boolean isCaseSensitive () {
        return caseSensitive;
    }


    /**
     * Constructor
     *
     * @param    prop                a  ComparedProperty
     * @param    ascending           a  boolean
     * @param    caseSensitive       a  boolean
     *
     */
    public CompareHint (PropertyName prop, boolean ascending, boolean caseSensitive) {
        this.propName = prop.getName();
        this.propNamespace = prop.getNamespace();
        this.ascending = ascending;
        this.caseSensitive = caseSensitive;
        this.comparedProperty = prop;
    }


    /**
     * Method getComparedProperty
     *
     * @return   a ComparedProperty
     *
     */
    public PropertyName getComparedProperty () {
        return comparedProperty;
    }
}

