/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/basic/ComparableResource.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.search.basic;

import org.apache.slide.search.CompareHint;
import org.apache.slide.search.RequestedResource;

/**
 * Represents one item of a searcheable set. One item is identified with a
 * unique URI.
 * A RequestedItem may be compared against a property represented by property
 * name and value (as String). These compare methods (equals(),
 * greaterThan(), ...) use three valued logic (TRUE, FALSE, UNKNOWN). UNKNOWN is
 * returned when this item does not know the property to compare against or
 * when the value of that property may not be converted to the datatype of this
 * items matching property (for example comparing a Float against the
 * string "foo").
 *
 * TODO: Namespace awareness!!
 *
 * @version $Revision: 1.2 $
 */
public interface ComparableResource extends RequestedResource {
    
    
    /**
     * returns the internal href. / means: the root of this slide
     * for example http://localhost/slide
     *
     * @return   the href of this item
     */
    public String getInternalHref ();
    
    
    /**
     * returns the external href. / means: the root of this server
     * for example http://localhost/
     *
     * @return   the href of this item
     */
    public String getExternalHref ();
    
    
    /**
     * Checks, if a property, represented by its name and value (as String),
     * is greater than the matching property within this item.
     *
     * @param    propName the name of the property to check
     * @param    literal  the value as String to check again
     *
     * @return   Literals.TRUE, Literals.FALSE or Literals.UNKNOWN
     *
     */
    public int greaterThan (String propName, String propNamespace, String literal);
    
    /**
     * Checks, if a property, represented by its name and value (as String),
     * is lower than the matching property within this item.
     *
     * @param    propName the name of the property to check
     * @param    literal  the value as String to check again
     *
     * @return   Literals.TRUE, Literals.FALSE or Literals.UNKNOWN
     *
     */
    public int lowerThan (String propName, String propNamespace, String literal);
    
    /**
     * Checks, if a property, represented by its name and value (as String),
     * is greater or equal than the matching property within this item.
     *
     * @param    propName the name of the property to check
     * @param    literal  the value as String to check again
     *
     * @return   Literals.TRUE, Literals.FALSE or Literals.UNKNOWN
     *
     */
    public int greaterThanEquals (String propName, String propNamespace, String literal);
    
    /**
     * Checks, if a property, represented by its name and value (as String),
     * is lower or equal than the matching property within this item.
     *
     * @param    propName the name of the property to check
     * @param    literal  the value as String to check again
     *
     * @return   Literals.TRUE, Literals.FALSE or Literals.UNKNOWN
     *
     */
    public int lowerThanEquals (String propName, String propNamespace, String literal);
    
    /**
     * Checks, if a property, represented by its name and value (as String),
     * is EQUAL the matching property within this item.
     *
     * @param    propName the name of the property to check
     * @param    literal  the value as String to check again
     *
     * @return   Literals.TRUE, Literals.FALSE or Literals.UNKNOWN
     *
     */
    public int equals (String propName, String propNamespace, String literal);
    
    /**
     * Retrieves the value for the given property of this Resource
     *
     * @param    propName            the property name
     *
     * @return   the value of the property within this item
     */
    public Object getThisValue (String propName, String propNamespace);
    
    
    /**
     * Compares this resource to another resource according to the given
     * compareHints. This is used by ordering. A value < 0 is retuned, if this
     * resource is to be placed before the other resource, not necessarily if
     * is lower than the other resource (depending on isAscending() in hints).
     *
     * @param    otherResource       the other resource to compare to
     * @param    hint                hints to do the compare (propName, isAscending...)
     *
     * @return   an int indicating the sort order.
     *
     *
     */
    public int compareTo (ComparableResource otherResource, CompareHint hint);
    
    /**
     * Method isDefined
     *
     * @param    propName            a  String
     *
     * @return   true if propName is defined in this resource.
     *
     */
    public boolean isDefined (String propName, String propNamespace);
    
    /**
     * Method propContains
     *
     * @param    propName            a  String
     * @param    literal             a  String
     *
     * @return   true if literal is contained in this prop's value
     *
     */
    public int propContains (String propName, String propNamespace, String literal);
    
    /**
     * Checks, if the content of the resource contains a specific literal.
     *
     * @param    literal             a  String
     *
     * @return   true if literal is contained in this resources' content
     *
     */
    public int contains (String literal);
    
}


