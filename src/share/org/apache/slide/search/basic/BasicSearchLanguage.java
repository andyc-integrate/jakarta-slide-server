/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/basic/BasicSearchLanguage.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.search.basic;

// import list
import org.apache.slide.content.NodeProperty.NamespaceCache;

import org.apache.slide.search.SearchLanguage;
import org.apache.slide.search.SearchQuery;
import org.apache.slide.search.QueryScope;
import org.apache.slide.search.SearchToken;
import org.apache.slide.search.BadQueryException;
import org.apache.slide.search.PropertyProvider;

import org.apache.slide.store.AbstractStore;
import org.apache.slide.common.SlideRuntimeException;
import org.apache.slide.common.Uri;

import org.jdom.a.Element;
import org.jdom.a.Document;

import org.jdom.a.input.SAXBuilder;

import java.io.StringReader;

/**
 * Represent the BasicSearchLanguage for Slide
 *
 * @version $Revision: 1.2 $
 */
public class BasicSearchLanguage extends SearchLanguage {

    static final String GRAMMAR_NAME =
        "basicsearch";

    static final String GRAMMAR_NAMESPACE = NamespaceCache.DEFAULT_URI;

    /** the property name for the store specific BasicQuery implementation if any */
    static public final String BASIC_QUERY_CLASS = "basicQueryClass";

    public BasicSearchLanguage () {}

    /**
     * Method getName
     *
     * @return   the name of this query language
     */
    public String getName() {
        return GRAMMAR_NAME;
    }

    /**
     * Retrieves the URI of this language
     *
     * @return   the namspace of this language
     */
    public String getGrammarUri() {
        return GRAMMAR_NAMESPACE;
    }

    /**
     * Creates a SearchQuery out of this queryString
     *
     * @param    queryString         an XML String describing this query
     * @param    token               SearchToken
     * @param    propertyProvider    the  PropertyProvider to use (may be
     *                               <code>null</code>).
     *
     * @return   the SearchQuery
     *
     * @throws   BadQueryException
     */
    public SearchQuery parseQuery (String queryString, SearchToken token, PropertyProvider propertyProvider) throws BadQueryException {
        // TODO: parse queryString and pass to parseQuery(Element, SearchToken);
        try {
            Document doc =
                new SAXBuilder ().build (new StringReader (queryString));

            Element root = doc.getRootElement();
            return parseQuery(root, token, propertyProvider);
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new BadQueryException (e.getMessage());
        }
    }

    /**
     * Creates a SearchQuery out of this queryElement
     *
     * @param    basicSearchElement  JDOM Element describing this query
     * @param    token               SearchToken
     * @param    propertyProvider    the  PropertyProvider to use (may be
     *                               <code>null</code>).
     *
     * @return   BasicSearchQuery
     *
     * @throws   SearchException
     *
     */
    public SearchQuery parseQuery (Element basicSearchElement, SearchToken token, PropertyProvider propertyProvider) throws BadQueryException {
        boolean useEnvelope = true;


        QueryScope scope = BasicQueryImpl.getScope (basicSearchElement);
        IBasicQuery query = null;


        if (useEnvelope) {
            query = new BasicQueryEnvelope (token, scope);

        }
        else {

//          Uri uri = new Uri (token.getNamespace(),   );
            Uri uri = token.getNamespace().getUri(token.getSlideToken(), token.getSlideContext().getSlidePath(scope.getHref()));

        AbstractStore store = (AbstractStore)uri.getStore();
        String className = (String)store.getParameter (BASIC_QUERY_CLASS);

        if (className != null) {
            try {
                Class queryClass = Class.forName (className);
                    query = (IBasicQuery) queryClass.newInstance();
                    ((IBasicQuery) query).init (token);
            }
            catch (Exception e) {
                e.printStackTrace();
                throw new SlideRuntimeException (e.getMessage());
            }
        }

        else {
            query =  new BasicQueryImpl(token);
        }
        }

        //BasicQueryImpl query =  new XBasicQueryImpl(token);
        query.parseQueryElement(basicSearchElement, propertyProvider);

        return (SearchQuery) query;
    }
}

