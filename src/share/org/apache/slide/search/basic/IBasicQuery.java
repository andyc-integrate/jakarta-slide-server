/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/basic/IBasicQuery.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.search.basic;

import org.apache.slide.common.RequestedProperties;
import org.apache.slide.common.ServiceAccessException;
import org.apache.slide.search.BadQueryException;
import org.apache.slide.search.InvalidScopeException;
import org.apache.slide.search.PropertyProvider;
import org.apache.slide.search.QueryScope;
import org.apache.slide.search.SearchQueryResult;
import org.apache.slide.search.SearchToken;
import org.apache.slide.store.AbstractStore;
import org.jdom.a.Element;

/**
 * A BasicQuery represents the query and is able to deliver a
 * SearchQueryResult using the execute method. It serves as a base class for
 * store specific implementations. It hosts the information about the SELECT,
 * FROM, WHERE, ORDERBY and LIMIT. It also holds a tree of
 * BasicSearchExpressions.
 *
 * @version $Revision: 1.2 $
 */
public interface IBasicQuery {

    /**
     * Method getStore
     *
     * @return   an AbstractStore
     *
     */
    public AbstractStore getStore ();

    /**
     * Method getSlidePath
     *
     * @return   a String
     *
     * @throws   InvalidScopeException
     *
     */
    String getSlidePath () throws InvalidScopeException;

    /**
     * Method getSearchToken
     *
     * @return   a SearchToken
     *
     */
    SearchToken getSearchToken ();

    /**
     * Builds the internal structure from the JDOM tree. Concrete implementations
     * may use parseQueryElementWithoutExpression to create most of the
     * objects describing the query.
     *
     * @param    basicSearchElement  the (root) expression Element.
     * @param    propertyProvider    the PropertyProvider to use (may be
     *                               <code>null</code>).
     *
     * @throws   BadQueryException
     */
    void parseQueryElement (Element basicSearchElement, PropertyProvider propertyProvider) throws BadQueryException;

    /**
     * Executes a request. A store specific implementation should overwrite
     * this to optimize the execution.
     *
     * @return   a SearchQueryResult
     *
     * @throws   ServiceAccessException
     *
     */
    SearchQueryResult execute () throws ServiceAccessException;


    /**
     * QueryScope accessor
     *
     * @return   the Scope
     *
     */
    public QueryScope getScope ();

    /**
     * Method getSelectedProperties
     *
     * @return   a SelectedPropertyList
     */
    RequestedProperties requestedProperties ();

    /**
     * Method getExpression
     *
     * @return   a BasicExpression
     *
     */
    IBasicExpression getExpression ();

    /**
     * Method isLimitDefined
     *
     * @return true if <limit> was specified
     */
    boolean isLimitDefined ();

    /**
     * Method getLimit
     *
     * @return   the value of <limit>
     */
    int getLimit ();

    /**
     * Method getPropertyProvider
     *
     * @return   a PropertyProvider
     *
     */
    PropertyProvider getPropertyProvider ();


    /**
     * Method init
     *
     * @param    token               a  SearchToken
     *
     */
    void init (SearchToken token);


    /**
     * Method getContentExpressionFactory
     *
     * @return   the content store / indexer specific ExpressionFactory
     *
     */
    IBasicExpressionFactory getContentExpressionFactory ();

    /**
     * Method getPropertiesExpressionFactory
     *
     * @return   the properties store / indexer specific ExpressionFactory
     *
     */
    IBasicExpressionFactory getPropertiesExpressionFactory ();
}

