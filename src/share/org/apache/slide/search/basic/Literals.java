/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/basic/Literals.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.search.basic;

/**
 * static container for some literals.
 *
 * @version $Revision: 1.2 $
 */
public class Literals {


    public static final String ALLPROP       = "allprop";
    public static final String AND           = "and";
    public static final String ASCENDING     = "ascending";
    public static final String BASICSEARCH   = "basicsearch";
    public static final String CASESENSITIVE = "casesensitive";
    public static final String CONTAINS      = "contains";
    public static final String DEPTH         = "depth";
    public static final String DESCENDING    = "descending";
    public static final String EQ            = "eq";
    public static final String EXCLUDE       = "exclude";
    public static final String EXCLUDE_LASTPATHSEGEMENT = "exclude-lastpathsegment";
    public static final String FROM          = "from";
    public static final String GT            = "gt";
    public static final String GTE           = "gte";
    public static final String HREF          = "href";
    public static final String INFINITY      = "infinity";
    public static final String INCLUDE_LASTPATHSEGEMENT = "include-lastpathsegment";
    public static final String ISCOLLECTION  = "is-collection";
    public static final String ISDEFINED     = "is-defined";
    public static final String ISPRINCIPAL   = "is-principal";
    public static final String LIKE          = "like";
    public static final String LIMIT         = "limit";
    public static final String LITERAL       = "literal";
    public static final String LT            = "lt";
    public static final String LTE           = "lte";
    public static final String NOT           = "not";
    public static final String NOT_CONTAINS      = "not-contains";
    public static final String NOT_EQ            = "not-eq";
    public static final String NOT_GT            = "not-gt";
    public static final String NOT_GTE           = "not-gte";
    public static final String NOT_ISCOLLECTION  = "not-is-collection";
    public static final String NOT_ISDEFINED     = "not-is-defined";
    public static final String NOT_ISPRINCIPAL   = "not-is-principal";
    public static final String NOT_LIKE          = "not-like";
    public static final String NOT_LT            = "not-lt";
    public static final String NOT_LTE           = "not-lte";
    public static final String NOT_PROPCONTAINS  = "not-propcontains";
    public static final String NRESULTS      = "nresults";
    public static final String OR            = "or";
    public static final String ORDER         = "order";
    public static final String ORDERBY       = "orderby";
    public static final String PROP          = "prop";
    public static final String PROPCONTAINS  = "propcontains";
    public static final String RESOURCETYPE  = "resourcetype";
    public static final String RESULTSET     = "resultset";
    public static final String SCOPE         = "scope";
    public static final String SELECT        = "select";
    public static final String WHERE         = "where";

    public static final int    TRUE        = 1;
    public static final int    FALSE       = 0;
    public static final int    UNKNOWN     = -1;
}

