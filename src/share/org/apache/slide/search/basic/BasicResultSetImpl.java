/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/basic/BasicResultSetImpl.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.search.basic;

// import list
import java.util.HashSet;
import java.util.Collection;

/**
 * An implementation of the {@link org.apache.slide.search.basic.IBasicResultSet
 * IBasicResultSet} interface.
 *
 * @version $Revision: 1.2 $
 *
 **/
public class BasicResultSetImpl extends HashSet implements IBasicResultSet {
    
    /**
     * Indicated if the result set is truncated for any reason.
     */
    protected boolean partial = false;
    

    /**
     * Creates an empty BasicResultSetImpl.
     */
    public BasicResultSetImpl() {
        this(false);
    }
    
    /**
     * Creates an empty BasicResultSetImpl and sets the value
     * returned by {@link #isPartialResultSet isPartialResultSet()}.
     *
     * @param      isPartialResult  the value to be returned by
     *                              {@link #isPartialResultSet isPartialResultSet()}.
     */
    public BasicResultSetImpl(boolean isPartialResult) {
        super();
        setPartialResultSet(isPartialResult);
    }
    
    /**
     * Creates a BasicResultSetImpl containing the elements of the given
     * <code>collection</code>
     *
     * @param      collection  the Collection whose elements to add.
     */
    public BasicResultSetImpl(Collection collection) {
        this(collection, false);
    }
    
    /**
     * Creates a BasicResultSetImpl containing the elements of the given
     * <code>collection</code>
     *
     * @param      collection       the Collection whose elements to add.
     * @param      isPartialResult  the value to be returned by
     *                              {@link #isPartialResultSet isPartialResultSet()}.
     */
    public BasicResultSetImpl(Collection collection, boolean isPartialResult) {
        super(collection);
        setPartialResultSet(isPartialResult);
    }
    
    
    /**
     * Returns <code>true</code> if the result set is truncated for any reason.
     *
     * @return     <code>true</code> if the result set is truncated for any reason.
     */
    public boolean isPartialResultSet() {
        return partial;
    }
    
    /**
     * Sets the value returned by {@link #isPartialResultSet isPartialResultSet()}.
     *
     * @param      partial  the new value to be returned by
     *                      {@link #isPartialResultSet isPartialResultSet()}.
     */
    public void setPartialResultSet(boolean partial) {
        this.partial = partial;
    }
}


