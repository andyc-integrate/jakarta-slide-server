/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/basic/expression/CheckResourceTypeExpression.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.search.basic.expression;

import org.apache.slide.content.NodeProperty.NamespaceCache;
import org.apache.slide.search.InvalidQueryException;
import org.apache.slide.search.basic.ComparableResource;
import org.apache.slide.search.basic.ComparableResourcesPool;
import org.apache.slide.search.basic.Literals;
import org.jdom.a.Element;

/**
 * Represents an expression that check for a specific resourcetype.
 *
 * @version $Revision: 1.2 $
 */
public abstract class CheckResourceTypeExpression extends ComparePropertyExpression {

    /**
     * Creates a resourcetype expression
     *
     * @param e                       jdom element, that describes the expression
     * @param requestedResourcesPool  the pool of resources to apply the expression to.
     */
    public CheckResourceTypeExpression (Element e, ComparableResourcesPool requestedResourcesPool) throws InvalidQueryException {
        super(e, requestedResourcesPool, new ComparedProperty(Literals.RESOURCETYPE, NamespaceCache.DEFAULT_URI));
    }

    /**
     * checks, if the value of resourcetype contains the specified literal
     *
     * @param    item                a  BasicDataItem
     *
     * @return   true, if item is a collection
     *
     */
    protected boolean compare (ComparableResource item) {
        String  resourceTypeValue =
            (String) item.getThisValue (comparedProperty.getProperty(),
                                        comparedProperty.getPropNamespace());

        int index = resourceTypeValue.indexOf (comparedProperty.getLiteral());

        return index > -1;
    }
}



