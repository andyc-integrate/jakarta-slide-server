/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/basic/expression/GTEExpression.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.search.basic.expression;

import org.apache.slide.search.InvalidQueryException;
import org.apache.slide.search.basic.ComparableResource;
import org.apache.slide.search.basic.ComparableResourcesPool;
import org.apache.slide.search.basic.Literals;
import org.jdom.a.Element;

/**
 * Represents an EQUALS expression.
 *
 * @version $Revision: 1.2 $
 */
public class GTEExpression extends ComparePropertyExpression {

    /**
     * Creates an EQ expression according to Element e
     *
     * @param e                       jdom element, that describes the expression
     * @param requestedResourcesPool  the pool of resources to apply the expression to.
     */
    public GTEExpression (Element e, ComparableResourcesPool requestedResourcesPool) throws InvalidQueryException {
        super(e, requestedResourcesPool, true);
    }

    /**
     * Checks item for equality against <prop><literal> of this expression.
     *
     * @param    item                a  RequestedResource
     *
     * @return   a boolean
     *
     */
    protected boolean compare (ComparableResource item) {
        return item.greaterThanEquals (
            comparedProperty.getProperty(),
            comparedProperty.getPropNamespace(),
            comparedProperty.getLiteral()) == Literals.TRUE;
    }


    /**
     * For debugging purpose.
     *
     * @return   This expression as string
     *
     */
    public String toString () {
        return super.toString (Literals.EQ);
    }
}

