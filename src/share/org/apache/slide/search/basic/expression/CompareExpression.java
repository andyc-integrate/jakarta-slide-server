/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/basic/expression/CompareExpression.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.search.basic.expression;

import java.util.Iterator;

import org.apache.slide.search.InvalidQueryException;
import org.apache.slide.search.PropertyProvider;
import org.apache.slide.search.SearchException;
import org.apache.slide.search.basic.BasicResultSetImpl;
import org.apache.slide.search.basic.ComparableResource;
import org.apache.slide.search.basic.ComparableResourcesPool;
import org.apache.slide.search.basic.IBasicResultSet;
import org.jdom.a.Element;

/**
 * Abstract base class for compare expressions (property compares, contains).
 *
 * @version $Revision: 1.2 $
 */
public abstract class CompareExpression extends GenericBasicExpression {

    /**
     * The pool of resources to apply the expression to.
     */
    protected ComparableResourcesPool requestedResourcesPool = null;

    /**
     * The PropertyProvider to use (may be <code>null</code>).
     */
    protected PropertyProvider propertyProvider = null;


    /**
     * Creates a compare expression according to Element e
     *
     * @param e                       jdom element, that describes the expression
     * @param requestedResourcesPool  the pool of resources to apply the expression to.
     */
    CompareExpression (Element e, ComparableResourcesPool requestedResourcesPool) throws InvalidQueryException {
        super (e);
        this.requestedResourcesPool = requestedResourcesPool;
        ((BasicResultSetImpl)resultSet).setPartialResultSet(requestedResourcesPool.partialResult());
    }

    /**
     * Executes the expression.
     *
     * @return   a Set of RequestedResource objects
     *
     * @throws   SearchException
     */
    public IBasicResultSet execute () throws SearchException {

        Iterator iterator = getRequestedResourcePool().getPool().iterator();
        while (iterator.hasNext()) {
            ComparableResource item =
                (ComparableResource)iterator.next();

            if (compare (item))
                resultSet.add (item);
        }
        return resultSet;
    }

    /**
     * Returns the RequestedResourcesPool to use.
     *
     * @return     the RequestedResourcesPool to use.
     */
    public ComparableResourcesPool getRequestedResourcePool() {
        return requestedResourcesPool;
    }

    /**
     * The concrete CompareExpression must overwrite this.
     *
     * @param    item    one BasicDataItem out of pool
     *
     * @return   a boolean
     *
     */
    protected abstract boolean compare (ComparableResource item);
}

