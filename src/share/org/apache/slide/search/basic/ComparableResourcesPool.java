/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/search/basic/ComparableResourcesPool.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


package org.apache.slide.search.basic;
import org.apache.slide.search.*;

import java.util.Set;
import java.util.Iterator;

/**
 * Represents the pool of all resources out of which the query result is
 * computed. This may be the complete scope of the query or a subset, when
 * the server the maximum depth of a query.
 *
 * @version $Revision: 1.2 $
 */
public interface ComparableResourcesPool {
    
    /**
     * Method resourceIterator
     *
     * @return   an Iterator over RequestedResource objects
     *
     */
    Iterator resourceIterator ();
    
    /**
     * Method getPool
     *
     * @return   the pool as Set of RequestedResource objects.
     *
     * @throws   BadQueryException
     *
     */
    Set getPool () throws BadQueryException ;
    
    
    /**
     * Indicates if the server truncated the result set.
     *
     * @return   a boolean
     *
     */
    boolean partialResult ();
    
    /**
     * Returns the scope of this ResourcePool.
     *
     * @return     the scope of this ResourcePool.
     */
    QueryScope getScope();
}

