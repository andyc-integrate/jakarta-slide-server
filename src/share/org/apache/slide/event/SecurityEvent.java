/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/event/SecurityEvent.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.event;

import org.apache.slide.common.SlideToken;
import org.apache.slide.common.Uri;
import org.apache.slide.common.Namespace;
import org.apache.slide.security.NodePermission;

import java.util.EventListener;
import java.util.EventObject;

/**
 * Security event class
 *
 * @version $Revision: 1.2 $
 */
public class SecurityEvent extends EventObject {
    public final static GrantPermission GRANT_PERMISSION = new GrantPermission();
    public final static RevokePermission REVOKE_PERMISSION = new RevokePermission();
    public final static DenyPermission DENY_PERMISSION = new DenyPermission();

    public final static String GROUP = "security";
    public final static AbstractEventMethod[] methods = new AbstractEventMethod[] { GRANT_PERMISSION, REVOKE_PERMISSION, DENY_PERMISSION };

    private Uri objectUri;
    private SlideToken token;
    private NodePermission permission;
    private Namespace namespace;

    public SecurityEvent(Object source, SlideToken token, Namespace namespace, Uri objectUri, NodePermission permission) {
        super(source);
        this.objectUri = objectUri;
        this.token = token;
        this.permission = permission;
        this.namespace = namespace;
    }

    public Namespace getNamespace() {
        return namespace;
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer(256);
        buffer.append(getClass().getName()).append("[object-uri=").append(objectUri);
        buffer.append(", permission=").append(permission);
        buffer.append("]");
        return buffer.toString();
    }

    public Uri getObjectUri() {
        return objectUri;
    }

    public SlideToken getToken() {
        return token;
    }

    public NodePermission getPermission() {
        return permission;
    }

    public final static class GrantPermission extends VetoableEventMethod {
        public GrantPermission() {
            super(GROUP, "grant-permission");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException {
            if ( listener instanceof SecurityListener ) ((SecurityListener)listener).grantPermission((SecurityEvent)event);
        }
   }

    public final static class DenyPermission extends VetoableEventMethod {
        public DenyPermission() {
            super(GROUP, "deny-permission");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException {
            if ( listener instanceof SecurityListener ) ((SecurityListener)listener).denyPermission((SecurityEvent)event);
        }
    }

    public final static class RevokePermission extends VetoableEventMethod {
        public RevokePermission() {
            super(GROUP, "revoke-permission");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException {
            if ( listener instanceof SecurityListener ) ((SecurityListener)listener).revokePermission((SecurityEvent)event);
        }
    }
}