/*
 *  Copyright 1999-2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.event;

import org.apache.slide.common.Domain;
import org.apache.slide.store.ExtendedStore;
import org.apache.slide.store.Store;
import org.apache.slide.util.logger.Logger;

/**
 * In response to an UriModifiedEvent this listener asks the Uri's Store to remove the Uri
 * from its cache.
 * 
 */
public class UncacheModifiedUriListener implements UriModifiedListener {
	
	public static final String LOG_CHANNEL = UncacheModifiedUriListener.class.getName();
	
	public UncacheModifiedUriListener() {
		Domain.log( "Creating UncacheModifiedUriListener.", LOG_CHANNEL, Logger.DEBUG );
	}

	public void modified(UriModifiedEvent event) {
		Store store = event.getUri().getStore();
		/*
		 * TODO: Moving the cache methods on ExtendedStore into their own interface (say CachingStore)
		 * would make this more portable.
		 */
		if ( store instanceof ExtendedStore ) {
			((ExtendedStore)store).removeObjectFromCache( event.getUri() );
		} else {
			Domain.log(
				"Invalid store type " + store + " while uncaching " + event.getUri().toString(),
				LOG_CHANNEL,
				Logger.WARNING );
		}
	}

}
