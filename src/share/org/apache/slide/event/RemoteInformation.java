/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/event/RemoteInformation.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.event;

/**
 * <p>The RemoteInformation interface.<p>
 * <p>
 * This is currently used by the webdav {@link org.apache.slide.webdav.method.PollMethod PollMethod}
 * to generate an XML representation of the data in an {@link org.apache.slide.event event}.
 * </p>
 * 
 */
public interface RemoteInformation {
	
    /**
     * The returned String[][] is assumed to be a String[?][2], where ? is
     * greater than or equal to 0. The format of the returned value is
     * assumed to be:
     * <pre>
     * {
     *     { "XML Element Name 1", "XML Element Value 1"},
     *     { "XML Element Name 2", "XML Element Value 2"},
     *     etc...
     * }
     * </pre>
     * One noteable value for the element name is "uri". The Slide context path
     * is preppended to the value of an element with this name before it is
     * returned to the client.
     * 
     * @return an array of name = value pairs.
     */
    public String[][] getInformation();
}
