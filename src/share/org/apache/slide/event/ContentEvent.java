/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/event/ContentEvent.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.event;

import org.apache.slide.common.SlideToken;
import org.apache.slide.common.Namespace;
import org.apache.slide.content.NodeRevisionContent;
import org.apache.slide.content.NodeRevisionDescriptor;
import org.apache.slide.content.NodeRevisionDescriptors;

import java.util.EventListener;
import java.util.EventObject;

/**
 * Content event class
 *
 * @version $Revision: 1.2 $
 */
public class ContentEvent extends ResourceEvent {
    public final static Create CREATE = new Create();
    public final static Remove REMOVE = new Remove();
    public final static Retrieve RETRIEVE = new Retrieve();
    public final static Store STORE = new Store();
    public final static Fork FORK = new Fork();
    public final static Merge MERGE = new Merge();

    public final static String GROUP = "content";
    public final static AbstractEventMethod[] methods = new AbstractEventMethod[] { CREATE, REMOVE, RETRIEVE, STORE, FORK, MERGE };

    private SlideToken token;
    private Namespace namespace;
    private NodeRevisionDescriptors revisionDescriptors;
    private NodeRevisionDescriptor revisionDescriptor;
    private NodeRevisionContent revisionContent;

    public ContentEvent(Object source, SlideToken token, Namespace namespace, String uri, NodeRevisionDescriptors revisionDescriptors) {
        this(source, token, namespace, uri, revisionDescriptors, null, null);
    }

    public ContentEvent(Object source, SlideToken token, Namespace namespace, String uri, NodeRevisionDescriptors revisionDescriptors, NodeRevisionDescriptor revisionDescriptor) {
        this(source, token, namespace, uri, revisionDescriptors, revisionDescriptor, null);
    }

    public ContentEvent(Object source, SlideToken token, Namespace namespace, String uri, NodeRevisionDescriptor revisionDescriptor, NodeRevisionContent revisionContent) {
        this(source, token, namespace, uri, null, revisionDescriptor, revisionContent);
    }

    public ContentEvent(Object source, SlideToken token, Namespace namespace, String uri, NodeRevisionDescriptor revisionDescriptor) {
        this(source, token, namespace, uri, null, revisionDescriptor, null);
    }

    public ContentEvent(Object source, SlideToken token, Namespace namespace, String uri, NodeRevisionDescriptors revisionDescriptors, NodeRevisionDescriptor revisionDescriptor, NodeRevisionContent revisionContent) {
        super(source, uri);
        this.token = token;
        this.namespace = namespace;
        this.revisionDescriptors = revisionDescriptors;
        this.revisionDescriptor = revisionDescriptor;
        this.revisionContent = revisionContent;
    }

    public SlideToken getToken() {
        return token;
    }

    public NodeRevisionDescriptors getRevisionDescriptors() {
        return revisionDescriptors;
    }

    public NodeRevisionDescriptor getRevisionDescriptor() {
        return revisionDescriptor;
    }

    public NodeRevisionContent getRevisionContent() {
        return revisionContent;
    }

    public Namespace getNamespace() {
        return namespace;
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer(256);
        buffer.append(getClass().getName()).append("[content");
        if ( revisionDescriptors != null ) buffer.append(" uri=").append(revisionDescriptors.getUri());
        if ( revisionDescriptor != null ) buffer.append(" contentType=").append(revisionDescriptor.getContentType());
        buffer.append("]");
        return buffer.toString();
    }

    public static AbstractEventMethod[] getMethods() {
        return methods;
    }

    public static String getGroup() {
        return GROUP;
    }

    public final static class Create extends VetoableEventMethod {
        public Create() {
            super(GROUP, "create");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof ContentListener ) ((ContentListener)listener).create((ContentEvent)event);
        }
    }

    public final static class Remove extends VetoableEventMethod {
        public Remove() {
            super(GROUP, "remove");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof ContentListener ) ((ContentListener)listener).remove((ContentEvent)event);
        }
    }

    public final static class Store extends VetoableEventMethod {
        public Store() {
            super(GROUP, "store");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof ContentListener ) ((ContentListener)listener).store((ContentEvent)event);
        }
    }

    public final static class Retrieve extends VetoableEventMethod {
        public Retrieve() {
            super(GROUP, "retrieve");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof ContentListener ) ((ContentListener)listener).retrieve((ContentEvent)event);
        }
    }

    public final static class Merge extends VetoableEventMethod {
        public Merge() {
            super(GROUP, "merge");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof ContentListener ) ((ContentListener)listener).merge((ContentEvent)event);
        }
    }

    public final static class Fork extends VetoableEventMethod {
        public Fork() {
            super(GROUP, "fork");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof ContentListener ) ((ContentListener)listener).fork((ContentEvent)event);
        }
    }
}