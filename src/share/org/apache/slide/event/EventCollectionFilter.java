/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/event/EventCollectionFilter.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.event;

import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

/**
 * @version $Revision: 1.2 $
 */

public class EventCollectionFilter {
    public static ContentEvent[] getChangedContents(EventCollection collection) {
        List changedContents = new ArrayList();
        if (collection != null)
        {
            List collectedEvents = collection.getCollection();
            for ( Iterator i = collectedEvents.iterator(); i.hasNext(); ) {
                EventCollection.Event event = (EventCollection.Event)i.next();
                if ( event.getMethod() == ContentEvent.STORE ) {
                    removeContentEvents(changedContents, ((ContentEvent)event.getEvent()).getUri());
                    changedContents.add(event.getEvent());
                } else if ( event.getMethod() == ContentEvent.REMOVE ) {
                    removeContentEvents(changedContents, ((ContentEvent)event.getEvent()).getUri());
                }
            }
        }
        ContentEvent[] changedContentEvents = new ContentEvent[changedContents.size()];
        return (ContentEvent [])changedContents.toArray(changedContentEvents);
    }

    public static ContentEvent[] getCreatedContents(EventCollection collection) {
        List changedContents = new ArrayList();
        if (collection != null)
        {
            List collectedEvents = collection.getCollection();
            for ( Iterator i = collectedEvents.iterator(); i.hasNext(); ) {
                EventCollection.Event event = (EventCollection.Event)i.next();
                if ( event.getMethod() == ContentEvent.CREATE ) {
                    removeContentEvents(changedContents, ((ContentEvent)event.getEvent()).getUri());
                    changedContents.add(event.getEvent());
                } else if ( event.getMethod() == ContentEvent.REMOVE ) {
                    removeContentEvents(changedContents, ((ContentEvent)event.getEvent()).getUri());
                }
            }
        }
        ContentEvent[] changedContentEvents = new ContentEvent[changedContents.size()];
        return (ContentEvent [])changedContents.toArray(changedContentEvents);
    }

    public static ContentEvent[] getRemovedContents(EventCollection collection) {
        List changedContents = new ArrayList();
        if (collection != null)
        {
            List collectedEvents = collection.getCollection();
            for ( Iterator i = collectedEvents.iterator(); i.hasNext(); ) {
                EventCollection.Event event = (EventCollection.Event)i.next();
                if ( event.getMethod() == ContentEvent.REMOVE ) {
                    removeContentEvents(changedContents, ((ContentEvent)event.getEvent()).getUri());
                    changedContents.add(event.getEvent());
                } else if ( event.getMethod() == ContentEvent.CREATE || event.getMethod() == ContentEvent.STORE ) {
                    removeContentEvents(changedContents, ((ContentEvent)event.getEvent()).getUri());
                }
            }
        }
        ContentEvent[] changedContentEvents = new ContentEvent[changedContents.size()];
        return (ContentEvent [])changedContents.toArray(changedContentEvents);
    }

    private static void removeContentEvents(List changedContents, String uri) {
        for ( Iterator i = changedContents.iterator(); i.hasNext(); ) {
            ContentEvent event = (ContentEvent)i.next();
            if ( ((ContentEvent)event).getUri().equals(uri) ) {
                i.remove();
            }
        }
    }
}