/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/event/SearchEvent.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.event;

import org.apache.slide.common.SlideToken;
import org.apache.slide.common.Namespace;
import org.apache.slide.search.SearchQuery;

import java.util.EventListener;
import java.util.EventObject;

/**
 * Search event class
 *
 * @version $Revision: 1.2 $
 */
public class SearchEvent extends EventObject {
    public final static Search SEARCH = new Search();

    private SlideToken token;
    private SearchQuery query;
    private Namespace namespace;

    public final static String GROUP = "search";
    public final static AbstractEventMethod[] methods = new AbstractEventMethod[] { SEARCH };

    public SearchEvent(Object source, SlideToken token, Namespace namespace, SearchQuery query) {
        super(source);
        this.token = token;
        this.namespace = namespace;
        this.query = query;
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer(256);
        buffer.append(getClass().getName()).append("[query=").append(query);
        buffer.append("]");
        return buffer.toString();
    }

    public Namespace getNamespace() {
        return namespace;
    }

    public SlideToken getToken() {
        return token;
    }

    public SearchQuery getQuery() {
        return query;
    }

    public final static class Search extends VetoableEventMethod {
        public Search() {
            super(GROUP, "search");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException {
            if ( listener instanceof SearchListener ) ((SearchListener)listener).search((SearchEvent)event);
        }
    }
}