/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/event/TransientEventCollector.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.event;

import java.util.EventObject;

/**
 * Transient event collector class
 *
 * @version $Revision: 1.2 $
 */
public class TransientEventCollector implements GlobalListener {
    private static ThreadLocal eventCollection = new ThreadLocal();

    public void vetoableEventFired(VetoableEventMethod method, EventObject event) throws VetoException {
        if ( eventCollection.get() != null ) {
            ((EventCollection)eventCollection.get()).addEvent(method, event);
        }
    }

    public void eventFired(EventMethod method, EventObject event) {
        if ( method == TransactionEvent.BEGIN ) {
            eventCollection.set(new EventCollection(this));
        } else if ( method == TransactionEvent.COMMITED ) {
            final EventCollection collection = (EventCollection)eventCollection.get();
            new Thread(new Runnable() {
                public void run() {
                    if ( EventCollection.COLLECTED.isEnabled() ) EventDispatcher.getInstance().fireEvent(EventCollection.COLLECTED, collection);
                }
            }).start();
        } else if ( eventCollection.get() != null ) {
            ((EventCollection)eventCollection.get()).addEvent(method, event);
        }
    }
}