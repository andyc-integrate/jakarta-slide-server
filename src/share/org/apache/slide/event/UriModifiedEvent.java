/*
 *  Copyright 1999-2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.event;

import java.util.EventListener;
import java.util.EventObject;

import org.apache.slide.common.Uri;

/**
 * Indicates that an Uri has been somehow modified.
 * 
 */
public class UriModifiedEvent extends EventObject {

	public static final UriModified URIMODIFIED = new UriModified();
	public final static AbstractEventMethod[] methods = new AbstractEventMethod[] { URIMODIFIED };
	
	public static final String GROUP = "urimodified";
	
	private Uri uri;
	
	public UriModifiedEvent(Object source, Uri uri) {
		super(source);
		this.uri = uri;
	}
	
	public Uri getUri() {
		return uri;
	}
	
	public static class UriModified extends EventMethod {

		public UriModified() {
			super( GROUP, "urimodified" );
		}

		public void fireEvent( EventListener listener, EventObject event ) {
			if ( listener instanceof UriModifiedListener ) ((UriModifiedListener)listener).modified((UriModifiedEvent)event);
		}
		
	}
}
