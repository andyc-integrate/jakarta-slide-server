/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/event/GenericEvent.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.event;

import java.util.EventListener;
import java.util.EventObject;

/**
 * The GenericEvent class
 * 
 */
public class GenericEvent extends EventObject implements RemoteInformation {
    public final static EventFired EVENT_FIRED = new EventFired();
    public final static VetoableEventFired VETOABLE_EVENT_FIRED = new VetoableEventFired();

    public final static String GROUP = "generic";
    public final static AbstractEventMethod[] methods = new AbstractEventMethod[] { EVENT_FIRED, VETOABLE_EVENT_FIRED };

    private String[][] information;

    public GenericEvent(Object source, String[][] information) {
        super(source);
        this.information = information;
    }

    public String[][] getInformation() {
        return information;
    }

    public final static class VetoableEventFired extends VetoableEventMethod {
        public VetoableEventFired() {
            super(GROUP, "vetoableEventFired");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException  {
            if ( listener instanceof GenericEventListener ) ((GenericEventListener)listener).vetoableEventFired((GenericEvent)event);
        }
    }

    public final static class EventFired extends EventMethod {
        public EventFired() {
            super(GROUP, "eventFired");
        }

        public void fireEvent(EventListener listener, EventObject event) {
            if ( listener instanceof GenericEventListener ) ((GenericEventListener)listener).eventFired((GenericEvent)event);
        }
    }
}
