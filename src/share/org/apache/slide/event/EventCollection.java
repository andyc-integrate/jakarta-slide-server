/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/event/EventCollection.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.event;

import java.util.*;

/**
 * Event collection class
 *
 * @version $Revision: 1.2 $
 */
public class EventCollection extends EventObject {
    List collection = new ArrayList();

    public final static Collected COLLECTED = new Collected();
    public final static VetoableCollected VETOABLE_COLLECTED = new VetoableCollected();

    public final static String GROUP = "event-collection";
    public final static AbstractEventMethod[] methods = new AbstractEventMethod[] { COLLECTED, VETOABLE_COLLECTED };

    public EventCollection(Object source) {
        super(source);
    }

    public void addEvent(AbstractEventMethod method, EventObject event) {
        synchronized ( collection ) {
            collection.add(new Event(method, event));
        }
    }

    public List getCollection() {
        return collection;
    }

    public String toString() {
        String NEWLINE = System.getProperty("line.separator");
        StringBuffer buffer = new StringBuffer(256);
        buffer.append(getClass().getName()).append("[collected events=");
        synchronized ( collection ) {
            for ( Iterator i = collection.iterator(); i.hasNext(); ) {
                buffer.append(NEWLINE);
                Event event = (Event)i.next();
                if ( event.getEvent() instanceof RemoteInformation ) {
                    StringBuffer infoBuffer = new StringBuffer();
                    String[][] information = ((RemoteInformation)event.getEvent()).getInformation();
                    boolean first = true;
                    for ( int j = 0; j < information.length; j++ ) {
                        if ( !first ) infoBuffer.append(", ");
                        first = false;
                        infoBuffer.append(information[j][0]).append("=").append(information[j][1]);
                    }
                    buffer.append("["+event.getClass().getName()+" [name="+event.getMethod().getId()+", information: "+infoBuffer.toString()+"]]");
                } else {
                    buffer.append("["+event.getClass().getName()+" [name="+event.getMethod().getId()+"]]");
                }
            }
        }
        buffer.append("]");
        return buffer.toString();
    }

    public static class Collected extends EventMethod {
        public Collected() {
            super(GROUP, "collected");
        }

        public void fireEvent(EventListener listener, EventObject event) {
            if (listener instanceof EventCollectionListener ) {
                ((EventCollectionListener)listener).collected((EventCollection)event);
            }
        }
    }

    public static class VetoableCollected extends VetoableEventMethod {
        public VetoableCollected() {
            super(GROUP, "vetoable-collected");
        }

        public void fireVetaoableEvent(EventListener listener, EventObject event) throws VetoException {
            if (listener instanceof EventCollectionListener ) {
                ((EventCollectionListener)listener).vetoableCollected((EventCollection)event);
            }
        }
    }

    public class Event {
        private AbstractEventMethod method;
        private EventObject event;

        public Event(AbstractEventMethod method, EventObject event) {
            this.method = method;
            this.event = event;
        }

        public AbstractEventMethod getMethod() {
            return method;
        }

        public EventObject getEvent() {
            return event;
        }
    }
}