/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/content/InsufficientStorageException.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */ 

package org.apache.slide.content;

import org.apache.slide.util.Messages;

/**
 * Exception thrown to indicate that a limitation of available storage space 
 * has been reached or exceeded.
 * 
 * @version $Revision: 1.2 $
 */
public class InsufficientStorageException
    extends ContentException {
    
    
    // ----------------------------------------------------------- Constructors
    
    
    /**
     * Constructor.
     * 
     * @param objectUri Uri of the object
     * @param userUri Uri of the user
     * @param details Details on the exception
     */
    public InsufficientStorageException(String objectUri, String userUri,
                                        String details) {
        super(Messages.format(InsufficientStorageException.class.getName(), 
                              objectUri, userUri, details));
        
        this.objectUri = objectUri;
        this.userUri = userUri;
        this.details = details;
    }
    
    
    // ----------------------------------------------------- Instance Variables
    
    
    /**
     * Object URI.
     */
    private String objectUri;
        
    
    /**
     * User URI.
     */
    private String userUri;
    
    
    /**
     * Exception details.
     */
    private String details;
    
    
    // ------------------------------------------------------------- Properties
    
    
    /**
     * Returns the URI of the object.
     * 
     * @return the object URI
     */
    public String getObjectUri() {
        
        if (objectUri == null) {
            return new String();
        } else {
            return objectUri;
        }
    }
    
    
    /**
     * Returns the URI of the user.
     * 
     * @return the user URI
     */
    public String getUserUri() {
        
        return userUri;
    }
    
    
    /**
     * Returns a String containing details about the exception.
     * 
     * @return details about the exception
     */
    public String getDetails() {
        
        return details;
    }
    
    
}

