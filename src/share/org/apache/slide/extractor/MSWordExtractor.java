/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/extractor/MSWordExtractor.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.extractor;

/**
 * Author: Ryan Rhodes
 * Date: Jun 26, 2004
 * Time: 12:34:29 AM
 */

import org.textmining.extraction.word.WordTextExtractorFactory;
import org.textmining.extraction.TextExtractor;

import java.io.*;


public class MSWordExtractor extends AbstractContentExtractor {

    public MSWordExtractor(String uri, String contentType, String namespace) {
        super(uri, contentType, namespace);
    }

    public Reader extract(InputStream content)  throws ExtractorException {
        try {

            // Create factory for extractors
            WordTextExtractorFactory wordTextExtractorFactory =
                    new WordTextExtractorFactory();

            // Get an instance of TextExtractor - which one depends on MS
            // Word version
            TextExtractor textExtractor =
                    wordTextExtractorFactory.textExtractor(content);

            // Get the text from the extractor
            String text = textExtractor.getText();          

            // Return the results
            StringReader reader = new StringReader(text);
            return reader;
        }
        catch(Exception e) {
            throw new ExtractorException(e.getMessage());
        }
    }

        public static void main(String[] args) throws Exception
        {
            FileInputStream in = new FileInputStream(args[0]);

            MSWordExtractor ex = new MSWordExtractor(null, null, null);

            Reader reader = ex.extract(in);

            int c;
            do
            {
                c = reader.read();

                System.out.print((char)c);
            }
            while( c != -1 );
        }
}
