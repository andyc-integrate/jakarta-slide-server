/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/extractor/ExtractorManager.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.extractor;

import org.apache.slide.util.conf.Configurable;
import org.apache.slide.util.conf.Configuration;
import org.apache.slide.util.conf.ConfigurationException;
import org.apache.slide.content.NodeRevisionDescriptors;
import org.apache.slide.content.NodeRevisionDescriptor;

import java.util.*;
import java.lang.reflect.Constructor;

/**
 * The ExtractorManager class
 */
public class ExtractorManager implements Configurable {
    private final static ExtractorManager manager = new ExtractorManager();

    private List extractors = new ArrayList();

    private ExtractorManager() {}

    public static ExtractorManager getInstance() {
        return manager;
    }

    public void addExtractor(Extractor extractor) {
        extractors.add(extractor);
    }

    public PropertyExtractor[] getPropertyExtractors(String namespace, NodeRevisionDescriptors descriptors, NodeRevisionDescriptor descriptor) {
        List matchingExtractors = new ArrayList();
        for ( Iterator i = extractors.iterator(); i.hasNext(); ) {
            Extractor extractor = (Extractor)i.next();
            if ( extractor instanceof PropertyExtractor && matches(extractor, namespace, descriptors, descriptor)) {
                matchingExtractors.add(extractor);
            }
        }
        PropertyExtractor[] extractors = new PropertyExtractor[matchingExtractors.size()];
        return (PropertyExtractor [])matchingExtractors.toArray(extractors);
    };

    public ContentExtractor[] getContentExtractors(String namespace, NodeRevisionDescriptors descriptors, NodeRevisionDescriptor descriptor) {
        List matchingExtractors = new ArrayList();
        for ( Iterator i = extractors.iterator(); i.hasNext(); ) {
            Extractor extractor = (Extractor)i.next();
            if ( extractor instanceof ContentExtractor && matches(extractor, namespace, descriptors, descriptor)) {
                matchingExtractors.add(extractor);
            }
        }
        ContentExtractor[] extractors = new ContentExtractor[matchingExtractors.size()];
        return (ContentExtractor [])matchingExtractors.toArray(extractors);
    };

    public Extractor[] getExtractors(String namespace, NodeRevisionDescriptors descriptors, NodeRevisionDescriptor descriptor) {
        List matchingExtractors = new ArrayList();
        for ( Iterator i = extractors.iterator(); i.hasNext(); ) {
            Extractor extractor = (Extractor)i.next();
            if ( matches(extractor, namespace, descriptors, descriptor)) {
                matchingExtractors.add(extractor);
            }
        }
        Extractor[] extractors = new Extractor[matchingExtractors.size()];
        return (Extractor [])matchingExtractors.toArray(extractors);
    };

    public boolean matches(Extractor extractor, String namespace, NodeRevisionDescriptors descriptors, NodeRevisionDescriptor descriptor) {
        boolean matching = true;
        if ( descriptor != null && extractor.getContentType() != null && !descriptor.getContentType().equals(extractor.getContentType()) ) {
            matching = false;
        }
        if ( descriptors != null && extractor.getUri() != null && !descriptors.getUri().startsWith(extractor.getUri()) ) {
            matching = false;
        }
        if ( descriptors != null && extractor.getNamespace() != null && !extractor.getNamespace().equals(namespace)) {
            matching = false;
        }
        return matching;
    }

    public void configure(Configuration config) throws ConfigurationException {
        Enumeration extractorConfigs = config.getConfigurations("extractor");
        while (extractorConfigs.hasMoreElements()) {
            Configuration extractorConfig = (Configuration)extractorConfigs.nextElement();
            String classname = extractorConfig.getAttribute("classname");
            String uri = extractorConfig.getAttribute("uri", null);
            String contentType = extractorConfig.getAttribute("content-type", null);
            String namespace = extractorConfig.getAttribute("namespace", null);
            try {
                Class extractorClass = Class.forName(classname);
                Extractor extractor = null;
                try {
                    Constructor extractorConstructor = extractorClass.getConstructor(new Class[] { String.class, String.class, String.class } );
                    extractor = (Extractor) extractorConstructor.newInstance(new String[] { uri, contentType, namespace });
                }
                catch (NoSuchMethodException e) {
                    Constructor extractorConstructor = extractorClass.getConstructor(new Class[] { String.class, String.class } );
                    extractor = (Extractor) extractorConstructor.newInstance(new String[] { uri, contentType });
                }
                if ( extractor instanceof Configurable ) {
                    ((Configurable)extractor).configure(extractorConfig.getConfiguration("configuration"));
                }
                addExtractor(extractor);
            } catch (ClassCastException e) {
                throw new ConfigurationException("Extractor '"+classname+"' is not of type Extractor", config);
            } catch (ConfigurationException e) {
                throw e;
            } catch (Exception e) {
                throw new ConfigurationException("Extractor '"+classname+"' could not be loaded", config);
            }
        }
    }
}