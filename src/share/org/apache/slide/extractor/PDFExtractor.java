/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/extractor/PDFExtractor.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.extractor;

import org.pdfbox.util.PDFTextStripper;
import org.pdfbox.pdfparser.PDFParser;
import org.pdfbox.pdmodel.PDDocument;

import java.io.*;

/**
 * Author: Ryan Rhodes
 * Date: Jun 26, 2004
 * Time: 4:03:00 AM
 */
public class PDFExtractor extends AbstractContentExtractor
{

    public PDFExtractor(String uri, String contentType, String namespace)
    {
        super(uri, contentType, namespace);
    }

    public Reader extract(InputStream content)  throws ExtractorException
    {
        try
        {
            PDFParser parser = new PDFParser( content );
            parser.parse();

            PDDocument document = parser.getPDDocument();

            CharArrayWriter writer = new CharArrayWriter();

            PDFTextStripper stripper = new PDFTextStripper();
            stripper.setLineSeparator("\n");
            stripper.writeText(document, writer);

            document.close();
            writer.close();

            return new CharArrayReader(writer.toCharArray());
        }
        catch(Exception e )
        {
            throw new ExtractorException(e.getMessage());
        }
    }

    public static void main(String[] args) throws Exception
    {
        FileInputStream in = new FileInputStream(args[0]);

        PDFExtractor ex = new PDFExtractor(null, null, null);

        Reader reader = ex.extract(in);

        int c = 0;
        do
        {
            c = reader.read();
            System.out.print((char)c);
        }
        while(c != -1);
    }
}
