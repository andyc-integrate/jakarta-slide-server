/*
 * $Header:
 * $Revision: 1.2 $
 * $Date:
 *
 * ====================================================================
 *
 * Copyright 1999 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.slide.store;
import java.util.Hashtable;
import org.apache.slide.common.XAServiceBase;
import org.apache.slide.common.ServiceAccessException;
import org.apache.slide.common.ServiceConnectionFailedException;
import org.apache.slide.common.ServiceDisconnectionFailedException;
import org.apache.slide.common.ServiceParameterErrorException;
import org.apache.slide.common.ServiceParameterMissingException;
import org.apache.slide.common.ServiceResetFailedException;
import org.apache.slide.common.Uri;
import org.apache.slide.content.NodeRevisionContent;
import org.apache.slide.content.NodeRevisionDescriptor;
import org.apache.slide.content.NodeRevisionNumber;
import org.apache.slide.search.IndexException;
import org.apache.slide.search.basic.IBasicExpressionFactory;
import org.apache.slide.search.basic.IBasicExpressionFactoryProvider;
import org.apache.slide.store.IndexStore;


/**
 * DefaultIndexer is loaded, when no Indexer was explicitly configured for a
 * store (content or properties). Initilializes with the store specific
 * indexer if store implements IBasicExpressionFactoryProvider, else with null,
 * which means, no search possible on store (unless generic search).
 * Fakes two phase commit.
 *
 * @version $Revision: 1.2 $
 */
public class DefaultIndexer extends XAServiceBase implements IndexStore
{
    private final IBasicExpressionFactoryProvider expressionProvider;
    
    /**
     * Uses the ExpressionFactory of associated store (if applicable)
     *
     * @param    associatedStore     a properties- or content store
     *
     */
    public DefaultIndexer (Object associatedStore) {
        if (associatedStore instanceof IBasicExpressionFactoryProvider) {
            expressionProvider = (IBasicExpressionFactoryProvider) associatedStore;
        }
        else {
            expressionProvider = null;
        }
    }
    
    /**
     * As defined in IBasicExpressionFactoryProvider
     *
     * @return   an IBasicExpressionFactory
     *
     */
    public IBasicExpressionFactory getBasicExpressionFactory()
    {
        if (expressionProvider != null) {
            return expressionProvider.getBasicExpressionFactory();
        }
        return null;
    }
    
    // used for two phase commit state
    private boolean started = false;
    
    
    /**
     * Index an object content. Do nothing.
     *
     * @param uri Uri
     * @exception IndexException Error accessing the Data Source
     */
    public void createIndex(Uri uri, NodeRevisionDescriptor revisionDescriptor,
                            NodeRevisionContent revisionContent)
        throws IndexException
    {
        // do nothing
    }
    
    /**
     * updates an index for a resource
     *
     * @param    uri                uri of the resource
     * @param    number             nodeRevisionNumber of the resource
     * @param    revisionContent    the content of the resource
     *
     * @throws   IndexException
     *
     */
    public void updateIndex(Uri uri, NodeRevisionDescriptor revisionDescriptor, NodeRevisionContent revisionContent) throws IndexException
    {
        // do nothing
    }
    
    
    /**
     * Drop an object revision from the index. Do nothing
     *
     * @param uri Uri
     * @exception IndexException Error accessing the Data Source
     */
    public void dropIndex(Uri uri, NodeRevisionNumber number)
        throws IndexException
    {
        // do nothing
    }
    
    /**
     * Connects to the underlying data source (if any is needed).
     *
     * @exception  ServiceConnectionFailedException Connection failed
     */
    public void connect() throws ServiceConnectionFailedException
    {
        started = true;
    }
    
    /**
     * This function tells whether or not the service is connected.
     *
     * @return boolean true if we are connected
     * @exception ServiceAccessException Service access error
     */
    public boolean isConnected() throws ServiceAccessException
    {
        return started;
    }
    
    /**
     * Initializes the service with a set of parameters. Those could be :
     * <li>User name, login info
     * <li>Host name on which to connect
     * <li>Remote port
     * <li>JDBC driver whoich is to be used :-)
     * <li>Anything else ...
     *
     * @param parameters Hashtable containing the parameters' names
     * and associated values
     * @exception ServiceParameterErrorException Incorrect service parameter
     * @exception ServiceParameterMissingException Service parameter missing
     */
    public void setParameters(Hashtable parameters) throws ServiceParameterErrorException, ServiceParameterMissingException
    {
        // do nothing
    }
    
    /**
     * Disconnects from the underlying data source.
     *
     * @exception ServiceDisconnectionFailedException Disconnection failed
     */
    public void disconnect() throws ServiceDisconnectionFailedException
    {
        started = false;
    }
    
    /**
     * Deletes service underlying data source, if possible (and meaningful).
     *
     * @exception ServiceResetFailedException Reset failed
     */
    public void reset() throws ServiceResetFailedException
    {
        // do nothing
    }
    
}

