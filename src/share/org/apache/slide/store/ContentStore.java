/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/store/ContentStore.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.store;

import org.apache.slide.common.Service;
import org.apache.slide.common.ServiceAccessException;
import org.apache.slide.common.Uri;
import org.apache.slide.content.NodeRevisionContent;
import org.apache.slide.content.NodeRevisionDescriptor;
import org.apache.slide.content.RevisionAlreadyExistException;
import org.apache.slide.content.RevisionNotFoundException;

/**
 * Store service.
 *
 * @version $Revision: 1.2 $
 */
public interface ContentStore extends Service {
    
    
    // ------------------------------------------------------ Interface Methods
    
    
    /**
     * Retrieve revision content.
     *
     * @param uri Uri
     * @param revisionDescriptor Node revision descriptor
     */
    NodeRevisionContent retrieveRevisionContent
        (Uri uri, NodeRevisionDescriptor revisionDescriptor)
        throws ServiceAccessException, RevisionNotFoundException;
    
    
    /**
     * Create revision content.
     * <p>
     * Notes:
     * <ul>
     * <li>If a content length is specified by the revisionDescriptor
     * (ie, revisionDescriptor.getContentLength != -1), then
     * the ContentStore MUST enforce it, and throw a ServiceAccessException
     * if an incorrect number of bytes are read. It MUST also revert any
     * changes made to the underlying repository.</li>
     * <li>If a content length is not specified by the revisionDescriptor
     * (ie, revisionDescriptor.getContentLength == -1), then the ContentStore
     * MUST read all the bytes available and then call
     * revisionDescriptor.setContentLength(numberOfBytesRead).</li>
     * </ul>
     *
     * @param uri Uri
     * @param revisionDescriptor Node revision descriptor
     * @param revisionContent Node revision content
     */
    void createRevisionContent
        (Uri uri, NodeRevisionDescriptor revisionDescriptor,
         NodeRevisionContent revisionContent)
        throws ServiceAccessException, RevisionAlreadyExistException;
    
    
    /**
     * Modify revision content.
     * <p>
     * Notes:
     * <ul>
     * <li>If a content length is specified by the revisionDescriptor
     * (ie, revisionDescriptor.getContentLength != -1), then
     * the ContentStore MUST enforce it, and throw a ServiceAccessException
     * if an incorrect number of bytes are read. It MUST also revert any
     * changes made to the unedrlying repository.</li>
     * <li>If a content length is not specified by the revisionDescriptor
     * (ie, revisionDescriptor.getContentLength == -1), then the ContentStore
     * MUST read all the bytes available and then call
     * revisionDescriptor.setContentLength(numberOfBytesRead).</li>
     * </ul>
     *
     * @param uri Uri
     * @param revisionDescriptor Node revision descriptor
     * @param revisionContent Node revision content
     */
    void storeRevisionContent
        (Uri uri, NodeRevisionDescriptor revisionDescriptor,
         NodeRevisionContent revisionContent)
        throws ServiceAccessException, RevisionNotFoundException;
    
    
    /**
     * Remove revision content.
     *
     * @param uri Uri
     * @param revisionDescriptor Node revision descriptor
     */
    void removeRevisionContent(Uri uri, NodeRevisionDescriptor revisionDescriptor)
        throws ServiceAccessException;
    
}
