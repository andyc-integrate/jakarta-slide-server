/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/store/SecurityStore.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */ 

package org.apache.slide.store;

import java.util.Enumeration;

import org.apache.slide.common.Service;
import org.apache.slide.common.ServiceAccessException;
import org.apache.slide.common.Uri;
import org.apache.slide.security.NodePermission;

/**
 * Store for NodePermission objects.
 * 
 * @version $Revision: 1.2 $
 */
public interface SecurityStore extends Service {
    
    
    // ------------------------------------------------------ Interface Methods
    
    
    /**
     * Grant a new permission.
     * 
     * @param permission Permission we want to create
     * @exception ServiceAccessException Error accessing the Data Source
     */
    void grantPermission(Uri uri, NodePermission permission)
        throws ServiceAccessException;
    
    
    /**
     * Revoke a permission.
     * 
     * @param permission Permission we want to create
     * @exception ServiceAccessException Error accessing the Data Source
     */
    void revokePermission(Uri uri, NodePermission permission)
        throws ServiceAccessException;
    
    
    /**
     * Revoke all the permissions on an object.
     * 
     * @param uri Uri of the object
     * @exception ServiceAccessException Error accessing the Data Source
     */
    void revokePermissions(Uri uri)
        throws ServiceAccessException;
    
    
    /**
     * Enumerate an object permissions.
     *
     * @param uri The uri of the object
     * @exception ServiceAccessException Error accessing the Descriptors Store
     * @return A enumeration of all 
     * {@link org.apache.slide.security.NodePermission permissions} on this object.
     */
    Enumeration enumeratePermissions(Uri uri)
        throws ServiceAccessException;
    
    
}
