/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/util/Iterator.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */ 

package org.apache.slide.util;

/**
 * An implementation of the JDK 1.2 Iterator interface.
 * I wrote this because I want people using 1.1.x to be able 
 * to use my apps, but I also wanted to get a start in moving my 
 * source to JDK 1.2<BR>
 * 
 * @see java.util.Iterator
 * @version $Revision: 1.2 $ $Date: 2006-01-22 22:49:05 $ 
**/
public interface Iterator {
    public boolean hasNext();
    public Object next();
    public Object remove();
} //-- Iterator
