/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/util/conf/AbstractConfiguration.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.util.conf;

import java.util.Enumeration;

/**
 * This is an abstract <code>Configuration</code> implementation that deals
 * with methods that can be abstracted away from underlying implementations.
 *
 *         (Betaversion Productions)
 *         (Apache Software Foundation)
 *         (Apache Software Foundation, Exoffice Technologies)
 * @version CVS $Revision: 1.2 $ $Date: 2006-01-22 22:49:05 $
 */
public abstract class AbstractConfiguration implements Configuration {
    /**
     * The location string containing information about this
     * <code>Configuration</code> location in the source file.
     */
    protected String location=null;

    /**
     * Construct a new <code>AbstractConfiguration</code> instance.
     */
    protected AbstractConfiguration() {
        this(null,-1);
    }

    /**
     * Construct a new <code>AbstractConfiguration</code> instance.
     */
    protected AbstractConfiguration(String source, int line) {
        super();
        this.location="";
        if (source!=null) this.location=source;
        if ((line>=0)&&(this.location.length()>0)) this.location+=" ";
        if (line>0) this.location+="line "+line;
        if (this.location.length()>0) this.location="("+this.location+")";
        else this.location=null;
    }

    /**
     * Returns the value of the configuration element as an <code>int</code>.
     */
    public int getValueAsInt()
	throws ConfigurationException {
        String value=this.getValue();
        try {
            if (value.startsWith("0x"))
                return(Integer.parseInt(value.substring(2),16));
            else if (value.startsWith("0o"))
                return(Integer.parseInt(value.substring(2),8));
            else if (value.startsWith("0b"))
                return(Integer.parseInt(value.substring(2),2));
            else return(Integer.parseInt(value));
        } catch (NumberFormatException e) {
            throw new ConfigurationException("Cannot parse the value of the "+
                "configuration element \""+this.getName()+"\" as an integer",
                this);
        }
	}

    /**
     * Returns the value of the configuration element as a <code>long</code>.
     */
    public long getValueAsLong()
	throws ConfigurationException {
        String value=this.getValue();
        try {
            if (value.startsWith("0x"))
                return(Long.parseLong(value.substring(2),16));
            else if (value.startsWith("0o"))
                return(Long.parseLong(value.substring(2),8));
            else if (value.startsWith("0b"))
                return(Long.parseLong(value.substring(2),2));
            else return(Integer.parseInt(value));
        } catch (NumberFormatException e) {
            throw new ConfigurationException("Cannot parse the value of the "+
                "configuration element \""+this.getName()+"\" as a long", this);
        }
	}

    /**
     * Returns the value of the configuration element as a <code>float</code>.
     */
    public float getValueAsFloat()
	throws ConfigurationException {
        String value=this.getValue();
        try {
            return(Float.valueOf(value).floatValue());
        } catch (NumberFormatException e) {
            throw new ConfigurationException("Cannot parse the value of the "+
                "configuration element \""+this.getName()+"\" as a float",
                this);
        }
    }

    /**
     * Returns the value of the configuration element as a <code>boolean</code>.
     */
    public boolean getValueAsBoolean()
	throws ConfigurationException {
        String value=this.getValue();
        if (value.equals("true")) return(true);
        if (value.equals("false")) return(false);
        throw new ConfigurationException("Cannot parse the value of the "+
            "configuration element \""+this.getName()+"\" as a boolean",
            this);
    }

    /**
     * Returns the value of the configuration element as a <code>String</code>.
     */
    public String getValue(String defaultValue) {
        try {
            return(this.getValue());
        } catch (ConfigurationException e) {
            return(defaultValue);
        }
    }

    /**
     * Returns the value of the configuration element as an <code>int</code>.
     */
    public int getValueAsInt(int defaultValue) {
        try {
            return(this.getValueAsInt());
        } catch (ConfigurationException e) {
            return(defaultValue);
        }
    }

    /**
     * Returns the value of the configuration element as a <code>long</code>.
     */
    public long getValueAsLong(long defaultValue) {
        try {
            return(this.getValueAsLong());
        } catch (ConfigurationException e) {
            return(defaultValue);
        }
    }

    /**
     * Returns the value of the configuration element as a <code>float</code>.
     */
    public float getValueAsFloat(float defaultValue) {
        try {
            return(this.getValueAsFloat());
        } catch (ConfigurationException e) {
            return(defaultValue);
        }
    }

    /**
     * Returns the value of the configuration element as a <code>boolean</code>.
     */
    public boolean getValueAsBoolean(boolean defaultValue) {
        try {
            return(this.getValueAsBoolean());
        } catch (ConfigurationException e) {
            return(defaultValue);
        }
    }

    /**
     * Returns the value of the attribute specified by its name as an
     * <code>int</code>.
     */
    public int getAttributeAsInt(String name)
	throws ConfigurationException {
        String value=this.getAttribute(name);
        try {
            if (value.startsWith("0x"))
                return(Integer.parseInt(value.substring(2),16));
            else if (value.startsWith("0o"))
                return(Integer.parseInt(value.substring(2),8));
            else if (value.startsWith("0b"))
                return(Integer.parseInt(value.substring(2),2));
            else return(Integer.parseInt(value));
        } catch (NumberFormatException e) {
            throw new ConfigurationException("Cannot parse the value of the "+
                "attribute \""+name+"\" of the configuration element \""+
                this.getName()+"\" as an integer",this);
        }
    }

    /**
     * Returns the value of the attribute specified by its name as a
     * <code>long</code>.
     */
    public long getAttributeAsLong(String name)
	throws ConfigurationException {
        String value=this.getAttribute(name);
        try {
            if (value.startsWith("0x"))
                return(Long.parseLong(value.substring(2),16));
            else if (value.startsWith("0o"))
                return(Long.parseLong(value.substring(2),8));
            else if (value.startsWith("0b"))
                return(Long.parseLong(value.substring(2),2));
            else return(Integer.parseInt(value));
        } catch (NumberFormatException e) {
            throw new ConfigurationException("Cannot parse the value of the "+
                "attribute \""+name+"\" of the configuration element \""+
                this.getName()+"\" as a long", this);
        }
    }

    /**
     * Returns the value of the attribute specified by its name as a
     * <code>float</code>.
     */
    public float getAttributeAsFloat(String name)
	throws ConfigurationException {
        String value=this.getAttribute(name);
        try {
            return(Float.valueOf(value).floatValue());
        } catch (NumberFormatException e) {
            throw new ConfigurationException("Cannot parse the value of the "+
                "attribute \""+name+"\" of the configuration element \""+
                this.getName()+"\" as a float", this);
        }
    }

    /**
     * Returns the value of the attribute specified by its name as a
     * <code>boolean</code>.
     */
    public boolean getAttributeAsBoolean(String name)
	throws ConfigurationException {
        String value=this.getAttribute(name);
        if (value.equals("true")) return(true);
        if (value.equals("false")) return(false);
        throw new ConfigurationException("Cannot parse the value of the "+
            "attribute \""+name+"\" of the configuration element \""+
            this.getName()+"\" as a boolean", this);
    }

    /**
     * Returns the value of the attribute specified by its name as a
     * <code>String</code>.
     */
    public String getAttribute(String name, String defaultValue) {
        try {
            return(this.getAttribute(name));
        } catch (ConfigurationException e) {
            return(defaultValue);
        }
    }

    /**
     * Returns the value of the attribute specified by its name as an
     * <code>int</code>.
     */
    public int getAttributeAsInt(String name, int defaultValue) {
        try {
            return(this.getAttributeAsInt(name));
        } catch (ConfigurationException e) {
            return(defaultValue);
        }
    }

    /**
     * Returns the value of the attribute specified by its name as a
     * <code>long</code>.
     */
    public long getAttributeAsLong(String name, long defaultValue) {
        try {
            return(this.getAttributeAsLong(name));
        } catch (ConfigurationException e) {
            return(defaultValue);
        }
    }

    /**
     * Returns the value of the attribute specified by its name as a
     * <code>float</code>.
     */
    public float getAttributeAsFloat(String name, float defaultValue) {
        try {
            return(this.getAttributeAsFloat(name));
        } catch (ConfigurationException e) {
            return(defaultValue);
        }
    }

    /**
     * Returns the value of the attribute specified by its name as a
     * <code>boolean</code>.
     */
    public boolean getAttributeAsBoolean(String name, boolean defaultValue) {
        try {
            return(this.getAttributeAsBoolean(name));
        } catch (ConfigurationException e) {
            return(defaultValue);
        }
    }

    /**
     * Return the first <code>Configuration</code> object child of this
     * associated with the given name.
     */
    public Configuration getConfiguration(String name) {
	    Enumeration e=this.getConfigurations(name);
	    if (e.hasMoreElements()) return((Configuration)e.nextElement());
        return(null);
	}

	/**
	 * Return a <code>String</code> indicating the position of this
	 * configuration element in a source file or URI or <b>null</b>.
	 */
	public String getLocation() {
	    return(this.location);
	}
}
