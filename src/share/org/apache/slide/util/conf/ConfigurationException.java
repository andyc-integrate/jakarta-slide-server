/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/util/conf/ConfigurationException.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.util.conf;

/**
 * Thrown when a <code>Configurable</code> component cannot be configured
 * properly, or if a value cannot be retrieved properly.
 *
 * @version CVS $Revision: 1.2 $ $Date: 2006-01-22 22:49:05 $
 */
public class ConfigurationException extends RuntimeException {

    /** The current configuration */
    private Configuration configuration=null;

    /**
     * Construct a new <code>ConfigurationException</code> instance.
     *
     * @param message The detail message for this exception (mandatory).
     * @param conf The configuration element.
     */
    public ConfigurationException(String message, Configuration conf) {
        super(message);
        this.configuration=conf;
    }

    /**
     * Get the <code>Configuration</code> element.
     *
     * @return <code>Configuration</code> element associated, or
     *         <code>null</code> if there is none.
     */
    public Configuration getConfiguration() {
        return (this.configuration);
    }

    /**
     * Return this <code>ConfigurationException</code> (if possible with
     * location information).
     */
    public String getMessage() {
        String msg=super.getMessage();

        if (this.configuration!=null) {
            msg = msg + " @ " + this.configuration.getLocation();
        }

        return(msg);
    }
}
