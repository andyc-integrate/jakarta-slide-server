/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/util/Messages.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */ 

package org.apache.slide.util;

import java.text.*;
import java.util.*;
import org.apache.slide.common.Domain;

/**
 * Messages resource bundle manager.
 * 
 * @version $Revision: 1.2 $ $Date: 2006-01-22 22:49:05 $
 */
public class Messages {
    
    
    // -------------------------------------------------------------- Constants
    
    
    /**
     * Resource filename.
     */
    public static final String ResourceName = 
        "org.apache.slide.util.resources.messages";
    
    
    // -------------------------------------------------------------- Variables
    
    
    /**
     * Resource bundle.
     */
    private static ResourceBundle _messages;
    
    
    /**
     * Formats.
     */
    private static Hashtable _formats;
    
    
    // --------------------------------------------------------- Public Methods
    
    
    /**
     * Format a message.
     * 
     * @param message Message to format
     * @param arg1 Argument 1
     */
    public static String format(String message, Object arg1) {
        return format( message, new Object[] { arg1 } );
    }
    
    
    /**
     * Format a message.
     * 
     * @param message Message to format
     * @param arg1 Argument 1
     * @param arg2 Argument 2
     */
    public static String format(String message, Object arg1, Object arg2) {
        return format( message, new Object[] { arg1, arg2 } );
    }
    
    
    /**
     * Format a message.
     * 
     * @param message Message to format
     * @param arg1 Argument 1
     * @param arg2 Argument 2
     * @param arg3 Argument 3
     */
    public static String format(String message, Object arg1, Object arg2, 
                                Object arg3) {
        return format( message, new Object[] { arg1, arg2, arg3 } );
    }
    
    
    /**
     * Format a message.
     * 
     * @param message Message to format
     * @param args Argument array
     */
    public static String format( String message, Object[] args )
    {
        MessageFormat mf;
        String msg;
        
        try {
            mf = (MessageFormat) _formats.get( message );
            if (mf == null) {
                try {
                    msg = _messages.getString( message );
                } catch (MissingResourceException except) {
                    return message;
                }
                mf = new MessageFormat(msg);
                _formats.put(message, mf);
            }
            return mf.format(args);
        } catch (Exception except) {
            return "An internal error occured while processing message " 
                + message;
        }
    }
    
    
    /**
     * Fetch a message from the resource bundle.
     * 
     * @param message Id of the message if the bundle
     */
    public static String message(String message) {
        try {
            return _messages.getString(message);
        } catch (MissingResourceException except) {
            return message;
        }
    }
    
    
    /**
     * Set current locale.
     * 
     * @param locale Current locale
     */
    public static void setLocale(Locale locale) {
        _formats = new Hashtable();
        try {
            if (locale == null) {
                _messages = ResourceBundle.getBundle(ResourceName); 
            } else {
                _messages = ResourceBundle.getBundle(ResourceName, locale);
            }
        } catch (Exception except) {
            _messages = new EmptyResourceBundle();
            Domain.error("Failed to locate messages resource " + ResourceName);
        }
    }
    
    
    // ------------------------------------------------------------ Initializer
    
    
    static {
        setLocale(Locale.getDefault());
    }
    
    
    // ---------------------------------------- EmptyResourceBundle Inner Class
    
    
    static class EmptyResourceBundle
        extends ResourceBundle
        implements Enumeration {
        
        public Enumeration getKeys() {
            return this;
        }
        
        protected Object handleGetObject(String name) {
            return "[Missing message " + name + "]";
        }
        
        public boolean hasMoreElements() {
            return false;
        }
        
        public Object nextElement() {
            return null;
        }
        
    }
    
}
