// vi: set ts=3 sw=3:
package org.apache.slide.util;

import java.util.Enumeration;
import java.util.NoSuchElementException;


/**
 * Implementaion of empty {@link java.util.Enumeration}.
 * 
 * <p>Does this exist in Comons Collections?
 */
public class EmptyEnumeration implements Enumeration
{

    public static Enumeration INSTANCE = new EmptyEnumeration();
    
    private EmptyEnumeration() {}
    
    public boolean hasMoreElements()
    {
        return false;
    }

    public Object nextElement()
    {
        throw new NoSuchElementException();
    }

}
