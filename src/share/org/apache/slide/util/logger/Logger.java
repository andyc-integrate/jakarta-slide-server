/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/util/logger/Logger.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.util.logger;

/**
 * The <code>Logger</code> block interface.
 * <p />
 * This interface must be implemented by those blocks willing to log data
 * coming from other blocks.
 * <p />
 * The six levels used for logging are (in order of gravity): EMERGENCY,
 * CRITICAL, ERROR, WARNING, INFO and DEBUG.
 *
 * @version 1.0 (CVS $Revision: 1.2 $ $Date: 2006-01-22 22:49:05 $)
 */
public interface Logger {
    
    
    // -------------------------------------------------------------- Constants
    
    
    /**
     * The EMERGENCY logging level (anything logged under this level means
     * that a <b>critical permanent error</b> was detected).
     * <p />
     * When a <code>Block</code> logs something with this level means that
     * any other task will be impossible without administrator's intervention.
     * <p />
     * Example:
     * <ul>
     * <li><i>
     * AJP handler cannot find the secret key file. Cannot process servlet
     * requests.
     * </i></li>
     * <li><i>
     * Cannot access to mail server database URL jdbc:mysql://localhost/users
     * (SQLException caught in driver initialization)
     * </i></li>
     * </ul>
     */
    public static final int EMERGENCY=0;
    
    
    /**
     * The CRITICAL logging level (anything logged under this level means that
     * a <b>critical temporary error</b> was detected).
     * <p />
     * When a <code>Block</code> logs something with this level means that
     * tasks similar to the one generating the error will be impossible
     * without administrator's intervention, but other tasks (maybe using
     * different resources) will be possible.
     * <p />
     * Example:
     * <ul>
     * <li><i>
     * Cannot serve servlet in zone &quot;myServletZone&quot; (repository not
     * found)
     * </i></li>
     * <li><i>
     * Cannot alias address &quot;listserv@apache.org&quot; to ListServlet
     * (mail servlet not found)
     * </i></li>
     * </ul>
     */
    public static final int CRITICAL=1;
    
    
    /**
     * The ERROR logging level (anything logged under this level means that a
     * <b>non-critical error</b> was detected).
     * <p />
     * When a <code>Block</code> logs something with this level means that
     * the current tasks failed to execute, but the stability of the block is
     * not compromised.
     * <p />
     * Example:
     * <ul>
     * <li><i>
     * Cannot serve servlet &quot;SnoopServlet&quot; to client 192.168.1.1 due
     * to security restriction
     * </i></li>
     * <li><i>
     * User &quot;ianosh&quot; denied access via POP3 protocol due to invalid
     * password
     * </i></li>
     * </ul>
     */
    public static final int ERROR=2;

    /**
     * The WARNING logging level (anything logged under this level means that
     * a <b>generic error</b> was detected).
     * <p />
     * When a <code>Block</code> logs something with this level means that
     * the current tasks failed to execute due to inconsistencies in the
     * request handled by the block.
     * <p />
     * Example:
     * <ul>
     * <li><i>
     * Cannot find servlet &quot;SnoopServlet&quot; requested by 192.168.1.1
     * via AJP protocol
     * </i></li>
     * <li><i>
     * Cannot deliver mail from &quot;root@localhost&quot; to
     * &quot;ianosh@iname.com&quot; via protocol SMTP (user not found)
     * </i></li>
     * </ul>
     */
    public static final int WARNING=4;
    
    
    /**
     * The INFO logging level (anything logged under this level means that
     * an <b>informative message</b> must be logged).
     * <p />
     * When a <code>Block</code> logs something with this level means that
     * something that may be interesting happened.
     * <p />
     * Example:
     * <ul>
     * <li><i>
     * Successfully handled request from 192.168.1.1 via AJP protocol for
     * &quot;SnoopServlet&quot; servlet
     * </i></li>
     * <li><i>
     * Mail from &quot;root@localhost&quot; delivered successfully to
     * &quot;ianosh@iname.com&quot; via protocol SMTP
     * </i></li>
     * </ul>
     */
    public static final int INFO=6;
    
    
    /**
     * The DEBUG logging level (anything logged under this level means that
     * an <b>debug message</b> must be logged).
     * <p />
     * When a <code>Block</code> logs something with this level means that
     * something that may be interesting while developing, tracking bugs or
     * misconfigurations.
     * <p />
     * <p />
     * Example:
     * <ul>
     * <li><i>
     * Requested servlet &quot;snoop&quot; was aliased to
     * &quot;org.apache.servlets.SnoopServlet&quot;
     * </i></li>
     * <li><i>
     * MX record for domain &quot;dom.tld&quot; resolved to host
     * &quot;mail.dom.tld&quot; (192.168.1.1) with preference level 10
     * </i></li>
     * </ul>
     */
    public static final int DEBUG=7;
    
    
    /**
     * Specify the default string for logging without a channel.
     */
    public static String DEFAULT_CHANNEL = "org.apache.slide.util.logger";
    
    
    // ------------------------------------------------------ Interface Methods
    
    
    /**
     * Log an object and an associated throwable thru the specified channel and with the specified level.
     *
     * @param data object to log
     * @param throwable throwable to be logged
     * @param channel channel name used for logging
     * @param level level used for logging
     */
    public void log(Object data, Throwable throwable, String channel, int level);

    
    /**
     * Log an object thru the specified channel and with the specified level.
     *
     * @param data The object to log.
     * @param channel The channel name used for logging.
     * @param level The level used for logging.
     */
    public void log(Object data, String channel, int level);
    
    
    /**
     * Log an object with the specified level.
     *
     * @param data The object to log.
     * @param level The level used for logging.
     */
    public void log(Object data, int level);
    
    
    /**
     * Log an object.
     *
     * @param data The object to log.
     */
    public void log(Object data);
    
        
    
    /**
     * Set the logger level for the default channel
     *
     * @param level The new log level
     */

    public void setLoggerLevel(int level);
    
    
            
    /**
     * Set the logger level for the specified channel
     *
     * @param channel The channel for which the level is set.
     * @param level The new log level
     */

    public void setLoggerLevel(String channel, int level);
    
        
    
    /**
     * Get the logger level for the default channel
     */

    public int getLoggerLevel();
    
    
            
    /**
     * Get the logger level for the specified channel
     *
     * @param channel The channel whose log level is returned.
     */

    public int getLoggerLevel(String channel);
            
    
        
    
    /**
     * Check if the channel with the specified level is enabled for logging.
     *
     * @param channel The channel specification
     * @param level   The level specification
     */

    public boolean isEnabled(String channel, int level);
            
        
    
    /**
     * Check if the default channel with the specified level is enabled for logging.
     *
     * @param level   The level specification
     */

    public boolean isEnabled(int level);
            
}
