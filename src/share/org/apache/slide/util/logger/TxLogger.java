/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/util/logger/TxLogger.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 2004 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.util.logger;

import org.apache.commons.transaction.util.LoggerFacade;

/**
 * Mapper from Slide logging to commons tx logging.
 * 
 * @version $Revision: 1.2 $
 */
public class TxLogger implements LoggerFacade {

	protected Logger logger;

	protected String logChannel;

	public TxLogger(Logger logger, String logChannel) {
		this.logger = logger;
		this.logChannel = logChannel;
	}

	public Logger getLogger() {
		return logger;
	}

	public LoggerFacade createLogger(String name) {
		return new TxLogger(logger, name);
	}

	public void logInfo(String message) {
		logger.log(message, logChannel, Logger.INFO);
	}

	public void logFine(String message) {
		logger.log(message, logChannel, Logger.DEBUG);
	}

	public boolean isFineEnabled() {
		return logger.isEnabled(logChannel, Logger.DEBUG);
	}

	public void logFiner(String message) {
		logger.log(message, logChannel, Logger.DEBUG);
	}

	public boolean isFinerEnabled() {
		return logger.isEnabled(logChannel, Logger.DEBUG);
	}

	public void logFinest(String message) {
		logger.log(message, logChannel, Logger.DEBUG);
	}

	public boolean isFinestEnabled() {
		return logger.isEnabled(logChannel, Logger.DEBUG);
	}

	public void logWarning(String message) {
		logger.log(message, logChannel, Logger.WARNING);
	}

	public void logWarning(String message, Throwable t) {
		logger.log(message, logChannel, Logger.WARNING);
		logger.log(t, logChannel, Logger.WARNING);
	}

	public void logSevere(String message) {
		logger.log(message, logChannel, Logger.EMERGENCY);
	}

	public void logSevere(String message, Throwable t) {
		logger.log(message, logChannel, Logger.EMERGENCY);
		logger.log(t, logChannel, Logger.EMERGENCY);
	}

}