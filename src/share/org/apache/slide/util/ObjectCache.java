/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/util/ObjectCache.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */ 

package org.apache.slide.util;

/**
 * Object cache interface.
 * <p>
 * Interface to an object cache to the objects manipulated by Slide. This
 * includes :
 * <ul>
 * <li>Uri objects</li>
 * <li>Object nodes</li>
 * <li>Revision descriptors</li>
 * <li>Permissions</li>
 * <li>Locks</li>
 * </ul>
 * <br>
 * The implementation of this interface is free to provide any kind of 
 * algorithm to limit cache size, like dropping LRU elements.
 * 
 * @version $Revision: 1.2 $ $Date: 2006-01-22 22:49:05 $
 */
public interface ObjectCache {
    
    
    // ------------------------------------------------------ Interface methods
    
    
    /**
     * Get the object associated with the key.
     * 
     * @param key Object's key
     * @return Object null if there is no object associated with that key in 
     * the cache, or the object value otherwise
     */
    Object get(Object key);
    
    
    /**
     * Add an object to the cache, or overwrite its value.
     * 
     * @param key Object's key
     * @param value Object's value
     */
    void put(Object key, Object value);
    
    
    /**
     * Remove object associated with the given key. Doesn't do anything if the 
     * key wasn't associated with any object.
     * 
     * @param key Object's key
     */
    void remove(Object key);
    
    
    /**
     * Clear object cache.
     */
    void clear();
    
    
}
