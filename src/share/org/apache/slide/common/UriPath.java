/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/common/UriPath.java,v 1.3 2007-09-11 15:39:26 peter-cvs Exp $
 * $Revision: 1.3 $
 * $Date: 2007-09-11 15:39:26 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.common;

import java.util.StringTokenizer;

/**
 * An URI path.
 *
 * @version $Revision: 1.3 $
 */
public final class UriPath {
    
    private String[] tokens;
    
    private UriPath() {
    }
    
    private UriPath(String[] tokens) {
        this.tokens = tokens;
    }
    
    public UriPath(String uri) {
        StringTokenizer t = new StringTokenizer( uri, "/" );
        this.tokens = new String[t.countTokens()];
        for (int i = 0; i < tokens.length; i++) {
            tokens[i] = t.nextToken();
        }
    }
    
    public String[] tokens() {
        return tokens;
    }
    
    public String lastSegment() {
        return tokens.length > 0
            ? tokens[ tokens.length - 1 ]
            : null;
    }
    
    public UriPath parent() {
        if (this.tokens.length == 0) {
            return null;
        }
        return subUriPath(0, tokens.length - 1);
    }
    
    public UriPath child( String segment ) {
        String[] ctokens = new String[tokens.length + 1];
        System.arraycopy(tokens, 0, ctokens, 0, tokens.length);
        ctokens[tokens.length] = segment;
        return new UriPath(ctokens);
    }
    
    public UriPath subUriPath(int start, int end) {
        UriPath result = new UriPath();
        result.tokens = new String[end - start];
        System.arraycopy(tokens, start, result.tokens, 0, result.tokens.length);
        return result;
    }
    
    public boolean equals(Object o) {
        boolean result = false;
        if (o instanceof UriPath) {
            UriPath other = (UriPath)o;
            if (other.tokens.length == this.tokens.length) {
                result = true;
                for (int i = 0; i < this.tokens.length; i++) {
                    if (!other.tokens[i].equals(this.tokens[i])) {
                        result = false;
                        break;
                    }
                }
            }
        }
        return result;
    }
    
    public int hashCode() {
        if (tokens.length > 0) {
            return tokens[tokens.length - 1].hashCode();
        }
        else {
            return 0;
        }
    }
    
    public String toString() {
        if (tokens.length > 0) {
            StringBuffer b = new StringBuffer();
            for (int i = 0; i < tokens.length; i++) {
                b.append("/").append(tokens[i]);
            }
            return b.toString();
        }
        else {
            return "/";
        }
    }
}
