/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/common/NamespaceAccessToken.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.common;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

import javax.transaction.TransactionManager;
import javax.transaction.UserTransaction;

import org.apache.slide.content.Content;
import org.apache.slide.lock.Lock;
import org.apache.slide.macro.Macro;
import org.apache.slide.search.Search;
import org.apache.slide.security.Security;
import org.apache.slide.structure.Structure;
import org.apache.slide.util.conf.Configuration;
import org.apache.slide.util.conf.ConfigurationException;
import org.apache.slide.util.logger.Logger;
import org.xml.sax.SAXException;

/**
 * The <code>NamespaceAccessToken</code> is an interface used to completely
 * hide the real {@link Namespace Namespace} object reference from the client
 * application. It is used by Slide to authenticate and control any operations
 * on the namespace.
 *
 * <p>
 *   Using the NamespaceAccessToken, the application can obtain access to the
 *   helper interfaces (<code>Content</code>, <code>Lock</code>,
 *   <code>Macro</code>, <code>Security</code> and <code>Structure</code>).
 * </p>
 * <p>
 *   The NamespaceAccessToken object implements the <tt>UserTransaction</tt>
 *   interface (see the Java Transaction API documentation for more details)
 *   to allow the client to control transaction demarcation.
 * </p>
 *
 * @version $Revision: 1.2 $
 */
public interface NamespaceAccessToken extends UserTransaction {
    
    
    // ------------------------------------------------------ Interface Methods
    
    
    /**
     * Retrive the namespace configuration.
     *
     * @return NamespaceConfig Namespace configuration
     */
    NamespaceConfig getNamespaceConfig();
    
    
    /**
     * Import data from configuration object.
     *
     * @param token SlideToken, used for access to the namespace
     * @param objectNodeConfiguration Configuration object
     * @exception ConfigurationException Something went wrong during the
     * reading of the XML
     * @exception UnknownObjectClassException Object class not found
     * @exception ServiceAccessException Error accessing service
     */
    void importData(SlideToken token,
                    Configuration objectNodeConfiguration)
        throws ConfigurationException, UnknownObjectClassException,
        ServiceAccessException;
    
    
    /**
     * Import data from reader.
     *
     * @param token SlideToken, used for access to the namespace
     * @param reader Reader
     * @exception ConfigurationException Something went wrong during the
     * reading of the XML
     * @exception UnknownObjectClassException Object class not found
     * @exception ServiceAccessException Error accessing service
     */
    void importData(SlideToken token, Reader reader)
        throws ConfigurationException, UnknownObjectClassException,
        ServiceAccessException, SAXException, IOException;
    
    
    /**
     * Saves Slide Data to XML.
     *
     * @param writer Writer
     * @exception SlideException
     */
    void exportData(SlideToken token, Writer writer)
        throws SlideException;
    
    
    /**
     * Saves Slide Data to XML.
     *
     * @param writer Writer
     * @param startNode Start generating XML from this node of the Slide tree
     * @exception SlideException
     */
    void exportData(SlideToken token, Writer writer, String startNode)
        throws SlideException;
    
    
    /**
     * Get the data helper.
     *
     * @return Data Data helper
     */
    Structure getStructureHelper();
    
    
    /**
     * Get the version helper.
     *
     * @return Version Version helper
     */
    Content getContentHelper();
    
    
    /**
     * Get the lock helper.
     *
     * @return Lock Lock helper
     */
    Lock getLockHelper();
    
    
    /**
     * Get the lock helper.
     *
     * @return Lock Lock helper
     */
    Search getSearchHelper();
    
    
    /**
     * Get the security helper.
     *
     * @return Security Security helper
     */
    Security getSecurityHelper();
    
    
    /**
     * Get the macro helper.
     *
     * @return Macro Macro helper
     */
    Macro getMacroHelper();
    
    
    /**
     * Disconnect.
     */
    void disconnect();
    
    
    /**
     * Get namespace name.
     *
     * @return String namespace name.
     */
    String getName();
    
    /**
     * Builds a new uri object to access this namespace.
     *
     * @param token SlideToken
     * @param uri Requested Uri
     * @return Uri
     */
    public Uri getUri(SlideToken token, String uri);
    

    /**
     * Get namespace logger.
     *
     * @return The logger associated with the namespace.
     */
    Logger getLogger();

    public TransactionManager getTransactionManager();

}
