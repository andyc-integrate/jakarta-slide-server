/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/common/NamespaceException.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.common;

import java.util.Vector;
import java.util.Enumeration;

/**
 * Namespace exception.
 *
 * @version $Revision: 1.2 $
 */
public class NamespaceException extends SlideException {
    
    
    // ------------------------------------------------------------ Constructor
    
    
    /**
     * Constructor.
     *
     * @param message Exception message
     */
    public NamespaceException(String message) {
        super(message, false);
        exceptions = new Vector();
    }
    
    
    // ----------------------------------------------------- Instance Variables
    
    
    /**
     * Vector of exceptions which have been put in this exception.
     */
    private Vector exceptions;
    
    
    // --------------------------------------------------------- Public Methods
    
    
    /**
     * Displays the exception message.
     *
     * @return String Message
     */
    public String toString() {
        String result = new String();
        if( exceptions != null ) {
            Enumeration list = exceptions.elements();
            while (list.hasMoreElements()) {
                SlideException e = (SlideException) list.nextElement();
                result = result + e.getMessage() + "\n";
            }
        }
        return result;
    }
    
    
    // -------------------------------------------------------- Package Methods
    
    
    /**
     * Add an exception to the nested exception.
     *
     * @param exception Exception to be added
     */
    void addException(SlideException exception) {
        exceptions.addElement(exception);
    }
    
    
    /**
     * Returns true if no exceptions have been put in this exception.
     *
     * @return boolean
     */
    boolean isEmpty() {
        return exceptions.isEmpty();
    }
    
}
