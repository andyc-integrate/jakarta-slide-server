/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/common/SlideException.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.common;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.slide.common.Domain;
import org.apache.slide.util.logger.Logger;


/**
 * Exception supertype for all Slide components.
 *
 * @version $Revision: 1.2 $
 */
public class SlideException extends Exception {

    private static final String CHANNEL = "org.apache.slide.common.SlideException";
    private static final int DEBUG = Logger.DEBUG;
    private static Logger LOGGER = Domain.getLogger();


    // ----------------------------------------------------------- Constructors


    /**
     * SlideException with logging.
     *
     * @param message Exception message
     */
    public SlideException(String message) {
        this(message, true);
    }


    /**
     * SlideException with or without logging.
     *
     * @param message Exception message
     * @param showTrace Defines whether or not something is logged
     */
    public SlideException(String message, boolean showTrace) {
        super(message);
        if (showTrace) {
            Domain.warn(message);
        }

      if (LOGGER.isEnabled(CHANNEL, DEBUG)) {
            StringWriter sw = new StringWriter();
            printStackTrace( new PrintWriter(sw, true) ); //autoFlush=true
            LOGGER.log( sw.toString(), CHANNEL, DEBUG );
      }
    }

}
