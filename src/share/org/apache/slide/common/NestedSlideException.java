/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/common/NestedSlideException.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.common;

import java.util.Vector;
import java.util.Enumeration;

/**
 * Nested Slide exception.
 *
 * @version $Revision: 1.2 $
 */
public class NestedSlideException extends SlideException {
    
    
    // ----------------------------------------------------------- Constructors
    
    
    /**
     * Constructor.
     */
    public NestedSlideException(String message) {
        super(message, false);
        exceptions = new Vector();
    }
    
    
    // ----------------------------------------------------- Instance Variables
    
    
    /**
     * Vector of exceptions which have been put in this exception.
     */
    protected Vector exceptions;
    
    
    // ------------------------------------------------------------- Properties
    
    
    /**
     * Add a new nested exception.
     *
     * @param exception Exception to be added
     */
    public void addException(SlideException exception) {
        exceptions.addElement(exception);
    }
    
    
    /**
     * Enumerate nested exceptions.
     *
     * @return Enumeration nested exceptions list
     */
    public Enumeration enumerateExceptions() {
        return exceptions.elements();
    }
    
    
    /**
     * Return the count of nested exceptions.
     *
     * @return int count of nested exceptions
     */
    public int getExceptionsCount() {
        return exceptions.size();
    }
    
    
    
    /**
     * Return the count of nested exceptions.
     *
     * @return int count of nested exceptions
     */
    public SlideException unpackSingleException() {
        if (getExceptionsCount() != 1) return null;
        return (SlideException)enumerateExceptions().nextElement();
    }
    
    
    /**
     * Returns true if no exceptions have been put in this exception.
     *
     * @return boolean
     */
    public boolean isEmpty() {
        return exceptions.isEmpty();
    }
    
    
    // --------------------------------------------------------- Object Methods
    
    
    /**
     * Displays the exception message.
     *
     * @return String exception message
     */
    public String toString() {
        String result = new String();
        if( exceptions != null ) {
            Enumeration list = exceptions.elements();
            while (list.hasMoreElements()) {
                SlideException e = (SlideException) list.nextElement();
                result = result + e.getMessage() + "\n";
            }
        }
        return result;
    }
    
}
