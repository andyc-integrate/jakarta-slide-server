/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/common/XAServiceBase.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.common;

import java.util.Hashtable;

import javax.transaction.xa.XAException;
import javax.transaction.xa.XAResource;
import javax.transaction.xa.Xid;

import org.apache.commons.transaction.util.xa.TransactionalResource;

/**
 * Slide Service dummy implementation.
 * 
 * @version $Revision: 1.2 $
 */
public class XAServiceBase extends AbstractXAServiceBase {

	protected boolean started = false;

	public void setParameters(Hashtable parameters)
			throws ServiceParameterErrorException,
			ServiceParameterMissingException {
	}

	public void connect() throws ServiceConnectionFailedException {
		started = true;
	}

	public void disconnect() throws ServiceDisconnectionFailedException {
		started = false;
	}

	public void reset() throws ServiceResetFailedException {
	}

	public boolean isConnected() throws ServiceAccessException {
		return started;
	}

	public boolean isSameRM(XAResource xares) throws XAException {
		return (xares == this);
	}

	public Xid[] recover(int flag) throws XAException {
		return null;
	}

	public int getTransactionTimeout() throws XAException {
		return 0;
	}

	public boolean setTransactionTimeout(int arg0) throws XAException {
		return false;
	}

	protected TransactionalResource createTransactionResource(Xid xid) {
		return new DummyTxResource(xid);
	}

    protected boolean includeBranchInXid() {
        return false;
    }

    protected class DummyTxResource implements TransactionalResource {
		Xid xid;

		int status;

		DummyTxResource(Xid xid) {
			this.xid = xid;
			status = STATUS_ACTIVE;
		}

		public void commit() throws XAException {
		}

		public void rollback() throws XAException {
		}

		public int prepare() throws XAException {
			// no check possible
			return XA_OK;
		}

		public int getStatus() {
			return status;
		}

		public void setStatus(int status) {
			this.status = status;
		}

		public Xid getXid() {
			return xid;
		}

        public void begin() throws XAException {
        }

        public void suspend() throws XAException {
        }

        public void resume() throws XAException {
        }
	}

}