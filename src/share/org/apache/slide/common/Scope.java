/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/common/Scope.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.common;

/**
 * Scope object.
 * <p/>
 * A scope represents a directory on the server. Ex : /foo/bar.txt belongs
 * to the /foo/ scope.
 *
 * @version $Revision: 1.2 $
 */
public final class Scope {
    
    public static final Scope ROOT = new Scope("/");
    
    // ----------------------------------------------------------- Constructors
    
    
    /**
     * Constructor.
     *
     * @param scope String representation of the scope.
     */
    public Scope(String scope) {
        this.scope = scope;
        scopeID = scope.hashCode();
    }
    
    
    // ----------------------------------------------------- Instance Variables
    
    
    /**
     * Hash code of the scope.
     */
    private int scopeID;
    
    
    /**
     * String representation of the scope.
     */
    private String scope;
    
    
    // --------------------------------------------------------- Public Methods
    
    
    /**
     * Returns the String representation of the scope.
     *
     * @return String
     */
    public String toString() {
        return this.scope;
    }
    
    
    /**
     * Tests if two scopes ore equal.
     *
     * @param obj Object to test
     * @return boolean True if the object is equal to the scope
     */
    public boolean equals(Object obj) {
        boolean result = false;
        if ((obj != null) && (obj instanceof Scope)) {
            Scope scope = (Scope) obj;
            result = (this.scopeID == scope.hashCode());
        }
        return result;
    }
    
    
    /**
     * Hash code.
     *
     * @return int Result is the hash code of the String representation
     * of the scope
     */
    public int hashCode() {
        return this.scopeID;
    }
    
}
