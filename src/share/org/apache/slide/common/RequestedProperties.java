/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/common/RequestedProperties.java,v 1.2 2006-01-22 22:47:24 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:47:24 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.slide.common;

import org.apache.slide.content.NodeProperty;
import java.util.Iterator;


/**
 * With this you may find out, if a specific property is wanted by the request.
 *
 * @version $Revision: 1.2 $
 */
public interface RequestedProperties {
	
	/**
	 * Checks, if the property identified by name and namespace, is requested
	 *
	 * @param    name          name of the property to be checked
	 * @param    namespace     namespace of the property to be checked
	 *
	 * @return   true, if property is requested
	 */
	boolean contains (String name, String namespace);
	
	/**
	 * Checks, if the NodeProperty is a RequestedProperty
	 *
	 * @param    property    NodeProperty to be checked
	 *
	 * @return   true, if property is requested
	 *
	 */
	boolean contains (NodeProperty property);
	
	/**
	 * Method isAllProp
	 *
	 * @return   true, if all properties are requested
	 *
	 */
	boolean isAllProp ();

    /** 
     * Set whether this is an allProp request or not.
     */
    void setIsAllProp(boolean isAllProp);
	
	/**
	 * Method getRequestedProperties
	 *
	 * @return   an Iterator to retrieve all RequestedProperty items
	 * @throws   IllegalStateException when isAllProp == true
	 */
	Iterator getRequestedProperties ();
}

