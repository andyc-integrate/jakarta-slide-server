/*
 * $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/org/apache/slide/security/AccessDeniedException.java,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
 * $Revision: 1.2 $
 * $Date: 2006-01-22 22:49:05 $
 *
 * ====================================================================
 *
 * Copyright 1999-2002 The Apache Software Foundation 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */ 

package org.apache.slide.security;

import org.apache.slide.util.Messages;

/**
 * Access denied.
 * 
 * @version $Revision: 1.2 $
 */
public class AccessDeniedException extends SecurityException {
    
    
    // ----------------------------------------------------------- Constructors
    
    
    /**
     * Constructor.
     * 
     * @param objectUri Object on which the access was denied
     * @param subjectUri Subject
     * @param actionUri Action which was attempted
     */
    public AccessDeniedException(String objectUri, String subjectUri, 
                                 String actionUri) {
        super(Messages.format(AccessDeniedException.class.getName(), 
                              objectUri, subjectUri, actionUri));
        this.objectUri = objectUri;
        this.subjectUri = subjectUri;
        this.actionUri  = actionUri;
    }
    
    
    // ----------------------------------------------------- Instance Variables
    
    
    /**
     * Object Uri.
     */
    private String objectUri;
    
    /**
     * Subject Uri.
     */
    private String subjectUri;
    
    /**
     * Action Uri.
     */
    private String actionUri;
    
    
    // ------------------------------------------------------------- Properties
    
    
    /**
     * Object Uri accessor.
     * 
     * @return String object Uri
     */
    public String getObjectUri() {
        if (objectUri == null) {
            return new String();
        } else {
            return objectUri;
        }
    }
    
   /**
     * Subject Uri accessor.
     *
     * @return String object Uri
     */
    public String getSubjectUri() {
        if (subjectUri == null) {
            return new String();
        } else {
            return subjectUri;
        }
    }
    
   /**
     * Object Uri accessor.
     *
     * @return String object Uri
     */
    public String getActionUri() {
        if (actionUri == null) {
            return new String();
        } else {
            return actionUri;
        }
    }
}
