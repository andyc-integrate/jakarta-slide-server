<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<html>
  
  <head>
    <!-- 
      
      $Header: /var/chroot/cvs/cvs/factsheetDesigner/extern/jakarta-slide-server-src-2.1-iPlus Edit/src/share/overview.html,v 1.2 2006-01-22 22:49:05 peter-cvs Exp $
      $Revision: 1.2 $
      $Date: 2006-01-22 22:49:05 $
 
      ====================================================================

      Copyright 1999-2002 The Apache Software Foundation 

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.

    -->
  </head>
  
  <body>
    <p>
      Welcome to the documentation of the 
      <a href="http://jakarta.apache.org/slide" target="_parent">Jakarta Slide</a>
      core API.
    </p>
    <h3>Architecture</h3>
    <p>
      The Slide architecture is a matrix of modules, ranging from high-level 
      to low-level services, clearly separating functionality around different 
      aspects (content, structure, security, locking and versioning).
    </p>
    <p>
      Content in Slide is organized in 
      <a href="org/apache/slide/common/Namespace.html">Namespaces</a>, which 
      contain a hierarchical tree of information (analogous to directories and 
      files in a filesystem). Multiple namespaces are aggregated in a 
      <a href="org/apache/slide/common/Domain.html">Domain</a>. The default 
      implementation of a Slide domain is as a static entity, so only one 
      domain can exist per JVM.
    </p>
    <p>
      The <a href="#helpers">high-level interfaces</a> (<i>Helpers</i>) are 
      meant to provide a simple, standardized way of manipulating a namespace 
      from a client application. Underneath these interfaces lie pluggable, 
      <a href="#stores">low-level services</a> (<i>Stores</i>), which take care 
      of actual storing the information.
    </p>
    <p align="center">
      <small><i>Overview of the Slide architecture<br><br></i></small>
      <img src="org/apache/slide/common/doc-files/architecture.png">
    </p>
    <a name="stores"><h3>Stores</h3></a>
    <p>
      Stores are low level services that handle the actual storage of content 
      and related data. Stores are totally pluggable, enabling the use of the 
      most appropriate storage method for the type of data to store.
    </p>
    <p>
      Two different kinds of services exist:
      <ul>
        <li>
          The <b>Descriptors</b> stores, which are responsible for storing 
          structure, locks, metadata, etc.
        </li>
        <li>
          The <b>Content</b> stores, which is only responsible for storing the 
          actual content.
        </li>
      </ul>
      This distinction has been made because it's easy to see that while some 
      repositories are very efficient at managing and indexing small amounts 
      of data (relational databases are a good example of this), others are 
      best for storing big chunks of data (for example filesystems).
    </p>
    <p>
      Within Slide, every object can possibly have a different kind of backing 
      low-level service. For example, some objects might be stored in a remote 
      LDAP directory, while others could be stored in an local SQL database. 
      Thus, the content of a namespace can be distributed across several 
      different descriptors and content stores.
    </p>
    <p>
      It is up to the administrator to choose how objects will be stored using 
      the Slide configuration file, which maps low-level services to 
      individual nodes in the namespace.
    </p>
    <p>
      Services are attributed to nodes in the namespace. This mapping is 
      automatically inherited by sub-nodes. Here is an example of how one 
      namespace might be mapped into different low-level services:
    </p>
    <p align="center">
      <small><i>Namespace mapped to multiple stores<br><br></i></small>
      <img src="org/apache/slide/common/doc-files/stores.png">
    </p>
    <p>
      It is obvious that decent transaction capabilities are required in each 
      individual store as well as across all the stores. Slide provides it's 
      own transaction manager based on the Java Transaction API (JTA) to 
      comply with this requirement. Operations that include multiple objects 
      and span various stores can be grouped in transactions and rolled back 
      if one of the operations fails.
    </p>
    <p align="center">
      <small><i>Transactions in Slide<br><br></i></small>
      <img src="org/apache/slide/common/doc-files/transactions.png">
    </p>
    <a name="helpers"><h3>Helpers</h3></a>
    <p>
      Slide also provides a higher level abstraction of the content management 
      system through a set of Java classes, called helpers. These encompass 
      all the functionality a client application will need, clearly separated 
      by aspect. These high-level services allow an application to access 
      content in a uniform fashion, no matter where it might be located, or 
      how it is phisically stored.
    </p>
    <p>
      Tight dependencies exist between the high-level services because of the 
      need to enforce security, locking and other contraints throughout the 
      client API.
    </p>
    <p>
      The following helpers are provided:
      <ul>
        <li><b>Structure</b>
          <br>
          Provides access to the hierarchical namespace tree.
          <br>
          <i><small>See the 
          <a href="org/apache/slide/structure/package-summary.html">org.apache.slide.structure</a>
          package for more details...</small></i>
          <br><br>
        </li>
        <li>
          <b>Content</b>
          <br>
          Manages object content, versioning and meta-data.
          <br>
          <i><small>See the 
          <a href="org/apache/slide/content/package-summary.html">org.apache.slide.content</a>
          package for more details...</small></i>
          <br><br>
        </li>
        <li>
          <b>Security</b>
          <br>
          Manages access control to objects in a namespace.
          <br>
          <i><small>See the 
          <a href="org/apache/slide/security/package-summary.html">org.apache.slide.security</a>
          package for more details...</small></i>
          <br><br>
        </li>
        <li>
          <b>Lock</b>
          <br>
          Provides locking functionality.
          <br>
          <i><small>See the 
          <a href="org/apache/slide/lock/package-summary.html">org.apache.slide.lock</a>
          package for more details...</small></i>
          <br><br>
        </li>
        <li>
          <b>Macro</b>
          <br>
          Provides object management functions that deal with operations which 
          span multiple helpers (like for example deleting, copying and moving 
          objects).
          <br>
          <i><small>See the 
          <a href="org/apache/slide/macro/package-summary.html">org.apache.slide.macro</a>
          package for more details...</small></i>
          <br><br>
        </li>
      </ul>
    </p>
    <p>
      Instances of these helpers for a specific namespace can be obtained 
      through the <a href="org/apache/slide/common/NamespaceAccessToken.html">NamespaceAccessToken</a>
      that you receive when accessing the namespace.
    </p>
  </body>
  
</html>
