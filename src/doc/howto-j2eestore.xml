<?xml version="1.0" encoding="ISO-8859-1"?>

<document>

  <properties>
        <author email="cbritton@metatomix.com">Colin Britton</author>
        <author email="ozeigermann@apache.org">Oliver Zeigermann</author>
        <title>J2EE/JDBC Store Howto</title>
    </properties>
  
  <body>
  
  <section name="Configuring Slide">
    <p>In order to use the J2EE stores, the Domain.xml file needs to contain the following configuration for the store:
<pre>
&lt;definition&gt;
    &lt;store name="j2ee"&gt;
        &lt;nodestore classname="org.apache.slide.store.impl.rdbms.J2EEStore"&gt;
            &lt;parameter name="datasource"&gt;jdbc/mtx&lt;/parameter&gt;
            &lt;parameter name="adapter">org.apache.slide.store.impl.rdbms.MySqlRDBMSAdapter&lt;/parameter>
            &lt;parameter name="compress">false&lt;/parameter>
        &lt;/nodestore&gt;
        &lt;securitystore&gt;
            &lt;reference store="nodestore"/&gt;
        &lt;/securitystore&gt;
        &lt;lockstore&gt;
            &lt;reference store="nodestore"/&gt;
        &lt;/lockstore&gt;
        &lt;revisiondescriptorsstore&gt;
            &lt;reference store="nodestore"/&gt;
        &lt;/revisiondescriptorsstore&gt;
        &lt;revisiondescriptorstore&gt;
            &lt;reference store="nodestore"/&gt;
        &lt;/revisiondescriptorstore&gt;
        &lt;contentstore&gt;
            &lt;reference store="nodestore"/&gt;
        &lt;/contentstore&gt;
    &lt;/store&gt;
    &lt;scope match="/" store="j2ee"/&gt;
&lt;/definition&gt;
</pre>
where the adapter determines which database adapter you want to use. In this case you configured the MySQL adapter. Most adapters
have a parameter to decide whether the content shall be compressed (zipped) before storing to the database. This <em>might</em>
be fast in some enviroments. This option is switched off here.
  </p>

	<p>You have to create the tables of the database schema manually. Schemata are available in 
	src/conf/schema if you have the source distribution or in slide/db-schema if you have the Tomcat bundled or binary distribution.</p>

    <p>If your store is not configured using a datasource looked up using JNDI you will have to provide more information to Slide
    	like this for example:
<pre>
	
&lt;definition&gt;
&lt;store name="MySqlStore">
  &lt;nodestore classname="org.apache.slide.store.impl.rdbms.JDBCStore">
    &lt;parameter name="adapter">org.apache.slide.store.impl.rdbms.MySqlRDBMSAdapter&lt;/parameter>
    &lt;parameter name="driver">com.mysql.jdbc.Driver&lt;/parameter>
    &lt;parameter name="url">jdbc:mysql://localhost/Slide&lt;/parameter>
    &lt;parameter name="user">root&lt;/parameter>
    &lt;parameter name="dbcpPooling">true&lt;/parameter>
    &lt;parameter name="maxPooledConnections">10&lt;/parameter>
    &lt;parameter name="isolation">SERIALIZABLE&lt;/parameter>
    &lt;parameter name="compress">false&lt;/parameter>
  &lt;/nodestore>
  &lt;contentstore>
    &lt;reference store="nodestore" />
  &lt;/contentstore>
  &lt;securitystore>
    &lt;reference store="nodestore" />
  &lt;/securitystore>
  &lt;lockstore>
    &lt;reference store="nodestore" />
  &lt;/lockstore>
  &lt;revisiondescriptorsstore>
    &lt;reference store="nodestore" />
  &lt;/revisiondescriptorsstore>
  &lt;revisiondescriptorstore>
    &lt;reference store="nodestore" />
  &lt;/revisiondescriptorstore>
 &lt;/store>
 &lt;scope match="/" store="MySqlStore"/&gt;
&lt;/definition&gt;
</pre>
You can see you will have to configure you driver, the connection url and the user for the database. You can optionally configure
if connection pooling using DBCP is enabled or not and if enabled how many connections shall be pooled. If you want you can
also choose the isolation level of your database. <code>SERIALIZABLE</code> is a safe choice, but - depending on you database - at least <code>READ COMMITTED</code> 
is recommended.
  </p>
  </section>

  <section name="Configuring Tomcat 4.x">
    <p>In order to use this with Tomcat you need to setup the datasource. 
    Instructions for this are at 
    <a href="http://jakarta.apache.org/tomcat/tomcat-4.0-doc/jndi-resources-howto.html">
    http://jakarta.apache.org/tomcat/tomcat-4.0-doc/jndi-resources-howto.html</a>.
    </p>
    <p>If you follow the above instructions you will make changes similar to the 
    following in web.xml and server.xml.</p>
<p>
<pre>
web.xml
&lt;resource-ref&gt;
    &lt;description&gt;Testing Tomcat-wide datasource usage&lt;/description&gt;
    &lt;res-ref-name&gt;jdbc/mtx&lt;/res-ref-name&gt;
    &lt;res-type&gt;javax.sql.DataSource&lt;/res-type&gt;
    &lt;res-auth&gt;Container&lt;/res-auth&gt;
&lt;/resource-ref&gt;

server.xml
&lt;DefaultContext debug="99"&gt;
    &lt;Resource name="jdbc/mtx" auth="Container" type="javax.sql.DataSource"/&gt;
    &lt;ResourceParams name="jdbc/mtx"&gt;
        &lt;parameter&gt;
            &lt;name&gt;user&lt;/name&gt;
            &lt;value&gt;bar&lt;/value&gt;
        &lt;/parameter&gt;
        &lt;parameter&gt;
            &lt;name&gt;password&lt;/name&gt;
            &lt;value&gt;foo&lt;/value&gt;
        &lt;/parameter&gt;
        &lt;parameter&gt;
            &lt;name&gt;driverClassName&lt;/name&gt;
            &lt;value&gt;org.hsqldb.jdbcDriver&lt;/value&gt;
        &lt;/parameter&gt;
        &lt;parameter&gt;
            &lt;name&gt;driverName&lt;/name&gt;
            &lt;value&gt;jdbc:hsqldb:slide&lt;/value&gt;
        &lt;/parameter&gt;
    &lt;/ResourceParams&gt;
&lt;/DefaultContext&gt;
</pre>
</p>
  </section>

</body>
</document>
